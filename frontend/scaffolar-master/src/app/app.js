// Angular APP
angular.module('app', [

  'ui.router',
  'ui.bootstrap',
  'angular.filter',
  'siyfion.sfTypeahead',
  'ngCookies',
  'ja.qr',
  'googlechart',
  'io-barcode'
]);

// _.str becomes a global variable if no module loading is detected
// Mix in non-conflict functions to Underscore namespace
_.mixin(_.str.exports());
