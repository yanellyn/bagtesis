angular.module('app')
  .controller('PrestamosController', function($rootScope, $scope, $http, $location, $cookieStore, paginationConfig, AuthService, API) {
    /*
	$scope.prueba = null;
    $scope.titulo = null;
    $scope.cantidad = null;
    $scope.disponibles = null;

    console.log('cookies: '+$cookieStore.get('auth_token'));
    
	$scope.add = function(){
		
		$http.get(API.url+'bagtesis/backend/public/prestamo_info',{
    	headers: {'X-Auth-Token': $cookieStore.get('auth_token')+'_id_'+$cookieStore.get('user_id') }}).
		  success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		    //$scope.prueba = data;
		    $scope.titulo = data.titulo;
		    $scope.cantidad = data.cantidad;
		    $scope.disponibles = data.cantidad_disponibles;
		    $scope.prestados = data.cantidad - data.cantidad_disponibles;
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    console.log('error-get-add()'+data);
		    if(status == 500)
		    	$location.path("/login");
		  });
	
	};*/



	$rootScope.search_selected = '';
	$scope.requestData={};
	$scope.requestData.ci = '';
	$scope.requestData.usuario_id = '';
	$scope.answer = {};
	$scope.final_answer = '';
	$scope.currentUserId = '';
	$scope.errorMessage = '';
	$scope.user_enabled = false;
	$scope.user_found = false;
	$scope.pref_answer = false;
	$scope.preferencias = [];
	$scope.prestamos = [];
	$scope.currentPage_pref = 1;
	$scope.pref_data = {};

	$scope.pref_model = [];
	$scope.pref_selected = {};
	$scope.pref_count = 0;

	$scope.pres_model = [];
	$scope.pres_selected = {};
	$scope.pres_count = 0;

	$scope.despliegue_pref = true;
	$scope.despliegue_pres = true;
	$scope.despliegue_dev = true;
	$scope.despliegue_sus = true;

	$scope.detailModalData = {};

	$scope.bool_disable = false;
	$scope.loading = false;

	$scope.modalContent = '';
	$scope.modalHeader = '';
	$scope.modalTable = false;
	$scope.modalB1 = '';
	$scope.modalB2 = '';
	$scope.modalShowB1 = true;
	$scope.modalShowB2 = true;
	$scope.modalAjaxLoader = false;
	$scope.onAction = function(){};





	/*
	* Función cambiar el icono de despliegue en las cabeceras de las tablas.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.cambiar_icono_despliegue = function(string){
		if(string == "pref")
			$scope.despliegue_pref = !$scope.despliegue_pref;
		if(string == "pres")
			$scope.despliegue_pres = !$scope.despliegue_pres;
		if(string == "dev")
			$scope.despliegue_dev = !$scope.despliegue_dev;
		if(string == "sus")
			$scope.despliegue_sus = !$scope.despliegue_sus;
	}

	$scope.count_barcode = 0;
	$scope.before_selected = '';

	/*
	* Función de pruebas para probar la pistola y los codigos de barra.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.barcode = function(search_selected){
		if(typeof search_selected == 'undefined'){ 
			$scope.count_barcode = 0;
			$scope.before_selected = '';
			return false;
		}

		if((search_selected.length > $scope.count_barcode+1 || search_selected.length < $scope.count_barcode-1) && search_selected.length >5){
			$scope.consultar_buscador_simple(search_selected);
			$scope.count_barcode = 0;
		}else
			if(search_selected.length > $scope.count_barcode+1 || search_selected.length < $scope.count_barcode-1){
				$scope.count_barcode = search_selected.length;
			}else{
				$scope.count_barcode = $scope.count_barcode+1;
			}
		$scope.before_selected = search_selected;
	}




	/*
	* Función actualizar la lista de
	* préstamos seleccionados mediante
	* la casilla de selección (checkbox).
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.updateSelectionPres = function(pres) {
		if($scope.pres_model[pres.id_ejemplar] == true){
			$scope.pres_selected[pres.id_ejemplar] = pres;
			$scope.pres_count = $scope.pres_count + 1;
		}else{
			$scope.pres_selected[pres.id_ejemplar] = null;
			$scope.pres_count = $scope.pres_count - 1;
		}
		console.log($scope.pres_model[pres.id_ejemplar]+' '+$scope.pres_count);
	};


	$scope.prefOneSelected = {};
	$scope.modalTableOne = false;

	

	/*
	* Función para desplegar ventana modal
	* preguntando al usuario administrador 
	* si desea eliminar una preferencia
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.pre_remove_one_pref = function(registro){
			console.log('preRemoveOnePref called()');
			$scope.modalHeader = '¿Desea eliminar la preferencia?';
			$scope.modalContent = 'Preferencia a eliminar:';
			$scope.modalTableOne = true;
			$scope.modalTable = false;
			$scope.prefOneSelected = registro;
			$scope.modalB1 = 'Eliminar';
			$scope.modalB2 = 'Cancelar';
			$scope.onAction = function(){

				$scope.remove_pref($scope.prefOneSelected);
			};
			$scope.modalShowB1 = true;
			$scope.modalShowB2 = true;
			
	};



	/*
	* Función para renovar un préstamo
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.renovar_pres = function(registro){
		console.log('renovar_pres called()');
		$scope.modalContent = 'Renovando préstamo, espere por favor ';
		$scope.modalAjaxLoader = true;
		$scope.modalTable = false;
		$scope.modalTableOne = false;
		$scope.modalShowB1 = false;
		$scope.modalShowB2 = false;

		$http.defaults.headers.common['X-Auth-Token'] = $cookieStore.get('auth_token')+'_id_'+$cookieStore.get('user_id');
		$http.get(API.url+'bagtesis/backend/public/prestamos/'+registro.id_ejemplar+'/'+$scope.requestData.usuario_id+'/renovar').
		  success(function(data, status, headers, config) {
		  	 console.log(data);
		  	 if(data.error){
		  	 	$scope.modalContent = 'Lo sentimos, no se pudo renovar el préstamo, '+data.error;
			  	$scope.modalB2 = 'Cerrar';
			  	$scope.modalShowB2 = true;
			  	$scope.modalAjaxLoader = false;
		  	 }else{
		  	 	if(data.search("la fecha de expiración ha pasado")>=0){
		  	 		$scope.modalContent = 'No se puede renovar el préstamo, la fecha de expiración ha pasado, el usuario ha quedado suspendido.';
		  	 	}else{
		  	 	 	$scope.modalContent = 'Préstamo renovado exitosamente!';
			  	}
			  	$scope.modalB2 = 'Cerrar';
			  	$scope.modalShowB2 = true;
			  	$scope.modalAjaxLoader = false;
		  	 }
		  	 //$scope.pres_model[registro.id_ejemplar] = false;
			 //$scope.pres_selected[registro.id_ejemplar] = null;
		  	 $scope.get_prestamos_table();
		  });
		  $scope.onAction = function(){};

	};

	/*
	* Función para devolver un préstamo
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.devolver_pres = function(registro){
		console.log('devolver_pres called()');
		$scope.modalContent = 'Guardando devolución, espere por favor ';
		$scope.modalAjaxLoader = true;
		$scope.modalTable = false;
		$scope.modalTableOne = false;
		$scope.modalShowB1 = false;
		$scope.modalShowB2 = false;

		$http.defaults.headers.common['X-Auth-Token'] = $cookieStore.get('auth_token')+'_id_'+$cookieStore.get('user_id');
		$http.get(API.url+'bagtesis/backend/public/prestamos/'+registro.id_ejemplar+'/'+$scope.requestData.usuario_id+'/'+$cookieStore.get('user_id')+'/devolver').
		  success(function(data, status, headers, config) {
		  	 console.log(data);
		  	 if(data.error){
		  	 	$scope.modalContent = 'Lo sentimos, no se pudo devolver el préstamo, '+data.error;
			  	$scope.modalB2 = 'Cerrar';
			  	$scope.modalShowB2 = true;
			  	$scope.modalAjaxLoader = false;
		  	 }else{
		  	 	if(data.search("suspendido")>=0){
		  	 		$scope.modalContent = 'Devolución registrada, '+data;
		  	 	}else{
		  	 	 	$scope.modalContent = 'Devolución registrada exitosamente!';
			  	}
			  	$scope.modalB2 = 'Cerrar';
			  	$scope.modalShowB2 = true;
			  	$scope.modalAjaxLoader = false;
		  	 }
		  	 //$scope.pres_model[registro.id_ejemplar] = false;
			 //$scope.pres_selected[registro.id_ejemplar] = null;
			 $scope.get_preferencias();
		  	 $scope.get_prestamos_table();
		  });
		  $scope.onAction = function(){};

	};



	//$scope.presOneSelected = {};


	/*
	* Función para desplegar ventana modal
	* preguntando al usuario administrador 
	* si desea renovar un préstamo
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.pre_renovar_un_pres = function(registro){
			console.log('preRenovarUnPres called()');
			$scope.modalHeader = '¿Desea renovar el préstamo?';
			$scope.modalContent = 'Préstamo a renovar:';
			$scope.modalTableOne = true;
			$scope.modalTable = false;
			$scope.prefOneSelected = registro;
			$scope.modalB1 = 'Renovar';
			$scope.modalB2 = 'Cancelar';
			$scope.onAction = function(){

				$scope.renovar_pres($scope.prefOneSelected);
			};
			$scope.modalShowB1 = true;
			$scope.modalShowB2 = true;
			
	};

	/*
	* Función para desplegar ventana modal
	* preguntando al usuario administrador 
	* si desea devolver un préstamo de un 
	* usuario.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.pre_devolver_un_pres = function(registro){
			console.log('preDevolverUnPres called()');
			$scope.modalHeader = '¿Desea devolver el préstamo?';
			$scope.modalContent = 'Préstamo a devolver:';
			$scope.modalTableOne = true;
			$scope.modalTable = false;
			$scope.prefOneSelected = registro;
			$scope.modalB1 = 'Devolver';
			$scope.modalB2 = 'Cancelar';
			$scope.onAction = function(){

				$scope.devolver_pres($scope.prefOneSelected);
			};
			$scope.modalShowB1 = true;
			$scope.modalShowB2 = true;
			
	};


	/*
	* Función para desplegar ventana modal
	* preguntando al usuario administrador 
	* si desea renovar los préstamos seleccionados.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.preRenovarPrestamos = function(){

		if($scope.pres_count >0 ){
			$scope.modalHeader = '¿Desea renovar los préstamos seleccionados?';
			$scope.modalContent = 'Préstamos a renovar:';
			$scope.modalTable = true;
			$scope.modalTableOne = false;
			$scope.modalB1 = 'Renovar';
			$scope.modalB2 = 'Cancelar';
			$scope.onAction = function(){
				$scope.renovarVariosPres();
			};
			$scope.modalShowB1 = true;
			$scope.modalShowB2 = true;
		}else{
			$scope.modalHeader = 'Aviso!';
			$scope.modalContent = 'No hay préstamos seleccionados.';
			$scope.modalTable = false;
			$scope.modalB2 = 'Ok';
			$scope.modalShowB1 = false;
			$scope.onAction = function(){};
		}
			
	};

	/*
	* Función para desplegar ventana modal
	* preguntando al usuario administrador 
	* si desea devolver los préstamos seleccionados.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.preDevolverPrestamos = function(){

		if($scope.pres_count >0 ){
			$scope.modalHeader = '¿Desea devolver los préstamos seleccionados?';
			$scope.modalContent = 'Préstamos a devolver:';
			$scope.modalTable = true;
			$scope.modalTableOne = false;
			$scope.modalB1 = 'Devolver';
			$scope.modalB2 = 'Cancelar';
			$scope.onAction = function(){
				$scope.devolverVariosPres();
			};
			$scope.modalShowB1 = true;
			$scope.modalShowB2 = true;
		}else{
			$scope.modalHeader = 'Aviso!';
			$scope.modalContent = 'No hay préstamos seleccionados.';
			$scope.modalTable = false;
			$scope.modalB2 = 'Ok';
			$scope.modalShowB1 = false;
			$scope.onAction = function(){};
		}
			
	};

	$scope.requestDataPres = {};

	/*
	* Función para renovar los préstamos
	* seleccionados.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.renovarVariosPres = function(){
		$scope.modalContent = 'Renovando préstamos, espere por favor ';
		$scope.modalAjaxLoader = true;
		$scope.modalTable = false;
		$scope.modalShowB1 = false;
		$scope.modalShowB2 = false;
		if($scope.pres_count>0){

			$scope.pres_selected.usuario_id = $scope.requestData.usuario_id;
			$scope.pres_selected.admin_id = $cookieStore.get('user_id');

			$scope.requestDataPres.pres_selected = $scope.pres_selected;
			console.log('pres selected: ');
			console.log($scope.pres_selected);

			$http.defaults.headers.common['X-Auth-Token'] = $cookieStore.get('auth_token')+'_id_'+$cookieStore.get('user_id');
			$http.post(API.url+'bagtesis/backend/public/prestamos/renovar',$scope.requestDataPres).
			  success(function(data, status, headers, config) {
			  	 console.log(data);
			  	 if(data.error){
			  	 	$scope.modalContent = 'Lo sentimos, no se pudo renovar los préstamos, '+data.error;
				  	$scope.modalB2 = 'Cerrar';
				  	$scope.modalShowB2 = true;
				  	$scope.modalAjaxLoader = false;
			  	 }else{
		  	 	 	$scope.modalContent = 'Préstamos renovados exitosamente!';
				  	$scope.modalB2 = 'Cerrar';
				  	$scope.modalShowB2 = true;
				  	$scope.modalAjaxLoader = false;
			  	 }
			  	 $scope.get_prestamos_table();
			  }).
			  error(function(data, status, headers, config) {
		  		 
		    	 $scope.modalContent = 'Lo sentimos, no se pudo renovar los préstamos.';
			  	 $scope.modalB2 = 'Cerrar';
			  	 $scope.modalShowB2 = true;
			  	 $scope.modalAjaxLoader = false;
			    
			  }).then(function(){
			  	$scope.unselect_all_pres();
			  	
			  });

		}
	};


	/*
	* Función para devolver los préstamos
	* seleccionados.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.devolverVariosPres = function(){
		$scope.modalContent = 'Devolviendo préstamos, espere por favor ';
		$scope.modalAjaxLoader = true;
		$scope.modalTable = false;
		$scope.modalShowB1 = false;
		$scope.modalShowB2 = false;
		if($scope.pres_count>0){

			$scope.pres_selected.usuario_id = $scope.requestData.usuario_id;
			$scope.pres_selected.admin_id = $cookieStore.get('user_id');

			$scope.requestDataPres.pres_selected = $scope.pres_selected;
			console.log('pres selected: ');
			console.log($scope.pres_selected);

			$http.defaults.headers.common['X-Auth-Token'] = $cookieStore.get('auth_token')+'_id_'+$cookieStore.get('user_id');
			$http.post(API.url+'bagtesis/backend/public/prestamos/devolver',$scope.requestDataPres).
			  success(function(data, status, headers, config) {
			  	 console.log(data);
			  	 if(data.error){
			  	 	$scope.modalContent = 'Lo sentimos, no se pudieron devolver los préstamos, '+data.error;
				  	$scope.modalB2 = 'Cerrar';
				  	$scope.modalShowB2 = true;
				  	$scope.modalAjaxLoader = false;
			  	 }else{
			  	 	if(data.search("suspendido")>=0){
		  	 			$scope.modalContent = 'Devolución registrada, '+data;
			  	 	}else{
			  	 	 	$scope.modalContent = 'Devolución registrada exitosamente!';
				  	}
				  	$scope.modalB2 = 'Cerrar';
				  	$scope.modalShowB2 = true;
				  	$scope.modalAjaxLoader = false;
			  	 }
			  	 $scope.get_preferencias();
			  	 $scope.get_prestamos_table();
			  }).
			  error(function(data, status, headers, config) {
		  		 
		    	 $scope.modalContent = 'Lo sentimos, no se pudo renovar los préstamos.';
			  	 $scope.modalB2 = 'Cerrar';
			  	 $scope.modalShowB2 = true;
			  	 $scope.modalAjaxLoader = false;
			    
			  }).then(function(){
			  	$scope.unselect_all_pres();
			  	
			  });

		}
	};




	$scope.all_pres = false;
	$scope.all_pres_count = 0;

	/*
	* Función para seleccionar o deseleccionar
	* todos los préstamos posibles que se estan mostrando.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.select_all_pres = function(){
		console.log('select_all_pres called()');
		$scope.all_pres = !$scope.all_pres;
		angular.forEach($scope.prestamos, function(pres, key){
				$scope.pres_model[pres.id_ejemplar] = $scope.all_pres;
				$scope.pres_selected[pres.id_ejemplar] = pres;
				$scope.all_pres_count = $scope.all_pres_count+1;

		});
		if($scope.all_pres)
			$scope.pres_count = $scope.all_pres_count;
		else
			$scope.pres_count = 0;

		$scope.all_pres_count = 0;
	};

	/*
	* Función para deseleccionar
	* todos los préstamos posibles que se estan mostrando.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.unselect_all_pres = function(){
		console.log('select_all_pres called()');
		$scope.all_pres = false;
		angular.forEach($scope.prestamos, function(pres, key){
				$scope.pres_model[pres.id_ejemplar] = $scope.all_pres;
				$scope.pres_selected[pres.id_ejemplar] = null;

		});

		$scope.pres_count = 0;

		$scope.all_pres_count = 0;
	};


	$scope.coincideUsuario = function(){
		/*console.log('coincideUsuario called()');
		console.log($cookieStore.get('user_id'));
		console.log($scope.requestData.usuario_id);*/
		if(AuthService.isLoggedIn()){
			if($cookieStore.get('user_id') == $scope.requestData.usuario_id)
				return true;
		}
		return false;
	};



	/*
	* Función para cambiar el color
	* del estado del usuario encontrado,
	* solvente: verde
	* suspendido: amarillo
	* suspensión indefinida: rojo
	*
	* Autor: Cesar Herrera
	*
	*/
    $scope.user_state_style = function(argument) {


    	if(argument.search("Solvente")>=0){
    		return {color:"green"};
    	}

    	if(argument.search("Suspendido por")>=0){
    		return {color:"#E2A70A"};
    	}

    	if(argument.search("Suspensión Indefinida")>=0){
    		return {color:"red"};
    	}

    };								

	/*Configuración de la paginación*/

	paginationConfig.firstText='Primera'; 
	paginationConfig.previousText='< Anterior';
	paginationConfig.nextText='Siguiente >';
	paginationConfig.lastText='Última';
	paginationConfig.boundaryLinks=false;
	paginationConfig.rotate=false;
	paginationConfig.itemsPerPage = 5;
	$scope.currentPage_r;

	/*Fin configuración de la paginación*/


	/*
	* Función para buscar un usuario
	* dada su cedula de identidad.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.buscar_ci = function(){
		console.log('buscar_ci() calledv');


			$scope.bool_disable = true;

			$scope.requestData.usuario_id = '';
			$scope.pref_answer = false;	
			$scope.pref_model = [];
			$scope.pres_model = [];
			$scope.pref_titles = [];
			$scope.pref_count = 0;

			$http.defaults.headers.common['X-Auth-Token'] = $cookieStore.get('auth_token')+'_id_'+$cookieStore.get('user_id');
			$http.post(API.url+'bagtesis/backend/public/prestamos/todos', $scope.requestData).
			  success(function(data, status, headers, config) {
			  	console.log('retorna: ');
			    console.log(data);
			    $scope.errorMessage = '';



			    $scope.answer.prestamos = data.prestamos;
			    $scope.prestamos = data.prestamos.data;
			    $scope.currentPage_p = data.prestamos.current_page;
			    $scope.numPages_p = data.prestamos.last_page;
			    $scope.maxSize_p = data.prestamos.per_page;
			    $scope.totalItems_p = data.prestamos.total;

			    $scope.bool_disable = false;
			    

			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			    console.log('error'+data);
			    $scope.answer = 'No Encontrado';
			    $scope.bool_disable = false;
			    
			  });
		


	};

	$scope.buscar_ci();


	/*
	* Función para obtener los préstamos de 
	* un usuario, tambien es usada para refrescar
	* la vista con nuevas préstamos.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.get_prestamos = function(){

		console.log('get_prestamos called()');

		$http.post(API.url+'bagtesis/backend/public/consulta_prestamos',$scope.requestData)
			.success(function(data, status, headers, config) {

				$scope.prestamos = data;
				/*$scope.currentPage_pres = data.current_page;
			    $scope.numPages_p = data.last_page;
			    $scope.maxSize_pres = data.per_page;
			    $scope.totalItems_pres = data.total;*/

			    $scope.pres_answer = true;

			    if($scope.all_pres){
			    	$scope.all_pres_count = 0;
					$scope.all_pres = !$scope.all_pres;
					$scope.select_all_pres();
				}
			});


	};

	/*
	* Función para obtener los préstamos de 
	* un usuario, tambien es usada para refrescar
	* la vista con nuevas préstamos.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.get_prestamos_table = function(){

		console.log('get_prestamos called()');

		$http.post(API.url+'bagtesis/backend/public/consulta_prestamos',$scope.requestData)
			.success(function(data, status, headers, config) {

				$scope.prestamos = data;
				/*$scope.currentPage_pres = data.current_page;
			    $scope.numPages_p = data.last_page;
			    $scope.maxSize_pres = data.per_page;
			    $scope.totalItems_pres = data.total;*/
			    $scope.answer.prestamos = data;
			    $scope.pres_answer = true;

			    if($scope.all_pres){
			    	$scope.all_pres_count = 0;
					$scope.all_pres = !$scope.all_pres;
					$scope.select_all_pres();
				}
			});


	};


	





    $scope.prom = [];
    $scope.prom_final = [];
    $scope.limitTo = 10;
    $scope.mensaje_buscador_simple = {'data': ''};

    /*
	* Función para cargar los datos de autocompletado.
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.loadingStates = function(val){

	  $scope.mensaje_buscador_simple['data'] = val;

	  var promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_typeahead', $scope.mensaje_buscador_simple)
	                  .success(function(data, status, headers, config) {

	                      $scope.prom = data;

	                  }).then(function(){

	                  		for (var i = 0; i < $scope.limitTo; i++) {
	                  			if(typeof $scope.prom[i] != 'undefined')
	                  				$scope.prom_final[i] = $scope.prom[i];
	                  		};

	                      return $scope.prom_final;
	                  });

	  return promise;

	};

	$scope.final_answer = false;
	$scope.recursos_data = {};



    /*
    * Funcion para desplegar un modal mostrando mayor detalle del recurso.
    *
    * Autor: Cesar Herrera
    */
    $scope.desplegar_recurso = function(id, ejemplar){
		console.log('desplegar recurso called()');

		$http.get(API.url+'bagtesis/backend/public/recursos/'+id+'/'+ejemplar+'/modal_info').
    		success(function(data, status, headers, config){
    			console.log(data);
				$scope.detailModalData = data;
				$scope.detailModalData.pdfurl = API.url + 'bagtesis/backend/public/recursos/'+id+'/pdf';
		});	


	};




	/*Concatenación Controlador Préstamos Consulta*/


	/**********************************************/
	/**********************************************/
	/**********************************************/
	/**********************************************/
	/**********************************************/
	/**********************************************/



 //$scope.requestData={};
	//$scope.requestData.ci = '';
	//$scope.requestData.usuario_id = '';
	$scope.answer2 = {};
	//$scope.currentUserId = '';
	//$scope.errorMessage = '';
	//$scope.modalData = {};
	$scope.user_found = false;



	$scope.pageChanged_p = function(){
		console.log('pageChanged_s() called');
		
		$http.post(API.url+'bagtesis/backend/public/consulta_prestamos?page='+$scope.currentPage_p,$scope.requestData).
		  success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		   
		    $scope.answer.prestamos = data;
		    $scope.currentPage_p = data.current_page;
		    $scope.numPages_p = data.last_page;
		    $scope.maxSize_p = data.per_page;
		    $scope.totalItems_p = data.total;

		    if($scope.all_pres){
		    	$scope.all_pres_count = 0;
				$scope.all_pres = !$scope.all_pres;
				$scope.select_all_pres();
			}

		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    console.log('error'+data);
		    $scope.answer = 'No Encontrado';
		    
		  });
		
	};

	$scope.desplegar_recurso2 = function(recurso2){
		$scope.modalData2 = recurso2;
	};




  });
