angular.module('app')
  .controller('MisPrestamosController', function($scope, $http, $location,$cookieStore, paginationConfig, AuthService, API) {
    	
    $scope.requestData={};
	$scope.requestData.ci = '';
	$scope.requestData.usuario_id = '';

	$scope.answer = {};
	$scope.currentUserId = '';
	$scope.errorMessage = '';

	/*pagination config*/
	paginationConfig.firstText='Primera'; 
	paginationConfig.previousText='< Anterior';
	paginationConfig.nextText='Siguiente >';
	paginationConfig.lastText='Última';
	paginationConfig.boundaryLinks=false;
	paginationConfig.rotate=false;
	paginationConfig.itemsPerPage = 5;


  	if(AuthService.isLoggedIn()){
  		$scope.requestData.usuario_id = $cookieStore.get('user_id');

  		/*$http.post(API.url+'bagtesis/backend/public/get_ci_by_id',$cookieStore.get('user_id'))
	    .success(function(response) {

	      $scope.requestData.ci = response;
		  $scope.requestData.usuario_id = $cookieStore.get('user_id');

	    })
	    .error(function(response) {});*/
		
		$http.post(API.url+'bagtesis/backend/public/consulta_ci',$scope.requestData).
		  success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		    console.log($scope.requestData);
		    $scope.errorMessage = '';
		    if(typeof data.name == 'undefined'){
		    	$scope.errorMessage = 'Usuario no encontrado.';
		    	return false;
		    }

		    $scope.answer.name = data.name;
		    $scope.requestData.usuario_id = data.usuario_id;
		    $scope.answer.lastname = data.lastname;
		    $scope.answer.dependencia = data.dependencia;
		    $scope.answer.type = data.type;
		    $scope.answer.status = data.status;
		    $scope.answer.date = data.date;
		    $scope.answer.prestamos = data.prestamos;

		    $scope.answer.devolucion = data.devolucion;
		    $scope.currentPage_d = data.devolucion.current_page;
		    $scope.numPages_d = data.devolucion.last_page;
		    $scope.maxSize_d = data.devolucion.per_page;
		    $scope.totalItems_d = data.devolucion.total;

		    $scope.answer.suspension = data.suspension;
		    $scope.currentPage_s = data.suspension.current_page;
		    $scope.numPages_s = data.suspension.last_page;
		    $scope.maxSize_s = data.suspension.per_page;
		    $scope.totalItems_s = data.suspension.total;

		    $scope.answer.prestamos = data.prestamos;
		    $scope.currentPage_p = data.prestamos.current_page;
		    $scope.numPages_p = data.prestamos.last_page;
		    $scope.maxSize_p = data.prestamos.per_page;
		    $scope.totalItems_p = data.prestamos.total;
		    

		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    console.log('error'+data);
		    $scope.answer = 'No Encontrado';
		    
		  });

    }




	$scope.pageChanged_d = function(){
		console.log('pageChanged() called');
		
		$http.post(API.url+'bagtesis/backend/public/consulta_devolucion?page='+$scope.currentPage_d,$scope.requestData).
		  success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		   
		    $scope.answer.devolucion = data;
		    $scope.currentPage_d = data.current_page;
		    $scope.numPages_d = data.last_page;
		    $scope.maxSize_d = data.per_page;
		    $scope.totalItems_d = data.total;

		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    console.log('error'+data);
		    $scope.answer = 'No Encontrado';
		    
		  });
		
	};

	$scope.pageChanged_s = function(){
		console.log('pageChanged_s() called');
		
		$http.post(API.url+'bagtesis/backend/public/consulta_suspension?page='+$scope.currentPage_s,$scope.requestData).
		  success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		   
		    $scope.answer.devolucion = data;
		    $scope.currentPage_s = data.current_page;
		    $scope.numPages_s = data.last_page;
		    $scope.maxSize_s = data.per_page;
		    $scope.totalItems_s = data.total;

		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    console.log('error'+data);
		    $scope.answer = 'No Encontrado';
		    
		  });
		
	};

	$scope.pageChanged_p = function(){
		console.log('pageChanged_s() called');
		
		$http.post(API.url+'bagtesis/backend/public/consulta_prestamos?page='+$scope.currentPage_p,$scope.requestData).
		  success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		   
		    $scope.answer.prestamos = data;
		    $scope.currentPage_p = data.current_page;
		    $scope.numPages_p = data.last_page;
		    $scope.maxSize_p = data.per_page;
		    $scope.totalItems_p = data.total;

		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    console.log('error'+data);
		    $scope.answer = 'No Encontrado';
		    
		  });
		
	};

	
  });
