angular.module('app')
  .controller('BuscadorSimpleController', 

    function($rootScope, $modal, $scope,$log,$http, paginationConfig, AuthService,$timeout, API, $cookieStore)  { 
        
        $scope.qrcodeString = 'http://192.168.10.35/bagtesis/backend/public/recursos/T043500038998/pdf';
        $scope.rutas_ids_recursos_seleccionados = [];
        $scope.ids_recursos = "";
        $scope.array_ids_recursos = [];
        $scope.ruta_index_recurso = "";
        $scope.bool_acciones=false;
        $scope.bool_table_recursos=false;
        $scope.paste = false;
        $scope.bool_correo_exitoso = false;
        $scope.bool_correo_error = false;
        $scope.loading_correo = false;
        $scope.bool_disable = false;
        $scope.bool_recursos_encontrados = false;
        $scope.aux_seleccionado = "";
        $scope.id_recursos = [];
        $scope.mypush_i = 0;
        $scope.selected = '';
        $scope.split_seleted = [];
        $scope.mensaje_buscador_simple = {'data': ''};
        $scope.carga = {'visibility': 'hidden'};
        $scope.carga_respuesta =  {'visibility': 'hidden'};
        $scope.div_correo = {};
        $scope.currentPage = 1;
        $scope.totalItems;
        $scope.buscando_select = false;
        $scope.autenticado = AuthService.isLoggedIn();
        $scope.bool_disable_acciones = true;
        $scope.bool_preferencia_exitoso = false;
        $scope.loading_acciones = false;
       // $recursos = [];
        $scope.states = [] ;//['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
          // Any function returning a promise object can be used to load values asynchronously
        $scope.recursos = [];
        $index_recurso;

        $scope.aux_respuesta='';

        $scope.correos = [{email: '', name:''}

                        ];

        $scope.mensaje_email= {  
                                
                              };

        $scope.class_check_seleccionar_todos = "btn btn-default";
        $scope.bool_seleccionar_todos = false;       
        $scope.num_items_pagina  = 10;         
        $scope.datos_qr = [];
        /*pagination config*/
        paginationConfig.firstText='Primera'; 
        paginationConfig.previousText='< Anterior';
        paginationConfig.nextText='Siguiente >';
        paginationConfig.lastText='Última';
        paginationConfig.boundaryLinks=false;
        paginationConfig.rotate=false;
        paginationConfig.itemsPerPage = $scope.num_items_pagina;
        $scope.maxSize = 20;


   /*     $scope.$watch('selected',function(){   
                $scope.states = [];
                $scope.split_seleted = [];
                $scope.mensaje_buscador_simple['data'] = $scope.selected;
               
                $scope.split_seleted = $scope.selected.split(' ');
               // alert($scope.split_seleted[0]);
              // alert( $scope.mensaje_buscador_simple['data'] );
                //alert('Exitos!');
               $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_typeahead', $scope.mensaje_buscador_simple).        
                    success(function(data, status, headers, config) {
                    
            //        $scope.respuesta_buscador_simple = data['data'];

                    $scope.states=[];
                    $scope.mypush.push(data);
                    $scope.states = data;

                     $scope.mypush_i++;

                    // this callback will be called asynchronously
                    // when the response is available
                    }).
                    error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    });   
            },true); */
          
          $scope.options_orden =  [
              { label: 'Normal', value: 'normal' },
              { label: 'Título A...Z', value: 'titulo_asc' },
              { label: 'Título Z...A', value: 'titulo_desc' },
              { label: 'Autor  A...Z', value: 'autor_asc' },
              { label: 'Autor  Z...A', value: 'autor_desc' },
              { label: 'Año ascendente', value: 'anio_asc' },
              { label: 'Año descendente', value: 'anio_desc' }
          ]
          ;
          $scope.orden_seleccionado =  $scope.options_orden[0];
          $scope.value_orden_seleccionado =  $scope.orden_seleccionado.value;



$scope.prom = [];
$scope.aux_selet = "";
$scope.aux_promise = [];



$scope.funcion_buscar = function (val){
  $scope.mensaje_buscador_simple['data'] = val;




  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_typeahead', $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;
                      $scope.aux_selet = val;
                      $scope.buscando_select = false;
  
                      return $scope.prom;
                  });
    return $scope.promise;

};



$scope.loadingStates = function(val){
 // $timeout(funcion_buscar, 3000);
      //  $scope.consultar_buscador_simple(val);
    return $scope.funcion_buscar(val);
};
        


//$scope.busco = false;

$scope.iniciar_typeahead = function(){


    
    $scope.split_seleted = [];
    $scope.mensaje_buscador_simple['data'] = $scope.selected;

    $scope.split_seleted = $scope.selected.split(' ');
   // alert($scope.split_seleted[0]);
  // alert( $scope.mensaje_buscador_simple['data'] );
    //alert('Exitos!');
   $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_typeahead', $scope.mensaje_buscador_simple).        
        success(function(data, status, headers, config) {
        
//        $scope.respuesta_buscador_simple = data['data'];

       // $scope.states=[];



        
        $scope.states = data;




        // this callback will be called asynchronously
        // when the response is available
        }).
        error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        });    
  
     }


      $scope.onSelect = function ($item, $model, $label) {
    $scope.$item = $item;
    $scope.$model = $model;
    $scope.$label = $label;
};

     $scope.getLocation = function(val) {
        return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
          params: {
            formatted_addressdress: val,
            sensor: false
          }
        }).then(function(response){
          return response.data.results.map(function(item){
            return item.formatted_address;
          });
        });
      }

  $scope.i = 0;
  $scope.getServiceRecurso = function (recurso_id){

        //  $scope.recursos = [{myindex: index},{titulo_id:titulo_id}];
    $http.get(API.url+'bagtesis/backend/public/recursos/'+recurso_id).
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
   
      $scope.recursos.push(data);
      $scope.recursos[$scope.i]['model_checkbox'] = false;

      $scope.i++;

  // alert(recurso_id+" "+index_recurso+" "+index_titulo);
 //   $scope.page.data[index_titulo]['recursos'][index_recurso]['titulos'] = $scope.titulos;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
  //  $scope.titulos = data;
    
  });
    

  }

  $scope.a ;
    $scope.consultar_buscador_simple = function(selected){

    if(selected!=""){
        $scope.i = 0;
      //  $scope.lector_barra = false;
        $scope.bool_acciones = true;
        $scope.bool_recursos_encontrados = false;
        $scope.bool_disable = true;
        $scope.bool_disable_acciones = true;

        $scope.loading = true;
        $scope.data = { 'data': selected};
        $scope.respuesta_buscador_simple = [];
        paginationConfig.itemsPerPage = $scope.num_items_pagina;
        
        $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_simple?page=1&num_items_pagina='+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value, $scope.data).
            success(function(data, status, headers, config) {
            $scope.a = data;
            $scope.pagina = data;
            $scope.id_recursos = data.data;
            $scope.recursos = [];
            $scope.currentPage = data.current_page;
            $scope.contador_caracteres = 0;
            $scope.totalItems = data.total;
            $scope.numPages = data.last_page;
            $scope.loading = false;
         //   alert($scope.num_items_pagina);
        //    paginationConfig.itemsPerPage = data.currentPage;
        //    paginationConfig.itemsPerPage = data.per_page;


            if($scope.lector_barra == true){
                $scope.bool_let = true;
                $scope.contador_caracteres = 0;
                $scope.lector_barra = false;
            }




        $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
            success(function(data, status, headers, config) {
            $scope.recursos = data;
            $scope.bool_disable = false;
            $scope.bool_recursos_encontrados = true;
      //      $scope.busco = true;
/*           angular.forEach($scope.id_recursos, function(value, key) {
              $scope.getServiceRecurso(value);

            });

*/          

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });



            $scope.bool_table_recursos=true;
       //     $scope.bool_acciones = false;

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });

          }  
    }




  /*  $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    }
  */

   $scope.getService =  function(route_peticion){
   
     $scope.i = 0 ;
     $scope.data = { 'data': $scope.selected}

     $scope.respuesta_buscador_simple = [];
     $http.post(route_peticion , $scope.data).
            success(function(data, status, headers, config) {

              $scope.id_recursos = data.data;
              $scope.recursos = [];
              $scope.recursos = [];


        $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
            success(function(data, status, headers, config) {
            $scope.recursos = data;
            
/*           angular.forEach($scope.id_recursos, function(value, key) {
              $scope.getServiceRecurso(value);

            });

*/          

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });



 /*             angular.forEach($scope.id_recursos, function(value, key) {
                $scope.getServiceRecurso(value);

              });
*/

              $scope.currentPage = data.current_page;
              $scope.totalItems = data.total;
              $scope.numPages = data.last_page;
            }).
                   
            error(function(data, status, headers, config) {

            });
   }



    $scope.pageChanged = function(currentPage){
        $scope.currentPage = currentPage;
    //    $scope.bool_acciones = false;
  //    $log.log('Page changed to: ' + $scope.currentPage);
      $scope.getService(API.url+'bagtesis/backend/public/buscadores/buscador_simple?page='+$scope.currentPage+"&num_items_pagina="+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value);

     // $scope.watchPage = newPage;
    }

$scope.getServiceRecursoAutores = function (recurso_id, index_recurso){
   // alert(recurso_id+" "+index_recurso+" "+index_titulo);    

    $http.get(API.url+'bagtesis/backend/public/recursos/'+recurso_id+'/autores').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
      $scope.autores = data;

      $scope.recursos[index_recurso]['autores'] = $scope.autores;
  // alert(recurso_id+" "+index_recurso+" "+index_titulo);

   // $scope.page.data[index_titulo]['recursos'][index_recurso]['autores'] = $scope.autores;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
 //   $scope.autores = data;
    
  });
}


$scope.getServiceRecursoDescriptores = function(recurso_id,index_recurso){
 
    $http.get(API.url+'bagtesis/backend/public/recursos/'+recurso_id+'/descriptores').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
   
    $scope.descriptores = data;
  //  alert($scope.descriptores);
  // alert(recurso_id+" "+index_recurso+" "+index_titulo);
    $scope.recursos[index_recurso]['descriptores'] = $scope.descriptores;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
   // $scope.autores = data;
    
  });
}



  $scope.add_correo = function(){
  //  alert($scope.index_titulo);
    $scope.correos.push({email: '',name:''});
    $scope.bool_correo_exitoso = false;
  }

  $scope.delete_correos = function(index){
    $scope.bool_correo_exitoso = false;
    if($scope.correos.length!=1){
        $scope.correos.splice(index,1);
    }
  }

  $scope.desplegar_correo = function(){
       // alert("desplegar correo");

       $scope.div_correo = {'display': 'inline-block'};
  }

  $scope.desplegar_2 = function (){
     $scope.div_correo = {'display':'none'};
 //    $scope.collapse_correo = {};
     // $scope.collapse_correo = "";
    //  $scope.collapse_correo = "collapse on";
  }


  $scope.repuesta;
  $scope.enviar_correo = function (){



     $scope.mensaje_email['correos'] = $scope.correos;
    $scope.mensaje_email['id_recursos'] = [$recursos[$scope.index_recurso]['id']]; 

  /*  $scope.iter = 0;
     angular.forEach($scope.recursos, function(value, key) {
         if(value.model_checkbox==true){
             this.push(value.id);
          }       
        }, $scope.mensaje_email['id_recursos']);

  */
    $http.post(API.url+'bagtesis/backend/public/recursos/mail', $scope.mensaje_email).
          success(function(data, status, headers, config) {
                $scope.respuesta = data;
            //    $scope.carga['visibility'] = 'hidden';
             //   $scope.carga_respuesta['visibility']= 'visible';
            //    alert($scope.respuesta);
            // this callback will be called asynchronously
            // when the response is available
            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });


 /*     $scope.carga['visibility'] = 'visible';
      $scope.carga_respuesta['visibility']= 'hidden';
   //   alert('Enviar Correo');
       $scope.mensaje_email['correos'] = $scope.correos;

   //   $scope.mensaje_email['datos'] = $scope.page[$scope.index_titulo]['recursos'][$scope.index_recurso];
     // $scope.hola=[$scope.page.data[$scope.index_titulo]['recursos'][$scope.index_recurso]];
        $scope.mensaje_email['datos'] = [$scope.recursos[$scope.index_recurso]];
  
        $http.post(API.url+'bagtesis/backend/public/recursos/mail', $scope.mensaje_email).
        success(function(data, status, headers, config) {
            $scope.respuesta = data;
            $scope.carga['visibility'] = 'hidden';
            $scope.carga_respuesta['visibility']= 'visible';
        // this callback will be called asynchronously
        // when the response is available
        }).
        error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        });
*/
  }



//  $scope.enviar_correos_


    $scope.generar_ids_recursos_seleccionados = function (){

     $scope.ids_recursos = "";
     $scope.array_ids_recursos = [];
     $scope.rutas_ids_recursos_seleccionados = [];
     $scope.datos_qr = [];
     $scope.iter = 0;
     angular.forEach($scope.recursos, function(value, key) {
        if(value.model_checkbox){
          $scope.ids_recursos = $scope.ids_recursos+value.id;
          $scope.ruta_qr = API.url+"bagtesis/backend/public/recursos/"+value.id+"/pdf";
          $scope.cota    = value.ubicacion;
          $scope.titulo  = value.titulos;
          $scope.ids_recursos = $scope.ids_recursos+" ";
          $scope.datos = {   
                            ruta_qr: $scope.ruta_qr,
                            titulo:  $scope.titulo,
                            cota:    $scope.cota
                         };
          $scope.datos_qr.push($scope.datos);
        }
        $scope.iter++;    
        }); 

      $scope.ids_recursos = $scope.ids_recursos.substring(0,$scope.ids_recursos.length-1);
      $scope.ruta_ids_recursos = API.url+"bagtesis/backend/public/recursos/"+$scope.ids_recursos+"/pdf";



 /*  for($scope.i=0; $scope.i<$scope.array_ids_recursos.length;$scope.i++){
          $scope.ids_recursos = $scope.ids_recursos+$scope.array_ids_recursos[$scope.i];
          $scope.rutas_ids_recursos_seleccionados.push("http://192.168.10.35/bagtesis/backend/public/recursos/"+$scope.array_ids_recursos[$scope.i]+"/pdf");
          if($scope.i != $scope.array_ids_recursos.length-1){
              $scope.ids_recursos = $scope.ids_recursos+" ";
          }
      }
*/

   //    alert($scope.ids_recursos_pdf);
  };



  $scope.desplegar_recurso = function (recurso_id,index_recurso){
    /* alert(recurso_id);
      alert(index_recurso);
      */
      $scope.bool_preferencia_exitoso = false;
      $scope.bool_correo_error = false;
      $scope.loading_acciones = false;
      $scope.autenticado = AuthService.isLoggedIn();
      $scope.recursos[index_recurso].model_checkbox = true;
      $scope.bool_acciones = true;
      $scope.bool_correo_exitoso = false;
      $scope.bool_disable_acciones = false;
      $scope.mensaje_email = {};
      $scope.correos = [
                          {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                      ];
      $scope.generar_ids_recursos_seleccionados();

      $scope.bool_button = false;
      $scope.div_correo_modal.show = false;
      $scope.index_recurso = index_recurso;
      $scope.div_qr.show = false;
      $scope.ruta_index_recurso = API.url+"bagtesis/backend/public/recursos/"+$scope.recursos[$scope.index_recurso].id+"/pdf";
      $scope.getServiceRecursoAutores(recurso_id,index_recurso);
      $scope.getServiceRecursoDescriptores(recurso_id,index_recurso);
  }


  $scope.div_correo = {};
  $scope.div_correo.show = false;
  $scope.bool_loged = false;
  $scope.lector_barra = false;
  $scope.contador_caracteres = 0;

  $scope.bool_let = false;
  $scope.verificar_lector = function(selected){
   // $scope.busco = false;


    if($scope.selected.length == 0){
      $scope.contador_caracteres = 0;
    }


   if($scope.selected.length >= 1){
      $scope.contador_caracteres++;
    }


    if($scope.contador_caracteres == 1 & $scope.lector_barra == false /*& selected.length>2*/){
        $scope.lector_barra = true;

          $scope.consultar_buscador_simple(selected);

    }

  }


$scope.validarEmail = function( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ){
        return true;
    }
    return false;   
};


$scope.validar_correos = function (){
    $scope.valor = false;
    $scope.correos.forEach(function(value, key) {
       $scope.validar = $scope.validarEmail(value.email);
         if($scope.validar){
            $scope.valor = true;
         }
      // alert(value.email);
    });
    return $scope.valor; 
};



  function partB() {
    
  }

  $scope.enviar_recursos_seleccionados = function(){    /* ------------------- */

    $scope.v = $scope.validar_correos();
    if($scope.v == true){
          $scope.bool_correo_error = true;
          $scope.bool_correo_exitoso = false;
    }else{      

      $scope.bool_correo_error = false;
      $scope.mensaje_email = {};
      $scope.mensaje_email['correos'] = $scope.correos;
      $scope.mensaje_email['id_recursos'] = []; 
      $scope.loading_correo = true;

      $scope.bool_disable = true;
      $scope.bool_correo_exitoso = false;
      $scope.iter = 0;


     angular.forEach($scope.recursos, function(value, key) {
         if(value.model_checkbox==true){
             this.push(value.id);
          }       
        }, $scope.mensaje_email['id_recursos']);
           $scope.bool_correo_exitoso = true;
           $scope.loading_correo = false;
           $scope.bool_disable = false;

    $http.post(API.url+'bagtesis/backend/public/recursos/mail', $scope.mensaje_email).
          success(function(data, status, headers, config) {
                $scope.respuesta = data;
                $scope.loading_correo = false;
              //  $scope.bool_correo_exitoso = true;
                $scope.bool_disable = false;

            //    $scope.carga['visibility'] = 'hidden';
             //   $scope.carga_respuesta['visibility']= 'visible';
           //     alert($scope.respuesta);
            // this callback will be called asynchronously
            // when the response is available
            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    
    }
  };

  $scope.bool_button = false;

  $scope.div_correo = function() {

        $scope.bool_button = true;


      $scope.correos = [
                          {email: '', name:''}
                      ];

      if($rootScope.userLogged){
              $scope.correos = [
                                  {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                                ];

      }

      $scope.mensaje_email = {};
      $scope.div_qr.show = false;
      $scope.div_pdf.show = false;

      $scope.div_correo.show = !$scope.div_correo.show;     
  
  };



  $scope.verficar_usuario = function(){
      $scope.bool_correo_exitoso = false;
      $scope.bool_correo_error = false;
      $scope.mensaje_email = {};


      if($rootScope.userLogged){
              $scope.correos = [
                                  {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                                ];

      }
  }


  $scope.verficar_recursos_seleccionados = function(){ 

    for(var i=0;i< $scope.recursos.length; i++){
          if($scope.recursos[i].model_checkbox == true){
              return true;
          }
    }
    return false;
  }


  $scope.recurso_seleccionado = function (){ 
    if($scope.verficar_recursos_seleccionados()){
        $scope.generar_ids_recursos_seleccionados();
        $scope.bool_acciones = true;
        $scope.bool_disable_acciones = false;
        //$scope.bool_disable = false;
    }else{
        $scope.bool_disable_acciones = true;
   //    $scope.bool_acciones = false;
       // $scope.bool_disable = true;
    }
  }



  $scope.div_correo_modal = function() {
      $scope.loading_acciones = false;
      $scope.bool_correo_exitoso = false;
      $scope.bool_preferencia_exitoso = false;
      $scope.bool_button = false;

      $scope.correos = [
                          {email: '', name:''}
                      ];

      if($rootScope.userLogged){
              $scope.correos = [
                                  {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                                ];

      }

      $scope.mensaje_email = {};
      $scope.div_qr.show = false;
      $scope.div_pdf.show = false;
      $scope.div_correo_modal.show = !$scope.div_correo_modal.show;     
  
  };



  $scope.div_pdf = {};
  $scope.div_pdf.show = false;
  $scope.div_pdf = function() {
      $scope.bool_preferencia_exitoso = false;    
      $scope.loading_acciones = false;
      $scope.bool_button = false;
      $scope.bool_loged = false;
      $scope.div_qr.show = false;
      $scope.div_correo_modal.show = false;
      $scope.div_pdf.show = !$scope.div_pdf.show;
  };

  $scope.div_qr = {}
  $scope.div_qr.show = false;
  $scope.div_qr = function() {
      $scope.bool_preferencia_exitoso = false;
      $scope.loading_acciones = false;
      $scope.bool_button = false;
      $scope.bool_loged = false;
      $scope.div_correo_modal.show = false;
      $scope.div_pdf.show = false;
      $scope.bool_correo_exitoso = false;
      $scope.div_qr.show = !$scope.div_qr.show;
  };

/*  $scope.delete = function(index){
    if($scope.inputs.length!=1){
      $scope.correos.splice(index,1);
    }  
      $scope.bool_correo_exitoso = false;
  };
*/

$scope.delete = function(index){

  $scope.inputs.splice(index,1);

};



  $scope.cambiar_color_hover = function (){
      $scope.color_hover.color = "#d9edf7";
  };


  $scope.descargar_pdf = function(){
      alert('descarga pdf');
    $http.get(API.url+'bagtesis/backend/public/recursos/T043500020532/pdf').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the respon se is available
    alert('Exitos!!');
       window.open(data);

    //  $scope.recursos.push(data);
    //  $scope.recursos[$scope.i]['model_checkbox'] = false;
    //  $scope.i++;

  // alert(recurso_id+" "+index_recurso+" "+index_titulo);
 //   $scope.page.data[index_titulo]['recursos'][index_recurso]['titulos'] = $scope.titulos;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
  //  $scope.titulos = data;
    
  });


  }



  $scope.seleccionar_todos = function (){
      $scope.bool_seleccionar_todos = !$scope.bool_seleccionar_todos;
      if($scope.bool_seleccionar_todos == true){
          $scope.bool_acciones = true; 
          $scope.bool_disable_acciones = false;
        //  $scope.bool_disable = false;
          $scope.class_check_seleccionar_todos = "btn btn-success";
          for ($scope.i =0 ; $scope.i< $scope.recursos.length; $scope.i++) {
              $scope.recursos[$scope.i].model_checkbox = true;
          };
      }else{
          $scope.bool_disable_acciones = true;
          $scope.class_check_seleccionar_todos = "btn btn-default";
          for ($scope.i =0 ; $scope.i< $scope.recursos.length; $scope.i++) {
              $scope.recursos[$scope.i].model_checkbox = false;
          };
    //    $scope.bool_acciones = false;
       // $scope.bool_disable = true;
      }
  }


 $scope.actualizar_perpage = function (num_items_pagina){
      paginationConfig.itemsPerPage = num_items_pagina;
 }


 $scope.verificar_id = function(val){
    for(var i=1; i < val.length; i++){
        if(parseInt(val[i])<0 || parseInt(val[i])>9 || val[i].charCodeAt(0)<48 || val[i].charCodeAt(0)>57){
             //   alert(parseInt(val[12]));
            return false;
        }
    }
    return true;
 };


 $scope.verificar_paste = function(val){
    if($scope.paste & val.length == 13 & val.indexOf(' ')<0 & (val[0] == 'T'  || val[0] == 'D')  &  $scope.verificar_id(val)){
        $scope.consultar_buscador_simple(val);
    }
 }

 $scope.pref_data = {};
 
$scope.preferencial = function(){
  console.log('preferencial called()');
  $scope.loading_acciones = true;
  $scope.bool_preferencia_exitoso = false;
  $scope.bool_correo_exitoso = false;
  $scope.bool_correo_error = false;
  $scope.div_qr.show = false;
  $scope.div_correo_modal.show = false;
  $scope.pref_data.usuario_id = $cookieStore.get('user_id');
  $scope.pref_data.recurso_id = $scope.recursos[$scope.index_recurso].id;
  $http.defaults.headers.common['X-Auth-Token'] = $cookieStore.get('auth_token')+'_id_'+$cookieStore.get('user_id');
  $http.post(API.url+'bagtesis/backend/public/preferencias',$scope.pref_data).
  success(function(data, status, headers, config) {
  console.log(data);
  $scope.loading_acciones = false;
  $scope.bool_preferencia_exitoso = true;
  $scope.pref_data = {};
      
 // $scope.get_pref
  });


 };


  });



