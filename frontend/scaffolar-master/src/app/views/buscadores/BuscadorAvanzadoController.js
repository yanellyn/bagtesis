angular.module('app')
  .controller('BuscadorAvanzadoController', 

    function($rootScope, $modal, $scope,$log,$http, paginationConfig, AuthService, API, $cookieStore)  { 


            $scope.inputs = [];
            $scope.bibliotecas = [];
            $scope.dependencias = [];
            $scope.selected = '';
            $scope.bool_add_button = false;
            $scope.bool_disable_acciones = true;
            $scope.loading_acciones = false;
            $scope.bool_preferencia_exitoso = false;
            $scope.bool_correo_exitoso = false;
            $scope.bool_correo_error = false;
            $scope.loading_correo = false;
            $scope.bool_disable = false;
            $scope.autenticado = AuthService.isLoggedIn();
            $scope.loading = false;
            $scope.styles_recursos = [{'background-color': ''},{'background-color': ''},{'background-color': ''},{'background-color': ''}];
            $scope.style_todo = "";
            $scope.style_tesis= "";
            $scope.style_monografia= "";
            $scope.style_publicacion_seriada= "";
            $scope.totalItems; 
            $scope.function_typeahead = "state for state in loading_titulo_States($viewValue) | limitTo:5  | filter:$viewValue";
            $scope.options = {

                                todo:  {
                                    opciones1: [   
                                                    {label: 'Título' , value: 'titulo'}, 
                                                    {label:'cota', value: 'cota'},
                                                    {label: 'Autor'  , value: 'autor'  },
                                                    {label: 'Materia', value: 'materia'},
                                                    {label: 'Palabra clave'  , value: 'palabra_clave'}    
                                                ],

                                    opciones2: [
                                                    {label: 'Y'   , value: 'AND'},
                                                    {label: 'O' , value: 'OR'    },
                                                    {label: 'NO'  , value: 'NOT' }
                                               ]
                                },      

                                monografia: {


                                    opciones1: [  
                                                    {label: 'Título' ,   value: 'titulo'   },          
                                                    {label: 'Cota'   ,   value: 'cota'     },
                                                    {label: 'Autor'  ,   value: 'autor'    },
                                                    {label: 'Editorial', value: 'editorial'},
                                                    {label: 'Edición',   value:  'edicion' },
                                                    {label: 'Isbn'   ,   value: 'isbn'     },
                                                    {label: 'Volumen',   value: 'volumen'  },
                                                    {label: 'Materia',   value: 'materia'},
                                                    {label: 'Palabra clave'  , value: 'palabra_clave'},
                                                    {label: 'Idioma' ,   value: 'idioma'   }
                                                ],

                                    opciones2: [
                                                    {label: 'Y'   , value: 'AND'},
                                                    {label: 'O' , value: 'OR'    },
                                                    {label: 'NO'  , value: 'NOT LIKE' }
                                               ]
                                },

                                tesis: {
                                    opciones1: [       
                                                    {label: 'Título' , value: 'titulo'  },     
                                                    {label: 'Cota'   , value: 'cota'    },
                                                    {label:'Materia', value: 'materia'  },
                                                    {label: 'Autor'  , value: 'autor'   },
                                                    {label: 'Jurado' , value: 'jurado'  },
                                                    {label: 'Tutor Académico' , value: 'tutor_academico'},
                                                    {label: 'Tipo de Trabajo' , value: 'tipo_de_trabajo'},
                                                    {label: 'Palabra clave'  , value: 'palabra_clave'},
                                                    {label: 'Idioma' , value: 'idioma'}

                                                ],

                                    opciones2: [
                                                    {label: 'Y'   , value: 'AND'},
                                                    {label: 'O' , value: 'OR'    },
                                                    {label: 'NO'  , value: 'NOT LIKE' }
                                               ]
                                },



                                publicacion_seriada: {
                                    opciones1: [
                                                    {label: 'Título' , value: 'titulo' },
                                                    {label: 'Autor'  , value: 'autor'  },
                                                    {label: 'Issn'   , value: 'issn'   },
                                                    {label: 'Volumen', value: 'volumen'},
                                                    {label: 'Palabra clave'  , value: 'palabra_clave'}
                                                ],

                                    opciones2: [
                                                    {label: 'Y'   , value: 'AND'},
                                                    {label: 'O' , value: 'OR'    },
                                                    {label: 'NO'  , value: 'NOT LIKE' }
                                               ]
                                },                   
                            };
                            $scope.options_bibliotecas = [];
                            $scope.options_bibliotecas_seleccionado = {};
                            $scope.options_dependencias = [];
                            $scope.options_dependencias_seleccionado = {};
                $scope.mensaje_buscador_avanzado = {};    
                $scope.anios = {anio_origen: '',
                               anio_destino: ''
                              };
                $scope.div_correo = {};
                $scope.bool_acciones=false;
                $scope.bool_table_recursos=false;   
                $scope.recursos = [];
                $index_recurso;

                $scope.aux_respuesta='';

                $scope.correos = [{email: '', name:''}

                                ];

                $scope.mensaje_email= {  
                                        
                                      };
                $scope.rutas_ids_recursos_seleccionados = [];
                $scope.ids_recursos = "";
                $scope.ruta_index_recurso = "";
                $scope.id_recursos = [];
                $scope.mypush_i = 0;
                $scope.class_check_seleccionar_todos = "btn btn-default";
                $scope.bool_seleccionar_todos = false;
                $scope.datos_qr = [];
                $scope.title_tooltip = "Borrar campos de búsqueda";
                $scope.mytooltips = [{title: 'Borrar campos', tooltip: 'tooltip', position: 'rigth'}];
                $scope.num_items_pagina = 10;

                        /*pagination config*/
                paginationConfig.firstText='Primera'; 
                paginationConfig.previousText='< Anterior';
                paginationConfig.nextText='Siguiente >';
                paginationConfig.lastText='Última';
                paginationConfig.boundaryLinks=false;
                paginationConfig.rotate=false;
                paginationConfig.itemsPerPage = 10;

                $scope.maxSize = 20;


          $scope.options_orden =  [
                                      { label: 'Normal', value: 'normal' },
                                      { label: 'Título A...Z', value: 'titulo_asc' },
                                      { label: 'Título Z...A', value: 'titulo_desc' },
                                      { label: 'Autor  A...Z', value: 'autor_asc' },
                                      { label: 'Autor  Z...A', value: 'autor_desc' },
                                      { label: 'Año ascendente', value: 'anio_asc' },
                                      { label: 'Año descendente', value: 'anio_desc' }
                                  ];
          $scope.orden_seleccionado =  $scope.options_orden[0];
          $scope.value_orden_seleccionado =  $scope.orden_seleccionado.value;



$scope.getServiceBibliotecas = function (){
   // alert('alert Biblioteca');
        $http.get(API.url+'bagtesis/backend/public/bibliotecas').
            success(function(data, status, headers, config) {
            $scope.bibliotecas = data;
            $scope.options_bibliotecas.push({label:'Todas',value:'todas'});
            angular.forEach($scope.bibliotecas, function(value, key) {
              this.push({label:value.biblioteca_salida, value: value.id});
            },$scope.options_bibliotecas);

            $scope.options_bibliotecas_seleccionado = $scope.options_bibliotecas[0];

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });
};

$scope.getServiceDependencias = function(){
        $http.get(API.url+'bagtesis/backend/public/dependencias').
            success(function(data, status, headers, config) {
            $scope.dependencias = data;
            $scope.options_dependencias.push({label:'Todas',value:'todas'});
            angular.forEach($scope.dependencias, function(value, key) {
              this.push({label:value.dependencia_salida, value: value.id});
            },$scope.options_dependencias);

            $scope.options_dependencias_seleccionado = $scope.options_dependencias[0];

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });


};

$scope.getServiceBibliotecas();
$scope.getServiceDependencias();

$scope.setType = function(optionName){
    $scope.inputs = [];
    $scope.bool_add_button = true;
    $scope.selected = optionName;

    switch(optionName) {
        case 'todo':
            $scope.style_todo = 'background-color:#CC8548';
            $scope.style_tesis= '';
            $scope.style_monografia= '';
            $scope.style_publicacion_seriada= '';

            break;
        case 'tesis':
            $scope.style_todo = '';
            $scope.style_tesis= 'background-color:#CC8548';
            $scope.style_monografia= '';
            $scope.style_publicacion_seriada= '';

            break;
        case 'monografia':

            $scope.style_todo = '';
            $scope.style_tesis= '';
            $scope.style_monografia= 'background-color:#CC8548';
            $scope.style_publicacion_seriada= '';            
            break;   
        case 'publicacion_seriada':
            $scope.style_todo = '';
            $scope.style_tesis= '';
            $scope.style_monografia= '';
            $scope.style_publicacion_seriada= 'background-color:#CC8548';


            break;                    
    }

    $scope.newOption($scope.options[$scope.selected].opciones1[0], "", $scope.options[$scope.selected].opciones2[0], "",$scope.selected);
}


$scope.newOption = function(selet_model1, text_model1, selet_model2, text_model2,selected){
    $scope.inputs.push({
        opt1: {
            select_model: selet_model1,
            text_model: text_model1,
            function_typeahead: '',
        }, 
        opt2: {
            select_model: selet_model2,
            text_model: text_model2,
        },
        
    });
};

$scope.add_option = function(){
    $scope.mytooltips.push({title: 'Borrar campos', tooltip: 'tooltip', position: 'rigth'});
   $scope.newOption($scope.options[$scope.selected].opciones1[0], "", $scope.options[$scope.selected].opciones2[0], "");
};

$scope.delete = function(index){
    if($scope.inputs.length!=1){
        $scope.inputs.splice(index,1);
    }
}

$scope.delete_correos = function(index){
    $scope.bool_correo_exitoso = false;
    if($scope.correos.length!=1){
        $scope.correos.splice(index,1);
    }
}

$scope.exitos = [];

$scope.consulta_buscador_avanzado = function(){
        $scope.currentPage = 1;
        $scope.bool_acciones = true;
        $scope.bool_disable_acciones = true;
        $scope.mensaje_buscador_avanzado['inputs'] = [];
        $scope.mensaje_buscador_avanzado['inputs'] = $scope.inputs;
        $scope.mensaje_buscador_avanzado['anios'] = $scope.anios;
        $scope.mensaje_buscador_avanzado['tipo_recurso'] = $scope.selected;
        $scope.loading = true;
        $scope.mensaje_buscador_avanzado['biblioteca'] = $scope.options_bibliotecas_seleccionado;
        $scope.mensaje_buscador_avanzado['dependencia'] = $scope.options_dependencias_seleccionado;
        $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_avanzado?page=1&num_items_pagina='+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value,  $scope.mensaje_buscador_avanzado).
            success(function(data, status, headers, config) {

            $scope.pagina = data;
            $scope.exitos= data;
            $scope.id_recursos = data.data;
            $scope.recursos = [];
            $scope.currentPage = data.current_page;
            $scope.contador_caracteres = 0;
            $scope.totalItems = data.total;
            $scope.numPages = data.last_page;
            $scope.loading = false;

        $scope.totalItems_s = data.total;
            if($scope.lector_barra == true){
                $scope.bool_let = true;
                $scope.contador_caracteres = 0;
                $scope.lector_barra = false;
            }

   
            $scope.bool_table_recursos=true;
          //  $scope.bool_acciones = false;


        $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
            success(function(data, status, headers, config) {
            $scope.recursos = data;
      //      $scope.busco = true;
/*           angular.forEach($scope.id_recursos, function(value, key) {
              $scope.getServiceRecurso(value);

            });

*/          

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });



    //      $scope.recursos = $scope.id_recursos;


            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });
  }

$scope.mensaje_buscador_simple = {};

$scope.buscar_titulo = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_titulo_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};


$scope.buscar_autor = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_autor_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};



$scope.buscar_jurado = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_jurado_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};


$scope.buscar_tutor_academico = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_tutor_academico_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};




$scope.buscar_cota = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_cota_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};


$scope.buscar_materia= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_materia_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};

$scope.buscar_palabra_clave= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_palabra_clave_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_tipo_de_trabajo= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_tipo_de_trabajo_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};



$scope.buscar_volumen= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_volumen_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_edicion = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_edicion_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_idioma = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_idioma_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_isbn = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_isbn_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_issn = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_issn_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_editorial = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_editorial_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
}; 




$scope.loadingStates = function(val,select_model_value){
 // $timeout(funcion_buscar, 3000);
      //  $scope.consultar_buscador_simple(val);


 //   return $scope.buscar_titulo(val);

    if(select_model_value == 'titulo'){
        return $scope.buscar_titulo(val);
    }else{
        if(select_model_value == 'autor'){
            return $scope.buscar_autor(val);           
        }else{
            if(select_model_value == 'cota'){
                return $scope.buscar_cota(val);                   
            }else{
                if(select_model_value == 'jurado'){
                    return $scope.buscar_jurado(val);   
                }else{
                    if(select_model_value == 'tutor_academico'){
                        return $scope.buscar_tutor_academico(val);                         
                    }else{
                        if(select_model_value == 'materia'){
                            return $scope.buscar_materia(val);
                        }else{
                            if(select_model_value == 'palabra_clave'){
                                return $scope.buscar_palabra_clave(val);
                            }else{
                                if(select_model_value == 'tipo_de_trabajo'){
                                    return $scope.buscar_tipo_de_trabajo(val);
                                }else{
                                    if(select_model_value == 'volumen'){
                                        return $scope.buscar_volumen(val);                                        
                                    }else{
                                        if(select_model_value == 'edicion'){
                                            return $scope.buscar_edicion(val);
                                        }else{
                                            if(select_model_value == 'idioma'){
                                                return $scope.buscar_idioma(val);
                                            }else{
                                                if(select_model_value == 'isbn'){
                                                    return $scope.buscar_isbn(val);
                                                }else{
                                                    if(select_model_value == 'issn'){
                                                        return $scope.buscar_issn(val);
                                                    }else{
                                                        if(select_model_value == 'editorial'){
                                                            return $scope.buscar_editorial(val);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

 //   return $scope.funcion_buscar_titulo(val);
};


$scope.colocar_typeahead = function(index,select_model_value){
 // alert(index+" "+select_model.value);

  if(select_model_value == "titulo"){
   //   $scope.inputs[index].opt1.function_typeahead = "state for state in loading_titulo_States($viewValue) | limitTo:5  | filter:$viewValue";
  }

}
    

   $scope.getService =  function(route_peticion){
   
     $scope.respuesta_buscador_simple = [];
     $http.post(route_peticion , $scope.mensaje_buscador_avanzado).
            success(function(data, status, headers, config) {

              $scope.id_recursos = data.data;
              $scope.recursos = [];
              $scope.recursos = [];
              $scope.currentPage = data.current_page;
              $scope.totalItems = data.total;
              $scope.numPages = data.last_page;

        $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
            success(function(data, status, headers, config) {
            $scope.recursos = data;
            

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });




            }).
                   
            error(function(data, status, headers, config) {

            });
   }



    $scope.pageChanged = function(currentPage){
        $scope.currentPage = currentPage;
        $scope.bool_acciones = false;
  //    $log.log('Page changed to: ' + $scope.currentPage);
      $scope.getService(API.url+'bagtesis/backend/public/buscadores/buscador_avanzado?page='+$scope.currentPage+'&num_items_pagina='+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value);

     // $scope.watchPage = newPage;
    }



$scope.getServiceRecursoAutores = function (recurso_id, index_recurso){
   // alert(recurso_id+" "+index_recurso+" "+index_titulo);    

    $http.get(API.url+'bagtesis/backend/public/recursos/'+recurso_id+'/autores').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
      $scope.autores = data;

      $scope.recursos[index_recurso]['autores'] = $scope.autores;
  // alert(recurso_id+" "+index_recurso+" "+index_titulo);

   // $scope.page.data[index_titulo]['recursos'][index_recurso]['autores'] = $scope.autores;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
 //   $scope.autores = data;
    
  });
}


$scope.getServiceRecursoDescriptores = function(recurso_id,index_recurso){
 
    $http.get(API.url+'bagtesis/backend/public/recursos/'+recurso_id+'/descriptores').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
   
    $scope.descriptores = data;
  //  alert($scope.descriptores);
  // alert(recurso_id+" "+index_recurso+" "+index_titulo);
    $scope.recursos[index_recurso]['descriptores'] = $scope.descriptores;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
   // $scope.autores = data;
    
  });
}


  $scope.add_correo = function(){
  //  alert($scope.index_titulo);
    $scope.correos.push({email: '',name:''});
    $scope.bool_correo_exitoso = false;

  }

  $scope.desplegar_correo = function(){
       // alert("desplegar correo");

       $scope.div_correo = {'display': 'inline-block'};
       $scope.bool_correo_exitoso = false;
  }

  $scope.desplegar_2 = function (){
     $scope.div_correo = {'display':'none'};
 //    $scope.collapse_correo = {};
     // $scope.collapse_correo = "";
    //  $scope.collapse_correo = "collapse on";
  }


  $scope.repuesta;
  $scope.enviar_correo = function (){



     $scope.mensaje_email['correos'] = $scope.correos;
    $scope.mensaje_email['id_recursos'] = [$recursos[$scope.index_recurso]['id']]; 

  /*  $scope.iter = 0;
     angular.forEach($scope.recursos, function(value, key) {
         if(value.model_checkbox==true){
             this.push(value.id);
          }       
        }, $scope.mensaje_email['id_recursos']);

  */
    $http.post(API.url+'bagtesis/backend/public/recursos/mail', $scope.mensaje_email).
          success(function(data, status, headers, config) {
                $scope.respuesta = data;
            //    $scope.carga['visibility'] = 'hidden';
             //   $scope.carga_respuesta['visibility']= 'visible';
                alert($scope.respuesta);
            // this callback will be called asynchronously
            // when the response is available
            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });


 /*     $scope.carga['visibility'] = 'visible';
      $scope.carga_respuesta['visibility']= 'hidden';
   //   alert('Enviar Correo');
       $scope.mensaje_email['correos'] = $scope.correos;

   //   $scope.mensaje_email['datos'] = $scope.page[$scope.index_titulo]['recursos'][$scope.index_recurso];
     // $scope.hola=[$scope.page.data[$scope.index_titulo]['recursos'][$scope.index_recurso]];
        $scope.mensaje_email['datos'] = [$scope.recursos[$scope.index_recurso]];
  
        $http.post(API.url+'bagtesis/backend/public/recursos/mail', $scope.mensaje_email).
        success(function(data, status, headers, config) {
            $scope.respuesta = data;
            $scope.carga['visibility'] = 'hidden';
            $scope.carga_respuesta['visibility']= 'visible';
        // this callback will be called asynchronously
        // when the response is available
        }).
        error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        });
*/
  }




  $scope.generar_ids_recursos_seleccionados_ = function (){
     $scope.ids_recursos = "";
     $scope.array_ids_recursos = [];
     $scope.rutas_ids_recursos_seleccionados = [];
     $scope.datos_qr = [];
     angular.forEach($scope.recursos, function(value, key) {
         if(value.model_checkbox==true){

           this.push(value.id);


          }    
        },$scope.array_ids_recursos); 


    for($scope.i=0; $scope.i<$scope.array_ids_recursos.length;$scope.i++){
          $scope.ids_recursos = $scope.ids_recursos+$scope.array_ids_recursos[$scope.i];
          $scope.rutas_ids_recursos_seleccionados.push("http://192.168.10.35/bagtesis/backend/public/recursos/"+$scope.array_ids_recursos[$scope.i]+"/pdf");
          if($scope.i != $scope.array_ids_recursos.length-1){
              $scope.ids_recursos = $scope.ids_recursos+" ";
          }
      }


   //    alert($scope.ids_recursos_pdf);
  }


    $scope.generar_ids_recursos_seleccionados = function (){

     $scope.ids_recursos = "";
     $scope.array_ids_recursos = [];
     $scope.rutas_ids_recursos_seleccionados = [];
     $scope.datos_qr = [];
     $scope.iter = 0;
     angular.forEach($scope.recursos, function(value, key) {
        if(value.model_checkbox){
          $scope.ids_recursos = $scope.ids_recursos+value.id;
          $scope.ruta_qr = API.url+"bagtesis/backend/public/recursos/"+value.id+"/pdf";
          $scope.cota    = value.ubicacion;
          $scope.titulo  = value.titulos;
          $scope.ids_recursos = $scope.ids_recursos+" ";
          $scope.datos = {   
                            ruta_qr: $scope.ruta_qr,
                            titulo:  $scope.titulo,
                            cota:    $scope.cota
                         };
          $scope.datos_qr.push($scope.datos);
        }
        $scope.iter++;    
        }); 

      $scope.ids_recursos = $scope.ids_recursos.substring(0,$scope.ids_recursos.length-1);
      $scope.ruta_ids_recursos = API.url+"bagtesis/backend/public/recursos/"+$scope.ids_recursos+"/pdf";



 /*  for($scope.i=0; $scope.i<$scope.array_ids_recursos.length;$scope.i++){
          $scope.ids_recursos = $scope.ids_recursos+$scope.array_ids_recursos[$scope.i];
          $scope.rutas_ids_recursos_seleccionados.push("http://192.168.10.35/bagtesis/backend/public/recursos/"+$scope.array_ids_recursos[$scope.i]+"/pdf");
          if($scope.i != $scope.array_ids_recursos.length-1){
              $scope.ids_recursos = $scope.ids_recursos+" ";
          }
      }
*/

   //    alert($scope.ids_recursos_pdf);
  };



  $scope.desplegar_recurso = function (recurso_id,index_recurso){
      $scope.bool_correo_exitoso = false;
      $scope.bool_correo_error = false;
      $scope.recursos[index_recurso].model_checkbox = true;
      $scope.bool_acciones = true;
      $scope.bool_disable_acciones = false; 
      $scope.mensaje_email = {};
      $scope.correos = [
                          {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                      ];
      $scope.generar_ids_recursos_seleccionados();

      $scope.bool_button = false;
      $scope.div_correo_modal.show = false;
      $scope.index_recurso = index_recurso;
      $scope.div_qr.show = false;
      $scope.ruta_index_recurso = API.url+"bagtesis/backend/public/recursos/"+$scope.recursos[$scope.index_recurso].id+"/pdf";
      $scope.getServiceRecursoAutores(recurso_id,index_recurso);
      $scope.getServiceRecursoDescriptores(recurso_id,index_recurso);

  };


  $scope.validarEmail = function( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ){
        return true;
    }
    return false;   
};


$scope.validar_correos = function (){
    $scope.valor = false;
    $scope.correos.forEach(function(value, key) {
       $scope.validar = $scope.validarEmail(value.email);
         if($scope.validar){
            $scope.valor = true;
         }
      // alert(value.email);
    });
    return $scope.valor; 
};





  $scope.enviar_recursos_seleccionados = function(){    /* ------------------- */

   //   alert('enviar recursos seleccionados');

  /*    $scope.getServiceRecursoAutores(recurso_id,index_recurso);
   $scope.getServiceRecursoDescriptores(recurso_id,index_recurso);
    */

    $scope.v = $scope.validar_correos();

    if($scope.v == true){
          $scope.bool_correo_error = true;
          $scope.bool_correo_exitoso = false;
    }else{      

      $scope.mensaje_email = {};
      $scope.mensaje_email['correos'] = $scope.correos;
      $scope.mensaje_email['id_recursos'] = []; 
      $scope.loading_correo = true;
      $scope.bool_disable = true;
      $scope.bool_correo_error = false;

    $scope.iter = 0;
     angular.forEach($scope.recursos, function(value, key) {
         if(value.model_checkbox==true){
             this.push(value.id);
          }       
        }, $scope.mensaje_email['id_recursos']);


    $http.post(API.url+'bagtesis/backend/public/recursos/mail', $scope.mensaje_email).
          success(function(data, status, headers, config) {
                $scope.respuesta = data;
                $scope.loading_correo = false;
                $scope.bool_correo_exitoso = true;
                $scope.bool_disable = false;
            //    $scope.carga['visibility'] = 'hidden';
             //   $scope.carga_respuesta['visibility']= 'visible';
            // this callback will be called asynchronously
            // when the response is available
            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }
  }




  $scope.bool_button = false;

  $scope.div_correo = function() {

        $scope.bool_button = true;


      $scope.correos = [
                          {email: '', name:''}
                      ];

      if($rootScope.userLogged){
              $scope.correos = [
                                  {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                                ];

      }

      $scope.mensaje_email = {};
      $scope.div_qr.show = false;
      $scope.div_pdf.show = false;

      $scope.div_correo.show = !$scope.div_correo.show;     
  
  };



  $scope.verficar_usuario = function(){

      $scope.bool_correo_exitoso = false;
      $scope.bool_correo_error = false;
      $scope.mensaje_email = {};
      $scope.correos = [
                          {email: '', name:''}
                      ];

      if($rootScope.userLogged){
              $scope.correos = [
                                  {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                                ];

      }
  }


  $scope.verficar_recursos_seleccionados = function(){ 

    for($scope.i=0;$scope.i< $scope.recursos.length; $scope.i++){
          if($scope.recursos[$scope.i].model_checkbox == true){
              return true;
          }
    }
    return false;
  }


  $scope.recurso_seleccionado = function (){ 
    if($scope.verficar_recursos_seleccionados()){
        $scope.generar_ids_recursos_seleccionados();
        $scope.bool_acciones = true;
          $scope.bool_disable_acciones = false;
    }else{
        $scope.bool_disable_acciones = true;
      //  $scope.bool_acciones = false;
    }
  }



  $scope.div_correo_modal = function() {
      $scope.bool_correo_exitoso = false;
      $scope.bool_preferencia_exitoso = false;
      $scope.bool_button = false;


      $scope.correos = [
                          {email: '', name:''}
                      ];

      if($rootScope.userLogged){
              $scope.correos = [
                                  {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                                ];

      }

      $scope.mensaje_email = {};
      $scope.div_qr.show = false;
      $scope.div_pdf.show = false;
      $scope.div_correo_modal.show = !$scope.div_correo_modal.show;     
  
  };



  $scope.div_pdf = {};
  $scope.div_pdf.show = false;
  $scope.div_pdf = function() {
      $scope.bool_button = false;
      $scope.bool_loged = false;
      $scope.div_qr.show = false;
      $scope.div_correo_modal.show = false;
      $scope.div_pdf.show = !$scope.div_pdf.show;
  };

  $scope.div_qr = {}
  $scope.div_qr.show = false;
  $scope.div_qr = function() {
      $scope.bool_preferencia_exitoso = false;
      $scope.bool_button = false;
      $scope.bool_loged = false;
      $scope.div_correo_modal.show = false;
      $scope.div_pdf.show = false;
      $scope.bool_correo_exitoso = false;
      $scope.div_qr.show = !$scope.div_qr.show;
  };

/*
  $scope.delete = function(index){
      $scope.correos.splice(index,1);
  }
*/



  $scope.cambiar_color_hover = function (){
      $scope.color_hover.color = "#d9edf7";
  }


  $scope.descargar_pdf = function(){
      alert('descarga pdf');
    $http.get(API.url+'bagtesis/backend/public/recursos/T043500020532/pdf').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the respon se is available
    alert('Exitos!!');
       window.open(data);

    //  $scope.recursos.push(data);
    //  $scope.recursos[$scope.i]['model_checkbox'] = false;
    //  $scope.i++;

  // alert(recurso_id+" "+index_recurso+" "+index_titulo);
 //   $scope.page.data[index_titulo]['recursos'][index_recurso]['titulos'] = $scope.titulos;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
  //  $scope.titulos = data;
    
  });


  }

  $scope.seleccionar_todos = function (){
      $scope.bool_seleccionar_todos = !$scope.bool_seleccionar_todos;

      if($scope.bool_seleccionar_todos == true){
          $scope.bool_acciones = true; 
          $scope.bool_disable_acciones = false;
          $scope.class_check_seleccionar_todos = "btn btn-success";
          for ($scope.i =0 ; $scope.i< $scope.recursos.length; $scope.i++) {
              $scope.recursos[$scope.i].model_checkbox = true;
          };
      }else{
          $scope.class_check_seleccionar_todos = "btn btn-default";
          for ($scope.i =0 ; $scope.i< $scope.recursos.length; $scope.i++) {
              $scope.recursos[$scope.i]['model_checkbox'] = false;
          };
         $scope.bool_disable_acciones = true;
      //    $scope.bool_acciones = false;
      }
  }

$scope.pref_data = {};

$scope.preferencial = function(){
  console.log('preferencial called()');
  $scope.loading_acciones = true;
  $scope.bool_preferencia_exitoso = false;
  $scope.bool_correo_exitoso = false;
  $scope.bool_correo_error = false;
  $scope.div_qr.show = false;
  $scope.div_correo_modal.show = false;
  $scope.pref_data.usuario_id = $cookieStore.get('user_id');
  $scope.pref_data.recurso_id = $scope.recursos[$scope.index_recurso].id;
  $http.defaults.headers.common['X-Auth-Token'] = $cookieStore.get('auth_token')+'_id_'+$cookieStore.get('user_id');
  $http.post(API.url+'bagtesis/backend/public/preferencias',$scope.pref_data).
  success(function(data, status, headers, config) {
  console.log(data);
  $scope.loading_acciones = false;
  $scope.bool_preferencia_exitoso = true;
  $scope.pref_data = {};
      
 // $scope.get_pref
  });


 };



});
