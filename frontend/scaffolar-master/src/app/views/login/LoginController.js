
angular.module('app')
	.controller('LoginController', function($rootScope, $scope, $location, $http,$cookies,$cookieStore, AuthService, API) { 
		
		$rootScope.user = {};
		$rootScope.user.username = '';
		$rootScope.user.password = '';
		$scope.answer = [];
		$scope.recover = {};
		$scope.recover.info = '';
		$scope.recover.email = '';

		if(angular.isUndefined($rootScope.userWelcome))
			$rootScope.userWelcome = '';

	
		
		
		$scope.submit = function(){

			console.log('login submit() called');

			AuthService.login($rootScope.user, function(response){

				$rootScope.userWelcome = 'Bienvenido '+$cookieStore.get('username');

    			//console.log('success:'+$cookieStore.get('user_id'));

			}, function(response){});
	      
	  		
	    };

	    $scope.recoverpass = function(){
/*
		  	console.log('recoverpass() called');

		  	$scope.recover.info = 'Le sera enviada una nueva contraseña a su correo electrónico.';
	      	
	  		$http.post(API.url+'bagtesis/backend/public/login',$rootScope.user).
		  	success(function(data, status, headers, config) {
			    // this callback will be called asynchronously
			    // when the response is available
			    console.log('success');
			    $scope.answer = data;
		  	}).
		  	error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			    console.log('error');
			    $scope.answer = data;
		  	});*/
	    };

	    $scope.restoreMessages = function(){

		  	$scope.recover.info = '';
			$scope.recover.email = '';

	    };
		
	}
);