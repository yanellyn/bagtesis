
angular.module('app')
	.controller('LogoutController', function($rootScope, $scope, $location, $http, $cookies, AuthService) { 
		

		$scope.answer = [];
		$scope.recover = {};
		$scope.recover.info = '';
		$scope.recover.email = '';

		console.log('logout() called');
		
  		AuthService.logout(function(response){

		    $rootScope.userWelcome = '';		    

			//console.log('success:'+$cookieStore.get('user_id'));

		}, function(response){});


		$location.path("/");
	}
);