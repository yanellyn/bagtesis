angular.module('app' )
  .controller('volumenesController', function($scope, $window, $http, API, $cookieStore, upload, $state) {
    $scope.idrecurso="";
    $scope.tipo_recurso = $cookieStore.get('tipo_recurso');
    $scope.tipo_recurso_ab =  ($scope.tipo_recurso.substring(0, 1)).toUpperCase();
    if( $scope.tipo_recurso_ab == 'O')
        $scope.tipo_recurso_ab = $cookieStore.get('tipo_recurso_id'); // validacion  agregada para el modulo de otros recursos
    if ($scope.tipo_recurso == "monografia"){
        $scope.nombre_recurso =  "Monografías";
        $scope.idrecurso = $cookieStore.get('id_recurso_libro');
        $scope.redirect = "libros";
    }else if ($scope.tipo_recurso == "tesis"){
        $scope.nombre_recurso =  "Trabajos Académicos";
        $scope.idrecurso = $cookieStore.get('id_recurso_tesis');
        $scope.redirect = "trabajosAcademicos";
    }else if ($scope.tipo_recurso == "serie"){
        $scope.idrecurso = $cookieStore.get('id_recurso_publicacion');
        $scope.nombre_recurso =  "Publicaciones Seriadas"; 
        $scope.redirect = "publicacionesSeriadas";    
    }else if ($scope.tipo_recurso == "otros"){
        $scope.idrecurso = $cookieStore.get('id_recurso_otros');
        $scope.nombre_recurso =  "Otros Recursos"; 
        $scope.redirect = "otrosRecursos";    
    }
    if(!$scope.idrecurso){
         return $state.go('home', {}, { reload: true });
    }
	 $('#loader').modal('show');

	$scope.nombreTitulo = "";
	$scope.titulosMostrar = "";
	$scope.loadingTitulos = false;
	$scope.tipoTitulos = "";
	$scope.botonAgregarTitulo = false;
	$scope.msjConfirmacion = "";
	$scope.msjInformacion = "";
	$scope.esAgregarTitulo = "";
	$scope.tituloRecurso = {};
    

    $scope.nroVolumenes ="";
    $scope.volumenes = [];
    $scope.volumenesOriginales = [];
    $scope.bool_add_button = true;
    $scope.tituloMostrar = "";

    $scope.obtenerTituloPrincipal = function(){
        $http.get(API.url + 'bagtesis/backend/public/recurso_titulos_principal/'+$scope.idrecurso)
        .success(function(data, status, headers, config) {           
            $('#loader').modal('hide');
                if(data != "" && data != null){
                    $scope.tituloMostrar = data;
                }else{    
                    $scope.tituloMostrar = "";
                }
        })
        .error(function(error) {
            $('#modalNroVolumenes').modal('hide'); 
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });
    }
    $scope.obtenerVolumenes = function(){
        //$('#loader').modal('show');
        $http.get(API.url + 'bagtesis/backend/public/volumenes/'+$scope.idrecurso)
            .success(function(data, status, headers, config) {           
                if(data["codigo"] == "0"){
                    $scope.volumenesOriginales = angular.copy(data["volumenes"]);
                    $scope.volumenes = data["volumenes"];
                    $scope.obtenerTituloPrincipal();
                }else{
                    $scope.mostrarPopupVolumenes();
                    $scope.obtenerTituloPrincipal();
                }
            })
            .error(function(error) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });
    }
	
    $scope.obtenerVolumenes();

    $scope.mostrarPopupVolumenes = function(){
        $('#loader').modal('hide');
        $('#modalNroVolumenes').modal('show'); 
    }

    $scope.validarInputNroVolumenes = function(){
        $scope.nroVolumenes  = $scope.nroVolumenes.replace(/\D/g,'');
    }

    $scope.aceptarInputNroVolumenes = function(){
        if ($scope.nroVolumenes == ""){
                $scope.nroVolumenes = 1;
        } 
        $('#modalNroVolumenes').modal('hide');
        for (i = 0; i < $scope.nroVolumenes; i++) { 
            $scope.volumenes.push({id:'', volumen: i+1, subtitulo: '', isbn: '', ano: ''});
            $scope.volumenesOriginales.push({id:'', volumen: i+1, subtitulo: '', isbn: '', ano: ''});
        }

    }

    $scope.limpiarVolumenes = function(){
        $('#confirmacion').modal('hide');
        $('#loader').modal('show');
        $scope.volumenes = $scope.volumenesOriginales;
        $('#loader').modal('hide');
        $('#informacion').modal('show');
    }


    $scope.newOption = function(){
        $scope.volumenes.push({ 
             id:"", volumen: "", subtitulo: "", isbn: "", ano: ""
        });
    };

    $scope.add_option = function(){
        $scope.newOption();
    };

    $scope.delete = function(index){
        if($scope.volumenes.length!=1){
            $scope.volumenes.splice(index,1);
        }
    }

    $scope.submitFormluarioVolumenes = function(){
        $('#confirmacionSub').modal('show');
    }

    $scope.guardarVolumenes = function(){
        $('#confirmacionSub').modal('hide');
        $('#loader').modal('show');
        $scope.recursoVolumenes = {};
        $scope.recursoVolumenes['volumenes'] = $scope.volumenes;
        $scope.recursoVolumenes['idrecurso'] = $scope.idrecurso;

        $http.post(API.url+'bagtesis/backend/public/guardar_volumen/'+$scope.idrecurso, $scope.recursoVolumenes)
            .success(function(data, status, headers, config) {
                $('#loader').modal('hide');
                if(data["codigo"] == "0"){
                    $scope.msjInformacion = 'Volúmenes modificados exitosamente' ;
                    $('#informacion').modal('show');
                    $scope.obtenerVolumenes();
                }

            }).error(function(error) {
                 $('#loader').modal('hide');
                 $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });
    }

    $scope.validarInput = function(value){
        if(value)
            return value.replace(/\D/g,'');
        else 
            return '';
    }





});