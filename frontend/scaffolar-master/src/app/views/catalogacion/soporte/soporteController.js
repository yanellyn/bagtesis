angular.module('app' )
  .controller('soporteController', function($scope, $window, $http, API, $cookieStore, paginationConfig, $state) {
    $scope.idrecurso="";
    $scope.tipo_recurso = $cookieStore.get('tipo_recurso');
    $scope.tipo_recurso_ab =  ($scope.tipo_recurso.substring(0, 1)).toUpperCase();
    if( $scope.tipo_recurso_ab == 'O')
        $scope.tipo_recurso_ab = $cookieStore.get('tipo_recurso_id'); // validacion  agregada para el modulo de otros recursos
    if ($scope.tipo_recurso == "monografia"){
        $scope.nombre_recurso =  "Monografías";
        $scope.idrecurso = $cookieStore.get('id_recurso_libro');
        $scope.redirect = "libros";
    }else if ($scope.tipo_recurso == "tesis"){
        $scope.nombre_recurso =  "Trabajos Académicos";
        $scope.idrecurso = $cookieStore.get('id_recurso_tesis');
        $scope.redirect = "trabajosAcademicos";
    }else if ($scope.tipo_recurso == "serie"){
        $scope.idrecurso = $cookieStore.get('id_recurso_publicacion');
        $scope.nombre_recurso =  "Publicaciones Seriadas"; 
        $scope.redirect = "publicacionesSeriadas";    
    }else if ($scope.tipo_recurso == "otros"){
        $scope.idrecurso = $cookieStore.get('id_recurso_otros');
        $scope.nombre_recurso =  "Otros Recursos"; 
        $scope.redirect = "otrosRecursos";    
    }
    if(!$scope.idrecurso){
         return $state.go('home', {}, { reload: true });
    }
 	$('#loader').modal('show');

	$scope.nombreSoporte = "";
      $scope.loadingSoporte = false;
	$scope.esAgregarSoporte = true;
	$scope.sopSistemaMostrar = "";
	$scope.sopSisSel = -1;
	$scope.sopSel = -1;
	$scope.esDelRecurso = false;
	$scope.esDelSistema = false;
	$scope.sopSistSelec = "";
	$scope.soportesMostrar = [];
	$scope.esDelSistema = "";
	$scope.bool_eliminar_derecurso = 0;
	$scope.bool_agregar_derecurso = 0;

	//PAGINADO DESCRIPTORES
	$scope.bool_table_recursos = false; 
    $scope.currentPage = 1;
    $scope.bool_seleccionar_todos = false;       
    $scope.num_items_pagina  = 15;         
    $scope.datos_qr = [];
    /*pagination config*/

    paginationConfig.firstText='Primera'; 
    paginationConfig.previousText='< Anterior';
    paginationConfig.nextText='Siguiente >';
    paginationConfig.lastText='Última';
    paginationConfig.boundaryLinks=false;
    paginationConfig.rotate=false;
    $scope.maxSize = 15;
    paginationConfig.itemsPerPage = $scope.num_items_pagina;


    $scope.obtener_soportes = function(mostrarInfromacion){

	    // Obtener Soportes del recurso
		$http.get(API.url + 'bagtesis/backend/public/obtener_inf_descriptiva_recurso/'+$scope.idrecurso).success(function(data, status, headers, config) {           
	        $scope.soportesMostrar = data

            // Obtener Soportes del sistema
            $http.get(API.url + 'bagtesis/backend/public/obtener_inf_descriptiva_sistema').success(function(data, status, headers, config) {           
                $scope.sopSistemaMostrar = data.data;
                $scope.currentPageSop = data.current_page;
                $scope.totalItemsSop = data.total;
                $scope.numPagesSop = data.last_page;
                $('#loader').modal('hide');
                if (mostrarInfromacion){
                    $('#informacion').modal('show');
                }
            }).
            error(function(error) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });  

	    }).
	    error(function(error) {
	        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');

	    });  

    }
	
	$scope.obtener_soportes();


    $scope.pageChangedSop = function(current_page){

		$http.get(API.url + 'bagtesis/backend/public/obtener_inf_descriptiva_sistema?page=' + current_page).success(function(data, status, headers, config) {           
            $scope.sopSistemaMostrar = data.data; 
	        $scope.currentPageSop = data.current_page;
	        $scope.totalItemsSop = data.total;
	        $scope.numPagesSop = data.last_page;
            }).
            error(function(error) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });  
	}


	$scope.seleccionarSopSis = function(soporte, indice){
	    $scope.sopSisSel = indice;
	    $scope.sopSistSelec = soporte;
	    $scope.sopSel = -1;
	    $scope.esDelRecurso = false;
	    $scope.esAgregarSoporte = false;
	    if(!$scope.validarExistenciaSop($scope.sopSistSelec.id)){
	     	$scope.esDelSistema = true;
	    }else{
	    	$scope.esDelSistema = false;
	    }
	    
	    $scope.nombreSoporte = soporte.soporte_salida;
		$scope.tipo = soporte.tipo;
	    $scope.idSoporte = soporte.id;

	};

	$scope.validarExistenciaSop = function(id){
	     array = $scope.soportesMostrar.filter(function(el) {
	        return el.id == id;
	     });
	    if(array.length > 0){
	    	console.log("Si existe en los recursos del documento");
	        return true;
	    }else{
	    	console.log("No existe en los recursos del documento");
	    } return false;
	}

	$scope.seleccionarSoporte = function(soporte, indice){
	    $scope.sopSel = indice;
	    $scope.sopSelec = soporte;
	    $scope.sopSisSel = -1;
	    $scope.esDelRecurso = true;
	    $scope.esDelSistema = false;
	    $scope.esAgregarSoporte = false;
	    
	    $scope.nombreSoporte = soporte.soporte_salida;
		$scope.tipo = soporte.tipo;
	    $scope.idSoporte = soporte.id;

	};

	$scope.agregarSoporteAlDocumento = function(){
	    if(!$scope.validarExistenciaSop($scope.sopSistSelec.id)){
	            $scope.soportesMostrar.push($scope.sopSistSelec);
	            $scope.esDelSistema = false;
	            $scope.bool_agregar_derecurso = 1;
	            $scope.agregar_soporte();
	    }
	}

	$scope.eliminarSoporteDelDocumento = function(){
            $scope.soportesMostrar = $scope.eliminarObjetoArray($scope.soportesMostrar, $scope.sopSelec.id);
            $scope.bool_eliminar_derecurso = 1;
        	$scope.eliminar_soporte();
	}

	$scope.eliminarObjetoArray = function(array, item){   
	    arrayRemoved = array.filter(function(el) {
	        return el.id !== item;
	    });
	    return arrayRemoved;
	}


	$scope.limpiar_todo = function(){
		$('#confirmacion').modal('hide');
    	$('#loader').modal('show');
    	$scope.limpiar_soporte();
       	$scope.obtener_soportes();
	}

	$scope.limpiar_soporte = function(){
		$scope.nombreSoporte = "";
		$scope.esAgregarSoporte = true;
		$scope.sopSisSel = -1;
		$scope.sopSel = -1;
		$scope.esDelRecurso = false;
		$scope.esDelSistema = false;
		$scope.sopSistSelec = "";
		$scope.soportesMostrar = [];
		$scope.esDelSistema = "";
	}



	$scope.submitFormluarioSoporte=function(){
         $('#confirmacionSub').modal('show');
    }

    $scope.agregar_soporte = function(){
    	$('#confirmacionSub').modal('hide');
    	$('#loader').modal('show');
    	$scope.dataSoporte = {};
        $scope.dataSoporte["id_recurso"] = $scope.idrecurso;
        $scope.dataSoporte["nombre_soporte"] = $scope.nombreSoporte;
        $scope.dataSoporte["idCatalogador"] = $cookieStore.get('user_id');

        $http.post(API.url + 'bagtesis/backend/public/agregar_soporte', $scope.dataSoporte).success(function(data, status, headers, config) {           
	        
          	if( $scope.bool_agregar_derecurso == 1 ){
                $('#loader').modal('hide');
          		$scope.bool_agregar_derecurso = 0;
          	}else if(data["codigo"] == "0"){  
                $scope.limpiar_soporte();
                $scope.msjInformacion = 'Soporte agregado exitosamente';
                $scope.obtener_soportes(true);
                 
            }else if(data["codigo"] == "1"){
                $('#loader').modal('hide');
                $scope.msjInformacion = 'El soporte ya existe';
            	$('#informacion').modal('show');  
            }
	         
	    }).
	    error(function(error) {
	        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
	    });

    }

    $scope.guardar_soporte = function(){
    	$('#confirmacionSub').modal('hide');
    	$('#loader').modal('show');
    	$scope.dataSoporte = {};
        $scope.dataSoporte["id_recurso"] = $scope.idrecurso;
        $scope.dataSoporte["nombre_soporte"] = $scope.nombreSoporte;
        $scope.dataSoporte["id_descriptor"] =  $scope.idSoporte;
        $scope.dataSoporte["descriptores"] = $scope.soportesMostrar;

        $scope.dataSoporte["idCatalogador"] = $cookieStore.get('user_id');

        $http.post(API.url + 'bagtesis/backend/public/guardar_soporte', $scope.dataSoporte).success(function(data, status, headers, config) {           
	        if(data["codigo"] == "0"){  
	        	//$scope.limpiar_soporte();
                $scope.msjInformacion = 'Soporte modificado exitosamente';
                $scope.obtener_soportes(true);     
            }else{
            	$scope.limpiar_soporte();
            	$scope.obtener_soportes(true);
            }

	    }).
	    error(function(error) {
	        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
	    });

    }



    $scope.eliminar_soporte = function(){ 
        $('#confirmacionEliminar').modal('hide');
        $('#loader').modal('show');
        $scope.dataSoporte = {};
        $scope.dataSoporte["id_recurso"] = $scope.idrecurso;
        $scope.dataSoporte["id_descriptor"] = $scope.idSoporte;
        $scope.dataSoporte["bool_eliminar_derecurso"] = $scope.bool_eliminar_derecurso;
        $scope.dataSoporte["idCatalogador"] = $cookieStore.get('user_id');
        


        $http.post(API.url+'bagtesis/backend/public/eliminar_soporte', $scope.dataSoporte)
            .success(function(data, status, headers, config) { 
            	 
          		if( $scope.bool_eliminar_derecurso == 1 ){
                    $scope.limpiar_soporte();
                    $('#loader').modal('hide');
          			$scope.bool_eliminar_derecurso = 0;
          		}else if(data["codigo"] == "0"){
                    $scope.limpiar_soporte();
                    $scope.msjInformacion = 'Soporte eliminado exitosamente' ;
                    $scope.obtener_soportes(true);

                }else if(data["codigo"] == "1"){
                	$scope.msjInformacion = 'El soporte no se puede eliminar debido a que se encuentra a asociado a otros recursos' ;
                    $('#loader').modal('hide');
                    $('#informacion').modal('show');
                   
                }

            }).error(function(error) {
            	$('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });
    }

    $scope.loadingStates   = function(val){
      console.log("val "+val);
      $scope.loadingSoporte = true;
      return $scope.buscarSoporte(val); 
  	};

  $scope.buscarSoporte = function (val){
      $scope.mensaje_buscador_simple = {};
      $scope.mensaje_buscador_simple['data'] = val;

      $scope.promise = $http.post(API.url+'bagtesis/backend/public/autocompletado_soporte', $scope.mensaje_buscador_simple)
                    .success(function(data, status, headers, config) {
                        console.log(JSON.stringify(data));
                        $scope.prom = data;
                        $scope.loadingSoporte = false;
                    }).then(function(){
                       
                        $scope.aux_promise = $scope.prom;  
                        return $scope.prom;
                    });
      
      return $scope.promise;
  };

  $scope.auxTrim = "";

    $scope.trimRequired = function(model) {
        
        if (model == "nombreSoporte" ){
            if($scope.nombreSoporte){
                $scope.auxTrim = $scope.nombreSoporte.trim();
            }else{
                $scope.auxTrim = "";
            }
        }
    }

    $scope.blurRequired = function (model) {
        if (model == "nombreSoporte"){
            $scope.nombreSoporte = $scope.auxTrim;
        }

        $scope.auxTrim = "";
      
    }


});