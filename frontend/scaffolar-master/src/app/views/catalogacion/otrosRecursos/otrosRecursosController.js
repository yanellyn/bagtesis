angular.module('app' )
  .controller('otrosRecursosController', function($rootScope, $scope, $window, $http, API, $cookieStore, $state, paginationConfig, upload) {
        $scope.idrecurso = $cookieStore.get('id_recurso_otros');
         $scope.ruta_qr = API.url+"bagtesis/backend/public/recursos/"+$scope.idrecurso+"/pdf";
        $cookieStore.put('tipo_recurso', 'otros');
        $cookieStore.put('tipo_recurso_id', $scope.tipo_doc_seleccionado);
        $rootScope.tipo_recurso = $cookieStore.get('tipo_recurso');
        $scope.isBusquedaSimple = "";
        $scope.idTitulo = "";
        $scope.idAutor = "";
        $scope.coleccion = 'coleccion';
        $scope.isColeccion = true;
        $scope.msjConfirmacion = "";
        $scope.msjInformacion = "";
        $scope.cotaLibro = "";
        $scope.nombreAutor = "" 
        $scope.apellidoAutor = "";
        $scope.tipoAutor = "";
        $scope.nombreTitulo = "";
        $scope.fechaEdicion = "";
        $scope.url = "";
        $scope.tipoContenido = "";
        $scope.contenido = "";
        $scope.resumen = "";
        $scope.datosAdicionales = "";
        $scope.notas = ""
        $scope.contenidoData = "";
        $scope.recursoAgregado = true;
        $scope.tipoAutor = "";
        $scope.tipoAutor1 = ["Personal", "Conferencia", "Institucional"];
        $scope.selectedBuscadorSimple = "";
        $scope.tipoFechaPrincipal = "fecha";
        $scope.fechaPrincipalActiva = true;
        $scope.anioPrincipal = "";
        $scope.dt_orig = "";
        $scope.dt_orig2 = "";
        $scope.bibliotecaSelect = 1;
        $scope.disciplina = "";
        $scope.esElectronico = 0;
        $scope.doi = "";
        $scope.nombreDocElectronico = "";
        $scope.nombreArchivo = "";
        $scope.archivoCargado = false;
        $scope.idEjemplar = "";
        $scope.tipo_docs = "";
        $scope.tipo_doc_seleccionado = "";
        $scope.nombre_tipodoc = "";
        $scope.loadingCampos = false;
        $scope.desplego = false;
        $scope.tipo_anterior = "";
        $scope.selected = "OTR";
        $scope.options_bibliotecas_seleccionado = {label:'Todas',value:'todas'};
        $scope.options_dependencias_seleccionado = {label:'Todas',value:'todas'};

/************************PRINCIPAL**********************************/

    //bibliotecas


    $scope.obtenerTipoDocs = function(){
        $http.get(API.url+'bagtesis/backend/public/tipo_docs_sinprincipales').
                success(function(data, status, headers, config) {
                    $scope.tipo_docs = data;
                    $scope.tipo_doc_seleccionado = "";
                }).
                error(function(data, status, headers, config) {
                });
    }
   
    $scope.obtenerTipoDocs();

    $http.get(API.url+'bagtesis/backend/public/bibliotecas').
                success(function(data, status, headers, config) {
                    $scope.bibliotecas = data;
                }).
                error(function(data, status, headers, config) {
                   
                }); 

   



/** administracion de tipos de recursos**/




$scope.obtener_campos_tipodoc = function(){

      $http.get(API.url+'bagtesis/backend/public/campos_adicionales?tipo_id='+ $scope.tipo_doc_seleccionado).
            success(function(data, status, headers, config) {
                    $scope.campos_adicionales = data;
                    $scope.mapearCamposAdicionales();
                }).
                error(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                }); 
}

$scope.cambiarTipoRecurso = function(){
    $cookieStore.put('tipo_recurso_id', $scope.tipo_doc_seleccionado);
    $scope.limpiarLibro();
    $scope.obtener_campos_tipodoc();
}

$scope.tipo_doc_actual = "";
$scope.claseTipoDoc = 1;

$scope.submitTipoDocs = function(){
    $('#confirmacionTipoDocs').modal('show');
}


$scope.agregarClaseFilaTipoDoc = function(tipo_doc){
    if($scope.tipo_doc_actual == tipo_doc.id)
        return "seleccionFila";
    else 
        return "";
}

$scope.encenderTipoDoc = function(tipo_doc, indice){
    $scope.tipo_doc_actual = tipo_doc.id;
    $scope.nombre_tipodoc = tipo_doc.descrip_doc_salida;
    $scope.claseTipoDoc = 0;
}

$scope.agregarTipoDoc = function(){
  $('#confirmacionTipoDocs').modal('hide');
    $('#loader').modal('show');
    $scope.tipo_doc_actual = "";
    data = {};
    data["nombre_tipo"] = $scope.nombre_tipodoc; 
    $http.post(API.url+'bagtesis/backend/public/tipo_docs', data).
        success(function(data, status, headers, config) {
            //limpiarInstitucion();
             
            if(data["codigo"] == "0"){
                $http.get(API.url+'bagtesis/backend/public/tipo_docs_sinprincipales').
                success(function(data, status, headers, config) {

                    $scope.tipo_docs = data;
                    $scope.tipo_doc_seleccionado = "";
                    $scope.msjInformacion = 'Tipo de recurso agregado exitosamente';
                    $('#loader').modal('hide');
                     $('#informacion').modal('show');
                     $scope.limpiarTipoDoc();

                }).
                error(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                });
            }else{
                 $scope.msjInformacion = 'El tipo de recurso ya existe';
                    $('#loader').modal('hide');
                    $('#informacion').modal('show');
            }
        }).
        error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');       
        });  
}

$scope.guardarTipoDoc = function(){
  $('#confirmacionTipoDocs').modal('hide');
    $('#loader').modal('show');
    data = {};
    data["nombre_tipo"] = $scope.nombre_tipodoc; 
    $http.put(API.url+'bagtesis/backend/public/tipo_docs/' + $scope.tipo_doc_actual, data).
        success(function(data, status, headers, config) {
            //limpiarInstitucion();
    
            if(data["codigo"] == "0"){
                $scope.claseTipoDoc = 0;
                $http.get(API.url+'bagtesis/backend/public/tipo_docs_sinprincipales').
                success(function(data, status, headers, config) {
                    $scope.tipo_docs = data;
                    $scope.tipo_doc_seleccionado = "";
                    $scope.msjInformacion = 'Tipo de recurso modificado exitosamente';
                    $('#loader').modal('hide');
                    $('#informacion').modal('show');

                }).
                error(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                });
            }else{
                 $scope.msjInformacion = 'Ya existe otro tipo de recurso con el nombre ingresado';
                    $('#loader').modal('hide');
                     $('#informacion').modal('show');
            }
        }).
        error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');   
        });  
}


$scope.eliminarTipoDoc = function(){
  $('#confirmacionTipoDocs').modal('hide');
    $('#loader').modal('show');
    data = {};
    $http.delete(API.url+'bagtesis/backend/public/tipo_docs/' + $scope.tipo_doc_actual).
        success(function(data, status, headers, config) {
            //limpiarInstitucion();
    
            if(data["codigo"] == "0"){
                $scope.claseTipoDoc = 0;
                $http.get(API.url+'bagtesis/backend/public/tipo_docs_sinprincipales').
                success(function(data, status, headers, config) {
                    $scope.tipo_docs = data;
                    $scope.tipo_doc_seleccionado = "";
                    $scope.msjInformacion = 'Tipo de recurso eliminado exitosamente';
                    $('#loader').modal('hide');
                    $('#informacion').modal('show');
                    $scope.limpiarTipoDoc();
                }).
                error(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                });
            }else{
                 $scope.msjInformacion = 'El tipo de recurso no puede ser eliminado debido a que contiene recursos asociados';
                    $('#loader').modal('hide');
                     $('#informacion').modal('show');
            }
        }).
        error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });  
}



$scope.limpiarTipoDoc = function(){
    $("#confirmacionTipoDocs").modal('hide');
    $scope.nombre_tipodoc = "";
    $scope.claseTipoDoc = 1;
    $scope.tipo_doc_actual = "";
}


/** administracion de campos adicionales*/



$scope.campo_adicional_actual = "";
$scope.claseCampoAdicional = 1;

$scope.submitCampoAdicional = function(){
    $('#confirmacionCampoAdicional').modal('show');
}


$scope.agregarClaseFilaCampoAdicional = function(campo_adicional){
    if($scope.campo_adicional_actual == campo_adicional.id)
        return "seleccionFila";
    else 
        return "";
}

$scope.encenderCampoAdicional = function(campo_adicional, indice){
    $scope.campo_adicional_actual = campo_adicional.id;
    $scope.nombre_campoadicional = campo_adicional.campo_salida;
    $scope.claseCampoAdicional = 0;
}

$scope.agregarCampoAdicional = function(){
  $('#confirmacionCampoAdicional').modal('hide');
    $('#loader').modal('show');
    $scope.campo_adicional_actual = "";
    data = {};
    data["campo"] = $scope.nombre_campoadicional;
    data["tipo_id"] = $scope.tipo_doc_seleccionado; 
    $http.post(API.url+'bagtesis/backend/public/campos_adicionales', data).
        success(function(data, status, headers, config) {
            //limpiarInstitucion();
             
            if(data["codigo"] == "0"){
                $http.get(API.url+'bagtesis/backend/public/campos_adicionales?tipo_id='+ $scope.tipo_doc_seleccionado).
                success(function(data, status, headers, config) {

                    $scope.campos_adicionales = data;
                    $scope.mapearCamposAdicionales();
                    $scope.msjInformacion = 'Campo adicional agregado exitosamente';
                    $('#loader').modal('hide');
                     $('#informacion').modal('show');
                     $scope.limpiarCampoAdicional();

                }).
                error(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                });
            }else{
                 $scope.msjInformacion = 'El campo adicional ya existe para este tipo de recurso';
                    $('#loader').modal('hide');
                    $('#informacion').modal('show');
            }
        }).
        error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');       
        });  
}

$scope.guardarCampoAdicional = function(){
  $('#confirmacionCampoAdicional').modal('hide');
    $('#loader').modal('show');
    data = {};
    data["campo"] = $scope.nombre_campoadicional; 
    $http.put(API.url+'bagtesis/backend/public/campos_adicionales/' + $scope.campo_adicional_actual, data).
        success(function(data, status, headers, config) {
            //limpiarInstitucion();
    
            if(data["codigo"] == "0"){
                $scope.claseCampoAdicional = 0;
                $http.get(API.url+'bagtesis/backend/public/campos_adicionales?tipo_id='+ $scope.tipo_doc_seleccionado).
                success(function(data, status, headers, config) {
                    $scope.campos_adicionales = data;
                    $scope.mapearCamposAdicionales();
                    $scope.msjInformacion = 'Campo adicional modificado exitosamente';
                    $('#loader').modal('hide');
                    $('#informacion').modal('show');

                }).
                error(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                });
            }else{
                 $scope.msjInformacion = 'Ya existe otro campo adicional con el nombre ingresado';
                    $('#loader').modal('hide');
                     $('#informacion').modal('show');
            }
        }).
        error(function(data, status, headers, config) {
           $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });  
}


$scope.eliminarCampoAdicional = function(){
  $('#confirmacionCampoAdicional').modal('hide');
    $('#loader').modal('show');

    $http.delete(API.url+'bagtesis/backend/public/campos_adicionales/' + $scope.campo_adicional_actual, {params: {id_tipo: $scope.tipo_doc_seleccionado}}).
        success(function(data, status, headers, config) {
            //limpiarInstitucion();
    
            if(data["codigo"] == "0"){
                $scope.claseCampoAdicional = 0;
                $http.get(API.url+'bagtesis/backend/public/campos_adicionales?tipo_id='+ $scope.tipo_doc_seleccionado).
                success(function(data, status, headers, config) {
                    $scope.campos_adicionales = data;
                    $scope.mapearCamposAdicionales();
                    $scope.msjInformacion = 'Campo adicional eliminado exitosamente';
                    $('#loader').modal('hide');
                    $('#informacion').modal('show');
                    $scope.limpiarCampoAdicional();
                }).
                error(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                });
            }else{
                 $scope.msjInformacion = 'El campo adicional no puede ser eliminado debido a que contiene recursos asociados';
                    $('#loader').modal('hide');
                     $('#informacion').modal('show');
            }
        }).
        error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });  
}



$scope.limpiarCampoAdicional = function(){
    $("#confirmacionCampoAdicional").modal('hide');
    $scope.nombre_campoadicional = "";
    $scope.claseCampoAdicional = 1;
    $scope.campo_adicional_actual = "";
}


    $scope.loadingStatesCamposAdicionales  = function(val){
        $scope.loadingCampos = true;
        return $scope.buscar_campos_adicionales(val);
    }


    $scope.buscar_campos_adicionales = function (val){
        $scope.mensaje_buscador_simple['data'] = val;
        $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscador_campos_typeahead', $scope.mensaje_buscador_simple)
                      .success(function(data, status, headers, config) {
                          $scope.prom = data;
                          $scope.loadingCampos = false;
                      }).then(function(){
                          $scope.aux_promise = $scope.prom;
                          
                          return $scope.prom;
                      });
    
        return $scope.promise;
    }




/*** cargar recurso electronico */
    
$scope.fileName= function(element) {
    $scope.$apply(function($scope) {
        $scope.nombreArchivo = $scope.idEjemplar + '_' + element.files[0].name + " ";
    });
};

$scope.uploadFile = function(){
    $('#loader').modal('show');
    var file = $scope.file;
    var name = $scope.name;
    console.log(name);
    console.log(file);
    var ruta = 'bagtesis/backend/public/cargar_recurso/'+$scope.idEjemplar+'/';
    upload.uploadFile(file,name, ruta).then(function(res){
        console.log(res);
        $scope.archivoCargado = true;
        $('#loader').modal('hide');
        $scope.msjInformacion = 'Archivo cargado exitosamente' ;
        $('#informacion').modal('show');
        $scope.esElectronico = 1;
        $scope.nombreArchivo = res.nombre;
       
        
    })
}

$scope.checkRecursoElectronico = function(){

  if($scope.esElectronico == 1){
    $scope.esElectronico = 0;
    $scope.doi = ""; 
  }else{

    $scope.esElectronico = 1;
  }
}


$scope.limpiarArchivo = function(){
    $scope.nombreArchivo = "";
    $scope.archivoCargado = false;
    angular.forEach(
        angular.element("input[type='file']"),
        function(inputElem) {
          angular.element(inputElem).val(null);
        });
    
}


$scope.eliminarArchivo = function(){
    $('#confirmacionCargarArchivo').modal('hide');
    $http.get(API.url+'bagtesis/backend/public/eliminar_carga_recurso/'+$scope.idEjemplar+'/'+$scope.nombreArchivo).
        success(function(data, status, headers, config) {
            $scope.msjInformacion = 'Archivo eliminado exitosamente' ;
            $('#informacion').modal('show');   
            $scope.nombreArchivo = "";
            $scope.archivoCargado = false;
            angular.forEach(
                angular.element("input[type='file']"),
                function(inputElem) {
                  angular.element(inputElem).val(null);
                });
        }).
        error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        }); 
}

$scope.descargarRecurso = function(){
      //window.location.href = API.url+ 'bagtesis/backend/app/storage/public/catalogacion/'+  $scope.nombreArchivo;
      window.open(API.url+ 'bagtesis/backend/app/storage/public/catalogacion/'+  $scope.nombreArchivo,'_blank');      
}




/*************************************************************************/
    if($scope.idrecurso !=""){
         $('#loader').modal('show');
       
         $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', [$scope.idrecurso]).
                success(function(data, status, headers, config) {
                    $scope.recursosBuscador = data;
                    $scope.desplegar_recurso($scope.idrecurso,0);
                     $('#loader').modal('hide');
                }).
                error(function(data, status, headers, config) {
                   $('#loader').modal('hide');
                });
    }

    $scope.mensaje_buscador_simple = {};
    $scope.loadingStates   = function(val,select_model_value, tipo_busqueda){
        $scope.selected = tipo_busqueda;
        $scope.nuevo = select_model_value;
        console.log("val "+val);
        console.log("select_model_value "+select_model_value);
        console.log("tipo_recurso "+tipo_busqueda); 

        if(select_model_value == 'titulo'){
            $scope.loadingTitulos = true;
            return $scope.buscar_titulo2(val);
        }else{
            if(select_model_value == 'autor'){
                $scope.loadingAutor = true;
                return $scope.buscar_autor2(val);           
            }
        }

    }


    $scope.buscar_titulo2 = function (val){
        $scope.mensaje_buscador_simple['data'] = val;
        $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_titulo_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                      .success(function(data, status, headers, config) {
                          $scope.prom = data;
                          console.log(JSON.stringify(data));
                          $scope.loadingTitulos = false;
                      }).then(function(){
                          $scope.aux_promise = $scope.prom;
                          
                          return $scope.prom;
                      });
    
        return $scope.promise;
    }


    $scope.buscar_autor2 = function (val){
        $scope.mensaje_buscador_simple['data'] = val;

        $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_autor_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                      .success(function(data, status, headers, config) {
                          console.log(JSON.stringify(data));
                          $scope.prom = data;
                          $scope.loadingAutor = false;
                      }).then(function(){
                         
                          $scope.aux_promise = $scope.prom;  
                          return $scope.prom;
                      });
        return $scope.promise;
    }

    $scope.onselectApellido = function (){
        var nombreYapellido = $scope.apellidoAutor.split(", "); 
        $scope.apellidoAutor = nombreYapellido[0];
        if(nombreYapellido.length > 1){
            $scope.nombreAutor = nombreYapellido[1];;   
        } 
    }

    $scope.submitFormluarioPrincipal=function(){
        if($scope.recursoAgregado){
            $scope.confirmarAgregarMonografia(); 
        }else{
            $scope.confirmarGuardarMonografia();
        }
    }
/************************FIN PRINCIPAL**********************************/



/****************************  Agregar Monografia   **********************************/
//tipo autor

$scope.tipoAutor1 = ["Personal", "Conferencia", "Institucional"];


$scope.seleccionarTipoAutor1 = function(){

    $scope.tipoAutor1Sel = $scope.tipoAutor;
    if ($scope.tipoAutor1Sel == null){
        $scope.tipoAutor1Sel = ""
    }
}


//contenido
$scope.contenidoSeleccion = ["Resumen", "Nota", "Datos adicionales"];
$scope.tipoContenido = "";

$scope.seleccionarTipoContenido = function(){
    if($scope.tipoContenido == "Resumen"){
        $scope.resumen = $scope.contenidoData;
        $scope.notas ="";
        $scope.datosAdicionales = "";
    }else if($scope.tipoContenido == "Nota"){     
        $scope.notas = $scope.contenidoData;
        $scope.datosAdicionales ="";
        $scope.resumen = "";
    }else if($scope.tipoContenido == "Datos adicionales"){
        $scope.datosAdicionales = $scope.contenidoData;
        $scope.notas ="";
        $scope.resumen = "";
    }
};

$scope.confirmarAgregarMonografia = function(){
    $('#confirmacionSub').modal('show');
}

$scope.validarInputAnio = function(anio){
        return anio.replace(/\D/g,'');
}

$scope.agregarTesis = function(){
    //alert(typeof $scope.NombredependenciaActual );
    anioPincipalFecha = "";
    $('#confirmacionSub').modal('hide');
    $('#loader').modal('show');
    $scope.seleccionarTipoContenido();
    $scope.dataRecurso = {};
    if($scope.tipoAutor == "Personal") {
        $scope.dataRecurso["nombreAutor"] = $scope.apellidoAutor + ", " + $scope.nombreAutor;    
    }else{
        $scope.dataRecurso["nombreAutor"] = $scope.apellidoAutor; 
    }
    if($scope.dt_orig!= "") {
        varspl = $scope.dt_orig.split('-');
        anioPincipalFecha = varspl[2] + varspl[1] + varspl[0]; 
    }
    
  
    $scope.dataRecurso["cota"] = "";
    $scope.dataRecurso["tipoAutor"] = $scope.tipoAutor[0];
    $scope.dataRecurso["nombreTitulo"] = $scope.nombreTitulo;
    $scope.dataRecurso["fechaEdicion"] = anioPincipalFecha;
    $scope.dataRecurso["url"] = $scope.url;
    $scope.dataRecurso["tipoLiter"] = $scope.tipo_doc_seleccionado;
    $scope.dataRecurso["idCatalogador"] = $cookieStore.get('user_id');
    $scope.dataRecurso["resumen"] = $scope.resumen;
    $scope.dataRecurso["notas"] = $scope.notas;      
    $scope.dataRecurso["datosAdicionales"] = $scope.datosAdicionales;
    $scope.dataRecurso["esElectronico"] = $scope.esElectronico;
    $scope.dataRecurso["nombreDocElectronico"] = $scope.nombreArchivo;
    $scope.dataRecurso["bibliotecaSelect"] = $scope.bibliotecaSelect;
    $scope.dataRecurso["campos_adicionales"] = $scope.campos_adicionales;
    
    if($scope.esElectronico == 1){
        $scope.dataRecurso["doi"] = $scope.doi;

    }else{
        $scope.dataRecurso["doi"] = "";
    }

    $http.post(API.url+'bagtesis/backend/public/agregar_otro_recurso', $scope.dataRecurso)
        .success(function(data, status, headers, config) {     
        if(data["codigo"] == "0"){
            $scope.idrecurso = data["id_recurso"];
             $scope.ruta_qr = API.url+"bagtesis/backend/public/recursos/"+$scope.idrecurso+"/pdf";
            $scope.idTitulo = data["id_titulo"];
            $scope.idAutor = data["id_autor"];
            $cookieStore.put('id_recurso_otros', $scope.idrecurso);
            $scope.recursoAgregado = false;    
            $scope.msjInformacion = ' Recurso catalogado exitosamente' ;
            $('#loader').modal('hide');
            $('#informacion').modal('show');  
        }else{
            $scope.msjInformacion = 'Ya existe un recurso de este tipo catalogado con este mismo título' ;
             $('#informacion').modal('show');  
            $('#loader').modal('hide');
        }
    }).
    error(function(error) {
        $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });
}

//**********GUARDAR MONOGRAFIA ***************/
$scope.confirmarGuardarMonografia = function(){

    if($scope.esElectronico == 1 && $scope.nombreArchivo.length > 0 && !$scope.archivoCargado){
         $scope.msjInformacion = 'Por favor cargue o limpie el archivo seleccionado antes de proceder a guardar el recurso' ;
        $('#informacion').modal('show');                  
    }else
        $('#confirmacionSub').modal('show');
} 

$scope.guardarTesis = function(){

    anioPincipalFecha = "";
    $('#confirmacionSub').modal('hide');
    $('#loader').modal('show');
    $scope.seleccionarTipoContenido();
    $scope.dataRecurso = {};
    if($scope.tipoAutor == "Personal") {
        $scope.dataRecurso["nombreAutor"] = $scope.apellidoAutor + ", " + $scope.nombreAutor;    
    }else{
        $scope.dataRecurso["nombreAutor"] = $scope.apellidoAutor; 
    }
    if($scope.dt_orig!= "") {
        varspl = $scope.dt_orig.split('-');
        anioPincipalFecha = varspl[2] + varspl[1] + varspl[0]; 
    }
    $scope.dataRecurso["idRecurso"] = $scope.idrecurso;
    $scope.dataRecurso["cota"] = "";
    $scope.dataRecurso["tipoAutor"] = $scope.tipoAutor[0];
    $scope.dataRecurso["nombreTitulo"] = $scope.nombreTitulo;
    $scope.dataRecurso["fechaEdicion"] = anioPincipalFecha;
    $scope.dataRecurso["url"] = $scope.url;
    $scope.dataRecurso["tipoLiter"] = $scope.tipo_doc_seleccionado;
    $scope.dataRecurso["idCatalogador"] = $cookieStore.get('user_id');
    $scope.dataRecurso["resumen"] = $scope.resumen;
    $scope.dataRecurso["notas"] = $scope.notas;    
    $scope.dataRecurso["datosAdicionales"] = $scope.datosAdicionales;
    $scope.dataRecurso["idAutor"] = $scope.idAutor;
    $scope.dataRecurso["idTitulo"] = $scope.idTitulo;
    $scope.dataRecurso["bibliotecaSelect"] = $scope.bibliotecaSelect;
    $scope.dataRecurso["esElectronico"] = $scope.esElectronico;
    $scope.dataRecurso["campos_adicionales"] = $scope.campos_adicionales;

    if($scope.esElectronico == 1){
        $scope.dataRecurso["doi"] = $scope.doi;
        if($scope.archivoCargado)
            $scope.dataRecurso["nombreDocElectronico"] = $scope.nombreArchivo;
        else
            $scope.dataRecurso["nombreDocElectronico"] = "";

    }else{
        $scope.dataRecurso["doi"] = "";
        $scope.dataRecurso["nombreDocElectronico"] = "";
    }


    $http.post(API.url+'bagtesis/backend/public/guardar_otro_recurso', $scope.dataRecurso)
            .success(function(data, status, headers, config) {     
                if(data["codigo"] == "0"){
                   $scope.msjInformacion = 'Recurso modificado exitosamente' ;
                      $('#loader').modal('hide');
                   $('#informacion').modal('show'); 
                }else{
                    $scope.msjInformacion = 'Ya existe un recurso de este tipo catalogado con este mismo título' ;
                     $('#informacion').modal('show');  
                    $('#loader').modal('hide');
                }
       // alert(JSON.stringify(data));
       // alert(JSON.stringify($scope.titulosMostrar));
        //alert(JSON.stringify($scope.tipoAutorPrincipal)); 
    }).
    error(function(error) {
      $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });  
}

$scope.eliminarLibro = function(){
    $('#confirmacionEliminar').modal('hide');
    $('#loader').modal('show');
    $scope.dataRecurso = {};
    $scope.dataRecurso["idRecurso"] = $scope.idrecurso;
    $scope.dataRecurso["idCatalogador"] = $cookieStore.get('user_id');

    $http.post(API.url+'bagtesis/backend/public/eliminar_otro_recurso', $scope.dataRecurso)
            .success(function(data, status, headers, config) {     
                if(data["codigo"] == "0"){

                       $('#loader').modal('hide');
                   $scope.msjInformacion = 'Recurso eliminado exitosamente' ;
                   $scope.limpiarLibro();
                   $('#informacion').modal('show'); 
                }else{ 
                    $scope.msjInformacion = 'El recurso con cota "' + $scope.cotaLibro + '" ya existe' ;
                    $('#loader').modal('hide');             
                }
       // alert(JSON.stringify(data));
       // alert(JSON.stringify($scope.titulosMostrar));
        //alert(JSON.stringify($scope.tipoAutorPrincipal)); 
    }).
    error(function(error) {
       $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });  

}



$scope.limpiarLibro = function(){
    
    $cookieStore.put('id_recurso_otros', "");
    $scope.idRecurso = "";
    $scope.dt_orig = "";
    $scope.editorialPrincipal = "";
    $scope.recursoAgregado = true;
    $scope.cotaLibro = "";
    $scope.tipoAutor = "";
    $scope.nombreAutor = "";
    $scope.apellidoAutor = "";
    $scope.nombreTitulo = "";
    $scope.edicion = "";
    $scope.fechaEdicion = "";
    $scope.url = "";
    $scope.resumen = "";
    $scope.notas = "";   
    $scope.datosAdicionales = "";
    $scope.idrecurso = "";
    $scope.tipoContenido= "";
    $scope.resumen = "";
    $scope.notas = "";
    $scope.contenidoData = "";
    $scope.esElectronico = 0;
    $scope.bibliotecaSelect = "1";
    $scope.limpiarArchivo();
    $scope.desplego = false;
    if($scope.campos_adicionales){
        for (var i = 0; i < $scope.campos_adicionales.length; i++) {
            $scope.campos_adicionales[i].descripcion = "";
        }
    }
}


$scope.deshabilitarOpcion = function(){
    if($scope.recursoAgregado){
        return "disabled"
    }
}



/****************************  Fin agregar Monografia   **********************************/

/**************BUsCADOR SIMPLE MONOGRAFIA *********************/////////////

    $scope.bool_table_recursos = false; 
    $scope.currentPage = 1;
    $scope.bool_seleccionar_todos = false;       
    $scope.num_items_pagina  = 10;         
    $scope.datos_qr = [];
    /*pagination config*/

    paginationConfig.firstText='Primera'; 
    paginationConfig.previousText='< Anterior';
    paginationConfig.nextText='Siguiente >';
    paginationConfig.lastText='Última';
    paginationConfig.boundaryLinks=false;
    paginationConfig.rotate=false;
    $scope.maxSize = 15;
    paginationConfig.itemsPerPage = $scope.num_items_pagina;
    $scope.popUpBusquedaSimple = false;

$scope.abrirPopUpBusqueda = function(val){
    if(val.length > 0){
         $scope.loadingBusquedaSimple = false;
        $scope.popUpBusquedaSimple = true;
        $scope.selectedBuscadorSimple = val;
        $scope.selectedBuscadorSimple2 = "";
        $scope.bool_table_recursos = false;
        $scope.recursosBuscador = [];
        $scope.consultar_buscador_simple(val);
        $('#buscadorSimple').modal('show');
    }
}


$scope.verificar_paste = function(val){
    if($scope.paste & val.length == 13 & val.indexOf(' ')<0 & (val[0] == 'T'  || val[0] == 'D')  &  $scope.verificar_id(val)){
        $scope.consultar_buscador_simple(val);
    }
 }


 $scope.verificar_id = function(val){
    for(var i=1; i < val.length; i++){
        if(parseInt(val[i])<0 || parseInt(val[i])>9 || val[i].charCodeAt(0)<48 || val[i].charCodeAt(0)>57){
             //   alert(parseInt(val[12]));
            return false;
        }
    }
    return true;
 };

    $scope.options_orden =  [
            { label: 'Normal', value: 'normal' },
            { label: 'Título A...Z', value: 'titulo_asc' },
            { label: 'Título Z...A', value: 'titulo_desc' },
            { label: 'Autor  A...Z', value: 'autor_asc' },
            { label: 'Autor  Z...A', value: 'autor_desc' },
            { label: 'Año ascendente', value: 'anio_asc' },
            { label: 'Año descendente', value: 'anio_desc' }
        ]
        ;
    $scope.orden_seleccionado =  $scope.options_orden[0];
    $scope.a ;


$scope.consultar_buscador_simple = function(selected){
    
    if(selected!=""){
        $scope.i = 0;
      //  $scope.lector_barra = false;
        $scope.bool_acciones = true;
        $scope.bool_recursos_encontrados = false;
        $scope.bool_disable = true;
        $scope.bool_disable_acciones = true;
        $scope.loading = true;
        if(!$scope.popUpBusquedaSimple)
            $scope.loadingBusquedaSimple = true;
        else 
             $scope.loadingBusquedaSimple = false;
        $scope.data = { 'data': selected};
        $scope.num_items_pagina = 10;
        $scope.respuesta_buscador_simple = [];
        paginationConfig.itemsPerPage = 10;//$scope.num_items_pagina;
        
        $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_simple2?page=1&num_items_pagina='+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value+'&tipo_recurso=OTR', $scope.data).
            success(function(data, status, headers, config) {
       
                $scope.a = data;
                $scope.pagina = data;
                $scope.id_recursos = data.data;
                $scope.recursos = [];
                $scope.currentPage = data.current_page;
                $scope.contador_caracteres = 0;
                $scope.totalItems = data.total;
                $scope.numPages = data.last_page;           
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
                $('#loader').modal('show');

                $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
                success(function(data, status, headers, config) {


                //alert("data2 "+ JSON.stringify(data));
                $scope.recursosBuscador = data;
                $scope.bool_disable = false;
                $scope.bool_recursos_encontrados = true;
                 $('#loader').modal('hide');
            }).
            error(function(data, status, headers, config) {
               // alert(JSON.stringify(data));
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
            });
            $scope.bool_table_recursos=true;
          
        
        }).error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.loading = false;
            $scope.loadingBusquedaSimple = false;
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
            });
    }  
}

$scope.funcion_buscar = function (val){
  $scope.mensaje_buscador_simple['data'] = val;
  $scope.mensaje_buscador_simple['autocompletado'] = val;
  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_typeahead?tipo_recurso=OTR', $scope.mensaje_buscador_simple)
                .success(function(data, status, headers, config) {
                      $scope.prom = data;
                  }).then(function(){
                      $scope.aux_promise = $scope.prom;
                      $scope.aux_selet = val;
                      $scope.buscando_select = false;
                     
                      return $scope.prom;
                  });
 
    return $scope.promise;
};

$scope.loadingStates3 = function(val){
    return $scope.funcion_buscar(val);
};

$scope.getService =  function(route_peticion){
    $('#loader').modal('show'); 
    $scope.i = 0 ;
    $scope.data = { 'data': $scope.selectedBuscadorSimple}

    $scope.respuesta_buscador_simple = [];
    $http.post(route_peticion , $scope.data).
        success(function(data, status, headers, config) {
            $scope.id_recursos = data.data;
            $scope.recursosBuscador = [];
            $scope.recursosBuscador = [];
            $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
            success(function(data, status, headers, config) {
                $scope.recursosBuscador = data;
                $('#loader').modal('hide');
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
            }).
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });
            $scope.currentPage = data.current_page;
            $scope.totalItems = data.total;
            $scope.numPages = data.last_page;
        }).
        error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.loading = false;
            $scope.loadingBusquedaSimple = false;
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });

}

    $scope.pageChanged = function(currentPage){
       // alert(currentPage);
        $scope.currentPage = currentPage;
        $scope.getService(API.url+'bagtesis/backend/public/buscadores/buscador_simple2?page='+$scope.currentPage+"&num_items_pagina="+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value+'&tipo_recurso=OTR', $scope.data);
    }   

    $scope.getServiceAvanzado =  function(route_peticion){
        $('#loader').modal('show');
        $scope.i = 0 ;
        $scope.data = { 'data': $scope.selectedBuscadorSimple}

        $scope.respuesta_buscador_simple = [];
        $http.post(route_peticion , $scope.mensaje_buscador_avanzado).
        success(function(data, status, headers, config) {
            $scope.id_recursos = data.data;
            $scope.recursosBuscador = [];
            $scope.recursosBuscador = [];
            $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
            success(function(data, status, headers, config) {
                $scope.recursosBuscador = data;
                $('#loader').modal('hide');
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;

            }).
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });
            $scope.currentPage = data.current_page;
            $scope.totalItems = data.total;
            $scope.numPages = data.last_page;
            }).
                   
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });

    }
    
    $scope.pageChangedAvanzado = function(currentPage){
            $scope.currentPage = currentPage;
            $scope.getServiceAvanzado(API.url+'bagtesis/backend/public/buscadores/buscador_avanzado?page='+$scope.currentPage+'&num_items_pagina='+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value,  $scope.mensaje_buscador_avanzado);
    }  

    $scope.desplegar_recurso = function (recurso_id,index_recurso){
        $scope.mostrarBuscador = true;
        $scope.limpiarLibro();
        $scope.desplego = true;
        $scope.campos_adicionales_recurso = {};
        

        $('#buscadorSimple').modal('hide');
        $('#busquedaAvanzada').modal('hide');
        
        recursoActual = $scope.recursosBuscador[index_recurso];
        $scope.tipo_doc_seleccionado = recursoActual.tipo_liter;
        $cookieStore.put('tipo_recurso_id', $scope.tipo_doc_seleccionado);
        $scope.obtener_campos_tipodoc();
        $scope.campos_adicionales_recurso = recursoActual.campos_adicionales;

       	if(recursoActual.ejemplar){
       		$scope.esElectronico = recursoActual.ejemplar.doc_electronico;
        	$scope.nombreArchivo = recursoActual.ejemplar.nombre_doc_electronico;
        	$scope.doi = recursoActual.ejemplar.doi;
        	$scope.idEjemplar = recursoActual.ejemplar.id;
       	}
        
        if ($scope.nombreArchivo && $scope.esElectronico == 1)
            $scope.archivoCargado = true;
        else
            $scope.archivoCargado = false;
        autores = recursoActual.autores;
        titulos = recursoActual.titulos;
        $scope.idrecurso = recurso_id;
         $scope.ruta_qr = API.url+"bagtesis/backend/public/recursos/"+$scope.idrecurso+"/pdf";
        $cookieStore.put('id_recurso_otros', $scope.idrecurso);
        $scope.recursoAgregado = false;
        $scope.cotaLibro = recursoActual.ubicacion;
        $scope.tipoAutor = "";
        $scope.nombreAutor = "";
        $scope.apellidoAutor = "";
        $scope.nombreTitulo = "";
        $scope.fechaEdicion = recursoActual.fecha_iso;
        $scope.url = recursoActual.url;
        $scope.bibliotecaSelect = recursoActual.bibliotecas[0].id;

        if(recursoActual.fecha_iso != null && parseInt(recursoActual.fecha_iso.substring(4,7))!=0 && recursoActual.fecha_iso.trim().length == 8 ) {
            $scope.tipoFechaPrincipal = "fecha";
            $scope.fechaPrincipalActiva = true;
            $scope.anioPrincipal = "";
            $scope.dt_orig =  $scope.setDate(recursoActual.fecha_iso.trim());
        }
      
        var aux = 0;
        var aux4= false;
        var indice = 0;

        for(var i = 0; i < autores.length; i++){
            if(autores[i].tipo_autor == "TA"){
                $scope.tutorTesis = autores[i].autor_salida;
                $scope.correoTutor = autores[i].correo;
                $scope.idTutorTesis = autores[i].id_autor;
                break;   
            }
        }
        for(var i = 0; i < autores.length; i++){
            if(autores[i].nombre == "Autor Principal"){
                indice = i; 
                break;   
            }
            if(autores[i].orden2){                         
                if(i == 0){
                    aux = autores[i].orden2;   
                }else if(autores[i].orden2 < aux){
                    aux = autores[i].orden2;
                    indice = i;
                }
            }else{
                indice = i;   
            }
        }

        if(autores.length > 0){
            if(autores[indice].tipo_autor_principal == "P"){
                aux4 = true;  
                $scope.tipoAutor = "Personal";
                var nombreYapellido = autores[indice].autor_salida.split(", ");  
                $scope.apellidoAutor = nombreYapellido[0];
                if(nombreYapellido.length > 1){
                    $scope.nombreAutor = nombreYapellido[1];;   
                } 
                $scope.idAutor = autores[indice].id_autor;
            }else if(autores[indice].tipo_autor_principal == "C"){
                aux4 = true;  
                $scope.apellidoAutor = autores[indice].autor_salida;
                $scope.tipoAutor = "Conferencia";
                 $scope.idAutor = autores[indice].id_autor;
            }else if(autores[indice].tipo_autor_principal == "I"){
                aux4 = true;  
                $scope.apellidoAutor = autores[indice].autor_salida;
                $scope.tipoAutor = "Institucional";
                $scope.idAutor = autores[indice].id_autor;
            }
        }else{
            $scope.tipoAutor = "";
            $scope.apellidoAutor = ""; 
            $scope.nombreAutor = ""; 
            $scope.idAutor = ""; 
        }
           
        if(!aux4){
            $scope.tipoAutor = "";
            $scope.apellidoAutor = ""; 
            $scope.nombreAutor = ""; 
            $scope.idAutor = ""; 
        }   
               
        var aux2 = 0;
        var indice2 = 0;
        var aux3 = false;
        for(var i = 0; i < titulos.length; i++){

            if(titulos[i].tipo_tit == 'OP'){
                if (titulos[i].orden){     
                    if(i == 0){
                        aux2 = titulos[i].orden;   
                    }else if(titulos[i].orden < aux2){
                        aux2 = titulos[i].orden;
                        indice2 = i;
                    }
                }else{
                    indice2 = i;                      
                }
               
            }   
        }
        if(titulos.length > 0){
            $scope.nombreTitulo = titulos[indice2].titulo_salida;
            $scope.idTitulo = titulos[indice2].id;    
        }
        else{ 
            $scope.nombreTitulo = "";
            $scope.idTitulo = "";    
        
        }
        if(recursoActual.resumen != ""){
            $scope.tipoContenido = "Resumen";
            $scope.contenidoData = recursoActual.resumen;        
        }else if(recursoActual.notas != ""){
            $scope.tipoContenido = "Nota";
            $scope.contenidoData = recursoActual.notas;          
        }else if(recursoActual.datos_adicionales != ""){
            $scope.tipoContenido = "Datos adicionales";  
            $scope.contenidoData = recursoActual.datos_adicionales;          
        }else { 
            $scope.contenidoData = "";    
            $scope.tipoContenido = "";
        }

      
          $('#loader').modal('hide');  
    }

    $scope.mapearCamposAdicionales = function(){
        if($scope.desplego){
           for (var i = 0; i < $scope.campos_adicionales_recurso.length; i++) {
                pos = $scope.campos_adicionales.map(function(e) { return e.id; }).indexOf($scope.campos_adicionales_recurso[i].id);
                if(pos > -1)
                    $scope.campos_adicionales[pos].descripcion = $scope.campos_adicionales_recurso[i].descripcion;
            }
        }
    }

    $scope.mapearCampoAdicional = function(indice, id, valor){
        if($scope.desplego){
            pos = $scope.campos_adicionales_recurso.map(function(e) { return e.id; }).indexOf(id);
            //alert(pos);
            if(pos > -1){
                $scope.campos_adicionales_recurso[pos].descripcion = valor;
            
            }else{
                  $scope.campos_adicionales_recurso.push($scope.campos_adicionales[indice])   
            }
            console.log(JSON.stringify($scope.campos_adicionales_recurso));


        }
    }

/**********************CALENDARIO**************************************************/
$scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.orig_change = function(orig){
        var dd = orig.getDate();
        var mm = orig.getMonth()+1; //January is 0!

        var yyyy = orig.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        }
        $scope.dt_orig = dd+'-'+mm+'-'+yyyy;
        console.log($scope.dt_orig);
       
    };
    $scope.orig_change2 = function(orig){
        var dd = orig.getDate();
        var mm = orig.getMonth()+1; //January is 0!

        var yyyy = orig.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        }
        $scope.dt_orig2 = dd+'-'+mm+'-'+yyyy;
        console.log($scope.dt_orig2);
       
    };

     $scope.setDate = function(fecha_ed) {
        var dd = fecha_ed.substring(6, 8);
        var mm = fecha_ed.substring(4, 6); //January is 0!
        var yyyy =  fecha_ed.substring(0, 4);
        
        dt_destt = dd+'-'+mm+'-'+yyyy;
        return dt_destt;
    };
  
    $scope.open_orig = function(event) {
        event.preventDefault();
        event.stopPropagation();
        $scope.opened_orig = true;
    };


    $scope.setMinDate = function(){
        var now = new Date();
        var oneYr = new Date();
        oneYr.setYear(now.getYear() - 5);
        $scope.minDate = oneYr;
    };
    $scope.setMinDate();

    $scope.dateOptions = {
       formatYear: 'yy',
       startingDay: 1
    };

$scope.today = function() {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    }
    return mm+'-'+dd+'-'+yyyy;
};

$scope.setearTipoFecha = function(){
    if($scope.tipoFechaPrincipal == "anio"){
        $scope.dt_orig = "";
        $scope.fechaPrincipalActiva = false;
    }else{
        $scope.anioPrincipal = "";
        $scope.fechaPrincipalActiva = true;
    }
}


//**********************************FIN CALENDARIO***********************************///

/****************************INICIO BUSCADOR AVANZADO*******************************///
$scope.getServiceBibliotecas = function (){
   // alert('alert Biblioteca');
        $http.get(API.url+'bagtesis/backend/public/bibliotecas').
            success(function(data, status, headers, config) {
            $scope.bibliotecas = data;
            $scope.options_bibliotecas.push({label:'Todas',value:'todas'});
            angular.forEach($scope.bibliotecas, function(value, key) {
              this.push({label:value.biblioteca_salida, value: value.id});
            },$scope.options_bibliotecas);

            $scope.options_bibliotecas_seleccionado = $scope.options_bibliotecas[0];

            }).
           
            error(function(data, status, headers, config) {
               
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });
};

$scope.getServiceDependencias = function(){
        $http.get(API.url+'bagtesis/backend/public/dependencias').
            success(function(data, status, headers, config) {
            $scope.dependencias = data;
            $scope.options_dependencias.push({label:'Todas',value:'todas'});
            angular.forEach($scope.dependencias, function(value, key) {
              this.push({label:value.dependencia_salida, value: value.id});
            },$scope.options_dependencias);
            $scope.options_dependencias_seleccionado = $scope.options_dependencias[0];
            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });
};
$scope.mostrarBuscador = true;
$scope.busquedaEncontrada = false;

$scope.options = {
                    opciones1: [  
                                    {label: 'Título' ,   value: 'titulo'   },          
                                    {label: 'Autor'  ,   value: 'autor'    },
                                    {label: 'Materia',   value: 'materia'},
                                    {label: 'Palabra clave'  , value: 'palabra_clave'},
                                    {label: 'Idioma' ,   value: 'idioma'   }
                                ],

                    opciones2: [
                                    {label: 'Y'   , value: 'AND'},
                                    {label: 'O' , value: 'OR'    },
                                    {label: 'NO'  , value: 'NOT LIKE' }
                               ]
                    };

$scope.mytooltips = [{title: 'Borrar campos', tooltip: 'tooltip', position: 'rigth'}];

$scope.anios = {anio_origen: '',
                anio_destino: ''
                };

$scope.options_bibliotecas = [];
//$scope.options_bibliotecas_seleccionado = {};
$scope.options_dependencias = [];
//$scope.options_dependencias_seleccionado = {};
$scope.num_items_pagina3 = 10;
$scope.options_orden =  [
                          { label: 'Normal', value: 'normal' },
                          { label: 'Título A...Z', value: 'titulo_asc' },
                          { label: 'Título Z...A', value: 'titulo_desc' },
                          { label: 'Autor  A...Z', value: 'autor_asc' },
                          { label: 'Autor  Z...A', value: 'autor_desc' },
                          { label: 'Año ascendente', value: 'anio_asc' },
                          { label: 'Año descendente', value: 'anio_desc' }
                      ];

$scope.orden_seleccionado =  $scope.options_orden[0];


$scope.newOption = function(selet_model1, text_model1, selet_model2, text_model2,selected){
    $scope.inputs.push({
        opt1: {
            select_model: selet_model1,
            text_model: text_model1,
            function_typeahead: '',
        }, 
        opt2: {
            select_model: selet_model2,
            text_model: text_model2,
        },      
    });
};

$scope.initBuscadorAvanzado = function(){
    $scope.bool_table_recursos = false;
    $scope.inputs = [];    
    $scope.newOption($scope.options.opciones1[0], "", $scope.options.opciones2[0], "",$scope.selected);
    $scope.mostrarBuscador = true;
    $scope.busquedaEncontrada = false;
    $scope.recursosBuscador = [];
    $('#busquedaAvanzada').modal('show');
}

$scope.bool_add_button = true;

$scope.getServiceBibliotecas();
$scope.getServiceDependencias();  


$scope.add_option = function(){

    $scope.mytooltips.push({title: 'Borrar campos', tooltip: 'tooltip', position: 'rigth'});
    $scope.newOption($scope.options.opciones1[0], "", $scope.options.opciones2[0], "",$scope.selected);
};

$scope.delete = function(index){
    if($scope.inputs.length!=1){
        $scope.inputs.splice(index,1);
    }
}



$scope.loadingStates4 = function(val,select_model_value){
$scope.selected = "OTR";
    if(select_model_value == 'titulo'){
        return $scope.buscar_titulo(val);
    }else{
        if(select_model_value == 'autor'){
            return $scope.buscar_autor(val);           
        }else{
            if(select_model_value == 'cota'){
                return $scope.buscar_cota(val);                   
            }else{
                if(select_model_value == 'jurado'){
                    return $scope.buscar_jurado(val);   
                }else{
                    if(select_model_value == 'tutor_academico'){
                        return $scope.buscar_tutor_academico(val);                         
                    }else{
                        if(select_model_value == 'materia'){
                            return $scope.buscar_materia(val);
                        }else{
                            if(select_model_value == 'palabra_clave'){
                                return $scope.buscar_palabra_clave(val);
                            }else{
                                if(select_model_value == 'tipo_de_trabajo'){
                                    return $scope.buscar_tipo_de_trabajo(val);
                                }else{
                                    if(select_model_value == 'volumen'){
                                        return $scope.buscar_volumen(val);                                        
                                    }else{
                                        if(select_model_value == 'edicion'){
                                            return $scope.buscar_edicion(val);
                                        }else{
                                            if(select_model_value == 'idioma'){
                                                return $scope.buscar_idioma(val);
                                            }else{
                                                if(select_model_value == 'isbn'){
                                                    return $scope.buscar_isbn(val);
                                                }else{
                                                    if(select_model_value == 'issn'){
                                                        return $scope.buscar_issn(val);
                                                    }else{
                                                        if(select_model_value == 'editorial'){
                                                            return $scope.buscar_editorial(val);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
 //   return $scope.funcion_buscar_titulo(val);
};



$scope.buscar_titulo = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_titulo_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};


$scope.buscar_autor = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_autor_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};



$scope.buscar_jurado = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_jurado_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};



$scope.buscar_tutor_academico = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_tutor_academico_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};




$scope.buscar_cota = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_cota_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};


$scope.buscar_materia= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_materia_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};

$scope.buscar_palabra_clave= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_palabra_clave_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_tipo_de_trabajo= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_tipo_de_trabajo_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};



$scope.buscar_volumen= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_volumen_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_edicion = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_edicion_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {
                      $scope.prom = data;
                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_idioma = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_idioma_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_isbn = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_isbn_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_issn = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_issn_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {
                      $scope.prom = data;
                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_editorial = function (val){
  $scope.mensaje_buscador_simple['data'] = val;
  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_editorial_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
}; 

$scope.consulta_buscador_avanzado = function(){
        $scope.mensaje_buscador_avanzado = {};
        $scope.currentPage = 1;
        $scope.mensaje_buscador_avanzado['inputs'] = [];
        $scope.mensaje_buscador_avanzado['inputs'] = $scope.inputs;
        $scope.mensaje_buscador_avanzado['anios'] = $scope.anios;
        $scope.mensaje_buscador_avanzado['tipo_recurso'] = $scope.selected;
        $scope.loading = true;
        $scope.mensaje_buscador_avanzado['biblioteca'] = $scope.options_bibliotecas_seleccionado;
        $scope.mensaje_buscador_avanzado['dependencia'] = $scope.options_dependencias_seleccionado;
        $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_avanzado?page=1&num_items_pagina='+$scope.num_items_pagina3+'&orden='+$scope.orden_seleccionado.value,  $scope.mensaje_buscador_avanzado).
            success(function(data, status, headers, config) {
            
                $scope.a = data;
                $scope.pagina = data;
                $scope.id_recursos = data.data;
                $scope.recursos = [];
                $scope.currentPage = data.current_page;
                $scope.contador_caracteres = 0;
                $scope.totalItems = data.total;
                $scope.numPages = data.last_page;           
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
                $('#loader').modal('show');

                $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
                success(function(data, status, headers, config) {
                
                    
                $scope.mostrarBuscador = false;
                $scope.busquedaEncontrada = true;

                //alert("data2 "+ JSON.stringify(data));
                $scope.recursosBuscador = data;
                $scope.bool_disable = false;
                $scope.bool_recursos_encontrados = true;
                 $('#loader').modal('hide');
            }).
            error(function(data, status, headers, config) {
               // alert(JSON.stringify(data));
               $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
            });
            $scope.bool_table_recursos=true;
          
        
        }).error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.loading = false;
            $scope.loadingBusquedaSimple = false;
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
            });
  }

/*******************************FIN BUSCADOR AVANZADO******************************/

/****************************** CODIGO DE BARRAS **********************************/

    $scope.mostrarModalCodigoBarra = function(){
        $('#mostrarBarras').modal('show');
        $('#mostrarBarras').on('shown.bs.modal', function () {
            $('#enterBarcode').focus();
        })  
    }

    $scope.buscarRecursoId = function(){
        $('#mostrarBarras').modal('hide');
        $('#loader').modal('show');
        $http.get(API.url+'bagtesis/backend/public/buscar_codigo_barra/'+$scope.codigoBarra).
        success(function(data, status, headers, config) {
            $scope.codigoBarra = "";
            if(data['codigo'] == 0){
                data = data['recurso'];
                $scope.idrecursoBarra = data.recurso_id;
                $scope.tipo_recurso = data.tipo_liter;
                if($scope.idrecursoBarra != "" && $scope.idrecursoBarra != null && $scope.tipo_recurso != "" && $scope.tipo_recurso != null){
                    $('#loader').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $scope.tipo_recurso_ab =  ($scope.tipo_recurso.substring(0, 1)).toUpperCase();

                    if( $scope.tipo_recurso_ab == 'O'){
                        $cookieStore.get('tipo_recurso_id',$scope.tipo_recurso);
                        $cookieStore.put('id_recurso_otros',$scope.idrecursoBarra);
                        $scope.redirect = "otrosRecursos";

                    }else if ($scope.tipo_recurso == "M"){
                        $cookieStore.put('id_recurso_libro',$scope.idrecursoBarra);
                        $scope.redirect = "librosPrincipal";

                    }else if ($scope.tipo_recurso == "T"){
                        $cookieStore.put('id_recurso_tesis',$scope.idrecursoBarra);
                        $scope.redirect = "trabajosAcademicos"; 

                    }else if ($scope.tipo_recurso == "S"){
                        $cookieStore.put('id_recurso_publicacion_s',$scope.idrecursoBarra);
                        $scope.redirect = "publicacionesSeriadas";    
                    }
                    
                    return $state.go($scope.redirect, {}, { reload: true });

                }else{
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                }
            }else{
                //recurso no existe en el sistema
                $scope.codigoBarra = "";
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Este recurso no existe en el sistema';
                $('#informacion').modal('show');
            }
        }).
        error(function(data, status, headers, config) {
            $scope.codigoBarra = "";
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        }); 
    }

    $scope.auxTrim = "";

    $scope.trimRequired = function(model) {
        
        if (model == "apellidoAutor" ){
            if($scope.apellidoAutor){
                $scope.auxTrim = $scope.apellidoAutor.trim();
            }else{
                $scope.auxTrim = "";
            }
        } else if (model == "nombreTitulo" ){
            if($scope.nombreTitulo){
                $scope.auxTrim = $scope.nombreTitulo.trim();
            }else{
                $scope.auxTrim = "";
            }
        } else if (model == "nombre_tipodoc" ){
            if($scope.nombre_tipodoc){
                $scope.auxTrim = $scope.nombre_tipodoc.trim();
            }else{
                $scope.auxTrim = "";
            }
        } else if (model == "nombre_campoadicional" ){
            if($scope.nombre_campoadicional){
                $scope.auxTrim = $scope.nombre_campoadicional.trim();
            }else{
                $scope.auxTrim = "";
            }
        } 
    }

    $scope.blurRequired = function (model) {
        if (model == "apellidoAutor"){
            $scope.apellidoAutor = $scope.auxTrim;
        } else if (model == "nombreTitulo"){
            $scope.nombreTitulo = $scope.auxTrim;
        } else if (model == "nombre_tipodoc"){
            $scope.nombre_tipodoc = $scope.auxTrim;
        } else if (model == "nombre_campoadicional"){
            $scope.nombre_campoadicional = $scope.auxTrim;
        }

        $scope.auxTrim = "";
      
    }
    

});


