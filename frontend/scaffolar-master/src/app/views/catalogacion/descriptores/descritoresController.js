angular.module('app' )
  .controller('descriptoresController', function($scope, $window, $http, API, $cookieStore, paginationConfig, $state) {
    $scope.idrecurso="";

    $scope.tipo_recurso = $cookieStore.get('tipo_recurso');
    $scope.tipo_recurso2 = $cookieStore.get('tipo_recurso');
    $scope.loadingDescriptor = false;
    $scope.tipo_recurso_ab =  ($scope.tipo_recurso.substring(0, 1)).toUpperCase();
    if( $scope.tipo_recurso_ab == 'O')
        $scope.tipo_recurso_ab = $cookieStore.get('tipo_recurso_id'); // validacion  agregada para el modulo de otros recursos
    if ($scope.tipo_recurso == "monografia"){
        $scope.nombre_recurso =  "Monografías";
        $scope.idrecurso = $cookieStore.get('id_recurso_libro');
        $scope.redirect = "libros";
    }else if ($scope.tipo_recurso == "tesis"){
         $scope.nombre_recurso =  "Trabajos Académicos";
        $scope.idrecurso = $cookieStore.get('id_recurso_tesis');
        $scope.redirect = "trabajosAcademicos"; 
    }else if ($scope.tipo_recurso == "serie"){
        $scope.idrecurso = $cookieStore.get('id_recurso_publicacion');
        $scope.nombre_recurso =  "Publicaciones Seriadas"; 
        $scope.redirect = "publicacionesSeriadas";    
    }else if ($scope.tipo_recurso == "otros"){
        $scope.idrecurso = $cookieStore.get('id_recurso_otros');
        $scope.nombre_recurso =  "Otros Recursos"; 
        $scope.redirect = "otrosRecursos";    
    }
    if(!$scope.idrecurso){
         return $state.go('home', {}, { reload: true });
    }

 	$('#loader').modal('show');
 	$scope.bool_agregar_derecurso = 0;
 	$scope.bool_eliminar_derecurso = 0; 
	$scope.nombreDescriptor = "";
	$scope.descDescriptores = "";
	$scope.codDescriptores = "";
	$scope.tipoDescriptor = "";
	$scope.tipoDescriptores = "";
	$scope.descriptoresMostrar = "";
	$scope.tipo_recurso = 'M';
	$scope.botonAgregarDescriptor = false;
	$scope.msjConfirmacion = "";
	$scope.msjInformacion = "";
	$scope.esAgregarDescriptor = "";
	$scope.descriptorRecurso = {};
	$scope.esMateriaTematica = false;
	$scope.aux_tematica = [];
	$scope.temDesc = "";
	$scope.genDesc = "";
	$scope.geoDesc = "";
	$scope.busqueda = "";
	$scope.tipoBusqueda = "";
	$scope.tipoDescBus = "";
	$scope.textoBusqueda = "";
	$scope.esDelRecurso = false;
	$scope.esDelSistema = false;
	$scope.desSistemaMostrar = [];
	$scope.currentPageDesc =1;
	$scope.descSisSel = -1;
	$scope.descSel = -1;
	$scope.vieneDeGuardar = false;


	//PAGINADO DESCRIPTORES
	$scope.bool_table_recursos = false; 
    $scope.currentPage = 1;
    $scope.bool_seleccionar_todos = false;       
    $scope.num_items_pagina  = 10;         
    $scope.datos_qr = [];
    /*pagination config*/

    paginationConfig.firstText='Primera'; 
    paginationConfig.previousText='< Anterior';
    paginationConfig.nextText='Siguiente >';
    paginationConfig.lastText='Última';
    paginationConfig.boundaryLinks=false;
    paginationConfig.rotate=false;
    $scope.maxSize = 15;
    paginationConfig.itemsPerPage = $scope.num_items_pagina;

	
	$scope.pageChangedDesc = function(current_page){

    	if($scope.tipoBusqueda == ""){
    		if($scope.textoBusqueda == ""){
    			$scope.busqueda = "/NINGUNO";
    		}else{
    			$scope.tipoDescBus = "NINGUNO";
    			$scope.busqueda = "/"+$scope.tipoDescBus+"/"+$scope.textoBusqueda;
    		}
    	}else{
    		if($scope.textoBusqueda == ""){
    			$scope.busqueda = "/"+$scope.tipoBusqueda;
    		}else{
    			$scope.busqueda = "/"+$scope.tipoBusqueda+"/"+$scope.textoBusqueda;
    		}

    	}

		$http.get(API.url + 'bagtesis/backend/public/obtener_descriptores_sistema'+$scope.busqueda+'?page=' + current_page).success(function(data, status, headers, config) {           
                $scope.desSistemaMostrar =  data.data; 
                $scope.currentPageDesc = data.current_page;
                $scope.totalItemsDesc = data.total;
                $scope.numPagesDesc = data.last_page;
            }).
            error(function(error) {
                $('#loader').modal('hide');
	            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
	            $('#informacion').modal('show');
            });  
	}

	// Tipos de Descriptores
	$scope.obtenerTiposDescriptores = function(){
		$http.get(API.url + 'bagtesis/backend/public/tipo_descriptores').success(function(data, status, headers, config) {           
	        $('#loader').modal('hide');
	        $scope.tipoDescriptores = data; 
	    }).
	    error(function(error) {
	        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
	    });
	}  

    $scope.obtener_descriptores_todos = function(mostrarInfromacion){
    	
    	// Descriptores del recurso
		$http.get(API.url + 'bagtesis/backend/public/descriptores_recurso?id_recurso='+$scope.idrecurso).success(function(data, status, headers, config) {           
	        $scope.descriptoresMostrar = data;

	       	//Descriptores del Sistema 
		    $scope.busqueda = "/NINGUNO";
		    $http.get(API.url + 'bagtesis/backend/public/obtener_descriptores_sistema'+$scope.busqueda).success(function(data, status, headers, config) {           
		        $scope.desSistemaMostrar = data.data;
		        $scope.currentPageDesc = data.current_page;
		       // $scope.contador_caracteres = 0;
		        $scope.totalItemsDesc = data.total;
		        $scope.numPagesDesc = data.last_page;

		        if (mostrarInfromacion){

		    		$('#loader').modal('hide');
		        	$('#informacion').modal('show');
		        }


		        if(!$scope.vieneDeGuardar){
		        	$scope.obtenerTiposDescriptores();	
		        }else{
		        	$scope.vieneDeGuardar = false;
		        }
		      
		      //  $scope.obtenerTiposDescriptores();
		    }).
		    error(function(error) {
		    	$('#loader').modal('hide');
	            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
	            $('#informacion').modal('show');
		    }); 
	    }).
	    error(function(error) {
	        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
	    });

    }
    
    $scope.obtener_descriptores_todos();


    $scope.busqueda_descriptores_sistema = function(){
    	$('#loader').modal('show');
    	$scope.textoBusqueda = $scope.textoBusqueda.trim();
    	if($scope.tipoBusqueda == ""){
    		$scope.tipoDescBus = "NINGUNO";
    	}else{
    		$scope.tipoDescBus = $scope.tipoBusqueda;
    	}

    	if($scope.textoBusqueda == ""){
    		$scope.busqueda = "/"+$scope.tipoDescBus;
    	}else{
    		$scope.busqueda = "/"+$scope.tipoDescBus+"/"+$scope.textoBusqueda;
    	}

 
    	
	    $http.get(API.url + 'bagtesis/backend/public/obtener_descriptores_sistema'+$scope.busqueda).success(function(data, status, headers, config) {           
	        $scope.desSistemaMostrar = data.data;
                $scope.currentPageDesc = data.current_page;
                $scope.totalItemsDesc = data.total;
                $scope.numPagesDesc = data.last_page;
                $('#loader').modal('hide');
                if($scope.totalItemsDesc == 0){
                	$scope.msjInformacion = 'Disculpe, no se encontraron coincidencias';
                	$('#informacion').modal('show');
                }
	         
	    }).
	    error(function(error) {
	        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
	    });
    }

    $scope.busqueda_descriptores_tipo = function(){
    	$('#loader').modal('show');
    	if($scope.tipoBusqueda == ""){
    		$scope.tipoDescBus = "NINGUNO";
    	}else{
    		$scope.tipoDescBus = $scope.tipoBusqueda;
    	}

    	$scope.textoBusqueda = "";

    	$scope.busqueda = "/"+$scope.tipoDescBus;

	    $http.get(API.url + 'bagtesis/backend/public/obtener_descriptores_sistema'+$scope.busqueda).success(function(data, status, headers, config) {           
	        	$scope.desSistemaMostrar = data.data;
                $scope.currentPageDesc = data.current_page;
                $scope.totalItemsDesc = data.total;
                $scope.numPagesDesc = data.last_page;
                $('#loader').modal('hide');
                if($scope.totalItemsDesc == 0){
                	$scope.msjInformacion = 'Disculpe, no se encontraron coincidencias';
                	$('#informacion').modal('show');
                }
	         
	    }).
	    error(function(error) {
	        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
	    });
    }


    $scope.seleccionarTipoDesc = function(){
    	if($scope.tipoDescriptor == "M-650"){
    		$scope.esMateriaTematica = true;
    	}else{
    		$scope.esMateriaTematica = false;
    	}

    }


	$scope.seleccionarDesc = function(desc, indice){
	    $scope.descSel = indice;
	    $scope.descSisSel = -1;
	    $scope.esDelRecurso = true;
	    $scope.esDelSistema = false;
	    $scope.descSelec = desc;
	    $scope.temDesc = "";
    	$scope.genDesc = "";
    	$scope.geoDesc = "";
    	console.log(JSON.stringify($scope.descSelec));

	    if(desc.tipo == "M-650"){
	    	$scope.esMateriaTematica = true;
	    	$scope.aux_tematica = desc.descriptor_salida.split(" - ");
	    	$scope.temDesc = $scope.aux_tematica[0];
	    	if($scope.aux_tematica.length > 1){
	    		$scope.genDesc = $scope.aux_tematica[1];
	    	}
	    	if($scope.aux_tematica.length > 2){
	    		$scope.geoDesc = $scope.aux_tematica[2];
	    	}
	    	$scope.tipoDescriptor = desc.tipo;
		    $scope.idDescriptor = desc.id;
		    $scope.nombreDescriptor = "";
		    $scope.descDescriptores = "";
			$scope.codDescriptores = "";
		    $scope.botonAgregarDescriptor = true;

	    }else{
	    	$scope.esMateriaTematica = false;
		    $scope.nombreDescriptor = desc.descriptor_salida;
		    $scope.descDescriptores = desc.describe;
			$scope.codDescriptores = desc.codigo_externo;
			$scope.tipoDescriptor = desc.tipo;
		    $scope.botonAgregarDescriptor = true;
		    $scope.idDescriptor = desc.id;
		    $scope.aux_tematica = [];
	    	$scope.temDesc = "";
	    	$scope.genDesc = "";
	    	$scope.geoDesc = "";
		}

	};

	$scope.seleccionarDescSis = function(desc, indice){
	    $scope.descSisSel = indice;
	    $scope.descSistSelec = desc;
	    $scope.descSel = -1;
	    $scope.esDelRecurso = false;
	    if(!$scope.validarExistenciaDes($scope.descSistSelec.id)){
	     	$scope.esDelSistema = true;
	    }else{
	    	$scope.esDelSistema = false;
	    }
	    

	    console.log(JSON.stringify($scope.descSistSelec));

	    if(desc.tipo == "M-650"){
	    	$scope.esMateriaTematica = true;
	    	$scope.aux_tematica = desc.descriptor_salida.split(" - ");
	    	$scope.temDesc = $scope.aux_tematica[0];
	    	if($scope.aux_tematica.length > 1){
	    		$scope.genDesc = $scope.aux_tematica[1];
	    	}
	    	if($scope.aux_tematica.length > 2){
	    		$scope.geoDesc = $scope.aux_tematica[2];
	    	}
	    	$scope.tipoDescriptor = desc.tipo;
		    $scope.idDescriptor = desc.id;
		    $scope.nombreDescriptor = "";
		    $scope.descDescriptores = "";
			$scope.codDescriptores = "";
		    $scope.botonAgregarDescriptor = true;

	    }else{
	    	$scope.esMateriaTematica = false;
		    $scope.nombreDescriptor = desc.descriptor_salida;
		    $scope.descDescriptores = desc.describe;
			$scope.codDescriptores = desc.codigo_externo;
			$scope.tipoDescriptor = desc.tipo;
		    $scope.botonAgregarDescriptor = true;
		    $scope.idDescriptor = desc.id;
		    $scope.aux_tematica = [];
	    	$scope.temDesc = "";
	    	$scope.genDesc = "";
	    	$scope.geoDesc = "";
		}

	};

	$scope.limpiarBusqueda = function(){
		$scope.tipoBusqueda = "";
		$scope.tipoDescBus = "";
		$scope.textoBusqueda = "";
		$scope.busqueda = "/NINGUNO";

    	$http.get(API.url + 'bagtesis/backend/public/obtener_descriptores_sistema'+$scope.busqueda).success(function(data, status, headers, config) {           
        $scope.desSistemaMostrar = data.data;
        $scope.currentPageDesc = data.current_page;
        $scope.totalItemsDesc = data.total;
        $scope.numPagesDesc = data.last_page;
         
	    }).
	    error(function(error) {
	        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
	    });
	}

	$scope.limpiar_todo = function(){
		$('#confirmacion').modal('hide');
    	$('#loader').modal('show');
		$scope.limpiarDescriptores();
        $scope.obtener_descriptores_todos(true);
	}

	$scope.limpiarDescriptores = function(){
	    $scope.nombreDescriptor = "";
	    $scope.descDescriptores = "";
		$scope.codDescriptores = "";
		$scope.tipoDescriptor = "";
	    $scope.botonAgregarDescriptor = false;
	    $scope.esDelRecurso = false;
	    $scope.esDelSistema = false;
	    $scope.busqueda = "";
		$scope.tipoBusqueda = "";
		$scope.tipoDescBus = "";
		$scope.textoBusqueda = "";
		$scope.descSel = -1;
		$scope.descSisSel = -1;
		$scope.aux_tematica = [];
    	$scope.temDesc = "";
    	$scope.genDesc = "";
    	$scope.geoDesc = "";
    	$scope.idDescriptor = "";
    	$scope.esMateriaTematica = false;

	}


    $scope.agregarDescDocumento = function(){
	    if(!$scope.validarExistenciaDes($scope.descSistSelec.id)){
	            $scope.descriptoresMostrar.push($scope.descSistSelec);
	            $scope.esDelSistema = false;
	            $scope.bool_agregar_derecurso = 1;
	            $scope.agregar_descriptor();
	    }
	}

	$scope.agregarDescSistema = function(){
	        $scope.descriptoresMostrar = $scope.eliminarObjetoArray($scope.descriptoresMostrar, $scope.descSelec.id);
	        $scope.descSel = -1;
	        $scope.bool_eliminar_derecurso = 1;
	        $scope.eliminarDescriptor();
	}

	$scope.eliminarObjetoArray = function(array, item){   
	    arrayRemoved = array.filter(function(el) {
	        return el.id !== item;
	    });
	    return arrayRemoved;
	}

	$scope.validarExistenciaDes = function(idDesc){
	     array = $scope.descriptoresMostrar.filter(function(el) {
	        return el.id == idDesc;
	     });
	    if(array.length > 0){
	    	console.log("Si existe en los recursos del documento");
	        return true;
	    }else{
	    	console.log("No existe en los recursos del documento");
	    } return false;
	}


	$scope.submitFormluarioDescriptor=function(){
         $('#confirmacionSub').modal('show');
    }

    $scope.agregar_descriptor = function(){
    	$('#confirmacionSub').modal('hide');
    	$('#loader').modal('show');
    	$scope.dataDesc = {};
        $scope.dataDesc["id_recurso"] = $scope.idrecurso;
        $scope.dataDesc["tipo_descriptor"] = $scope.tipoDescriptor;
        $scope.dataDesc["temDesc"] = $scope.temDesc;
        $scope.dataDesc["genDesc"] =  $scope.genDesc;
        $scope.dataDesc["geoDesc"] = $scope.geoDesc;
        $scope.dataDesc["descriptor"] = $scope.nombreDescriptor;
        $scope.dataDesc["describe"] = $scope.descDescriptores;
        $scope.dataDesc["codigoExterno"] =  $scope.codDescriptores;

        $scope.dataDesc["idCatalogador"] = $cookieStore.get('user_id');
        //alert(JSON.stringify($scope.dataDesc));

        $http.post(API.url + 'bagtesis/backend/public/agregar_descriptor', $scope.dataDesc).success(function(data, status, headers, config) {           
	        //alert(JSON.stringify(data));
	        
          	if( $scope.bool_agregar_derecurso == 1 ){
          		$('#loader').modal('hide');
          		$scope.bool_agregar_derecurso = 0;
          	} else  if(data["codigo"] == "0"){  
                $scope.limpiarDescriptores();
                $scope.msjInformacion = 'Descriptor agregado exitosamente';
                $scope.obtener_descriptores_todos(true);
                 
            }else if(data["codigo"] == "1"){
                $scope.msjInformacion = 'El descriptor ya existe';
            	$scope.obtener_descriptores_todos(true);
            }

	         
	    }).
	    error(function(error) {
	        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
	    });
    }

    $scope.guardar_descriptor = function(){
    	$('#confirmacionSub').modal('hide');
    	$('#loader').modal('show');
    	$scope.dataDesc = {};
        $scope.dataDesc["id_recurso"] = $scope.idrecurso;
        $scope.dataDesc["tipo_descriptor"] = $scope.tipoDescriptor;
        $scope.dataDesc["temDesc"] = $scope.temDesc;
        $scope.dataDesc["genDesc"] =  $scope.genDesc;
        $scope.dataDesc["geoDesc"] = $scope.geoDesc;
        $scope.dataDesc["descriptor"] = $scope.nombreDescriptor;
        $scope.dataDesc["describe"] = $scope.descDescriptores;
        $scope.dataDesc["codigoExterno"] =  $scope.codDescriptores;
        $scope.dataDesc["id_descriptor"] =  $scope.idDescriptor;
        $scope.dataDesc["descriptores"] = $scope.descriptoresMostrar;

        $scope.dataDesc["idCatalogador"] = $cookieStore.get('user_id');
        //alert(JSON.stringify($scope.dataDesc));
        //return;

        $http.post(API.url + 'bagtesis/backend/public/guardar_descriptores_recurso', $scope.dataDesc).success(function(data, status, headers, config) {           
	        
	        if(data["codigo"] == "0"){  
	        	console.log(JSON.stringify(data));
	        	if($scope.descSel == -1){
                //	$scope.limpiarDescriptores();
	        	}
                $scope.msjInformacion = 'Descriptor modificado exitosamente';
                $scope.vieneDeGuardar = true;
                $scope.obtener_descriptores_todos(true);
                 
            }else{
            	$('#loader').modal('hide');
            	$scope.limpiarDescriptores();
            }

	    }).
	    error(function(error) {
	        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
	    });

    }



    $scope.eliminarDescriptor = function(){ 
        $('#confirmacionEliminar').modal('hide');
        $('#loader').modal('show');
        $scope.descriptorRecurso = {};
        $scope.descriptorRecurso["id_recurso"] = $scope.idrecurso;
        $scope.descriptorRecurso["id_descriptor"] = $scope.idDescriptor;
        $scope.descriptorRecurso["tipo_descriptor"] = $scope.tipoDescriptor;
        $scope.descriptorRecurso["bool_eliminar_derecurso"] = $scope.bool_eliminar_derecurso;
        
        $scope.descriptorRecurso["idCatalogador"] = $cookieStore.get('user_id');
        //alert(JSON.stringify($scope.descriptorRecurso));
        

        $http.post(API.url+'bagtesis/backend/public/eliminar_descriptores_recurso', $scope.descriptorRecurso)
            .success(function(data, status, headers, config) {     
                //alert(JSON.stringify(data));
                //return;
                
          		if( $scope.bool_eliminar_derecurso == 1 ){
          			$scope.limpiarDescriptores();
          			$('#loader').modal('hide');
          			$scope.bool_eliminar_derecurso = 0;
          		}else
                if(data["codigo"] == "0"){
                    $scope.limpiarDescriptores();
                    $scope.msjInformacion = 'Descriptor eliminado exitosamente' ;
                    $scope.obtener_descriptores_todos(true);

                }else if(data["codigo"] == "1"){
                	$('#loader').modal('hide');
                	$scope.msjInformacion = 'El descriptor no se puede eliminar debido a que se encuentra a asociado a otros recursos' ;
                    $('#informacion').modal('show');

                }

            }).error(function(error) {
            	$('#loader').modal('hide');
	            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
	            $('#informacion').modal('show');
            });

    }

    $scope.loadingStatesDescriptor = function(val){
        console.log("val "+val);
        $scope.loadingDescriptor = true;
        return $scope.buscar_descriptor(val); 
    };

    $scope.buscar_descriptor = function (val){
        console.log("Entro a buscar_descriptor");
        $scope.datosAutocompletarDescriptor = {};
        $scope.datosAutocompletarDescriptor['tipo'] = $scope.tipoDescriptor
        $scope.datosAutocompletarDescriptor['data'] = val;
        if( $scope.tipoDescriptor == "" || $scope.tipoDescriptor == null || $scope.tipoDescriptor == undefined){
			$scope.datosAutocompletarDescriptor['tipo'] = "";
		}else if( $scope.tipoDescriptor == "M-650"){
			$scope.datosAutocompletarDescriptor['tipo'] = "M-650-A";
		}

        $scope.promise = $http.post(API.url+'bagtesis/backend/public/autocompletado_descriptor_typeahead', $scope.datosAutocompletarDescriptor)
                      .success(function(data, status, headers, config) {
                          console.log(JSON.stringify(data));
                          $scope.prom = data;
                          $scope.loadingDescriptor = false;
                      }).then(function(){
                         
                          $scope.aux_promise = $scope.prom;  
                          return $scope.prom;
                      });
        
        return $scope.promise;
    };

    $scope.onselectDescriptor = function (){
    	console.log($scope.nombreDescriptor);
    	if($scope.esMateriaTematica == false){
    		if($scope.nombreDescriptor.tipo== "M-650-A"){
	    		$scope.esMateriaTematica = true;
	        	$scope.temDesc = $scope.nombreDescriptor.descriptor_salida;
	    		$scope.tipoDescriptor = "M-650";
	    	}else{
	    		$scope.esMateriaTematica = false;
	    		$scope.tipoDescriptor = $scope.nombreDescriptor.tipo;
		        $scope.descDescriptores = $scope.nombreDescriptor.descripcion;
		        $scope.codDescriptores = $scope.nombreDescriptor.codigo;
		        $scope.nombreDescriptor = $scope.nombreDescriptor.descriptor_salida;
	    	}
    	}else{
    		if($scope.temDesc.tipo== "M-650-A"){
	    		$scope.esMateriaTematica = true;
	        	$scope.temDesc = $scope.temDesc.descriptor_salida;
	    		$scope.tipoDescriptor = "M-650";
	    	}else{
	    		$scope.esMateriaTematica = false;
	    		$scope.tipoDescriptor = $scope.temDesc.tipo;
		        $scope.descDescriptores = $scope.temDesc.descripcion;
		        $scope.codDescriptores = $scope.temDesc.codigo;
		        $scope.nombreDescriptor = $scope.temDesc.descriptor_salida;
	    	}
    	}
        
    };


    $scope.auxTrim = "";

    $scope.trimRequired = function(model) {
        
        if (model == "nombreDescriptor" ){
            if($scope.cotaLibro){
                $scope.auxTrim = $scope.nombreDescriptor.trim();
            }else{
                $scope.auxTrim = "";
            }
        }
    }

    $scope.blurRequired = function (model) {
        if (model == "nombreDescriptor"){
            $scope.nombreDescriptor = $scope.auxTrim;
        }
        $scope.auxTrim = "";
      
    }


});