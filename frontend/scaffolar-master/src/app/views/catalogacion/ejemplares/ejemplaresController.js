angular.module('app' )
  .controller('ejemplaresController', function($scope, $window, $http, API, $cookieStore, upload, $state, IO_BARCODE_TYPES) {
    $scope.idrecurso="";
    $scope.tipo_recurso = $cookieStore.get('tipo_recurso');
    $scope.tipo_recurso_ab =  ($scope.tipo_recurso.substring(0, 1)).toUpperCase();
    if( $scope.tipo_recurso_ab == 'O')
        $scope.tipo_recurso_ab = $cookieStore.get('tipo_recurso_id'); // validacion  agregada para el modulo de otros recursos
    if ($scope.tipo_recurso == "monografia"){
        $scope.nombre_recurso =  "Monografías";
        $scope.cota =  $cookieStore.get('cota');
        $scope.idrecurso = $cookieStore.get('id_recurso_libro');
        $scope.redirect = "libros";
    }else if ($scope.tipo_recurso == "tesis"){
        $scope.nombre_recurso =  "Trabajos Académicos";
        $scope.cota =  $cookieStore.get('cota');
        $scope.idrecurso = $cookieStore.get('id_recurso_tesis');
        $scope.redirect = "trabajosAcademicos";
    }else if ($scope.tipo_recurso == "serie"){
        $scope.idrecurso = $cookieStore.get('id_recurso_publicacion');
        $cookieStore.put('cota','');
        $scope.nombre_recurso =  "Publicaciones Seriadas";

        $scope.cota =  ""; 
        $scope.redirect = "publicacionesSeriadas";    
    }else if ($scope.tipo_recurso == "otros"){
        $scope.idrecurso = $cookieStore.get('id_recurso_otros');
        $cookieStore.put('cota','');
        $scope.cota =  ""; 
        $scope.nombre_recurso =  "Otros Recursos"; 
        $scope.redirect = "otrosRecursos";    
    }
    if(!$scope.idrecurso){
         return $state.go('home', {}, { reload: true });
    }
    $scope.ejemplares= [];
    $scope.estadoActual = "";
    $scope.estados = [];
    $scope.salaActual = "";
    $scope.encuadernacion = "";
    $scope.propietario = "";
    $scope.adquisicion = "";
    $scope.proveedor = "";
    $scope.reservacion = 0;
    $scope.etiqueta = "";
    $scope.prestamo = "";
    ejemplarActual ="";
    $scope.claseAzul = "";
    agregarEjemplar = false;
    eliminarEjemplar = false;
    editarEjemplar = false;
    $scope.esElectronico = 0;
    $scope.doi = "";
    $scope.nombreDocElectronico = "";
    $scope.nombreArchivo = "";
    $scope.archivoCargado = false;
    $scope.idEjemplar = "";
    $cota = $cookieStore.get('cota');
    $scope.mostrarFecha = false;

    $scope.types = IO_BARCODE_TYPES;
    console.log(JSON.stringify(IO_BARCODE_TYPES));

    /*** cargar recurso electronico */
        
    $scope.fileName= function(element) {
        $scope.$apply(function($scope) {
            $scope.nombreArchivo = $scope.idEjemplar + '_' + element.files[0].name + " ";
        });
    };

    $scope.uploadFile = function(){
        $('#loader').modal('show');
        var file = $scope.file;
        var name = $scope.name;
        console.log(name);
        console.log(file);
        var ruta = 'bagtesis/backend/public/cargar_recurso/'+$scope.idEjemplar+'/';
        upload.uploadFile(file,name, ruta).then(function(res){
            console.log(res);
            $scope.archivoCargado = true;
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Archivo cargado exitosamente' ;
            $('#informacion').modal('show');
            $scope.esElectronico = 1;
            $scope.nombreArchivo = res.nombre;
           
            
        })
    }

    $scope.checkRecursoElectronico = function(){
      if($scope.esElectronico == 1){
        $scope.esElectronico = 0;
        $scope.doi = ""; 
      }else{

        $scope.esElectronico = 1;
      }
    }
    $scope.eliminarArchivo = function(){
        $('#confirmacionCargarArchivo').modal('hide');
        $http.get(API.url+'bagtesis/backend/public/eliminar_carga_recurso/'+$scope.idEjemplar+'/'+$scope.nombreArchivo).
            success(function(data, status, headers, config) {
                $scope.msjInformacion = 'Archivo eliminado exitosamente' ;
                $('#informacion').modal('show');   
                $scope.nombreArchivo = "";
                $scope.archivoCargado = false;
                angular.forEach(
                    angular.element("input[type='file']"),
                    function(inputElem) {
                      angular.element(inputElem).val(null);
                    });
            }).
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            }); 
    }


    $scope.limpiarArchivo = function(){
        $scope.nombreArchivo = "";
        $scope.archivoCargado = false;
        angular.forEach(
            angular.element("input[type='file']"),
            function(inputElem) {
              angular.element(inputElem).val(null);
            });
        
    }

    $scope.descargarRecurso = function(){
        //window.location.href = API.url+ 'bagtesis/backend/app/storage/public/catalogacion/'+  $scope.nombreArchivo;
        window.open(API.url+ 'bagtesis/backend/app/storage/public/catalogacion/'+  $scope.nombreArchivo,'_blank');
    }

    $scope.verificarReservacion = function(){
        if($scope.reservacion ==1)
            return true;
        else
            return false;
    }
    $scope.obtenerEjemplares = function(){
        $http.post(API.url + 'bagtesis/backend/public/ejemplares/obtener_ejemplares?idrecurso='+$scope.idrecurso).success(function(data, status, headers, config) { 
            $('#loader').modal('hide');           
            $scope.ejemplares = data;  
            console.log(JSON.stringify(data));
            if(agregarEjemplar){
                $scope.msjInformacion = 'Ejemplar agregado exitosamente' ;
                $('#informacion').modal('show');
                agregarEjemplar = false;  
                
            }
            if(eliminarEjemplar){
                $scope.msjInformacion = 'Ejemplar eliminado exitosamente' ;
                $('#informacion').modal('show'); 
                eliminarEjemplar = false; 
                $scope.limpiarEjemplar();

            }
            if(editarEjemplar){
                $scope.msjInformacion = 'Ejemplar editado exitosamente' ;
                $('#informacion').modal('show'); 
                editarEjemplar = false; 
            }
        }).
        error(function(error) {   
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        }); 
    }  

    if($scope.idrecurso){
        $('#loader').modal('show'); 
        $scope.obtenerEjemplares();   
    } 

    $http.get(API.url + 'bagtesis/backend/public/ejemplares_tipoestado/obtener_estados_ejemplares').success(function(data, status, headers, config) {         
        $scope.estados = data;  
    }).
    error(function(error) {   
        $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });         



    $scope.agregarClaseFilaEjemplar = function(ejemplar){
        if(ejemplarActual == ejemplar.id)
           return "seleccionFila";
        else 
            return "";
    }

    $scope.encenderEjemplar = function(ejemplar, indice){
        $scope.limpiarEjemplar();
        $scope.claseAzul = "seleccionFila";
        ejemplarActual = ejemplar.id;
        $scope.etiqueta = ejemplar.id;

        if(ejemplar.estado)
            $scope.estadoActual = ejemplar.estado;
        if(ejemplar.prestamo)
            $scope.prestamo = ejemplar.prestamo;
        if(ejemplar.tipo_encuader)
            $scope.encuadernacion = ejemplar.tipo_encuader;
        if(ejemplar.propiedad)
            $scope.propietario = ejemplar.propiedad;
        if(ejemplar.forma_adquisicion)
            $scope.adquisicion =  ejemplar.forma_adquisicion;
        if(ejemplar.proveedor)
            $scope.proveedor = ejemplar.proveedor;
        if(ejemplar.reserva)
            $scope.reservacion =  ejemplar.reserva;
        if(ejemplar.nro_inventario){
            $scope.nro_inventario = ejemplar.nro_inventario;
            $scope.mostrarFecha = true;
        }else{
            $scope.mostrarFecha = false;
        }
        $scope.esElectronico = ejemplar.doc_electronico;
        $scope.nombreArchivo = ejemplar.nombre_doc_electronico;
        $scope.doi = ejemplar.doi;
        $scope.idEjemplar = ejemplar.id;
        if ($scope.nombreArchivo && $scope.esElectronico == 1)
            $scope.archivoCargado = true;
        else
            $scope.archivoCargado = false;
    }

    $scope.agregarEjemplar = function(){
        $('#confirmacionEjemplar').modal('hide');
        $('#loader').modal('show'); 
        $scope.dataEjemplar = {};
     //   $scope.dataEjemplar["etiqueta"] = $scope.etiqueta;
        if($scope.estadoActual == ""){
            $scope.estadoActual = 'N';    
        }
        $scope.dataEjemplar["idRecurso"] = $scope.idrecurso;
        $scope.dataEjemplar["estado"] = $scope.estadoActual;
        $scope.dataEjemplar["prestamo"] =  $scope.prestamo;
        $scope.dataEjemplar["tipoencuader"] = $scope.encuadernacion;
        $scope.dataEjemplar["propietario"] = $scope.propietario;
        $scope.dataEjemplar["adquisicion"] =  $scope.adquisicion ;
        $scope.dataEjemplar["proveedor"] = $scope.proveedor;
        $scope.dataEjemplar["reserva"] =  $scope.reservacion;
        $scope.dataEjemplar["tipo_liter"] =  $scope.tipo_recurso_ab;
        $scope.dataEjemplar["idCatalogador"] = $cookieStore.get('user_id');
        $scope.dataEjemplar["esElectronico"] = $scope.esElectronico;
        $scope.dataEjemplar["nombreDocElectronico"] = "";
        
        if($scope.esElectronico == 1){
            $scope.dataEjemplar["doi"] = $scope.doi;
        }else{
            $scope.dataEjemplar["doi"] = "";
        }

        $http.post(API.url + 'bagtesis/backend/public/ejemplares/agregar_ejemplar', $scope.dataEjemplar).success(function(data, status, headers, config) {     
                if(data["codigo"] == "0"){
                    agregarEjemplar = true;
                    $scope.obtenerEjemplares();
                    $scope.etiqueta = data["id_ejemplar"];
                    ejemplarActual = data["id_ejemplar"];
                    $scope.claseAzul = "seleccionFila";
                }else{
                    $('#loader').modal('hide');
                }             
        }).
        error(function(error) {   
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });    
    
    }

/*

  if($scope.esElectronico == 1 && $scope.nombreArchivo.length > 0 && !$scope.archivoCargado){
            $scope.msjInformacion = 'Por favor cargue o limpie el archivo seleccionado antes de proceder a guardar el recurso';
            $('#informacion').modal('show');                  
        }else{
        $scope.msjConfirmacion = '¿Está seguro que desea eliminar el ejemplar?';
        $('#confirmacionEliminarEjemplar').modal('show');


        }*/


    $scope.submitEjemplares = function(){
        if($scope.msjConfirmacion == '¿Está seguro que desea modificar este ejemplar?' && $scope.esElectronico == 1 && $scope.nombreArchivo.length > 0 && !$scope.archivoCargado){
            $scope.msjInformacion = 'Por favor cargue o limpie el archivo seleccionado antes de proceder a guardar el ejemplar';
            $('#informacion').modal('show');                  
        }else
            $('#confirmacionEjemplar').modal('show');
    }

    $scope.eliminarEjemplar = function(){
        $('#confirmacionEjemplar').modal('hide');
        $('#loader').modal('show'); 
        $scope.dataEjemplar = {};
        $scope.dataEjemplar["idEjemplar"] = ejemplarActual;
        $scope.dataEjemplar["idCatalogador"] = $cookieStore.get('user_id');
        $scope.dataEjemplar["idRecurso"] = $scope.idrecurso;
        $http.post(API.url + 'bagtesis/backend/public/ejemplares/eliminar_ejemplar', $scope.dataEjemplar).success(function(data, status, headers, config) {     
                if(data["codigo"] == "0"){
                    eliminarEjemplar = true;
                    $scope.obtenerEjemplares();
                   
                }else{
                    $('#loader').modal('hide');
                }             
        }).
        error(function(error) {   
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });    
    
    }

    $scope.limpiarEjemplar = function(){
        $('#confirmacionEjemplar').modal('hide');
        $scope.estadoActual = "";
        $scope.encuadernacion = "";
        $scope.propietario = "";
        $scope.adquisicion = "";
        $scope.proveedor = "";
        $scope.reservacion = 0;
        $scope.etiqueta = "";
        $scope.prestamo = "";
        ejemplarActual ="";
        $scope.claseAzul = "";
        $scope.esElectronico = 0;
        $scope.doi = "";
        $scope.nombreDocElectronico = "";
        $scope.nombreArchivo = "";
        $scope.archivoCargado = false;
        $scope.idEjemplar = "";
        $scope.nro_inventario = "";
        $scope.mostrarFecha = false;

    }

    $scope.guardarEjemplar = function(){
        $('#confirmacionEjemplar').modal('hide');
        $('#loader').modal('show'); 
        $scope.dataEjemplar = {};
     //   $scope.dataEjemplar["etiqueta"] = $scope.etiqueta;
        if($scope.estadoActual == ""){
            $scope.estadoActual = 'N';    
        }
        $scope.dataEjemplar["idEjemplar"] = ejemplarActual;
        $scope.dataEjemplar["estado"] = $scope.estadoActual;
        $scope.dataEjemplar["prestamo"] =  $scope.prestamo;
        $scope.dataEjemplar["tipoencuader"] = $scope.encuadernacion;
        $scope.dataEjemplar["propietario"] = $scope.propietario;
        $scope.dataEjemplar["adquisicion"] =  $scope.adquisicion ;
        $scope.dataEjemplar["proveedor"] = $scope.proveedor;
        $scope.dataEjemplar["reserva"] =  $scope.reservacion;
        $scope.dataEjemplar["tipo_liter"] =  $scope.tipo_recurso_ab;
        $scope.dataEjemplar["idCatalogador"] = $cookieStore.get('user_id');
        $scope.dataEjemplar["esElectronico"] = $scope.esElectronico;

        if($scope.esElectronico == 1){
            $scope.dataEjemplar["doi"] = $scope.doi;
            if($scope.archivoCargado)
                $scope.dataEjemplar["nombreDocElectronico"] = $scope.nombreArchivo;
            else
                $scope.dataEjemplar["nombreDocElectronico"] = "";

        }else{
            $scope.dataEjemplar["doi"] = "";
            $scope.dataEjemplar["nombreDocElectronico"] = "";
        }
        
        
        $scope.dataEjemplar["idRecurso"] = $scope.idrecurso;

        $http.post(API.url + 'bagtesis/backend/public/ejemplares/modificar_ejemplar', $scope.dataEjemplar).success(function(data, status, headers, config) {   
                if(data["codigo"] == "0"){

                    editarEjemplar = true;
                    $scope.obtenerEjemplares();
                }else{
                    $('#loader').modal('hide');
                }             
        }).
        error(function(error) {   
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });   
        
    }

    //["EAN","UPC","ITF","ITF14","CODE39","CODE128B","CODE128C","Pharmacode"]
      $scope.tipo_codigo_barra = "CODE39";
      $scope.diseno_codigo_barra = {
        width: 1,
        height: 50,
        displayValue: true,
        font: 'monospace',
        textAlign: 'center',
        fontSize: 15,
        backgroundColor: 'white',
        lineColor: 'black'
    }

    $scope.mostrar_codigo_barra = function(codigo_ejemplar){
        console.log(codigo_ejemplar);
        if(codigo_ejemplar){
            $('#imprimir_etiqueta').modal('show');
             $scope.cod_ejemplar = codigo_ejemplar;

        }

    }

    $scope.imprimir = function(){

    }
});

(function (angular) {
    'use strict';

    function printDirective() {
        var printSection = document.getElementById('printSection');

        // if there is no printing section, create one

        if (!printSection) {
            printSection = document.createElement('div');
            printSection.id = 'printSection';
            document.body.appendChild(printSection);
        }

        function link(scope, element, attrs) {
            element.on('click', function () {
                var elemToPrint = document.getElementById(attrs.printElementId);
                if (elemToPrint) {
                    printElement(elemToPrint);
                    window.print();
                    printSection.innerHTML = '';
                }
            });

            window.onafterprint = function () {
                // clean the print section before adding new content
                printSection.innerHTML = '';
            }
        }

        function printElement(elem) {
            // clones the element you want to print
            var domClone = elem.cloneNode(true);
            printSection.appendChild(domClone);
        }

        return {
            link: link,
            restrict: 'A'
        };
    }

    angular.module('app').directive('ngPrint', [printDirective]);
}(window.angular));
