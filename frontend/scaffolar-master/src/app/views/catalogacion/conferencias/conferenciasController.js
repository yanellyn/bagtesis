angular.module('app' )
  .controller('conferenciasController', function($scope, $window, $http, API, $cookieStore, $state) {
    $scope.idrecurso="";
    $scope.tipo_recurso = $cookieStore.get('tipo_recurso');
    $scope.tipo_recurso_ab =  ($scope.tipo_recurso.substring(0, 1)).toUpperCase();
    if( $scope.tipo_recurso_ab == 'O')
        $scope.tipo_recurso_ab = $cookieStore.get('tipo_recurso_id'); // validacion  agregada para el modulo de otros recursos
    if ($scope.tipo_recurso == "monografia"){
        $scope.nombre_recurso =  "Monografías";
        $scope.idrecurso = $cookieStore.get('id_recurso_libro');
        $scope.redirect = "libros";
    }else if ($scope.tipo_recurso == "tesis"){
        $scope.nombre_recurso =  "Trabajos Académicos";
        $scope.idrecurso = $cookieStore.get('id_recurso_tesis');
        $scope.redirect = "trabajosAcademicos";
    }else if ($scope.tipo_recurso == "serie"){
        $scope.idrecurso = $cookieStore.get('id_recurso_publicacion');
        $scope.nombre_recurso =  "Publicaciones Seriadas"; 
        $scope.redirect = "publicacionesSeriadas";    
    }else if ($scope.tipo_recurso == "otros"){
        $scope.idrecurso = $cookieStore.get('id_recurso_otros');
        $scope.nombre_recurso =  "Otros Recursos"; 
        $scope.redirect = "otrosRecursos";    
    }
    if(!$scope.idrecurso){
         return $state.go('home', {}, { reload: true });
    }
    $('#loader').modal('show');

    $scope.paises = "";
    $scope.nombreConf = "";
    $scope.ciudadConf = "";
    $scope.patrocinadorConf = "";
    $scope.fechaConf = "";
    $scope.paisConf = "";
    $scope.esAgregarConferencia = false;
    $scope.botonAgregarConf = false;
    $scope.msjInformacion="";
    $scope.msjConfirmacion="";
    $scope.loading = false;

    $scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

      $scope.selecionarConferencia = function(){
        $scope.ciudadConf =  $scope.nombreConf.ciudad;
        $scope.patrocinadorConf =  $scope.nombreConf.patrocinador;
        $scope.fechaConf = $scope.setDate( $scope.nombreConf.fecha);
        $scope.paisConf =  $scope.nombreConf.pais_id;
    }

     $scope.loadingStates   = function(val){        
            return $scope.buscar_conferencia(val); 
        
    };

    $scope.buscar_conferencia = function (val){
        $scope.loading = true;
        console.log("Entro a buscar autor");
        $scope.mensaje_buscador_simple = {};
        $scope.mensaje_buscador_simple['data'] = val;

        //$scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_autor_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
        $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/autocompletado_conferencia_typeahead', $scope.mensaje_buscador_simple)
                      .success(function(data, status, headers, config) {
                          console.log(JSON.stringify(data));
                          $scope.prom = data;
                          $scope.loading = false;
                      }).then(function(){
                         
                          $scope.aux_promise = $scope.prom;  
                          return $scope.prom;
                      });
        return $scope.promise;
    };

    

    $scope.setDate = function(fecha_ed) {
        if(fecha_ed != "" ){
            var dd = fecha_ed.substring(6, 8);
            var mm = fecha_ed.substring(4, 6); //January is 0!
            var yyyy =  fecha_ed.substring(0, 4);       
            dt_destt = dd+'-'+mm+'-'+yyyy;
            return dt_destt;
        }else{
            return "";
        }
    };


    $scope.obtener_conferencia = function(){
        $http.get(API.url+'bagtesis/backend/public/conferencias/'+$scope.idrecurso).
            success(function(data, status, headers, config) {
                if(data["codigo"] == "0"){
                    $scope.nombreConf = data["datos"].conferencia_salida;
                    $scope.ciudadConf = data["datos"].ciudad;
                    $scope.patrocinadorConf = data["datos"].patrocinador;
                    $scope.fechaConf = $scope.setDate(data["datos"].fecha);
                    $scope.paisConf = data["datos"].pais_id;
                     $scope.botonAgregarConf = true;
                    $('#loader').modal('hide');
                }else{
                    $('#loader').modal('hide');
                     $scope.botonAgregarConf = false;
                }

            }).
            error(function(data, status, headers, config) {
               $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });
    }


    $http.get(API.url+'bagtesis/backend/public/paises').
                success(function(data, status, headers, config) {
                    $scope.paises = data;
                }).
                error(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                   
                }); 
    
    $scope.obtener_conferencia();


    $scope.submitFormluarioConferencia=function(){
         $('#confirmacionSub').modal('show');
    }


    $scope.agregar_conferencia = function(){
        $('#confirmacionSub').modal('hide');
        $('#loader').modal('show');
        $scope.nuevaConferencia = {};
        if(typeof  $scope.nombreConf == "object" ){
             $scope.nuevaConferencia['nombreConf'] =  $scope.nombreConf.nombre;
        }else{
             $scope.nuevaConferencia['nombreConf'] =  $scope.nombreConf;    
        }
        $scope.nuevaConferencia['ciudadConf'] =  $scope.ciudadConf;
        $scope.nuevaConferencia['paisConf'] =  $scope.paisConf;
        $scope.nuevaConferencia["idCatalogador"] = $cookieStore.get('user_id');
        if($scope.fechaConf != "") {
            varspl = $scope.fechaConf.split('-');
            $scope.nuevaConferencia['fechaConf'] = varspl[2] + varspl[1] + varspl[0];
        }else{
             $scope.nuevaConferencia['fechaConf'] = "";
        }
        $scope.nuevaConferencia['patrocinadorConf'] =  $scope.patrocinadorConf;
        $scope.nuevaConferencia['idRecurso'] =  $scope.idrecurso;

        $http.post(API.url+'bagtesis/backend/public/conferencias', $scope.nuevaConferencia).
                success(function(data, status, headers, config) {
                   if(data["codigo"] == "0"){
                        $scope.botonAgregarConf = true;
                        $scope.msjInformacion = 'Conferencia agregada exitosamente';
                        $('#loader').modal('hide');
                        $('#informacion').modal('show');
                    }

                }).
                error(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                });
    }

    $scope.eliminar_conferencia = function(){
        $('#confirmacionEliminar').modal('hide');
        $('#loader').modal('show');
        $http.delete(API.url+'bagtesis/backend/public/conferencias/'+$scope.idrecurso,{params: {idCatalogador: $cookieStore.get('user_id')}}).
            success(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                    $scope.botonAgregarConf = false;
                    $scope.limpiar_conferencia();

            }).
            error(function(data, status, headers, config) {
               $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            }); 
    }

    $scope.guardar_conferencia = function(){
        $('#confirmacionSub').modal('hide');
        $('#loader').modal('show');
        $scope.nuevaConferencia = {};
        if(typeof  $scope.nombreConf == "object" ){
             $scope.nuevaConferencia['nombreConf'] =  $scope.nombreConf.nombre;
        }else{
             $scope.nuevaConferencia['nombreConf'] =  $scope.nombreConf;    
        }
        $scope.nuevaConferencia['ciudadConf'] =  $scope.ciudadConf;
        $scope.nuevaConferencia['paisConf'] =  $scope.paisConf;
        $scope.nuevaConferencia["idCatalogador"] = $cookieStore.get('user_id');
        if($scope.fechaConf != "") {
            varspl = $scope.fechaConf.split('-');
            $scope.nuevaConferencia['fechaConf'] = varspl[2] + varspl[1] + varspl[0];
        }else{
             $scope.nuevaConferencia['fechaConf'] = "";
        }
        $scope.nuevaConferencia['patrocinadorConf'] =  $scope.patrocinadorConf;
        $scope.nuevaConferencia['idRecurso'] =  $scope.idrecurso;

        $http.put(API.url+'bagtesis/backend/public/conferencias/'+$scope.idrecurso, $scope.nuevaConferencia).
                success(function(data, status, headers, config) {
                    if(data["codigo"] == "0"){
                        $scope.obtener_conferencia();
                         $scope.msjInformacion = 'Conferencia modificada exitosamente';
                        $('#loader').modal('hide');
                         $('#informacion').modal('show');
                    }

                }).
                error(function(data, status, headers, config) {
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                   
                }); 
        
    }


    $scope.limpiar_conferencia = function(){
        $('#confirmacion').modal('hide');
        $scope.nombreConf = "";
        $scope.ciudadConf = "";
        $scope.patrocinadorConf = "";
        $scope.fechaConf = "";
        $scope.paisConf = "";
        $('#informacion').modal('show');

        
    }

    $scope.orig_change = function(orig){
        var dd = orig.getDate();
        var mm = orig.getMonth()+1; //January is 0!

        var yyyy = orig.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        }
        $scope.fechaConf = dd+'-'+mm+'-'+yyyy;
        console.log($scope.fechaConf);
       
    };

    $scope.open_orig = function(event) {
        if( $scope.opened_orig){
             $scope.opened_orig = false;
        }else{
             $scope.opened_orig = true;
        }
        event.preventDefault();
        event.stopPropagation();
    };

    $scope.dateOptions = {
       formatYear: 'yy',
       startingDay: 1
    };

    $scope.auxTrim = "";

    $scope.trimRequired = function(model) {
        
        if (model == "nombreConf" ){
            if($scope.nombreConf){
                $scope.auxTrim = $scope.nombreConf.trim();
            }else{
                $scope.auxTrim = "";
            }
        } else if (model == "ciudadConf" ){
            if($scope.ciudadConf){
                $scope.auxTrim = $scope.ciudadConf.trim();
            }else{
                $scope.auxTrim = "";
            }
        } 
    }

    $scope.blurRequired = function (model) {
        if (model == "nombreConf"){
            $scope.nombreConf = $scope.auxTrim;
        } else if (model == "ciudadConf"){
            $scope.ciudadConf = $scope.auxTrim;
        } 

        $scope.auxTrim = "";
      
    }

});