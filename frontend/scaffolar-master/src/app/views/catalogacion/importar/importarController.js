angular.module('app')
  .controller('importarController', 

    function($cookieStore, $rootScope, $modal, $scope,$log,$http, paginationConfig, AuthService, API, $cookieStore, upload)  {
   
        $scope.validaciones = [];
        $scope.errores = false;
        $scope.nombreArchivo = '';
        $scope.mostrarTabla = false;
        $scope.tipo_docs = "";
        $http.get(API.url+'bagtesis/backend/public/tipo_docs').
            success(function(data, status, headers, config) {
                $scope.tipo_docs = data;
            }).
            error(function(data, status, headers, config) {

            });
        $scope.descargarFormato = function(){
            window.open(API.url+'bagtesis/backend/app/storage/public/catalogacion/formato_importar_recursos.xlsx');    
        } 

        $scope.uploadFile = function(){
            $('#loader').modal('show');
            var file = $scope.file;
            var name = $scope.name;
            console.log(name);
            console.log(file);
            var ruta = 'bagtesis/backend/public/importar/'; 
            upload.uploadFile(file,name,ruta).then(function(res){
                console.log(res);
                $('#loader').modal('hide');
                $scope.validaciones = res.validaciones;
                if(!$scope.validaciones.length > 0 ){
                    $scope.eliminarArchivo();
                    $scope.msjInformacion = "Los recursos se han importado satisfactoriamente";
                    $('#informacion').modal('show');
                }
            })
        }

        $scope.fileName= function(element) {
            $scope.$apply(function($scope) {
                $scope.nombreArchivo= element.files[0].name;
            });
        };

        $scope.eliminarArchivo = function(){
            $('#confirmacionCargarArchivo').modal('hide');
            $('#confirmacion').modal('hide');
            $scope.nombreArchivo= '';
            $scope.validaciones = [];
            angular.forEach(
            angular.element("input[type='file']"),
            function(inputElem) {
              angular.element(inputElem).val(null);
            });
        }


    });

angular.module('app' ).directive('uploaderModel',function($parse){
    return{
        restrict:'A',
        link: function(scope,iElement,iAttrs){
            iElement.on("change",function(e){
                $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0])
            });

        }
    };
});


angular.module('app').service('upload',function($cookieStore, API,$http,$q){
    this.uploadFile = function(file,name,ruta){
        var deferred = $q.defer();
        var promise = deferred.promise;
        var formData = new FormData();
        formData.append("name", name);
        formData.append("file", file);
        $http.post(API.url + ruta + $cookieStore.get('user_id'), formData,{
            headers:{
                "Content-type": undefined
            },
            transformRequest: angular.indentity
        })
        .success(function(res){
            deferred.resolve(res);
        })
        .error(function(msg,code){
            deferred.reject(msg);
        })
        return promise;
    }
})


