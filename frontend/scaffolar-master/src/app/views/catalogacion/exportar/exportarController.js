angular.module('app')
  .controller('exportarController', 

    function($rootScope, $modal, $scope,$log,$http, paginationConfig, AuthService, API, $cookieStore)  {
            $scope.inputs = [];
            $scope.bibliotecas = [];
            $scope.dependencias = [];
            $scope.selected = '';
            $scope.bool_add_button = false;
            $scope.bool_disable_acciones = true;
            $scope.loading_acciones = false;
            $scope.loading_correo = false;
            $scope.bool_disable = false;
            $scope.autenticado = AuthService.isLoggedIn();
            $scope.loading = false;
            $scope.styles_recursos = [{'background-color': ''},{'background-color': ''},{'background-color': ''},{'background-color': ''}];
            $scope.style_todo = "";
            $scope.style_tesis= "";
            $scope.style_monografia= "";
            $scope.style_publicacion_seriada= "";
            $scope.totalItems; 
            $scope.msjInformacion = "";
            $scope.function_typeahead = "state for state in loading_titulo_States($viewValue) | limitTo:5  | filter:$viewValue";
            $scope.options = {

                                todo:  {
                                    opciones1: [   
                                                    {label: 'Título' , value: 'titulo'}, 
                                                    {label:'cota', value: 'cota'},
                                                    {label: 'Autor'  , value: 'autor'  },
                                                    {label: 'Materia', value: 'materia'},
                                                    {label: 'Palabra clave'  , value: 'palabra_clave'}    
                                                ],

                                    opciones2: [
                                                    {label: 'Y'   , value: 'AND'},
                                                    {label: 'O' , value: 'OR'    },
                                                    {label: 'NO'  , value: 'NOT' }
                                               ]
                                },      

                                monografia: {


                                    opciones1: [  
                                                    {label: 'Título' ,   value: 'titulo'   },          
                                                    {label: 'Cota'   ,   value: 'cota'     },
                                                    {label: 'Autor'  ,   value: 'autor'    },
                                                    {label: 'Editorial', value: 'editorial'},
                                                    {label: 'Edición',   value:  'edicion' },
                                                    {label: 'Isbn'   ,   value: 'isbn'     },
                                                    {label: 'Volumen',   value: 'volumen'  },
                                                    {label: 'Materia',   value: 'materia'},
                                                    {label: 'Palabra clave'  , value: 'palabra_clave'},
                                                    {label: 'Idioma' ,   value: 'idioma'   }
                                                ],

                                    opciones2: [
                                                    {label: 'Y'   , value: 'AND'},
                                                    {label: 'O' , value: 'OR'    },
                                                    {label: 'NO'  , value: 'NOT LIKE' }
                                               ]
                                },

                                tesis: {
                                    opciones1: [       
                                                    {label: 'Título' , value: 'titulo'  },     
                                                    {label: 'Cota'   , value: 'cota'    },
                                                    {label:'Materia', value: 'materia'  },
                                                    {label: 'Autor'  , value: 'autor'   },
                                                    {label: 'Jurado' , value: 'jurado'  },
                                                    {label: 'Tutor Académico' , value: 'tutor_academico'},
                                                    {label: 'Tipo de Trabajo' , value: 'tipo_de_trabajo'},
                                                    {label: 'Palabra clave'  , value: 'palabra_clave'},
                                                    {label: 'Idioma' , value: 'idioma'}

                                                ],

                                    opciones2: [
                                                    {label: 'Y'   , value: 'AND'},
                                                    {label: 'O' , value: 'OR'    },
                                                    {label: 'NO'  , value: 'NOT LIKE' }
                                               ]
                                },



                                publicacion_seriada: {
                                    opciones1: [
                                                    {label: 'Título' , value: 'titulo' },
                                                    {label: 'Autor'  , value: 'autor'  },
                                                    {label: 'Issn'   , value: 'issn'   },
                                                    {label: 'Volumen', value: 'volumen'},
                                                    {label: 'Palabra clave'  , value: 'palabra_clave'}
                                                ],

                                    opciones2: [
                                                    {label: 'Y'   , value: 'AND'},
                                                    {label: 'O' , value: 'OR'    },
                                                    {label: 'NO'  , value: 'NOT LIKE' }
                                               ]
                                },                   
                            };
                            $scope.options_bibliotecas = [];
                            $scope.options_bibliotecas_seleccionado = {};
                            $scope.options_dependencias = [];
                            $scope.options_dependencias_seleccionado = {};
                $scope.mensaje_buscador_avanzado = {};    
                $scope.anios = {anio_origen: '',
                               anio_destino: ''
                              };
                $scope.div_correo = {};
                $scope.bool_acciones=false;
                $scope.bool_table_recursos=false;   
                $scope.recursos = [];
                $index_recurso;

                $scope.aux_respuesta='';

                $scope.correos = [{email: '', name:''}

                                ];

                $scope.mensaje_email= {  
                                        
                                      };
                $scope.rutas_ids_recursos_seleccionados = [];
                $scope.ids_recursos = "";
                $scope.ruta_index_recurso = "";
                $scope.id_recursos = [];
                $scope.mypush_i = 0;
                $scope.class_check_seleccionar_todos = "btn btn-default";
                $scope.bool_seleccionar_todos = false;
                $scope.datos_qr = [];
                $scope.title_tooltip = "Borrar campos de búsqueda";
                $scope.mytooltips = [{title: 'Borrar campos', tooltip: 'tooltip', position: 'rigth'}];
                $scope.num_items_pagina = 10;

                        /*pagination config*/
                paginationConfig.firstText='Primera'; 
                paginationConfig.previousText='< Anterior';
                paginationConfig.nextText='Siguiente >';
                paginationConfig.lastText='Última';
                paginationConfig.boundaryLinks=false;
                paginationConfig.rotate=false;
                paginationConfig.itemsPerPage = 10;

                $scope.maxSize = 20;


          $scope.options_orden =  [
                                      { label: 'Normal', value: 'normal' },
                                      { label: 'Título A...Z', value: 'titulo_asc' },
                                      { label: 'Título Z...A', value: 'titulo_desc' },
                                      { label: 'Autor  A...Z', value: 'autor_asc' },
                                      { label: 'Autor  Z...A', value: 'autor_desc' },
                                      { label: 'Año ascendente', value: 'anio_asc' },
                                      { label: 'Año descendente', value: 'anio_desc' }
                                  ];
          $scope.orden_seleccionado =  $scope.options_orden[0];
          $scope.value_orden_seleccionado =  $scope.orden_seleccionado.value;



$scope.getServiceBibliotecas = function (){
   // alert('alert Biblioteca');
        $http.get(API.url+'bagtesis/backend/public/bibliotecas').
            success(function(data, status, headers, config) {
            $scope.bibliotecas = data;
            $scope.options_bibliotecas.push({label:'Todas',value:'todas'});
            angular.forEach($scope.bibliotecas, function(value, key) {
              this.push({label:value.biblioteca_salida, value: value.id});
            },$scope.options_bibliotecas);

            $scope.options_bibliotecas_seleccionado = $scope.options_bibliotecas[0];

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });
};

$scope.getServiceDependencias = function(){
        $http.get(API.url+'bagtesis/backend/public/dependencias').
            success(function(data, status, headers, config) {
            $scope.dependencias = data;
            $scope.options_dependencias.push({label:'Todas',value:'todas'});
            angular.forEach($scope.dependencias, function(value, key) {
              this.push({label:value.dependencia_salida, value: value.id});
            },$scope.options_dependencias);

            $scope.options_dependencias_seleccionado = $scope.options_dependencias[0];

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });


};

$scope.getServiceBibliotecas();
$scope.getServiceDependencias();
$scope.setType = function(optionName){
    $scope.inputs = [];
    $scope.bool_add_button = true;
    $scope.selected = optionName;

    switch(optionName) {
        case 'todo':
            $scope.style_todo = 'background-color:#CC8548';
            $scope.style_tesis= '';
            $scope.style_monografia= '';
            $scope.style_publicacion_seriada= '';

            break;
        case 'tesis':
            $scope.style_todo = '';
            $scope.style_tesis= 'background-color:#CC8548';
            $scope.style_monografia= '';
            $scope.style_publicacion_seriada= '';

            break;
        case 'monografia':

            $scope.style_todo = '';
            $scope.style_tesis= '';
            $scope.style_monografia= 'background-color:#CC8548';
            $scope.style_publicacion_seriada= '';            
            break;   
        case 'publicacion_seriada':
            $scope.style_todo = '';
            $scope.style_tesis= '';
            $scope.style_monografia= '';
            $scope.style_publicacion_seriada= 'background-color:#CC8548';


            break;                    
    }

    $scope.newOption($scope.options[$scope.selected].opciones1[0], "", $scope.options[$scope.selected].opciones2[0], "",$scope.selected);
}


$scope.newOption = function(selet_model1, text_model1, selet_model2, text_model2,selected){
    $scope.inputs.push({
        opt1: {
            select_model: selet_model1,
            text_model: text_model1,
            function_typeahead: '',
        }, 
        opt2: {
            select_model: selet_model2,
            text_model: text_model2,
        },
        
    });
};
$scope.bool_add_button = true;
$scope.selected = "todo";
$scope.newOption($scope.options["todo"].opciones1[0], "", $scope.options["todo"].opciones2[0], "","todo");
$scope.style_todo = 'background-color:#CC8548';
$scope.style_tesis= '';
$scope.style_monografia= '';
$scope.style_publicacion_seriada= '';
$scope.add_option = function(){
    $scope.mytooltips.push({title: 'Borrar campos', tooltip: 'tooltip', position: 'rigth'});
   $scope.newOption($scope.options[$scope.selected].opciones1[0], "", $scope.options[$scope.selected].opciones2[0], "");
};

$scope.delete = function(index){
    if($scope.inputs.length!=1){
        $scope.inputs.splice(index,1);
    }
}



$scope.exitos = [];

$scope.consulta_buscador_avanzado = function(){
        
        $scope.currentPage = 1;
        $scope.bool_acciones = true;
        $scope.bool_disable_acciones = true;
        $scope.mensaje_buscador_avanzado['inputs'] = [];
        $scope.mensaje_buscador_avanzado['inputs'] = $scope.inputs;
        $scope.mensaje_buscador_avanzado['anios'] = $scope.anios;
        $scope.mensaje_buscador_avanzado['tipo_recurso'] = $scope.selected;
        $scope.loading = true;
        $scope.mensaje_buscador_avanzado['biblioteca'] = $scope.options_bibliotecas_seleccionado;
        $scope.mensaje_buscador_avanzado['dependencia'] = $scope.options_dependencias_seleccionado;
        $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_avanzado?page=1&num_items_pagina='+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value,  $scope.mensaje_buscador_avanzado).
            success(function(data, status, headers, config) {
            $('#loader').modal('show');
            $scope.pagina = data;
            $scope.exitos= data;
            $scope.id_recursos = data.data;
            $scope.recursos = [];
            $scope.currentPage = data.current_page;
            $scope.contador_caracteres = 0;
            $scope.totalItems = data.total;
            $scope.numPages = data.last_page;
            $scope.loading = false;

        $scope.totalItems_s = data.total;
            if($scope.lector_barra == true){
                $scope.bool_let = true;
                $scope.contador_caracteres = 0;
                $scope.lector_barra = false;
            }

   
            $scope.bool_table_recursos=true;
          //  $scope.bool_acciones = false;


        $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
            success(function(data, status, headers, config) {
            $scope.recursos = data;
            $('#loader').modal('hide');
      //      $scope.busco = true;
/*           angular.forEach($scope.id_recursos, function(value, key) {
              $scope.getServiceRecurso(value);

            });

*/          

            }).
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });



    //      $scope.recursos = $scope.id_recursos;


            }).
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });
  }

$scope.mensaje_buscador_simple = {};

$scope.buscar_titulo = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_titulo_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};


$scope.buscar_autor = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_autor_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};



$scope.buscar_jurado = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_jurado_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};


$scope.buscar_tutor_academico = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_tutor_academico_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};




$scope.buscar_cota = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_cota_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};


$scope.buscar_materia= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_materia_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};

$scope.buscar_palabra_clave= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_palabra_clave_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_tipo_de_trabajo= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_tipo_de_trabajo_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};

$scope.buscar_volumen= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_volumen_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_edicion = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_edicion_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {
                      $scope.prom = data;
                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_idioma = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_idioma_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {
                      $scope.prom = data;
                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_isbn = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_isbn_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_issn = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_issn_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_editorial = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_editorial_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
}; 


$scope.loadingStates = function(val,select_model_value){
 // $timeout(funcion_buscar, 3000);
      //  $scope.consultar_buscador_simple(val);


 //   return $scope.buscar_titulo(val);

    if(select_model_value == 'titulo'){
        return $scope.buscar_titulo(val);
    }else{
        if(select_model_value == 'autor'){
            return $scope.buscar_autor(val);           
        }else{
            if(select_model_value == 'cota'){
                return $scope.buscar_cota(val);                   
            }else{
                if(select_model_value == 'jurado'){
                    return $scope.buscar_jurado(val);   
                }else{
                    if(select_model_value == 'tutor_academico'){
                        return $scope.buscar_tutor_academico(val);                         
                    }else{
                        if(select_model_value == 'materia'){
                            return $scope.buscar_materia(val);
                        }else{
                            if(select_model_value == 'palabra_clave'){
                                return $scope.buscar_palabra_clave(val);
                            }else{
                                if(select_model_value == 'tipo_de_trabajo'){
                                    return $scope.buscar_tipo_de_trabajo(val);
                                }else{
                                    if(select_model_value == 'volumen'){
                                        return $scope.buscar_volumen(val);                                        
                                    }else{
                                        if(select_model_value == 'edicion'){
                                            return $scope.buscar_edicion(val);
                                        }else{
                                            if(select_model_value == 'idioma'){
                                                return $scope.buscar_idioma(val);
                                            }else{
                                                if(select_model_value == 'isbn'){
                                                    return $scope.buscar_isbn(val);
                                                }else{
                                                    if(select_model_value == 'issn'){
                                                        return $scope.buscar_issn(val);
                                                    }else{
                                                        if(select_model_value == 'editorial'){
                                                            return $scope.buscar_editorial(val);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

 //   return $scope.funcion_buscar_titulo(val);
};


$scope.colocar_typeahead = function(index,select_model_value){
 // alert(index+" "+select_model.value);

  if(select_model_value == "titulo"){
   //   $scope.inputs[index].opt1.function_typeahead = "state for state in loading_titulo_States($viewValue) | limitTo:5  | filter:$viewValue";
  }

}
    

   $scope.getService =  function(route_peticion){
   
     $scope.respuesta_buscador_simple = [];
     $http.post(route_peticion , $scope.mensaje_buscador_avanzado).
            success(function(data, status, headers, config) {

              $scope.id_recursos = data.data;
              $scope.recursos = [];
              $scope.recursos = [];
              $scope.currentPage = data.current_page;
              $scope.totalItems = data.total;
              $scope.numPages = data.last_page;

        $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
            success(function(data, status, headers, config) {
            $scope.recursos = data;
            

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });




            }).
                   
            error(function(data, status, headers, config) {

            });
   }



    $scope.pageChanged = function(currentPage){
        $scope.currentPage = currentPage;
        $scope.bool_acciones = false;
  //    $log.log('Page changed to: ' + $scope.currentPage);
      $scope.getService(API.url+'bagtesis/backend/public/buscadores/buscador_avanzado?page='+$scope.currentPage+'&num_items_pagina='+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value);

     // $scope.watchPage = newPage;
    }



$scope.getServiceRecursoAutores = function (recurso_id, index_recurso){
   // alert(recurso_id+" "+index_recurso+" "+index_titulo);    

    $http.get(API.url+'bagtesis/backend/public/recursos/'+recurso_id+'/autores').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
      $scope.autores = data;

      $scope.recursos[index_recurso]['autores'] = $scope.autores;
  // alert(recurso_id+" "+index_recurso+" "+index_titulo);

   // $scope.page.data[index_titulo]['recursos'][index_recurso]['autores'] = $scope.autores;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
 //   $scope.autores = data;
    
  });
}


$scope.getServiceRecursoDescriptores = function(recurso_id,index_recurso){
 
    $http.get(API.url+'bagtesis/backend/public/recursos/'+recurso_id+'/descriptores').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
   
    $scope.descriptores = data;
  //  alert($scope.descriptores);
  // alert(recurso_id+" "+index_recurso+" "+index_titulo);
    $scope.recursos[index_recurso]['descriptores'] = $scope.descriptores;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {

    
  });
}



    $scope.generar_ids_recursos_seleccionados = function (){
        $('#loader').modal('show');
        
        $scope.ids_recursos = [];
        for (var i = 0; i < $scope.recursos.length; i++) {
            if($scope.recursos[i].model_checkbox){
                $scope.ids_recursos.push($scope.recursos[i].id);    
            }
        };
        
        data = {};
        data["ids_recursos"] = $scope.ids_recursos;
        $http.post(API.url+'bagtesis/backend/public/exportar', data )
        .success(function(data, status, headers, config) {  
           console.log(JSON.stringify(data));
           $('#loader').modal('hide');
           if(data["codigo"] == "0"){
             $scope.nombreArchivo = data["nombre"];
             
            window.location.href = API.url+ 'bagtesis/backend/app/storage/public/catalogacion/'+  $scope.nombreArchivo + '.xlsx';
            
           }else{   
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');

           }
            
           // angular.element(document.querySelector('#clickDescargar')).click();
           
        }).
        error(function(error) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });
    };

    $scope.descargar_archivo = function(){
        alert("etrosd");
        console.log("sd");
       // window.open(API.url+ 'bagtesis/backend/app/storage/public/catalogacion/'+  $scope.nombreArchivo + '.xlsx');  
    }


  $scope.verficar_recursos_seleccionados = function(){ 

    for($scope.i=0;$scope.i< $scope.recursos.length; $scope.i++){
          if($scope.recursos[$scope.i].model_checkbox == true){
              return true;
          }
    }
    return false;
  }


  $scope.recurso_seleccionado = function (){ 
    if($scope.verficar_recursos_seleccionados()){
        //$scope.generar_ids_recursos_seleccionados();
        $scope.bool_acciones = true;
          $scope.bool_disable_acciones = false;
    }else{
        $scope.bool_disable_acciones = true;
      //  $scope.bool_acciones = false;
    }
  }


  $scope.seleccionar_todos = function (){
      $scope.bool_seleccionar_todos = !$scope.bool_seleccionar_todos;

      if($scope.bool_seleccionar_todos == true){
          $scope.bool_acciones = true; 
          $scope.bool_disable_acciones = false;
          $scope.class_check_seleccionar_todos = "btn btn-success";
          for ($scope.i =0 ; $scope.i< $scope.recursos.length; $scope.i++) {
              $scope.recursos[$scope.i].model_checkbox = true;
          };
      }else{
          $scope.class_check_seleccionar_todos = "btn btn-default";
          for ($scope.i =0 ; $scope.i< $scope.recursos.length; $scope.i++) {
              $scope.recursos[$scope.i]['model_checkbox'] = false;
          };
         $scope.bool_disable_acciones = true;
      //    $scope.bool_acciones = false;
      }
  }


$scope.verRecursos = function(){
    console.log(JSON.stringify($scope.recursos));
}
});
