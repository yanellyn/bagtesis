angular.module('app' )
  .controller('publicacionesSeriadasController', function($rootScope, $scope, $window, $http, API, $cookieStore, $state,paginationConfig, upload) {
        $scope.idrecurso = $cookieStore.get('id_recurso_publicacion_s');
         $scope.ruta_qr = API.url+"bagtesis/backend/public/recursos/"+$scope.idrecurso+"/pdf";
        $cookieStore.put('tipo_recurso', 'serie');
        $rootScope.tipo_recurso = $cookieStore.get('tipo_recurso');
        $scope.isAgregarEditorial = false;
        $scope.isLimpiarEditorial = false;
        $scope.isGuardarEditorial = false;
        $scope.isEliminarEditorial = false;
        $scope.isObtenerEditorial = false;
        $scope.guardarEditorialModal = false;
        $scope.isBusquedaSimple = "";
        $scope.idTitulo = "";
        $scope.idAutor = "";
        $scope.coleccion = 'coleccion';
        $scope.isColeccion = true;
        $scope.msjConfirmacion = "";
        $scope.msjInformacion = "";
        $scope.cotaLibro = "";
        $scope.nombreAutor = "" 
        $scope.apellidoAutor = "";
        $scope.tipoAutor = "";
        $scope.nombreTitulo = "";
        $scope.edicion = "";
        $scope.fechaEdicion = "";
        $scope.url = "";
        $scope.colacion = "";
        $scope.issn = "";
        $scope.numeracion = "";
        $scope.impresion = "";
        $scope.volumen = "";
        $scope.coleccion = "";
        $scope.nombreColeccionSerie = "";
        $scope.numeroColeccionSerie = "";
        $scope.tipoContenido = "";
        $scope.contenido = "";
        $scope.resumen = "";
        $scope.datosAdicionales = "";
        $scope.notas = ""
        $scope.contenidoData = "";
        $scope.recursoAgregado = true;
        $scope.tipoAutor = "";
        $scope.tipoAutor1 = ["Personal", "Conferencia", "Institucional"];
        $scope.selectedBuscadorSimple = "";
        $scope.tipoFechaPrincipal = "fecha";
        $scope.fechaPrincipalActiva = true;
        $scope.anioPrincipal = "";
        $scope.dt_orig = "";
        $scope.dt_orig2 = "";
        $scope.paisesSelect = "";
        $scope.currentPageEditorial = 1;
        $scope.bibliotecaSelect = 1;
        $scope.tipoSerie = "S";
        $scope.periodicidad = "";
        $scope.regularidad = "";
        $scope.nros = "";
        $scope.vols = "";
        $scope.diseminacion = "";
        $scope.existencia = "";
        $scope.vieneDePrincipal = false;
        $scope.selected =  "publicacion_seriada";
        $scope.options_bibliotecas_seleccionado = {label:'Todas',value:'todas'};
        $scope.options_dependencias_seleccionado = {label:'Todas',value:'todas'};

$scope.obtenerEditorialesSistema = function(){
    $scope.busquedaEditorial = "";
  //  $scope.currentPageEditorial = 1;
    $scope.busquedaEditorialActiva = false;
    $http.get(API.url + 'bagtesis/backend/public/obtener_editoriales_sistema?page=' + $scope.idrecurso).success(function(data, status, headers, config) {    //alert("sda");       
        $scope.editorialesSistema =  data.data;  
        $scope.currentPageEditorial = data.current_page;
        $scope.totalItemsEditorial = data.total;
        $scope.numPagesEditorial = data.last_page;
    }).
    error(function(error) {
        //alert("sdb");console.log(error);
    });  
}
/************************PRINCIPAL**********************************/
    //paises
    $http.get(API.url+'bagtesis/backend/public/paises').
                success(function(data, status, headers, config) {
                    $scope.paises = data;
                }).
                error(function(data, status, headers, config) {
             
                }); 

    //bibliotecas
    $http.get(API.url+'bagtesis/backend/public/bibliotecas').
                success(function(data, status, headers, config) {
                    $scope.bibliotecas = data;
                }).
                error(function(data, status, headers, config) {
                   
                }); 

    if($scope.idrecurso !=""){
         $('#loader').modal('show');
        console.log("entro");
         $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', [$scope.idrecurso]).
                success(function(data, status, headers, config) {
                    $scope.recursosBuscador = data;
                    $scope.vieneDePrincipal = true;
                    $scope.obtenerEditoriales();
                }).
                error(function(data, status, headers, config) {
                    console.log("ERRRO" + JSON.stringify(data));
                });
    }else{
        $scope.obtenerEditorialesSistema();
    }


    $scope.mensaje_buscador_simple = {};
    $scope.loadingStates   = function(val,select_model_value, tipo_busqueda){
        $scope.selected = tipo_busqueda;
        $scope.nuevo = select_model_value;
        console.log("val "+val);
        console.log("select_model_value "+select_model_value);
        console.log("tipo_recurso "+tipo_busqueda); 

        if(select_model_value == 'titulo'){
            $scope.loadingTitulos = true;
            return $scope.buscar_titulo2(val);
        }else{
            if(select_model_value == 'autor'){
                $scope.loadingAutor = true;
                return $scope.buscar_autor2(val);           
            }
        }

    }


    $scope.buscar_titulo2 = function (val){
        $scope.mensaje_buscador_simple['data'] = val;
        $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_titulo_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                      .success(function(data, status, headers, config) {
                          $scope.prom = data;
                          console.log(JSON.stringify(data));
                          $scope.loadingTitulos = false;
                      }).then(function(){
                          $scope.aux_promise = $scope.prom;
                          
                          return $scope.prom;
                      });
    
        return $scope.promise;

    }



    $scope.onselectApellido = function (){
        var nombreYapellido = $scope.apellidoAutor.split(", "); 
        $scope.apellidoAutor = nombreYapellido[0];
        if(nombreYapellido.length > 1){
            $scope.nombreAutor = nombreYapellido[1];;   
        } 

    }

    $scope.submitFormluarioPrincipal=function(){
        if($scope.recursoAgregado){
            $scope.confirmarAgregarMonografia(); 
        }else{
            $scope.confirmarGuardarMonografia();
        }
    }
/************************FIN PRINCIPAL**********************************/



/************************EDITORIALES**********************************/

//editoriales del sistema
$scope.claseFlecha = "";
$scope.claseFlecha2 = "";  
actual = "";
$scope.nombreEditorial = "";
$scope.nombreFabricante = "";
$scope.ciudadFabricacion = "";
$scope.fechaManufactura = "";
//$scope.paginaActualEditorial = 1;
$scope.currentPageEditorial =1;
$scope.editorialesSistema=[];
$scope.editorialesDocumento = [];
editorialSistemaActual = "";
editorialDocumentoActual = "";
actual = "";
$scope.idEditorialActual = 0;
$scope.auxClaseFlecha = "";
dataRE = {};



$scope.buscarEditorialesSistema = function(){
     $('#loader').modal('show');    
     if($scope.busquedaEditorial.length > 0){
        
        dataRES = {};
        dataRES["data"] = $scope.busquedaEditorial; 
        $http.post(API.url + 'bagtesis/backend/public/buscadores/buscador_editorial_autocompletado_typeahead?page=1', dataRES).success(function(data, status, headers, config) {         
                if(data.data.length > 0){
                    $scope.currentPageEditorial =1;
                    $scope.busquedaEditorialActiva = true;
                    $scope.editorialesSistema =  data.data; 
                    $scope.currentPageEditorial = data.current_page;
                   // $scope.contador_caracteres = 0;
                    $scope.totalItemsEditorial = data.total;
                    $scope.numPagesEditorial = data.last_page;
                    $('#loader').modal('hide');
                }else{
                    $scope.msjInformacion = 'Disculpe, no se encontraron coincidencias' ;
                    $('#loader').modal('hide'); 
                    $('#informacion').modal('show'); 
                }    
        }).
        error(function(data, status, headers, config) {
        }); 
    } 
}

$scope.obtenerEditoriales = function(){
    console.log("entroo here");
    dataRE["idRecurso"] = $scope.idrecurso;
    $http.post(API.url + 'bagtesis/backend/public/obtener_editoriales_recurso', dataRE).success(function(data, status, headers, config) {           
            $scope.editorialesDocumento = data; 
             //console.log("entroo here333" + JSON.stringify(data));
            if(data.length > 0){
                //console.log("entroo here33888   " + $scope.editorialesDocumento[0].editorial);
                $scope.editorialPrincipal = data[0].editorial_salida;
                $scope.paisPrincipal = data[0].pais    
            }
    }).
      error(function(data, status, headers, config) {
    });            

    
    $http.get(API.url + 'bagtesis/backend/public/obtener_editoriales_sistema?page='+$scope.currentPageEditorial).success(function(data, status, headers, config) {    //alert("sda");       
         $('#loader').modal('hide'); 
        $scope.editorialesSistema =  data.data;  
        $scope.currentPageEditorial = data.current_page;
       // $scope.contador_caracteres = 0;
        $scope.totalItemsEditorial = data.total;
        $scope.numPagesEditorial = data.last_page;
        
        if(!$scope.isAgregarEditorial && !$scope.isGuardarEditorial && !$scope.isEliminarEditorial && !$scope.isLimpiarEditorial){
            $scope.isObtenerEditorial = true;
            if($scope.vieneDePrincipal){
                $scope.vieneDePrincipal = false;
                $scope.desplegar_recurso($scope.idrecurso,0);

            }
        }else{
            $aux = false;
            if($scope.isGuardarEditorial)
                 $scope.msjInformacion = ' Editorial modificada exitosamente';   
            else if ($scope.isAgregarEditorial)
                 $scope.msjInformacion = ' Editorial agregada exitosamente';
            else if ($scope.isEliminarEditorial)
                $scope.msjInformacion = ' Editorial eliminada exitosamente';
            else if ($scope.isLimpiarEditorial ){
                $aux = true;
               // $scope.msjInformacion = ' Se limpio la pantalla exitosamente';
            }

            else if($scope.guardarEditorialModal){
                $scope.limpiarEditorial();
                $aux = true;
            }

            

            
            $('#loader').modal('hide'); 
            if( !$aux )
                $('#informacion').modal('show');
                 $aux = false; 
            $scope.isAgregarEditorial = false;
            $scope.isGuardarEditorial = false;
            $scope.isEliminarEditorial = false
            $scope.isLimpiarEditorial = false;
            $scope.guardarEditorialModal = false;
        }
    }).
    error(function(error) {
         $('#loader').modal('hide'); 
       // alert("sdb");console.log(error);
    });  
}

$scope.pageChangedEditorial = function(current_page){
  if(!$scope.busquedaEditorialActiva){
    tipoPeticion = "obtener_editoriales_sistema";  
  }else{
    tipoPeticion = "buscadores/buscador_editorial_autocompletado_typeahead";
  }
   $http.get(API.url + 'bagtesis/backend/public/' + tipoPeticion + '?page=' + current_page).success(function(data, status, headers, config) {                   
                $scope.editorialesSistema =  data.data; 
                $scope.currentPageEditorial = data.current_page;
               // $scope.contador_caracteres = 0;
                $scope.totalItemsEditorial = data.total;
                $scope.numPagesEditorial = data.last_page;
            }).
            error(function(error) {
                console.log(error);
              });  
}

$scope.agregarClaseFilaEditorialSistema = function(editorial){
    if((editorialSistemaActual.id == editorial.id) && (actual == "sistema"))
        return "seleccionFila";
    else 
        return "";
}

$scope.agregarClaseFilaEditorialDocumento = function(editorial){
    if((editorialDocumentoActual.id == editorial.id) && (actual == "documento"))
        return "seleccionFila";
    else 
        return "";
}

$scope.encenderFlechaArribaEditorial = function(editorial, indice){
    $scope.idEditorialActual = editorial.id;
    actual = "documento";
    $scope.claseFlecha2 = "flechaAzul";
    $scope.claseFlecha = "";
    $scope.auxClaseFlecha = ""; 
    editorialDocumentoActual = editorial;
    $scope.indiceEditorialDocumentoActual = indice;
    $scope.paisesSelect =  editorial.pais_id;
    $scope.nombreEditorial = editorial.editorial_salida;
    $scope.nombreFabricante = editorial.fabricante;
    $scope.ciudadFabricacion = editorial.ciudad_fabricacion;
    $scope.dt_orig2 = editorial.fecha_manufactura;
    $scope.nombreCiudad = editorial.ciudad;

}

$scope.encenderFlechaAbajoEditorial = function(editorial, indice){
     $scope.idEditorialActual = editorial.id;
    if(!$scope.validarExistenciaEditorial(editorial.id)){
        $scope.claseFlecha = "flechaAzul";
        $scope.auxClaseFlecha = "flechaAzul"; 
    }else{
         $scope.claseFlecha = "";   
         $scope.auxClaseFlecha = "flechaAzul"; 
    }

    actual = "sistema";    
    
    $scope.claseFlecha2 = "";
    editorialSistemaActual = editorial;
    $scope.indiceEditorialSistemaActual = indice;
    $scope.paisesSelect =  editorial.pais_id;
    $scope.nombreEditorial = editorial.editorial_salida;
    $scope.nombreFabricante = editorial.fabricante;
    $scope.ciudadFabricacion = editorial.ciudad_fabricacion;
    $scope.dt_orig2 = editorial.fecha_manufactura;
    $scope.nombreCiudad = editorial.ciudad;
    $scope.validarExistenciaEditorial();
}

$scope.agregarEditorialDocumento = function(){
    if(!$scope.validarExistenciaEditorial(editorialSistemaActual.id)){
        if($scope.claseFlecha == "flechaAzul"){
            $scope.editorialesDocumento.push(editorialSistemaActual);
           // $scope.editorialesSistema = $scope.eliminarObjetoArray($scope.editorialesSistema, editorialSistemaActual.id);
           
            editorialSistemaActual = "";
        //$scope.isLimpiarEditorial = true;
            editorialDocumentoActual = "";
            $scope.claseFlecha = "";
            $scope.dt_orig2 = "";
            $scope.claseFlecha2 = ""; 
            $scope.auxClaseFlecha = ""; 
            $scope.paisesSelect =  "";
            $scope.nombreEditorial = "";
            $scope.nombreFabricante = "";
            $scope.ciudadFabricacion = "";
            $scope.fechaManufactura = "";
            $scope.nombreCiudad = "";
            $scope.idEditorialActual = 0;
        }
    }
}

$scope.agregarEditorialSistema = function(){
    if($scope.claseFlecha2 == "flechaAzul"){
        $scope.editorialesDocumento = $scope.eliminarObjetoArray($scope.editorialesDocumento, editorialDocumentoActual.id);
        $scope.claseFlecha2 = "";
        $scope.claseFlecha = "";
        $scope.auxClaseFlecha = "";
    }
    editorialSistemaActual = "";
    $scope.isLimpiarEditorial = true;
    editorialDocumentoActual = "";
    $scope.claseFlecha = "";
    $scope.dt_orig2 = "";
    $scope.claseFlecha2 = ""; 
    $scope.auxClaseFlecha = ""; 
    $scope.paisesSelect =  "";
    $scope.nombreEditorial = "";
    $scope.nombreFabricante = "";
    $scope.ciudadFabricacion = "";
    $scope.fechaManufactura = "";
    $scope.nombreCiudad = "";
    $scope.idEditorialActual = 0;
}

$scope.eliminarObjetoArray = function(array, item){   
    arrayRemoved = array.filter(function(el) {
        return el.id !== item;
    });
    return arrayRemoved;
}

$scope.validarExistenciaEditorial = function(idEditorial){
     array = $scope.editorialesDocumento.filter(function(el) {
        return el.id == idEditorial;
     });
    if(array.length > 0){
        return true;
    }else return false;
}


$scope.moveUpEditorial = function(num) {
    
    if (num > 0) {
        console.log("moveUp");
        tmp = $scope.editorialesDocumento[num - 1];
        $scope.editorialesDocumento[num - 1] = $scope.editorialesDocumento[num];
        $scope.editorialesDocumento[num] = tmp;
        //$scope.editorialesDocumento[num - 1].orden = $scope.editorialesDocumento[num - 1].orden - 1;
        //$scope.editorialesDocumento[num].orden = $scope.editorialesDocumento[num].orden + 1;
        $scope.indiceEditorialDocumentoActual--;
    }
  };
    
$scope.moveDownEditorial = function(num) {
    if (num < $scope.editorialesDocumento.length - 1) {
        console.log("moveDown");             
        tmp = $scope.editorialesDocumento[num + 1];
        $scope.editorialesDocumento[num + 1] = $scope.editorialesDocumento[num];
        $scope.editorialesDocumento[num] = tmp;
        //$scope.editorialesDocumento[num + 1].orden = $scope.editorialesDocumento[num + 1].orden + 1;
        //$scope.editorialesDocumento[num].orden = $scope.editorialesDocumento[num].orden - 1;
         
        $scope.indiceEditorialDocumentoActual++;
    }
};

$scope.eliminarEditorial = function(){
    $('#loader').modal('show');
    $('#confirmacionEditorial').modal('hide');
    dataEditorial = {};
    dataEditorial["idEditorial"] = $scope.idEditorialActual; 
    dataEditorial["idRecurso"] = $scope.idrecurso; 
     $http.post(API.url + 'bagtesis/backend/public/eliminar_editoriales', dataEditorial).success(function(data, status, headers, config) {    //alert("sda");       
        if(data["codigo"] == "0"){
            $scope.limpiarEditorial(); 
            $scope.isEliminarEditorial = true;
            $scope.obtenerEditoriales();

        }else{
            $('#loader').modal('hide');
            $scope.msjInformacion = 'La editorial no se puede eliminar debido a que se encuentra a asociada a otros recursos';
            $('#informacion').modal('show');

        }


    }).
    error(function(error) {
        $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });  
}

$scope.limpiarEditorial = function(){
     $('#confirmacionEditorial').modal('hide');
      $('#loader').modal('show');
    editorialSistemaActual = "";
//$scope.isLimpiarEditorial = true;
    editorialDocumentoActual = "";
    $scope.claseFlecha = "";
    $scope.dt_orig2 = "";
    $scope.claseFlecha2 = ""; 
    $scope.auxClaseFlecha = ""; 
    $scope.paisesSelect =  "";
    $scope.nombreEditorial = "";
    $scope.nombreFabricante = "";
    $scope.ciudadFabricacion = "";
    $scope.fechaManufactura = "";
    $scope.nombreCiudad = "";
    $scope.idEditorialActual = 0;
    $scope.obtenerEditoriales();
}

$scope.submitEditoriales = function(){
     $('#confirmacionEditorial').modal('show');
}

$scope.agregarEditorial = function(){
    $('#confirmacionEditorial').modal('hide');
    $('#loader').modal('show');
    dataEditorial = {};
    dataEditorial["idPais"] = $scope.paisesSelect;
    dataEditorial["editorial"] = $scope.nombreEditorial;
    dataEditorial["fabricante"] = $scope.nombreFabricante;
    dataEditorial["ciudadFabricante"] = $scope.ciudadFabricacion;
    dataEditorial["fecha"] = $scope.dt_orig2;
    dataEditorial["ciudad"] = $scope.nombreCiudad;
    dataEditorial["tipoDoc"] = "S";
    dataEditorial["idRecurso"] = $scope.idrecurso;

    $http.post(API.url + 'bagtesis/backend/public/agregar_editoriales', dataEditorial).success(function(data, status, headers, config) {    //alert("sda");       
        if(data["codigo"] == "0"){
             $scope.limpiarEditorial(); 
            $scope.isAgregarEditorial = true;
            $scope.obtenerEditoriales();
           
        }else{
            $('#loader').modal('hide');
            $scope.msjInformacion = 'La editorial ya existe';
            $('#informacion').modal('show');


        }
    }).
    error(function(error) {
        $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });  
   
}

$scope.guardarEditorial= function(){
    $('#confirmacionEditorial').modal('hide');
    $('#loader').modal('show');
    dataEditorial = {};
    dataEditorial["editoriales"] = $scope.editorialesDocumento;
    dataEditorial["idRecurso"] = $scope.idrecurso;
    dataEditorial["idPais"] = $scope.paisesSelect;
    dataEditorial["editorial"] = $scope.nombreEditorial;
    dataEditorial["fabricante"] = $scope.nombreFabricante;
    dataEditorial["ciudadFabricante"] = $scope.ciudadFabricacion;
    dataEditorial["fecha"] = $scope.dt_orig2;
    dataEditorial["ciudad"] = $scope.nombreCiudad;
    dataEditorial["idEditorial"] = $scope.idEditorialActual; 
    dataEditorial["tipoDoc"] = "S";
    $http.post(API.url + 'bagtesis/backend/public/guardar_editoriales', dataEditorial).success(function(data, status, headers, config) {    //alert("sda");       
         if(data["codigo"] == "0"){
                if(dataEditorial["idEditorial"]!=0){
                    editorialSistemaActual = data["editorial"];
                }
                if(!$scope.guardarEditorialModal){
                    $scope.isGuardarEditorial = true;    
                }
               
                $scope.obtenerEditoriales();      
            }
    }).
    error(function(error) {
         $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });      
    
}

 $scope.loadingEditorial = false;
$scope.loadingStatesEditorial = function(val,select_model_value, tipo_busqueda){
    if(select_model_value == 'editorial'){
        $scope.loadingEditorial = true;
        return $scope.buscar_editorialesE(val); 
    }
};

$scope.buscar_editorialesE = function (val){
    console.log("Entro a buscar editorial");
    $scope.mensaje_buscador_simple = {};
    $scope.mensaje_buscador_simple['data'] = val;
    //$scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_autor_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
    $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_editorial_autocompletado_typeahead?page=-1', $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {
                      console.log(JSON.stringify(data));
                      $scope.prom = data;
                      $scope.loadingEditorial = false;
                  }).then(function(){
                     
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};

  $scope.onselectEditorial = function (){
   //alert( $scope.nombreEditorial.fabricante);
    console.log(JSON.stringify($scope.nombreEditorial));
    $scope.paisesSelect =  $scope.nombreEditorial.pais_id;
    $scope.nombreFabricante = $scope.nombreEditorial.fabricante;
    $scope.ciudadFabricacion = $scope.nombreEditorial.ciudad_fabricacion;
    $scope.dt_orig2 =  $scope.nombreEditorial.fecha_manufactura;
    $scope.nombreCiudad = $scope.nombreEditorial.ciudad;
    $scope.nombreEditorial = $scope.nombreEditorial.editorial_salida;

};
/************************FIN EDITORIALES**********************************/



/****************************  Agregar Monografia   **********************************/
//tipo autor


//contenido
$scope.contenidoSeleccion = ["Resumen", "Nota", "Datos adicionales"];
$scope.tipoContenido = "";

$scope.seleccionarTipoContenido = function(){
    if($scope.tipoContenido == "Resumen"){
        $scope.resumen = $scope.contenidoData;
        $scope.notas ="";
        $scope.datosAdicionales = "";
    }else if($scope.tipoContenido == "Nota"){     
        $scope.notas = $scope.contenidoData;
        $scope.datosAdicionales ="";
        $scope.resumen = "";
    }else if($scope.tipoContenido == "Datos adicionales"){
        $scope.datosAdicionales = $scope.contenidoData;
        $scope.notas ="";
        $scope.resumen = "";
    }
};

$scope.confirmarAgregarMonografia = function(){
    $('#confirmacionSub').modal('show');
}


$scope.agregarMonografia = function(){
    $('#confirmacionSub').modal('hide');
    $('#loader').modal('show');

    $scope.seleccionarTipoContenido();
    $scope.dataRecurso = {};  
    $scope.dataRecurso["nombreTitulo"] = $scope.nombreTitulo;
    $scope.dataRecurso["issn"] = $scope.issn;
    $scope.dataRecurso["impresion"] = $scope.impresion;
    $scope.dataRecurso["tipoLiter"] = "S";
    $scope.dataRecurso["idCatalogador"] = $cookieStore.get('user_id');
    $scope.dataRecurso["resumen"] = $scope.resumen;
    $scope.dataRecurso["notas"] = $scope.notas;    
    $scope.dataRecurso["datosAdicionales"] = $scope.datosAdicionales;
    $scope.dataRecurso["bibliotecaSelect"] = $scope.bibliotecaSelect;
    $scope.dataRecurso["periodicidad"] = $scope.periodicidad;
    $scope.dataRecurso["regularidad"] = $scope.regularidad;
    $scope.dataRecurso["tipo"] = $scope.tipoSerie;
    $scope.dataRecurso["nros"] = $scope.nros;
    $scope.dataRecurso["vols"] = $scope.vols;
    $scope.dataRecurso["url"] = $scope.url;
    $scope.dataRecurso["diseminacion"] = $scope.diseminacion;
    $scope.dataRecurso["issn"] = $scope.issn;
    $scope.dataRecurso["existencia"] = $scope.existencia;
    
    $http.post(API.url+'bagtesis/backend/public/publicaciones/agregar_publicacion', $scope.dataRecurso)
            .success(function(data, status, headers, config) {     
                if(data["codigo"] == "0"){
                    $scope.idrecurso = data["id_recurso"];
                     $scope.ruta_qr = API.url+"bagtesis/backend/public/recursos/"+$scope.idrecurso+"/pdf";
                    $scope.idTitulo = data["id_titulo"];
                    $cookieStore.put('id_recurso_publicacion_s', $scope.idrecurso);
                    $scope.recursoAgregado = false;    
                    $scope.msjInformacion = ' La publicación seriada fue catalogada exitosamente';
                    $('#loader').modal('hide');
                    $('#informacion').modal('show');  
                }else{
                    $scope.msjInformacion = 'Ya existe otra publicación seriada asociada a ese título' ;
                    $('#loader').modal('hide'); 
                    $('#informacion').modal('show');   
                }
    }).
    error(function(error) {
       $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });
}

//**********GUARDAR MONOGRAFIA ***************/



$scope.confirmarGuardarMonografia = function(){
    $('#confirmacionSub').modal('show');

} 

$scope.guardarMonografia = function(){
    $('#confirmacionSub').modal('hide'); 
       $('#loader').modal('show');
    $scope.seleccionarTipoContenido();
 
    $scope.dataRecurso = {};  
    $scope.dataRecurso["nombreTitulo"] = $scope.nombreTitulo;
    $scope.dataRecurso["issn"] = $scope.issn;
    $scope.dataRecurso["impresion"] = $scope.impresion;
    $scope.dataRecurso["tipoLiter"] = "S";
    $scope.dataRecurso["idCatalogador"] = $cookieStore.get('user_id');
    $scope.dataRecurso["resumen"] = $scope.resumen;
    $scope.dataRecurso["notas"] = $scope.notas;    
    $scope.dataRecurso["datosAdicionales"] = $scope.datosAdicionales;
    $scope.dataRecurso["bibliotecaSelect"] = $scope.bibliotecaSelect;
    $scope.dataRecurso["periodicidad"] = $scope.periodicidad;
    $scope.dataRecurso["regularidad"] = $scope.regularidad;
    $scope.dataRecurso["tipo"] = $scope.tipoSerie;
    $scope.dataRecurso["nros"] = $scope.nros;
    $scope.dataRecurso["vols"] = $scope.vols;
    $scope.dataRecurso["url"] = $scope.url;
    $scope.dataRecurso["diseminacion"] = $scope.diseminacion;
    $scope.dataRecurso["issn"] = $scope.issn;
    $scope.dataRecurso["existencia"] = $scope.existencia;
    $scope.dataRecurso["idRecurso"] = $scope.idrecurso;
    $scope.dataRecurso["idTitulo"] = $scope.idTitulo;


 $http.post(API.url+'bagtesis/backend/public/publicaciones/guardar_publicacion', $scope.dataRecurso)
            .success(function(data, status, headers, config) {     
                if(data["codigo"] == "0"){
                   $scope.msjInformacion = 'La publicación seriada fue modificada exitosamente' ;
                   $('#loader').modal('hide');
                   $('#informacion').modal('show'); 
                }else{
                    $scope.msjInformacion = 'Ya existe otra publicación seriada asociada a ese título';
                    $('#loader').modal('hide'); 
                    $('#informacion').modal('show'); 

                }

    }).
    error(function(error) {
     $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });  
}

$scope.eliminarLibro = function(){
    $('#confirmacionEliminar').modal('hide');
    $('#loader').modal('show');
    $scope.dataRecurso = {};
    $scope.dataRecurso["idRecurso"] = $scope.idrecurso;
    $scope.dataRecurso["idCatalogador"] = $cookieStore.get('user_id');

    $http.post(API.url+'bagtesis/backend/public/publicaciones/eliminar_publicacion', $scope.dataRecurso)
            .success(function(data, status, headers, config) {     
                if(data["codigo"] == "0"){
                    $scope.limpiarLibro();
                       $('#loader').modal('hide');
                   $scope.msjInformacion = 'La publicación seriada fue eliminada exitosamente' ;
                   $('#informacion').modal('show'); 
                }else{             
                }
       // alert(JSON.stringify(data));
       // alert(JSON.stringify($scope.titulosMostrar));
        //alert(JSON.stringify($scope.tipoAutorPrincipal)); 
    }).
    error(function(error) {
        $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });  

}



$scope.limpiarLibro = function(){
    
    $cookieStore.put('id_recurso_publicacion_s', "");
 
    $scope.editorialPrincipal = "";
    $scope.paisPrincipal = "";
    $scope.anioPrincipal = "";
    $scope.recursoAgregado = true;
    $scope.nombreTitulo = "";
    $scope.url = "";
    $scope.issn = "";
    $scope.impresion = "";
    $scope.volumen = "";
    $scope.resumen = "";
    $scope.notas = "";   
    $scope.datosAdicionales = "";
    $scope.idrecurso = "";
    $scope.tipoContenido= "";
    $scope.resumen = "";
    $scope.notas = "";
    $scope.contenidoData = "";
    $scope.tipoSerie = "S";
    $scope.periodicidad = "";
    $scope.regularidad = "";
    $scope.nros = "";
    $scope.vols = "";
    $scope.diseminacion = "";
    $scope.existencia = "";
    $scope.bibliotecaSelect = "1";
    $rootScope.numeroSeleccionado = false;
   
}


$scope.deshabilitarOpcion = function(){
    if($scope.recursoAgregado || !$rootScope.numeroSeleccionado){
        return "disabled"
    }
}

$scope.deshabilitarOpcionNumeros = function(){
    if($scope.recursoAgregado){
        return "disabled"
    }
}



/****************************  Fin agregar Monografia   **********************************/

/**************Buscador simple publicaciones seriadas *********************/////////////

    $scope.bool_table_recursos = false; 
    $scope.currentPage = 1;
    $scope.bool_seleccionar_todos = false;       
    $scope.num_items_pagina  = 10;         
    $scope.datos_qr = [];
    /*pagination config*/

    paginationConfig.firstText='Primera'; 
    paginationConfig.previousText='< Anterior';
    paginationConfig.nextText='Siguiente >';
    paginationConfig.lastText='Última';
    paginationConfig.boundaryLinks=false;
    paginationConfig.rotate=false;
    $scope.maxSize = 15;
    paginationConfig.itemsPerPage = $scope.num_items_pagina;
    $scope.popUpBusquedaSimple = false;

$scope.abrirPopUpBusqueda = function(val){
    if(val.length > 0){
         $scope.loadingBusquedaSimple = false;
        $scope.popUpBusquedaSimple = true;
        $scope.selectedBuscadorSimple = val;
        $scope.selectedBuscadorSimple2 = "";
        $scope.bool_table_recursos = false;
        $scope.recursosBuscador = [];
        $scope.consultar_buscador_simple(val);
        $('#buscadorSimple').modal('show');
    }
}


$scope.verificar_paste = function(val){
    if($scope.paste & val.length == 13 & val.indexOf(' ')<0 & (val[0] == 'T'  || val[0] == 'D')  &  $scope.verificar_id(val)){
        $scope.consultar_buscador_simple(val);
    }
 }


 $scope.verificar_id = function(val){
    for(var i=1; i < val.length; i++){
        if(parseInt(val[i])<0 || parseInt(val[i])>9 || val[i].charCodeAt(0)<48 || val[i].charCodeAt(0)>57){
             //   alert(parseInt(val[12]));
            return false;
        }
    }
    return true;
 };

    $scope.options_orden =  [
            { label: 'Normal', value: 'normal' },
            { label: 'Título A...Z', value: 'titulo_asc' },
            { label: 'Título Z...A', value: 'titulo_desc' },
            { label: 'Autor  A...Z', value: 'autor_asc' },
            { label: 'Autor  Z...A', value: 'autor_desc' },
            { label: 'Año ascendente', value: 'anio_asc' },
            { label: 'Año descendente', value: 'anio_desc' }
        ]
        ;
    $scope.orden_seleccionado =  $scope.options_orden[0];
    $scope.a ;


$scope.consultar_buscador_simple = function(selected){
    
    if(selected!=""){
        $scope.i = 0;
      //  $scope.lector_barra = false;
        $scope.bool_acciones = true;
        $scope.bool_recursos_encontrados = false;
        $scope.bool_disable = true;
        $scope.bool_disable_acciones = true;
        $scope.loading = true;
        if(!$scope.popUpBusquedaSimple)
            $scope.loadingBusquedaSimple = true;
        else 
             $scope.loadingBusquedaSimple = false;
        $scope.data = { 'data': selected};
        $scope.num_items_pagina = 10;
        $scope.respuesta_buscador_simple = [];
        paginationConfig.itemsPerPage = 10;//$scope.num_items_pagina;
        
        $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_simple2?page=1&num_items_pagina='+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value+'&tipo_recurso=publicacion_seriada', $scope.data).
            success(function(data, status, headers, config) {
       
                $scope.a = data;
                $scope.pagina = data;
                $scope.id_recursos = data.data;
                $scope.recursos = [];
                $scope.currentPage = data.current_page;
                $scope.contador_caracteres = 0;
                $scope.totalItems = data.total;
                $scope.numPages = data.last_page;           
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
                $('#loader').modal('show');

                $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
                success(function(data, status, headers, config) {


                //alert("data2 "+ JSON.stringify(data));
                $scope.recursosBuscador = data;
                $scope.bool_disable = false;
                $scope.bool_recursos_encontrados = true;
                 $('#loader').modal('hide');
            }).
            error(function(data, status, headers, config) {
               // alert(JSON.stringify(data));
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
            });
            $scope.bool_table_recursos=true;
          
        
        }).error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.loading = false;
            $scope.loadingBusquedaSimple = false;
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
            });
    }  
}

$scope.funcion_buscar = function (val){
  $scope.mensaje_buscador_simple['data'] = val;
  $scope.mensaje_buscador_simple['autocompletado'] = val;
  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_typeahead?tipo_recurso=publicacion_seriada', $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {
                      $scope.prom = data;
                  }).then(function(){
                      $scope.aux_promise = $scope.prom;
                      $scope.aux_selet = val;
                      $scope.buscando_select = false;
                     
                      return $scope.prom;
                  });
    return $scope.promise;
};

$scope.loadingStates3 = function(val){
    return $scope.funcion_buscar(val);
};

$scope.getService =  function(route_peticion){
    $('#loader').modal('show'); 
    $scope.i = 0 ;
    $scope.data = { 'data': $scope.selectedBuscadorSimple}

    $scope.respuesta_buscador_simple = [];
    $http.post(route_peticion , $scope.data).
        success(function(data, status, headers, config) {
            $scope.id_recursos = data.data;
            $scope.recursosBuscador = [];
            $scope.recursosBuscador = [];
            $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
            success(function(data, status, headers, config) {
                $scope.recursosBuscador = data;
                $('#loader').modal('hide');
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
            }).
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });
            $scope.currentPage = data.current_page;
            $scope.totalItems = data.total;
            $scope.numPages = data.last_page;
        }).
        error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.loading = false;
            $scope.loadingBusquedaSimple = false;
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });

}

    $scope.pageChanged = function(currentPage){
       // alert(currentPage);
        $scope.currentPage = currentPage;
        $scope.getService(API.url+'bagtesis/backend/public/buscadores/buscador_simple2?page='+$scope.currentPage+"&num_items_pagina="+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value+'&tipo_recurso=publicacion_seriada', $scope.data);
    }   

    $scope.getServiceAvanzado =  function(route_peticion){
    $('#loader').modal('show');
    $scope.i = 0 ;
    $scope.data = { 'data': $scope.selectedBuscadorSimple}

    $scope.respuesta_buscador_simple = [];
    $http.post(route_peticion , $scope.mensaje_buscador_avanzado).
    success(function(data, status, headers, config) {
        $scope.id_recursos = data.data;
        $scope.recursosBuscador = [];
        $scope.recursosBuscador = [];
        $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
        success(function(data, status, headers, config) {
            $scope.recursosBuscador = data;
            $('#loader').modal('hide');
            $scope.loading = false;
            $scope.loadingBusquedaSimple = false;

        }).
        error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.loading = false;
            $scope.loadingBusquedaSimple = false;
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });
        $scope.currentPage = data.current_page;
        $scope.totalItems = data.total;
        $scope.numPages = data.last_page;
        }).
               
        error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.loading = false;
            $scope.loadingBusquedaSimple = false;
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });

}
    
    $scope.pageChangedAvanzado = function(currentPage){
            $scope.currentPage = currentPage;
            $scope.getServiceAvanzado(API.url+'bagtesis/backend/public/buscadores/buscador_avanzado?page='+$scope.currentPage+'&num_items_pagina='+$scope.num_items_pagina+'&orden='+$scope.orden_seleccionado.value,  $scope.mensaje_buscador_avanzado);
    }  

    $scope.desplegar_recurso = function(recurso_id,index_recurso){
        $scope.limpiarLibro();
        $scope.idrecurso = recurso_id;
        $scope.mostrarBuscador = true;
        recursoActual = $scope.recursosBuscador[index_recurso];
        
        if(!$scope.isObtenerEditorial){
            $('#loader').modal('show');  
            dataRE["idRecurso"] = recursoActual.id;
            $http.post(API.url + 'bagtesis/backend/public/obtener_editoriales_recurso', dataRE).success(function(data, status, headers, config) {           
                $scope.editorialesDocumento = data;  
                if(data.length > 0){
                    $scope.editorialPrincipal = data[0].editorial_salida;
                    $scope.paisPrincipal = data[0].pais    
                }
         }).
          error(function(data, status, headers, config) {
          });
        }else{
            $scope.isObtenerEditorial= false;   
        }

        
     
     
        titulos = recursoActual.titulos;
        $scope.idrecurso = recurso_id;
         $scope.ruta_qr = API.url+"bagtesis/backend/public/recursos/"+$scope.idrecurso+"/pdf";
        $cookieStore.put('id_recurso_publicacion_s', $scope.idrecurso);
        $scope.recursoAgregado = false;
        $scope.nombreTitulo = "";
        $scope.edicion = recursoActual.edicion;
        $scope.fechaEdicion = recursoActual.fecha_iso;
        $scope.url = recursoActual.publicacion_seriada[0].url;
        $scope.issn = recursoActual.issn;
        $scope.bibliotecaSelect = recursoActual.bibliotecas[0].id;
        $scope.tipoSerie = recursoActual.publicacion_seriada.tipo;
        $scope.periodicidad = recursoActual.publicacion_seriada[0].periodo ? recursoActual.publicacion_seriada[0].periodo: '';
        $scope.regularidad =  recursoActual.publicacion_seriada[0].regularidad ?  recursoActual.publicacion_seriada[0].regularidad : "";
        $scope.tipoSerie =  recursoActual.publicacion_seriada[0].tipo;
        $scope.nros =  recursoActual.publicacion_seriada[0].vols_aso;
        $scope.vols =  recursoActual.publicacion_seriada[0].nros_aso;
        $scope.diseminacion = recursoActual.publicacion_seriada[0].disemina;
        $scope.existencia = recursoActual.publicacion_seriada[0].existencia;
        if(recursoActual.publicacion_seriada[0].impresion){
            $scope.impresion = recursoActual.publicacion_seriada[0].impresion; 
        }else{
            $scope.impresion = "";   
        }

    
               
        var aux2 = 0;
        var indice2 = 0;
        var aux3 = false;
        for(var i = 0; i < titulos.length; i++){

            if(titulos[i].tipo_tit == 'OP'){
                if (titulos[i].orden){     
                    if(i == 0){
                        aux2 = titulos[i].orden;   
                    }else if(titulos[i].orden < aux2){
                        aux2 = titulos[i].orden;
                        indice2 = i;
                    }
                }else{
                    indice2 = i;                      
                }
               
            }   
        }
        if(titulos.length > 0){
            $scope.nombreTitulo = titulos[indice2].titulo_salida;
            $scope.idTitulo = titulos[indice2].id;    
        }
        else{ 
            $scope.nombreTitulo = "";
            $scope.idTitulo = "";    
        
        }
        if(recursoActual.resumen != ""){
            $scope.tipoContenido = "Resumen";
            $scope.contenidoData = recursoActual.resumen;        
        }else if(recursoActual.notas != ""){
            $scope.tipoContenido = "Nota";
            $scope.contenidoData = recursoActual.notas;          
        }else if(recursoActual.datos_adicionales != ""){
            $scope.tipoContenido = "Datos adicionales";  
            $scope.contenidoData = recursoActual.datos_adicionales;          
        }else { 
            $scope.contenidoData = "";    
            $scope.tipoContenido = "";
        }

        
        $('#loader').modal('hide');  
        $('#buscadorSimple').modal('hide');
        $('#busquedaAvanzada').modal('hide');
    }

/**********************CALENDARIO**************************************************/
$scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.orig_change = function(orig){
        var dd = orig.getDate();
        var mm = orig.getMonth()+1; //January is 0!

        var yyyy = orig.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        }
        $scope.dt_orig = dd+'-'+mm+'-'+yyyy;
        console.log($scope.dt_orig);
       
    };
    $scope.orig_change2 = function(orig){
        var dd = orig.getDate();
        var mm = orig.getMonth()+1; //January is 0!

        var yyyy = orig.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        }
        $scope.dt_orig2 = dd+'-'+mm+'-'+yyyy;
        console.log($scope.dt_orig2);
       
    };

     $scope.setDate = function(fecha_ed) {
        var dd = fecha_ed.substring(6, 8);
        var mm = fecha_ed.substring(4, 6); //January is 0!
        var yyyy =  fecha_ed.substring(0, 4);
        
        dt_destt = dd+'-'+mm+'-'+yyyy;
        return dt_destt;
    };
  
    $scope.open_orig = function(event) {
        event.preventDefault();
        event.stopPropagation();
        $scope.opened_orig = true;
    };


    $scope.setMinDate = function(){
        var now = new Date();
        var oneYr = new Date();
        oneYr.setYear(now.getYear() - 5);
        $scope.minDate = oneYr;
    };
    $scope.setMinDate();

    $scope.dateOptions = {
       formatYear: 'yy',
       startingDay: 1
    };

$scope.today = function() {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    }
    return mm+'-'+dd+'-'+yyyy;
};

$scope.setearTipoFecha = function(){
    if($scope.tipoFechaPrincipal == "anio"){
        $scope.dt_orig = "";
        $scope.fechaPrincipalActiva = false;
    }else{
        $scope.anioPrincipal = "";
        $scope.fechaPrincipalActiva = true;
    }
}

$scope.validarInputAnio = function(anio){
        return anio.replace(/\D/g,'');
}

//**********************************FIN CALENDARIO***********************************///

/****************************INICIO BUSCADOR AVANZADO*******************************///
$scope.getServiceBibliotecas = function (){
   // alert('alert Biblioteca');
        $http.get(API.url+'bagtesis/backend/public/bibliotecas').
            success(function(data, status, headers, config) {
            $scope.bibliotecas = data;
            $scope.options_bibliotecas.push({label:'Todas',value:'todas'});
            angular.forEach($scope.bibliotecas, function(value, key) {
              this.push({label:value.biblioteca_salida, value: value.id});
            },$scope.options_bibliotecas);

            $scope.options_bibliotecas_seleccionado = $scope.options_bibliotecas[0];

            }).
           
            error(function(data, status, headers, config) {
               
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });
};

$scope.getServiceDependencias = function(){
        $http.get(API.url+'bagtesis/backend/public/dependencias').
            success(function(data, status, headers, config) {
            $scope.dependencias = data;
            $scope.options_dependencias.push({label:'Todas',value:'todas'});
            angular.forEach($scope.dependencias, function(value, key) {
              this.push({label:value.dependencia_salida, value: value.id});
            },$scope.options_dependencias);
            $scope.options_dependencias_seleccionado = $scope.options_dependencias[0];
            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });
};
$scope.mostrarBuscador = true;
$scope.busquedaEncontrada = false;

$scope.options = {
                    opciones1: [  
                                    {label: 'Título' ,   value: 'titulo'   },          
                                    {label: 'Autor'  ,   value: 'autor'    },
                                    {label: 'Editorial', value: 'editorial'},
                                    {label: 'ISSN'   ,   value: 'issn'     },
                                    {label: 'Materia',   value: 'materia'},
                                    {label: 'Palabra clave'  , value: 'palabra_clave'},
                                    {label: 'Idioma' ,   value: 'idioma'   }
                                ],

                    opciones2: [
                                    {label: 'Y'   , value: 'AND'},
                                    {label: 'O' , value: 'OR'    },
                                    {label: 'NO'  , value: 'NOT LIKE' }
                               ]
                    };

$scope.mytooltips = [{title: 'Borrar campos', tooltip: 'tooltip', position: 'rigth'}];

$scope.anios = {anio_origen: '',
                anio_destino: ''
                };

$scope.options_bibliotecas = [];
//$scope.options_bibliotecas_seleccionado = {};
$scope.options_dependencias = [];
//$scope.options_dependencias_seleccionado = {};
$scope.num_items_pagina3 = 10;
$scope.options_orden =  [
                          { label: 'Normal', value: 'normal' },
                          { label: 'Título A...Z', value: 'titulo_asc' },
                          { label: 'Título Z...A', value: 'titulo_desc' },
                          { label: 'Autor  A...Z', value: 'autor_asc' },
                          { label: 'Autor  Z...A', value: 'autor_desc' },
                          { label: 'Año ascendente', value: 'anio_asc' },
                          { label: 'Año descendente', value: 'anio_desc' }
                      ];

$scope.orden_seleccionado =  $scope.options_orden[0];


$scope.newOption = function(selet_model1, text_model1, selet_model2, text_model2,selected){
    $scope.inputs.push({
        opt1: {
            select_model: selet_model1,
            text_model: text_model1,
            function_typeahead: '',
        }, 
        opt2: {
            select_model: selet_model2,
            text_model: text_model2,
        },      
    });
};

$scope.initBuscadorAvanzado = function(){
    $scope.bool_table_recursos = false;
    $scope.inputs = [];    
    $scope.newOption($scope.options.opciones1[0], "", $scope.options.opciones2[0], "",$scope.selected);
    $scope.mostrarBuscador = true;
    $scope.busquedaEncontrada = false;
    $scope.recursosBuscador = [];
    $('#busquedaAvanzada').modal('show');
}

$scope.bool_add_button = true;

$scope.getServiceBibliotecas();
$scope.getServiceDependencias();  


$scope.add_option = function(){

    $scope.mytooltips.push({title: 'Borrar campos', tooltip: 'tooltip', position: 'rigth'});
    $scope.newOption($scope.options.opciones1[0], "", $scope.options.opciones2[0], "",$scope.selected);
};

$scope.delete = function(index){
    if($scope.inputs.length!=1){
        $scope.inputs.splice(index,1);
    }
}



$scope.loadingStates4 = function(val,select_model_value){
$scope.selected = "publicacion_seriada";
    if(select_model_value == 'titulo'){
        return $scope.buscar_titulo(val);
    }else{
        if(select_model_value == 'autor'){
            return $scope.buscar_autor(val);           
        }else{
            if(select_model_value == 'cota'){
                return $scope.buscar_cota(val);                   
            }else{
                if(select_model_value == 'jurado'){
                    return $scope.buscar_jurado(val);   
                }else{
                    if(select_model_value == 'tutor_academico'){
                        return $scope.buscar_tutor_academico(val);                         
                    }else{
                        if(select_model_value == 'materia'){
                            return $scope.buscar_materia(val);
                        }else{
                            if(select_model_value == 'palabra_clave'){
                                return $scope.buscar_palabra_clave(val);
                            }else{
                                if(select_model_value == 'tipo_de_trabajo'){
                                    return $scope.buscar_tipo_de_trabajo(val);
                                }else{
                                    if(select_model_value == 'volumen'){
                                        return $scope.buscar_volumen(val);                                        
                                    }else{
                                        if(select_model_value == 'edicion'){
                                            return $scope.buscar_edicion(val);
                                        }else{
                                            if(select_model_value == 'idioma'){
                                                return $scope.buscar_idioma(val);
                                            }else{
                                                if(select_model_value == 'isbn'){
                                                    return $scope.buscar_isbn(val);
                                                }else{
                                                    if(select_model_value == 'issn'){
                                                        return $scope.buscar_issn(val);
                                                    }else{
                                                        if(select_model_value == 'editorial'){
                                                            return $scope.buscar_editorial(val);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
 //   return $scope.funcion_buscar_titulo(val);
};



$scope.buscar_titulo = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_titulo_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};


$scope.buscar_autor = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_autor_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};



$scope.buscar_jurado = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_jurado_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};



$scope.buscar_tutor_academico = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_tutor_academico_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};




$scope.buscar_cota = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_cota_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;

};


$scope.buscar_materia= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_materia_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};

$scope.buscar_palabra_clave= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_palabra_clave_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_tipo_de_trabajo= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_tipo_de_trabajo_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};



$scope.buscar_volumen= function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_volumen_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_edicion = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_edicion_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};

$scope.buscar_idioma = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_idioma_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_isbn = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_isbn_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_issn = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_issn_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {

                      $scope.prom = data;

                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
};


$scope.buscar_editorial = function (val){
  $scope.mensaje_buscador_simple['data'] = val;

  $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_editorial_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
                  .success(function(data, status, headers, config) {
                      $scope.prom = data;
                  }).then(function(){
                      $scope.aux_promise = $scope.prom;  
                      return $scope.prom;
                  });
    return $scope.promise;
}; 



$scope.consulta_buscador_avanzado = function(){
        $scope.mensaje_buscador_avanzado = {};
        $scope.currentPage = 1;
        $scope.mensaje_buscador_avanzado['inputs'] = [];
        $scope.mensaje_buscador_avanzado['inputs'] = $scope.inputs;
        $scope.mensaje_buscador_avanzado['anios'] = $scope.anios;
        $scope.mensaje_buscador_avanzado['tipo_recurso'] = "publicacion_seriada";
        $scope.loading = true;
        $scope.mensaje_buscador_avanzado['biblioteca'] = $scope.options_bibliotecas_seleccionado;
        $scope.mensaje_buscador_avanzado['dependencia'] = $scope.options_dependencias_seleccionado;
        $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_avanzado?page=1&num_items_pagina='+$scope.num_items_pagina3+'&orden='+$scope.orden_seleccionado.value,  $scope.mensaje_buscador_avanzado).
            success(function(data, status, headers, config) {
            
                $scope.a = data;
                $scope.pagina = data;
                $scope.id_recursos = data.data;
                $scope.recursos = [];
                $scope.currentPage = data.current_page;
                $scope.contador_caracteres = 0;
                $scope.totalItems = data.total;
                $scope.numPages = data.last_page;           
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
                $('#loader').modal('show');

                $http.post(API.url+'bagtesis/backend/public/recursos/recursos_info', $scope.id_recursos).
                success(function(data, status, headers, config) {
                
                    
                $scope.mostrarBuscador = false;
                $scope.busquedaEncontrada = true;

                //alert("data2 "+ JSON.stringify(data));
                $scope.recursosBuscador = data;
                $scope.bool_disable = false;
                $scope.bool_recursos_encontrados = true;
                 $('#loader').modal('hide');
            }).
            error(function(data, status, headers, config) {
               // alert(JSON.stringify(data));
                $scope.loading = false;
                $scope.loadingBusquedaSimple = false;
            });
            $scope.bool_table_recursos=true;
          
        
        }).error(function(data, status, headers, config) {
            $('#loader').modal('hide');
            $scope.loading = false;
            $scope.loadingBusquedaSimple = false;
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
            });
  }

/*******************************FIN BUSCADOR AVANZADO******************************///
/****************************** CODIGO DE BARRAS **********************************/

    $scope.mostrarModalCodigoBarra = function(){
        $('#mostrarBarras').modal('show');
        $('#mostrarBarras').on('shown.bs.modal', function () {
            $('#enterBarcode').focus();
        })  
    }

    $scope.buscarRecursoId = function(){
        $('#mostrarBarras').modal('hide');
        $('#loader').modal('show');
        $http.get(API.url+'bagtesis/backend/public/buscar_codigo_barra/'+$scope.codigoBarra).
        success(function(data, status, headers, config) {
            $scope.codigoBarra = "";
            if(data['codigo'] == 0){
                data = data['recurso'];
                $scope.idrecursoBarra = data.recurso_id;
                $scope.tipo_recurso = data.tipo_liter;
                if($scope.idrecursoBarra != "" && $scope.idrecursoBarra != null && $scope.tipo_recurso != "" && $scope.tipo_recurso != null){
                    $('#loader').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $scope.tipo_recurso_ab =  ($scope.tipo_recurso.substring(0, 1)).toUpperCase();

                    if( $scope.tipo_recurso_ab == 'O'){
                        $cookieStore.get('tipo_recurso_id',$scope.tipo_recurso);
                        $cookieStore.put('id_recurso_otros',$scope.idrecursoBarra);
                        $scope.redirect = "otrosRecursos";

                    }else if ($scope.tipo_recurso == "M"){
                        $cookieStore.put('id_recurso_libro',$scope.idrecursoBarra);
                        $scope.redirect = "librosPrincipal";

                    }else if ($scope.tipo_recurso == "T"){
                        $cookieStore.put('id_recurso_tesis',$scope.idrecursoBarra);
                        $scope.redirect = "trabajosAcademicos"; 

                    }else if ($scope.tipo_recurso == "S"){
                        $cookieStore.put('id_recurso_publicacion_s',$scope.idrecursoBarra);
                        $scope.redirect = "publicacionesSeriadas";    
                    }
                    
                    return $state.go($scope.redirect, {}, { reload: true });

                }else{
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                    $('#informacion').modal('show');
                }
            }else{
                //recurso no existe en el sistema
                $scope.codigoBarra = "";
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Este recurso no existe en el sistema';
                $('#informacion').modal('show');
            }
        }).
        error(function(data, status, headers, config) {
            $scope.codigoBarra = "";
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        }); 
    }

    $scope.auxTrim = "";

    $scope.trimRequired = function(model) {
        
        if (model == "nombreTitulo" ){
            if($scope.nombreTitulo){
                $scope.auxTrim = $scope.nombreTitulo.trim();
            }else{
                $scope.auxTrim = "";
            }
        } else if (model == "nombreEditorial" ){
            if($scope.nombreEditorial){
                $scope.auxTrim = $scope.nombreEditorial.trim();
            }else{
                $scope.auxTrim = "";
            }
        } else if (model == "nombreCiudad" ){
            if($scope.nombreCiudad){
                $scope.auxTrim = $scope.nombreCiudad.trim();
            }else{
                $scope.auxTrim = "";
            }
        } else if (model == "nombreFabricante" ){
            if($scope.nombreFabricante){
                $scope.auxTrim = $scope.nombreFabricante.trim();
            }else{
                $scope.auxTrim = "";
            }
        } 
    }

    $scope.blurRequired = function (model) {
        if (model == "nombreTitulo"){
            $scope.nombreTitulo = $scope.auxTrim;
        } else if (model == "nombreEditorial"){
            $scope.nombreEditorial = $scope.auxTrim;
        } else if (model == "nombreCiudad"){
            $scope.nombreCiudad = $scope.auxTrim;
        } else if (model == "nombreFabricante"){
            $scope.nombreFabricante = $scope.auxTrim;
        } 

        $scope.auxTrim = "";
      
    }
    

});


