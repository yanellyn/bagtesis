angular.module('app' )
  .controller('numerosController', function($rootScope, $scope, $window, $http, API, $cookieStore, upload, $state) {

    $scope.idrecurso="";
    $scope.tipo_recurso = $cookieStore.get('tipo_recurso');
    $scope.tipo_recurso_ab =  ($scope.tipo_recurso.substring(0, 1)).toUpperCase();
    $scope.nombre_recurso =  "Publicaciones Seriadas"; 
    $scope.redirect = "publicacionesSeriadas"; 
    $scope.idrecurso = $cookieStore.get('id_recurso_publicacion_s');
    $scope.numero_seleccionado = $cookieStore.get('id_recurso_publicacion');

	$scope.ano_nuevo_agregar = "";
    $scope.volumen_nuevo_agregar = "";
    $scope.numero_inicial = "";
    $scope.numero_final = "";
    $scope.agregarVol = false;
    $scope.agregarNum = false;
    $scope.eliminarNum = false;
    $scope.guardarNum = false;
    $scope.modificarVol = false;

    $scope.volumen_numero_agregar = "";
    $scope.ano_numero_agregar = "";
    $scope.ano_numero_agregar_iso = "";
    $scope.colacion = "";
    $scope.numeracion = "";
    $scope.datos_adicionales = "";
    $scope.existenciaEjemplar = "E";
    $scope.numero_obj = "";
    $scope.guardar = true;
    $scope.volumenes_numeros = [];

    $scope.limpiarNumeros = function(){
        $('#confirmacionNumeros').modal('hide');
        $scope.ano_nuevo_agregar = "";
        $scope.volumen_nuevo_agregar = "";
        $scope.numero_inicial = "";
        $scope.numero_final = "";
        $scope.volumen_numero_agregar = "";
        $scope.ano_numero_agregar = "";
        $scope.ano_numero_agregar_iso = "";
        $scope.numero_seleccionado = "";
        $cookieStore.put('id_recurso_publicacion', "");
        $rootScope.numeroSeleccionado = false;
        $scope.colacion = "";
        $scope.numeracion = "";
        $scope.datos_adicionales = "";
        $scope.existenciaEjemplar = "E";
        $scope.guardar = false;
    }
    $scope.deshabilitarOpcion = function(){
        if(!$scope.numero_seleccionado){
            return "disabled"
        }
    }

    $scope.deshabilitarOpcionEjemplar = function(){
        if(!$scope.numero_seleccionado || $scope.existenciaEjemplar == "I"  || ($scope.existenciaEjemplar == "E" && !$scope.guardar) ){
            return "disabled"
        }
    }


    $scope.obtenerVolumenes = function(){
        $('#loader').modal('show');
        data = {};
        data["id_recurso_publicacion"] = $scope.idrecurso;


        
        $http.post(API.url + 'bagtesis/backend/public/obtener_volumenes_numeros', data)
            .success(function(data, status, headers, config) {           

                $('#loader').modal('hide');
                var auxFechaIso = {};
                var auxVols = {};
                var fechas_iso = [];
                var fecha_iso = "";
                 
                for (var i = 0; i < data.length; i++) {
                    if($scope.numero_seleccionado == data[i].id){
                        $scope.numero_obj = data[i];
                    }
                    data[i].fecha_iso_salida = data[i].fecha_iso.substring(0,4);  
                    fecha_iso = data[i].fecha_iso;
                    volumen = data[i].volumen;
                    if(i>0){
                        if(data[i-1].volumen == data[i].volumen && data[i-1].numero == data[i].numero && data[i-1].fecha_iso == data[i].fecha_iso)
                            continue;
                    }
                    if (fecha_iso in auxFechaIso) {  
                        if(volumen in auxFechaIso[fecha_iso])
                            auxFechaIso[fecha_iso][volumen].push(data[i]);
                        else        
                            auxFechaIso[fecha_iso][volumen] = [data[i]];
                    }else {  
                        if(!auxFechaIso[fecha_iso])
                            auxFechaIso[fecha_iso] = {};
                        auxFechaIso[fecha_iso][volumen] = [data[i]];
                    }
                }  
             

                for(var j in auxFechaIso){
                    fechas_iso.push(auxFechaIso[j]);  
                }

               
                $scope.fechas_iso_final = [];
                for(i = 0; i< fechas_iso.length; i++){
                    for(var j in fechas_iso[i]){
                        $scope.fechas_iso_final.push(fechas_iso[i][j]);  
                    }
                }

                $scope.volumenes_numeros =  $scope.fechas_iso_final;
                console.log(JSON.stringify($scope.fechas_iso_final));

                aux = false;
                if($scope.agregarVol){
                    $scope.msjInformacion = 'Volumen agregado satisfactoriamente ';
                    aux = true;
                }else if($scope.agregarNum){
                    $scope.msjInformacion = 'Número(s) agregado(s) satisfactoriamente ';
                    aux = true;
                }else if($scope.eliminarNum){
                    $scope.msjInformacion = 'Número eliminado satisfactoriamente ';
                    aux = true;
                }else if($scope.guardarNum){
                    $scope.msjInformacion = 'Número modificado satisfactoriamente ';
                    aux = true;
                }else if($scope.modificarVol){
                    $scope.msjInformacion = 'Volumen modificado satisfactoriamente ';
                    aux = true;
                }
             
                if(aux){
                    $('#informacion').modal('show');
                    $scope.agregarVol = false;
                    $scope.agregarNum = false;
                    $scope.guardarNum = false;
                    $scope.eliminarNum = false;
                    $scope.modificarVol = false;
                    aux = false;
                }

                if($scope.numero_seleccionado){
                        $scope.seleccionarRecurso($scope.numero_obj);
                }
              
            })
            .error(function(error) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });
    }
    $scope.obtenerVolumenes();

    $scope.submitAgregarVolumen = function(){
        if($scope.numero_final != ''){
            if( parseInt($scope.numero_final) < parseInt($scope.numero_inicial)){
                $scope.msjInformacion = 'El número inicial debe ser menor que el número final';
                $('#informacion').modal('show');
            }else{
                $scope.msjConfirmacion = "¿Está seguro que desea agregar este volumen?"
                $('#confirmacionAgregarVolumen').modal('show');
            }
        }else{
            $scope.msjConfirmacion = "¿Está seguro que desea agregar este volumen?"
                $('#confirmacionAgregarVolumen').modal('show');
        }
    }


    $scope.agregarVolumen = function(){
        $('#confirmacionAgregarVolumen').modal('hide');
        $('#loader').modal('show');
        data = {};
        if($scope.ano_nuevo_agregar.length < 4){
            aux = $scope.ano_nuevo_agregar;
            for(i = $scope.ano_nuevo_agregar.length; i<4; i++){
                aux = "0" + aux;
            }
            $scope.ano_nuevo_agregar = aux;
        }
       
        data["inicial"] = $scope.numero_inicial;


        if(!$scope.numero_final)
            data["final"] = $scope.numero_inicial;
        else
            data["final"] = $scope.numero_final;            

        data["volumen"] = $scope.volumen_nuevo_agregar;
        data["fecha_iso"] = $scope.ano_nuevo_agregar + "00" + "00";
        data["id_recurso_publicacion"] = $scope.idrecurso;
        data["idCatalogador"] = $cookieStore.get('user_id');

  
        $http.post(API.url+'bagtesis/backend/public/agregar_volumen_serie', data).
            success(function(data, status, headers, config) {
                if(data["codigo"] == "0"){
                    
                    $('#modal_volumen').modal('hide');
                    $scope.limpiarNumeros();
                    $scope.agregarVol = true;
                    $scope.obtenerVolumenes();

                
                }else{
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Los datos introducidos corresponden con un volumen ya existente ';
                    $('#informacion').modal('show');

                }
            }).
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');

            }); 
    }

    $scope.tablaBotonAgregar = function(iso, iso_salida, volumen){
        $scope.volumen_numero_agregar = volumen;
        $scope.ano_numero_agregar = iso_salida;
        $scope.ano_numero_agregar_iso = iso;
         $('#modal_numero').modal('show');
                

    }


    $scope.validarInput = function(value){
        if(value)
            return value.replace(/\D/g,'');
        else 
            return '';
    }

    $scope.submitAgregarNumero = function(){
        if($scope.numero_final != ''){
        	console.log($scope.numero_inicial + ' ' + $scope.numero_final );
            if( parseInt($scope.numero_final) < parseInt($scope.numero_inicial)){
                $scope.msjInformacion = 'El número inicial debe ser menor que el número final';
                $('#informacion').modal('show');
            }else{
                $scope.msjConfirmacion = "¿Está seguro que desea agregar estos números?"
                $('#confirmacionAgregarNumero').modal('show');
            }
        }else{
            $scope.msjConfirmacion = "¿Está seguro que desea agregar este número?"
                $('#confirmacionAgregarNumero').modal('show');
        }
    }

    $scope.agregarNumero = function(){
        $('#confirmacionAgregarNumero').modal('hide');
        $('#loader').modal('show');
        data = {};       
        data["inicial"] = $scope.numero_inicial;


        if(!$scope.numero_final)
            data["final"] = $scope.numero_inicial;
        else
            data["final"] = $scope.numero_final;            

        data["volumen"] = $scope.volumen_numero_agregar;
        data["fecha_iso"] = $scope.ano_numero_agregar_iso;
        data["id_recurso_publicacion"] = $scope.idrecurso;
        data["idCatalogador"] = $cookieStore.get('user_id');
  
        $http.post(API.url+'bagtesis/backend/public/agregar_numero_serie', data).
            success(function(data, status, headers, config) {
                if(data["codigo"] == "0"){
                    $('#modal_numero').modal('hide');
                    $scope.limpiarNumeros();
                    $scope.agregarNum = true;
                    $scope.obtenerVolumenes();
                   

                }else{
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Ya existen registros asociados a los números introducios en ese rango';
                    $('#informacion').modal('show');

                }
            }).
            error(function(data, status, headers, config) {
               $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');

            }); 
    }

    $scope.seleccionarRecurso = function(numero, indice){ 
        if(numero.existencia == "I")
            $scope.guardar = false;
        else
             $scope.guardar = true;
        $scope.numero_obj = numero; 
        $scope.numero_seleccionado = numero.id;
        $cookieStore.put('id_recurso_publicacion', numero.id);
        $rootScope.numeroSeleccionado = true;  
        $scope.colacion = numero.paginas;
        $scope.numeracion = numero.numeracion;
        $scope.datos_adicionales = numero.datos_adicionales;
        if(numero.existencia) 
            $scope.existenciaEjemplar = numero.existencia;
        else{

        }
   

  
    }

    $scope.agregarClaseNumero = function(numero){
        if(numero.id == $scope.numero_seleccionado){
            return "seleccionNumero";
        }
        else 
            return "";
    }




    $scope.submitNumeros = function(){
            
        $('#confirmacionNumeros').modal('show');
    }    

    $scope.eliminarNumero = function(numero){
        $('#confirmacionNumeros').modal('hide');
        $('#loader').modal('show');
        data["idRecurso"] = $scope.numero_seleccionado;
        data["idCatalogador"] = $cookieStore.get('user_id');
        data["id_recurso_publicacion"] = $scope.idrecurso;
        $http.post(API.url+'bagtesis/backend/public/eliminar_numero', data).
            success(function(data, status, headers, config) {
                if(data["codigo"] == "0"){
                    $('#modal_numero').modal('hide');
                    $scope.limpiarNumeros();
                    $scope.eliminarNum = true;
                    $scope.obtenerVolumenes();
                }
            }).
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');

            }); 
    }



    $scope.guardarNumero = function(){
        $('#confirmacionNumeros').modal('hide');
        $('#loader').modal('show');       
        data["colacion"] = $scope.colacion;
        data["numeracion"] = $scope.numeracion;
        data["datos_adicionales"] = $scope.datos_adicionales;
        data["idRecurso"] = $scope.numero_seleccionado;
        data["existencia"] = $scope.existenciaEjemplar;
        data["idCatalogador"] = $cookieStore.get('user_id');
        data["id_recurso_publicacion"] = $scope.idrecurso;

        $http.post(API.url+'bagtesis/backend/public/modificar_numero', data).
            success(function(data, status, headers, config) {
                if(data["codigo"] == "0"){
                    $scope.guardar = true;
                    $('#modal_numero').modal('hide');
                    $scope.guardarNum = true;
                    $scope.obtenerVolumenes();
                }
            }).
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');

            }); 
    }

    $scope.submitEditarVolumen = function(){
        $scope.msjConfirmacion = "¿Está seguro que desea modificar este volumen?"
        $('#confirmacionEditarVolumen').modal('show');
    }

    $scope.tablaBotonEditarVolumen = function(iso, iso_salida, volumen){
        $scope.volumen_numero_agregar = volumen;
        $scope.ano_numero_agregar = iso_salida;
        $scope.ano_numero_agregar_iso = iso;
        $scope.volumen_nuevo_agregar = volumen;
        $scope.ano_nuevo_agregar = iso_salida;

        $('#modal_editar_volumen').modal('show');
    }
    

    $scope.modificarVolumen = function(){
        $('#confirmacionEditarVolumen').modal('hide');
        $('#loader').modal('show');
        data = {};
        if($scope.ano_nuevo_agregar.length < 4){
            aux = $scope.ano_nuevo_agregar;
            for(i = $scope.ano_nuevo_agregar.length; i<4; i++){
                aux = "0" + aux;
            }
            $scope.ano_nuevo_agregar = aux;
        }
        data["volumen_anterior"] = $scope.volumen_numero_agregar;    
        data["volumen_actual"] = $scope.volumen_nuevo_agregar;
        data["fecha_iso"] = $scope.ano_nuevo_agregar + "00" + "00";
        data["fecha_iso_anterior"] = $scope.ano_numero_agregar_iso;

        data["id_recurso_publicacion"] = $scope.idrecurso;
        data["idCatalogador"] = $cookieStore.get('user_id');
  
        $http.post(API.url+'bagtesis/backend/public/editar_volumen', data).
            success(function(data, status, headers, config) {
                if(data["codigo"] == "0"){        
                    $('#modal_editar_volumen').modal('hide');
                    $scope.modificarVol = true;
                    $scope.limpiarNumeros();
                    $scope.obtenerVolumenes();
                }else{
                    $('#loader').modal('hide');
                    $scope.msjInformacion = 'Los datos introducidos corresponden con un volumen ya existente ';
                    $('#informacion').modal('show');
                }
            }).
            error(function(data, status, headers, config) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');

            }); 
    }
});