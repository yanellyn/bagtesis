angular.module('app' )
  .controller('titulosController', function($scope, $window, $http, API, $cookieStore, $state) {
    $scope.idrecurso="";
    $scope.tipo_recurso = $cookieStore.get('tipo_recurso');
    $scope.tipo_recurso_ab =  ($scope.tipo_recurso.substring(0, 1)).toUpperCase();
    if( $scope.tipo_recurso_ab == 'O')
        $scope.tipo_recurso_ab = $cookieStore.get('tipo_recurso_id'); // validacion  agregada para el modulo de otros recursos
    if ($scope.tipo_recurso == "monografia"){
        $scope.nombre_recurso =  "Monografías";
        $scope.idrecurso = $cookieStore.get('id_recurso_libro');
        $scope.redirect = "libros";
    }else if ($scope.tipo_recurso == "tesis"){
        $scope.nombre_recurso =  "Trabajos Académicos";
        $scope.idrecurso = $cookieStore.get('id_recurso_tesis');
        $scope.redirect = "trabajosAcademicos";
    }else if ($scope.tipo_recurso == "serie"){
        $scope.idrecurso = $cookieStore.get('id_recurso_publicacion');
        $scope.nombre_recurso =  "Publicaciones Seriadas"; 
        $scope.redirect = "publicacionesSeriadas";    
    }else if ($scope.tipo_recurso == "otros"){
        $scope.idrecurso = $cookieStore.get('id_recurso_otros');
        $scope.nombre_recurso =  "Otros Recursos"; 
        $scope.redirect = "otrosRecursos";    
    }
    if(!$scope.idrecurso){
         return $state.go('home', {}, { reload: true });
    }
    $('#loader').modal('show');



	$scope.nombreTitulo = "";
	$scope.titulosMostrar = "";
	$scope.loadingTitulos = false;
	$scope.tipoTitulos = "";
	$scope.botonAgregarTitulo = false;
	$scope.msjConfirmacion = "";
	$scope.msjInformacion = "";
	$scope.esAgregarTitulo = "";
	$scope.tituloRecurso = {};

	$http.get(API.url + 'bagtesis/backend/public/tipo_titulos?tipo_recurso='+$scope.tipo_recurso_ab).success(function(data, status, headers, config) {           
        $scope.tipoTitulos = data; 
        $http.post(API.url + 'bagtesis/backend/public/recursos/'+$scope.idrecurso+'/titulos').success(function(data, status, headers, config) {           
        $scope.titulosMostrar = data;
        $('#loader').modal('hide');
         
        }).
        error(function(error) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });
    }).
    error(function(error) {
        $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });  


	



	$scope.seleccionarTitulo = function(titulo, indice){
	    $scope.tituloSel = indice;
	    $scope.tipoTitulo = titulo.tipo_tit;
	    $scope.nombreTitulo = titulo.titulo_salida;
	    $scope.botonAgregarTitulo = true;
	    $scope.idTitulo = titulo.id;

	};

	$scope.moveUpTitulo = function(num) {
	    if (num > 0) {
	      tmp = $scope.titulosMostrar[num - 1];
	      $scope.titulosMostrar[num - 1] = $scope.titulosMostrar[num];
	      $scope.titulosMostrar[num] = tmp;
	      $scope.tituloSel--;
	    }
	  };
	$scope.moveDownTitulo = function(num) {
	    if (num < $scope.titulosMostrar.length - 1) {
	      tmp = $scope.titulosMostrar[num + 1];
	      $scope.titulosMostrar[num + 1] = $scope.titulosMostrar[num];
	      $scope.titulosMostrar[num] = tmp;
	      $scope.tituloSel++;
	    }
	};

	$scope.seleccionarTituloAutocompletado = function(){
	    $scope.tipoTitulo = $scope.nombreTitulo.tipo_titulo;
	    $scope.nombreTitulo = $scope.nombreTitulo.titulo;

	};


    $scope.limpiarTitulo = function(){
        $('#confirmacion').modal('hide');  
        $('#loader').modal('show'); 
        $http.post(API.url + 'bagtesis/backend/public/recursos/'+$scope.idrecurso+'/titulos').success(function(data, status, headers, config) {           
            $scope.titulosMostrar = data;
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Se limpio la pantalla exitosamente' ;
            $('#informacion').modal('show'); 
        }). 
        error(function(error) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });

    	$scope.tituloRecurso = {};
        $scope.tituloSel = -1;
	    $scope.tipoTitulo = "";
	    $scope.nombreTitulo = "";
	    $scope.esAgregarTitulo = "";
	    $scope.botonAgregarTitulo = false;

    }

    $scope.eliminarAutor = function(){
        $('#confirmacionEliminar').modal('hide');
        $('#loader').modal('show');
        $scope.tituloRecurso = {};
        $scope.tituloRecurso["id_recurso"] = $scope.idrecurso;
        $scope.tituloRecurso["id_titulo"] = $scope.idTitulo;
        $scope.tituloRecurso["idCatalogador"] = $cookieStore.get('user_id');

        $http.post(API.url+'bagtesis/backend/public/titulos/eliminar_titulo_recurso', $scope.tituloRecurso)
            .success(function(data, status, headers, config) {     
               
                if(data["codigo"] == "0"){
                    //$scope.limpiarTitulo();
                    $scope.tituloRecurso = {};
                    $scope.tituloSel = -1;
                    $scope.tipoTitulo = "";
                    $scope.nombreTitulo = "";
                    $scope.esAgregarTitulo = "";
                    $scope.botonAgregarTitulo = false;
                    $http.post(API.url + 'bagtesis/backend/public/recursos/'+$scope.idrecurso+'/titulos').success(function(data, status, headers, config) {           
				        $scope.titulosMostrar = data;
                        $('#loader').modal('hide');
				         
				    }).
				    error(function(error) {
				       $('#loader').modal('hide');
                        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                        $('#informacion').modal('show');
				    });
                    $scope.msjInformacion = ' Titulo eliminado exitosamente' ;
                    $('#informacion').modal('show');  
                }
                $('#loader').modal('hide');

            }).error(function(error) {
                 $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });

    }


     $scope.modificarTitulo = function(){
        $('#confirmacionSub').modal('hide');
         $('#loader').modal('show');
        $scope.tituloRecurso = {};
        $scope.tituloRecurso["id_recurso"] = $scope.idrecurso;
        $scope.tituloRecurso["tipo_recurso"] = $scope.tipo_recurso_ab;
        $scope.tituloRecurso["nombreTitulo"] = $scope.nombreTitulo;    
        $scope.tituloRecurso["tipoTitulo"] = $scope.tipoTitulo;
        $scope.tituloRecurso["id_titulo"] = $scope.idTitulo;
        $scope.tituloRecurso["orden_titulos"] = $scope.titulosMostrar;

        $scope.tituloRecurso["idCatalogador"] = $cookieStore.get('user_id');


        $http.post(API.url+'bagtesis/backend/public/titulos/modificar_titulo_recurso', $scope.tituloRecurso)
            .success(function(data, status, headers, config) {     
                        
                if(data["codigo"] == "0"){
                    $http.post(API.url + 'bagtesis/backend/public/recursos/'+$scope.idrecurso+'/titulos').success(function(data, status, headers, config) {           
				        $scope.titulosMostrar = data;
				         
				    }).
                    error(function(error) {
                        $('#loader').modal('hide');
                        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                        $('#informacion').modal('show');
                    });
                    $scope.msjInformacion = 'Titulo modificado exitosamente' ;
                    $('#informacion').modal('show');
                    console.log(JSON.stringify(data));
                }
                 $('#loader').modal('hide');

            }).error(function(error) {
                 $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });
    }

    $scope.agregarTitulo = function(){
        $('#confirmacionSub').modal('hide');
         $('#loader').modal('show');
        $scope.tituloRecurso = {};
        $scope.tituloRecurso["id_recurso"] = $scope.idrecurso;
        $scope.tituloRecurso["tipo_recurso"] = $scope.tipo_recurso_ab;
        $scope.tituloRecurso["nombreTitulo"] = $scope.nombreTitulo;    
        $scope.tituloRecurso["tipoTitulo"] = $scope.tipoTitulo;
        $scope.tituloRecurso["idCatalogador"] = $cookieStore.get('user_id');

        $http.post(API.url+'bagtesis/backend/public/titulos/agregar_titulo_recurso', $scope.tituloRecurso)
            .success(function(data, status, headers, config) {     
                        
                if(data["codigo"] == "0"){

                    $http.post(API.url + 'bagtesis/backend/public/recursos/'+$scope.idrecurso+'/titulos').success(function(data, status, headers, config) {           
				        $scope.titulosMostrar = data;
				         
				    }).
                    error(function(error) {
                        $('#loader').modal('hide');
                        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                        $('#informacion').modal('show');
                    });
                    $scope.tituloRecurso = {};
                    $scope.tituloSel = -1;
                    $scope.tipoTitulo = "";
                    $scope.nombreTitulo = "";
                    $scope.esAgregarTitulo = "";
                    $scope.botonAgregarTitulo = false;
                    $scope.msjInformacion = 'Titulo agregado exitosamente' ;
                    $('#informacion').modal('show');
                    console.log(JSON.stringify(data));
                }else  if(data["codigo"] == "1"){
                       $scope.msjInformacion = 'El titulo ya existe';
                    $('#informacion').modal('show');

                }
                 $('#loader').modal('hide');
            }).error(function(error) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });
    }






    $scope.submitFormluarioTitulo=function(){
         $('#confirmacionSub').modal('show');
    }



	$scope.mensaje_buscador_simple = {};
	$scope.loadingStates   = function(val,select_model_value, tipo_busqueda){
	    $scope.selected = tipo_busqueda;
	    $scope.nuevo = select_model_value;
	    console.log("val "+val);
	    console.log("select_model_value "+select_model_value);
	    console.log("tipo_recurso "+tipo_busqueda);
	    
        
    	if(select_model_value == 'titulo'){
            $scope.loadingTitulos = true;
        	return $scope.buscar_titulo(val);
        }
    }

    $scope.buscar_titulo = function (val){
		$scope.mensaje_buscador_simple['data'] = val;
		$scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_titulo_autocompletado_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
		              .success(function(data, status, headers, config) {
		                  $scope.prom = data;
	                      //alert(JSON.stringify(data));
	                      $scope.loadingTitulos = false;
		              }).then(function(){
	                      $scope.loadingTitulos = false;
		                  $scope.aux_promise = $scope.prom;
	                      
		                  return $scope.prom;
		              });

		return $scope.promise;

	};

    $scope.auxTrim = "";

    $scope.trimRequired = function(model) {
        
        if (model == "nombreTitulo" ){
            if($scope.nombreTitulo){
                $scope.auxTrim = $scope.nombreTitulo.trim();
            }else{
                $scope.auxTrim = "";
            }
        } 
    }

    $scope.blurRequired = function (model) {
        if (model == "nombreTitulo"){
            $scope.nombreTitulo = $scope.auxTrim;
        }
        $scope.auxTrim = "";
      
    }

});