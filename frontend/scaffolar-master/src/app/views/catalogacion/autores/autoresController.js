angular.module('app' )
  .controller('autoresController', function($scope, $window, $http, API, $cookieStore, $state) {
    $scope.tipo_recurso = $cookieStore.get('tipo_recurso');
    $scope.tipo_recurso_ab =  ($scope.tipo_recurso.substring(0, 1)).toUpperCase();
    if($scope.tipo_recurso_ab == 'O')
        $scope.tipo_recurso_ab = $cookieStore.get('tipo_recurso_id'); // validacion  agregada para el modulo de otros recursos
    if ($scope.tipo_recurso == "monografia"){
        $scope.nombre_recurso =  "Monografías";
        $scope.idrecurso = $cookieStore.get('id_recurso_libro');
        $scope.redirect = "libros";
    }else if ($scope.tipo_recurso == "tesis"){
        $scope.nombre_recurso =  "Trabajos Académicos";
        $scope.idrecurso = $cookieStore.get('id_recurso_tesis');
        $scope.redirect = "trabajosAcademicos";
    }else if ($scope.tipo_recurso == "serie"){
        $scope.idrecurso = $cookieStore.get('id_recurso_publicacion');
        $scope.nombre_recurso =  "Publicaciones Seriadas"; 
        $scope.redirect = "publicacionesSeriadas";    
    }else if ($scope.tipo_recurso == "otros"){
        $scope.idrecurso = $cookieStore.get('id_recurso_otros');
        $scope.nombre_recurso =  "Otros Recursos"; 
        $scope.redirect = "otrosRecursos";    
    }
    if(!$scope.idrecurso){
         return $state.go('home', {}, { reload: true });
    } 
    $('#loader').modal('show');


	/***********************Inicio Autores del Recurso ************************************/
    $scope.tipoAutor = "";
    $scope.tipoAutor2 = "";
    $scope.nombreAutor = "";
    $scope.apellidoAutor = "";
    $scope.fechaAutor = "";
    $scope.correoAutor = "";
    //$scope.entidadAutor = "";
    $scope.urlAutor = "";
    $scope.autoresMostrar = "";
    $scope.botonAgregarAutor = false;
    
    $scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.setDate = function(fecha_ed) {
        if(fecha_ed != "" ){
            var dd = fecha_ed.substring(6, 8);
            var mm = fecha_ed.substring(4, 6); //January is 0!
            var yyyy =  fecha_ed.substring(0, 4);       
            dt_destt = dd+'-'+mm+'-'+yyyy;
            return dt_destt;
        }else{
            return "";
        }
    };

    /************************Inicio Tipo Autores**********************************/
    $scope.tipoAutor1 = ["Personal", "Conferencia", "Institucional"];

    $scope.seleccionarTipoAutor1 = function(){
        if ($scope.tipoAutor == null){
            $scope.tipoAutor = ""
        }
    }

    $http.get(API.url + 'bagtesis/backend/public/tipo_autores?tipo_recurso='+$scope.tipo_recurso_ab).success(function(data, status, headers, config) {           
        $scope.tipoAutores2 = data; 
        /************************Fin Tipo Autores**********************************/
        //ID con muchos autores: T043500038315
        $http.post(API.url + 'bagtesis/backend/public/autores/obtener_autores?id_recurso=' +  $scope.idrecurso + '&tipo_recurso='+$scope.tipo_recurso_ab).success(function(data, status, headers, config) {     
            $('#loader').modal('hide');  
            $scope.autoresMostrar = data; 
        }).
        error(function(error) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });           
    }).
    error(function(error) {
        $('#loader').modal('hide');
        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
        $('#informacion').modal('show');
    });         

    


	/***********************Fin Autores del Recurso ************************************/

 


    $scope.limpiarAutor = function(){
        $('#confirmacion').modal('hide');  
        $('#loader').modal('show');  
        $http.post(API.url + 'bagtesis/backend/public/autores/obtener_autores?id_recurso=' +  $scope.idrecurso + '&tipo_recurso='+$scope.tipo_recurso_ab).success(function(data, status, headers, config) {     
        $('#loader').modal('hide');  

        $scope.autoresMostrar = data; 
        $scope.msjInformacion = 'Se limpio la pantalla exitosamente' ;
        $('#informacion').modal('show'); 
        }).
        error(function(error) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });

        $scope.tipoAutor = "";
        $scope.tipoAutor2 = "";
        $scope.nombreAutor = "";
        $scope.apellidoAutor = "";
        $scope.fechaAutor = "";
        $scope.correoAutor = "";
        $scope.entidadAutor = "";
        $scope.urlAutor = "";
        $scope.esAgregarAutor = "";
        $scope.autorSel = -1;
        $scope.botonAgregarAutor = false;
    }

    $scope.submitFormluarioAutor=function(){
         $('#confirmacionSub').modal('show');
    }

    $scope.agregarAutor = function(){
        $('#confirmacionSub').modal('hide');
        $('#loader').modal('show');  
        $scope.autorRecurso = {};
        $scope.autorRecurso["id_recurso"] =  $scope.idrecurso;
        $scope.autorRecurso["tipo_recurso"] = $scope.tipo_recurso_ab;
        if($scope.tipoAutor == "Personal" && $scope.nombreAutor) {
            if($scope.nombreAutor.trim() == "") {
                $scope.dataRecurso["nombreAutor"] = $scope.apellidoAutor;
            }else{
                $scope.dataRecurso["nombreAutor"] = $scope.apellidoAutor + ", " + $scope.nombreAutor;    
            }   
        }else{
             $scope.autorRecurso["nombreAutor"] = $scope.apellidoAutor; 
        }
        $scope.autorRecurso["tipoAutor1"] = $scope.tipoAutor[0];
        $scope.autorRecurso["tipoAutor2"] = $scope.tipoAutor2;
        
        if($scope.fechaAutor) {
            varspl = $scope.fechaAutor.split('-');
            $scope.autorRecurso["fechaAutor"] = varspl[2] + varspl[1] + varspl[0];
        }else{
             $scope.autorRecurso["fechaAutor"] = "";
        }
        $scope.autorRecurso["correoAutor"] = $scope.correoAutor;
        $scope.autorRecurso["entidadAutor"] = $scope.entidadAutor;
        $scope.autorRecurso["urlAutor"] = $scope.urlAutor;
        $scope.autorRecurso["idCatalogador"] = $cookieStore.get('user_id');

        $http.post(API.url+'bagtesis/backend/public/autores/agregar_autor_recurso', $scope.autorRecurso)
            .success(function(data, status, headers, config) {     
                        
                if(data["codigo"] == "0"){
                    $('#loader').modal('hide');  
                        //$scope.limpiarAutor();
                    $scope.tipoAutor = "";
                    $scope.tipoAutor2 = "";
                    $scope.nombreAutor = "";
                    $scope.apellidoAutor = "";
                    $scope.fechaAutor = "";
                    $scope.correoAutor = "";
                    $scope.entidadAutor = "";
                    $scope.urlAutor = "";
                    $scope.esAgregarAutor = "";
                    $scope.autorSel = -1;
                    $http.post(API.url + 'bagtesis/backend/public/autores/obtener_autores?id_recurso=' +  $scope.idrecurso + '&tipo_recurso='+$scope.tipo_recurso_ab).success(function(data, status, headers, config) {           
                        $scope.autoresMostrar = data;                   
                    }).
                    error(function(error) {
                        $('#loader').modal('hide');
                        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                        $('#informacion').modal('show');
                    });
                    $scope.msjInformacion = ' Autor agregado exitosamente' ;
                    $('#informacion').modal('show');  
                }else if(data["codigo"] == "1"){
                     $('#loader').modal('hide');  
                    $scope.msjInformacion = ' El autor ya existe' ;
                    $('#informacion').modal('show');  
                }

            }).error(function(error) {
                 $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });

    }

    $scope.modificarAutor = function(){
        $('#confirmacionSub').modal('hide');
        $('#loader').modal('show');  
        $scope.autorRecurso = {};
        $scope.autorRecurso["id_recurso"] = $scope.idrecurso;
//        $scope.autorRecurso["id_recurso"] = 'T043500038315';
       
        $scope.autorRecurso["tipo_recurso"] = $scope.tipo_recurso_ab;
        if($scope.tipoAutor == "Personal"  && $scope.nombreAutor) {
            if($scope.nombreAutor.trim() == "") {
                $scope.dataRecurso["nombreAutor"] = $scope.apellidoAutor;
            }else{
                $scope.dataRecurso["nombreAutor"] = $scope.apellidoAutor + ", " + $scope.nombreAutor;    
            }       
        }else{
             $scope.autorRecurso["nombreAutor"] = $scope.apellidoAutor; 
        }
        $scope.autorRecurso["tipoAutor1"] = $scope.tipoAutor[0];
        $scope.autorRecurso["tipoAutor2"] = $scope.tipoAutor2;
        if($scope.fechaAutor != "") {
            varspl = $scope.fechaAutor.split('-');
            $scope.autorRecurso["fechaAutor"] = varspl[2] + varspl[1] + varspl[0];
        }else{
             $scope.autorRecurso["fechaAutor"] = "";
        }
        $scope.autorRecurso["correoAutor"] = $scope.correoAutor;
        $scope.autorRecurso["entidadAutor"] = $scope.entidadAutor;
        $scope.autorRecurso["urlAutor"] = $scope.urlAutor;
        $scope.autorRecurso["id_autor"] = $scope.idAutor;

        $scope.autorRecurso["orden_autores"] = $scope.autoresMostrar;
        
        $scope.autorRecurso["idCatalogador"] = $cookieStore.get('user_id');

        //alert(JSON.stringify($scope.autorRecurso));

        $http.post(API.url+'bagtesis/backend/public/autores/modificar_autor_recurso', $scope.autorRecurso)
            .success(function(data, status, headers, config) {     
                        
                if(data["codigo"] == "0"){
                    //$scope.limpiarAutor();
                    $http.post(API.url + 'bagtesis/backend/public/autores/obtener_autores?id_recurso=' +  $scope.idrecurso + '&tipo_recurso='+$scope.tipo_recurso_ab)
                    .success(function(data, status, headers, config) {           
                        $scope.autoresMostrar = data;
                         
                    }).
                    error(function(error) {
                        $('#loader').modal('hide');
                        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                        $('#informacion').modal('show');
                    });
                    $scope.msjInformacion = 'Autor modificado exitosamente' ;
                    $('#informacion').modal('show');
                    console.log(JSON.stringify(data));
                    
                }
                $('#loader').modal('hide'); 

            }).error(function(error) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });

    }

    $scope.eliminarAutor = function(){
        $('#confirmacionEliminar').modal('hide');
         $('#loader').modal('show');  
        $scope.autorRecurso = {};
        $scope.autorRecurso["id_recurso"] = $scope.idrecurso;
        $scope.autorRecurso["id_autor"] = $scope.idAutor;
        $scope.autorRecurso["idCatalogador"] = $cookieStore.get('user_id');

        $http.post(API.url+'bagtesis/backend/public/autores/eliminar_autor_recurso', $scope.autorRecurso)
            .success(function(data, status, headers, config) {     
                $('#loader').modal('hide');       
                if(data["codigo"] == "0"){

                    //$scope.limpiarAutor();
                    $scope.tipoAutor = "";
                    $scope.tipoAutor2 = "";
                    $scope.nombreAutor = "";
                    $scope.apellidoAutor = "";
                    $scope.fechaAutor = "";
                    $scope.correoAutor = "";
                    $scope.entidadAutor = "";
                    $scope.urlAutor = "";
                    $scope.esAgregarAutor = "";
                    $scope.autorSel = -1;
                    $scope.botonAgregarAutor = false;
                    $http.post(API.url + 'bagtesis/backend/public/autores/obtener_autores?id_recurso=' +  $scope.idrecurso + '&tipo_recurso='+$scope.tipo_recurso_ab).success(function(data, status, headers, config) {           
                        $scope.autoresMostrar = data;
                         
                    }).
                    error(function(error) {
                       $('#loader').modal('hide');
                        $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                        $('#informacion').modal('show');
                    });
                    $scope.msjInformacion = ' Autor eliminado exitosamente' ;
                    $('#informacion').modal('show');  
                }

            }).error(function(error) {
                $('#loader').modal('hide');
                $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
                $('#informacion').modal('show');
            });

    }

	$scope.seleccionarAutor = function(autor, indice){
		$scope.botonAgregarAutor = true;
	    $scope.autorSel = indice;
        if(autor.tipo_autor == "P"){
            $scope.tipoAutor = "Personal";
            $scope.aux_name = autor.autor_salida.split(", ");
            $scope.apellidoAutor = $scope.aux_name[0];
            if($scope.aux_name.length > 1){
                $scope.nombreAutor = $scope.aux_name[1];
            }
        }else if(autor.tipo_autor == "I"){
            $scope.tipoAutor = "Institucional";
            $scope.apellidoAutor = autor.autor_salida;
        }else if(autor.tipo_autor == "C"){
            $scope.tipoAutor = "Conferencia";
            $scope.apellidoAutor = autor.autor_salida;
        }
        $scope.tipoAutor2 = autor.id_tipo_autor_2;

        //alert(JSON.stringify($scope.fechaAutor));
        if(autor.fecha_nac_dec){
            $scope.fechaAutor =  $scope.setDate(autor.fecha_nac_dec);
        }else{
            $scope.fechaAutor =  "";
        }
        $scope.correoAutor = autor.dir_electr;
        $scope.entidadAutor = "";
        $scope.urlAutor = autor.url;
        $scope.idAutor = autor.id;
        //alert(JSON.stringify(autor.orden));
	};

	$scope.moveUpAutor = function(num) {
	    if (num > 0) {
			console.log("moveUp");
			tmp = $scope.autoresMostrar[num - 1];
			$scope.autoresMostrar[num - 1] = $scope.autoresMostrar[num];
			$scope.autoresMostrar[num] = tmp;
            //$scope.autoresMostrar[num - 1].orden = $scope.autoresMostrar[num - 1].orden - 1;
            //$scope.autoresMostrar[num].orden = $scope.autoresMostrar[num].orden + 1;
			$scope.autorSel--;
	    }
	  };
	
	$scope.moveDownAutor = function(num) {
	    if (num < $scope.autoresMostrar.length - 1) {
			console.log("moveDown");             
			tmp = $scope.autoresMostrar[num + 1];
			$scope.autoresMostrar[num + 1] = $scope.autoresMostrar[num];
			$scope.autoresMostrar[num] = tmp;
            //$scope.autoresMostrar[num + 1].orden = $scope.autoresMostrar[num + 1].orden + 1;
            //$scope.autoresMostrar[num].orden = $scope.autoresMostrar[num].orden - 1;
             
			$scope.autorSel++;
	    }
	};



    $scope.loadingStates   = function(val,select_model_value, tipo_busqueda){
        $scope.selected = tipo_busqueda;
        $scope.nuevo = select_model_value;
        console.log("val "+val);
        console.log("select_model_value "+select_model_value);
        console.log("tipo_recurso "+tipo_busqueda);
        
    
        if(select_model_value == 'autor'){
            $scope.loadingAutor = true;
            return $scope.buscar_autor(val); 
        }
    };

    $scope.buscar_autor = function (val){
        console.log("Entro a buscar autor");
        $scope.mensaje_buscador_simple = {};
        $scope.mensaje_buscador_simple['data'] = val;

        //$scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_autor_typeahead?tipo_recurso='+$scope.selected, $scope.mensaje_buscador_simple)
        $scope.promise = $http.post(API.url+'bagtesis/backend/public/buscadores/buscador_autor_autocompletado_typeahead?tipo_recurso='+$scope.selected+'&tipo_autor='+$scope.tipoAutor[0], $scope.mensaje_buscador_simple)
                      .success(function(data, status, headers, config) {
                          console.log(JSON.stringify(data));
                          $scope.prom = data;
                          $scope.loadingAutor = false;
                      }).then(function(){
                         
                          $scope.aux_promise = $scope.prom;  
                          return $scope.prom;
                      });
        
        return $scope.promise;
    };

    $scope.onselectApellido = function (){

        $scope.fechaAutor = $scope.apellidoAutor.fecha_nac_dec;
        $scope.correoAutor = $scope.apellidoAutor.dir_electr;
        $scope.urlAutor = $scope.apellidoAutor.url;

        if ($scope.tipoAutor[0] == "P"){
            var nombreYapellido = $scope.apellidoAutor.autor_salida;
            nombreYapellido = nombreYapellido.split(", "); 
            $scope.apellidoAutor = nombreYapellido[0];
            if(nombreYapellido.length > 1){
                $scope.nombreAutor = nombreYapellido[1];
            } 
        }else{
            $scope.nombreAutor = $scope.apellidoAutor.autor_salida;
        }
    };

    $scope.orig_change = function(orig){
        var dd = orig.getDate();
        var mm = orig.getMonth()+1; //January is 0!

        var yyyy = orig.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        }
        $scope.fechaAutor = dd+'-'+mm+'-'+yyyy;
        console.log($scope.fechaAutor);
       
    };

    $scope.open_orig = function(event) {
        if( $scope.opened_orig){
             $scope.opened_orig = false;
        }else{
             $scope.opened_orig = true;
        }
        event.preventDefault();
        event.stopPropagation();
    };

    $scope.dateOptions = {
       formatYear: 'yy',
       startingDay: 1
    };

    $scope.auxTrim = "";
    
    $scope.trimRequired = function(model) {
        
        if (model == "apellidoAutor" ){
            if($scope.apellidoAutor){
                $scope.auxTrim = $scope.apellidoAutor.trim();
            }else{
                $scope.auxTrim = "";
            }
        } 
    }

    $scope.blurRequired = function (model) {
        if (model == "apellidoAutor"){
            $scope.apellidoAutor = $scope.auxTrim;
        }

        $scope.auxTrim = "";
      
    }

});