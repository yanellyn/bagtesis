angular.module('app')
  .controller('IndicesController', function(AuthService,$rootScope, paginationConfig, $modal, $scope,$log,$http, API) { 
  $scope.islogged = AuthService.isLoggedIn();
  $scope.show_table_paginate = false;
  $scope.loading = false;
  $scope.correo_modal= false; 
  $scope.bool_disable_acciones = true;
  $scope.data = AuthService.getUserData();
  $scope.valor = {'visibility': 'hidden'};
  $totalItems = 1;
  $scope.carga = {'visibility': 'hidden'};
  $scope.carga_respuesta =  {'visibility': 'hidden'};
  $scope.div_correo = {};
  $scope.collapse_correo = "collapse";
  $scope.bool_correo_exitoso = false;
  $scope.bool_correo_error = false;
  $scope.loading_correo = false;
  $scope.bool_disable = false;
  $scope.div_qr_show = false;
 // $scope.myisbn =  {'visibility','hidden'};
  $scope.service_route;
  $scope.page;
  $scope.currentPage;
  $scope.mybool_service = 0;
  $scope.aux =  $scope.currentPage;
  $scope.recursos_titulo = {};
	$scope.options_indice = [
    	{ label: 'Títulos', value: 't' },
    	{ label: 'Autores', value: 'a' }
  	];
  $scope.myindex = 0;
  $scope.index_titulo;
  $scope.index_recurso=0; 
  $scope.num_items_pagina=10;
  $scope.bool_button = false;
  $scope.correos = [{email: '', name:''}

                  ];
  $scope.ruta = API.url+"bagtesis/backend/public/recursos/T043500009080/pdf";
  $scope.mensaje_email= {  
                          
                        };

  	$scope.indice_seleccionado = $scope.options_indice[0];
    $scope.value_indice_seleccionado = $scope.indice_seleccionado.value;
    $scope.class_check_seleccionar_todos = "btn btn-default";
    $scope.bool_seleccionar_todos = false;
 //   $scope.bool_acciones = false;
    $scope.options_doc =  [
      { label: 'Todos', value: 'todos' },
      { label: 'Libros', value: 'M' },
      { label: 'Tesis', value: 'T' },
      { label: 'Publicaciones', value: 'S' }
    ]
    ;

 
    $scope.doc_seleccionado =  $scope.options_doc[0];
    $scope.value_doc_seleccionado =  $scope.doc_seleccionado.value;

    $scope.num_items_pagina = 10;


    $scope.options_orden =  [
        { label: 'Ascendente', value: 'ASC' },
        { label: 'Descendente', value: 'DESC' },
    ]
    ;
    $scope.ruta_ids_recursos = "";
    $scope.ruta_index_recurso = ""; 
    $scope.ids_recursos = "";
    $scope.array_ids_recursos = [];
    $scope.orden_seleccionado =  $scope.options_orden[0];
    $scope.value_orden_seleccionado =  $scope.orden_seleccionado.value;
    $scope.datos_qr = [];

    paginationConfig.firstText='Primera'; 
    paginationConfig.previousText='< Anterior';
    paginationConfig.nextText='Siguiente >';
    paginationConfig.lastText='Última';
    paginationConfig.boundaryLinks=false;
    paginationConfig.rotate=false;


    $scope.getAbecedario = function(){
      $scope.abecedario = [];
        for(var i=0; i<26; i++){
          letra = String.fromCharCode(65+i);
          $scope.abecedario.push({label:letra,value:letra});
        }
        return $scope.abecedario;
    }


    $scope.options_letra =    $scope.getAbecedario();

/*  $scope.anio_origen  = 1990;
    $scope.anio_destino = 2000;
  */  
  //  alert(Chr(65));



   $scope.letra_seleccionada =  $scope.options_letra[0];
   $scope.value_letra_seleccionada =  $scope.letra_seleccionada.value;


   $scope.paginate = function (){     
      $scope.bool_disable = true;
      $scope.bool_disable_acciones = true;
      $scope.options_select = {
         tipo_indice : $scope.value_indice_seleccionado,
         tipo_doc: $scope.value_doc_seleccionado,
         letra: $scope.value_letra_seleccionada,
         num_items_pagina: $scope.num_items_pagina,
         orden: $scope.value_orden_seleccionado,
    /*   anio_origen: $scope.anio_origen,
         anio_destino: $scope.anio_destino
    */
      };
      $scope.bool_acciones = true;

      switch($scope.options_select.tipo_indice) {
        case 't':
            $scope.mybool_service = 1;
            $scope.service_route = $scope.getServiceRoute($scope.options_select);           

            $scope.getService(API.url+'bagtesis/backend/public'+$scope.service_route+'?page='+$scope.currentPage+"&num_items_pagina="+$scope.options_select.num_items_pagina/*+"&orden="+$scope.options_select.orden+"&anio_origen="+$scope.options_select.anio_origen+"&anio_destino="+$scope.options_select.anio_destino */);
            $scope.currentPage = 1;
            break;
        case 'a':
      //      alert($scope.options_select.tipo_indice);
            $scope.service_route = $scope.getServiceRoute($scope.options_select);
            $scope.getService(API.url+'bagtesis/backend/public'+$scope.service_route+'?page='+$scope.currentPage+"&num_items_pagina="+$scope.options_select.num_items_pagina/*+"&orden="+$scope.options_select.orden+"&anio_origen="+$scope.options_select.anio_origen+"&anio_destino="+$scope.options_select.anio_destino */);
            $scope.currentPage = 1;
            break;
        default:
            alert('Error');
      }
   }
$scope.rec;
$scope.rt;



   $scope.getService =  function(route_peticion){
            $scope.loading = true;
            $scope.show_table_paginate = false;
            $http.get(route_peticion).
            success(function(data, status, headers, config) {
            // this callback will be called asynchronously
            // when the response is available
           
            $scope.page = data;
            $scope.totalItems = data.total;
            
                               
            for (var i = 0; i < $scope.page.data.length; i++){
                  $scope.page.data[i]['recursos'] = [];
                  $scope.page.data[i].model_checkbox = false;
                  $scope.getServiceRecursos($scope.page.data[i].id,i);
            }

            $scope.loading = false;
            $scope.show_table_paginate = true;
            $scope.bool_disable = false;
          }).
          error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            
            $scope.page = data;
            

          });

   }

  $scope.delete_correos = function(index){
    $scope.bool_correo_exitoso = false;
    if($scope.correos.length!=1){
        $scope.correos.splice(index,1);
    }
  };

   

   $scope.getServiceRoute = function (options_select){ 
      $scope.service_route = '';
      $scope.currentPage = 1;


      if(options_select.tipo_indice == 't'){
          $scope.service_route = '/titulos';

          if(options_select.tipo_doc!= 'todos'){
              $scope.service_route += '/tipo_doc/'+options_select.tipo_doc;
          }

          if(options_select.letra!= 'todas'){
               $scope.service_route += '/letra/'+options_select.letra;
          }

          return $scope.service_route;          

      }else{
        //alert('Construir Ruta para Autores');
          $scope.service_route = '/autores';

          if(options_select.tipo_doc!= 'todos'){
              $scope.service_route += '/tipo_doc/'+options_select.tipo_doc;
          }

          if(options_select.letra!= 'todas'){
               $scope.service_route += '/letra/'+options_select.letra;
          }

          return $scope.service_route;          

      }
   }


  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };

  $scope.pageChanged = function(){

//    $log.log('Page changed to: ' + $scope.currentPage);
    $scope.getService(API.url+'bagtesis/backend/public'+$scope.service_route+'?page='+$scope.currentPage+"&num_items_pagina="+$scope.options_select.num_items_pagina/*+"&orden="+$scope.options_select.orden+"&anio_origen="+$scope.options_select.anio_origen+"&anio_destino="+$scope.options_select.anio_destino */);

   // $scope.watchPage = newPage;
  };

  $scope.maxSize = 20;
  $scope.bigTotalItems = $scope.totalItems;
 // $scope.bigCurrentPage = 1;

  $scope.valor=0;



  $scope.getServiceRecursos = function (titulo_id,index){
   // alert(titulo_id);
    $scope.recursos = [{myindex: index},{titulo_id:titulo_id}];
       if($scope.options_select.tipo_indice == 't'){
      $http.get(API.url+'bagtesis/backend/public/recursos/titulos/'+titulo_id).
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
   
    $scope.recursos_titulos = data;
    $scope.page.data[index]['recursos'] = [];
    $scope.page.data[index]['recursos'] = $scope.recursos_titulos;





  //  $scope.page.data[index]['recursos'] = $scope.recursos_titulos;
    for(var i=0; i< $scope.recursos_titulos.length; i++){

     // alert($scope.page.data[index]['recursos'][i].id);
    //   $scope.getServiceRecursoTitulos($scope.page.data[index]['recursos'][i].id, i, index );
    }

   // $scope.page.date[index]['recursos']['titulos']  = 

  // alert(($scope.recursos_titulos).length);
  //  $scope.getServiceRecursoTitulos('fdcz',1,0);
  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
    $scope.recursos_titulos = data;
    

  });
}else{
    //    alert(API.url+'bagtesis/backend/public/autores/'+titulo_id+'/recursos/tipo_doc/'+$scope.options_select.tipo_doc);
        $http.get(API.url+'bagtesis/backend/public/autores/'+titulo_id+'/recursos/tipo_doc/'+$scope.options_select.tipo_doc).
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
   //  alert();
    $scope.recursos_titulos = data;
    $rootScope.respuesta_recursos = data; 
    $scope.page.data[index]['recursos'] = $scope.recursos_titulos;


    for(var i=0; i< $scope.recursos_titulos.length; i++){
     // alert($scope.page.data[index]['recursos'][i].id);
      $scope.getServiceRecursoTitulos($scope.page.data[index]['recursos'][i].id, i, index );
   
    }

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
    $scope.recursos_titulos = data;
    

  });
}
  
  }

  $scope.last = 0;

  $scope.desplegar_recursos = function(titulo_id, index){
    $scope.div_qr_show = false;
    $scope.correos = [
                      {email: '', name:''}
                     ];
    $scope.mensaje_email = {};
    $scope.page.data[index]['recursos'] = $scope.getServiceRecursos(titulo_id,index);
    $scope.page.data[index].model_checkbox = true;
    $scope.bool_acciones = true;
    $scope.bool_disable_acciones = false;
    $scope.correo_modal = false;
  }





$scope.getServiceRecursoTitulos = function(recurso_id, index_recurso, index_titulo ){


    $http.get(API.url+'bagtesis/backend/public/recursos/'+recurso_id+'/titulos').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
    
    $scope.titulos = data;
  // alert(recurso_id+" "+index_recurso+" "+index_titulo);
    $scope.page.data[index_titulo]['recursos'][index_recurso]['titulos'] = $scope.titulos;



   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
    $scope.titulos = data;
    
  });
}

$scope.predicate = '';

$scope.getServiceRecursoAutores = function (recurso_id, index_recurso, index_titulo){
   // alert(recurso_id+" "+index_recurso+" "+index_titulo);    

    $http.get(API.url+'bagtesis/backend/public/recursos/'+recurso_id+'/autores').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
   
    $scope.autores = data;
  // alert(recurso_id+" "+index_recurso+" "+index_titulo);

    $scope.page.data[index_titulo]['recursos'][index_recurso]['autores'] = $scope.autores;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
    $scope.autores = data;
    
  });
}



$scope.getServiceRecursoDescriptores = function(recurso_id, index_recurso, index_titulo){
 
    $http.get(API.url+'bagtesis/backend/public/recursos/'+recurso_id+'/descriptores').
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
   
    $scope.descriptores = data;
  // alert(recurso_id+" "+index_recurso+" "+index_titulo);
    $scope.page.data[index_titulo]['recursos'][index_recurso]['descriptores'] = $scope.descriptores;
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    
    $scope.autores = data;
    
  });
}



  $scope.desplegar_recurso = function(id,index_recurso,titulo_id){
  
  //  alert(id+" "+index_recurso+" "+titulo_id);
  $scope.bool_correo_exitoso = false;
  $scope.bool_correo_error = false;
  $scope.correo_modal = false;
  $scope.div_qr_show = false;
  $scope.correos=[{email:'',name:''}];

  if($rootScope.userLogged){
              $scope.correos = [
                                  {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                               ];
  }
 // alert($scope.correos);
   // alert(getById($scope.page.data,titulo_id));
  $scope.carga_respuesta['visibility']= 'hidden';
  $scope.i=0;
//   alert($scope.page.data[$id].id);
  $scope.index_recurso=index_recurso; 

   while(1){
      if($scope.page.data[$scope.i].id == titulo_id){
       
     //   $scope.page.data[$scope.i]['recursos'][index_recurso]['titulos'];
        $scope.getServiceRecursoTitulos($scope.page.data[$scope.i]['recursos'][index_recurso].id,index_recurso,$scope.i);
        $scope.getServiceRecursoAutores($scope.page.data[$scope.i]['recursos'][index_recurso].id,index_recurso,$scope.i);
        $scope.getServiceRecursoDescriptores($scope.page.data[$scope.i]['recursos'][index_recurso].id,index_recurso,$scope.i);

        $scope.predicate ='+orden';
        $scope.index_titulo=$scope.i;
         
         break;
      }

      $scope.i++;
   }


    $http.get(API.url+'bagtesis/backend/public/recursos/'+$scope.page.data[$scope.i].recursos[index_recurso].id).
    success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
        $scope.page.data[$scope.i].recursos[index_recurso] = data;
      //  alert($scope.page.data[$scope.i].recursos[$scope.index_recurso]);
   // $scope.page.data[index]['recursos'] = $scope.recursos_titulos;

  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    

    
  });




   $scope.ruta_index_recurso = API.url+'bagtesis/backend/public/recursos/'+$scope.page.data[$scope.index_titulo]['recursos'][$scope.index_recurso].id+'/pdf';
   $scope.page.data[$scope.i].model_checkbox = true;

  };

  $scope.verifica;
  $scope.getServiceTitulos = function(recurso_id){
    alert(recurso_id);
  };

  $scope.add_correo = function(){
  //  alert($scope.index_titulo);
    $scope.bool_correo_exitoso = false;
    $scope.correos.push({email: '',name:''});

  };

  $scope.desplegar_correo = function(){
       // alert("desplegar correo");
       $scope.div_correo = {'display': 'inline-block'};
  };

  $scope.desplegar_2 = function (){
     $scope.div_correo = {'display':'none'};
 //    $scope.collapse_correo = {};
     // $scope.collapse_correo = "";
    //  $scope.collapse_correo = "collapse on";
  };


  $scope.repuesta;
  $scope.enviar_correo = function (){
      $scope.carga['visibility'] = 'visible';
      $scope.carga_respuesta['visibility']= 'hidden';

      $scope.mensaje_email['correos'] = $scope.correos;

   //   $scope.mensaje_email['datos'] = $scope.page[$scope.index_titulo]['recursos'][$scope.index_recurso];
     // $scope.hola=[$scope.page.data[$scope.index_titulo]['recursos'][$scope.index_recurso]];
        $scope.mensaje_email['datos'] = [$scope.page.data[$scope.index_titulo]['recursos'][$scope.index_recurso]];
  
        $http.post(API.url+'bagtesis/backend/public/recursos/mail', $scope.mensaje_email).
        success(function(data, status, headers, config) {
            $scope.respuesta = data;
            $scope.carga['visibility'] = 'hidden';
            $scope.carga_respuesta['visibility']= 'visible';
        // this callback will be called asynchronously
        // when the response is available
        }).
        error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        });
  };


  $scope.div_correo = {};
  $scope.div_correo.show = false;
  $scope.div_correo = function() {
      $scope.div_qr_show = false;
      $scope.div_qr.show = false;
      $scope.div_pdf.show = false;
      $scope.div_correo.show = !$scope.div_correo.show;
  };


  $scope.div_pdf = {};
  $scope.div_pdf.show = false;
  $scope.div_pdf = function() {
      $scope.div_qr_show = false;
      $scope.div_qr.show = false;
      $scope.div_correo.show = false;
      $scope.div_pdf.show = !$scope.div_pdf.show;
  };

  $scope.div_qr = false;
  $scope.div_qr.show = false;

  $scope.div_qr = function() {
      $scope.correo_modal = false;
   //   $scope.div_correo.show = false;
      $scope.div_pdf.show = false;
      $scope.bool_correo_exitoso = false;
      $scope.div_qr_show = !$scope.div_qr_show;
  };


  $scope.div_correo_modal = function() {
    $scope.bool_correo_exitoso = false;
    $scope.div_qr_show = false;
    $scope.bool_button = false;

      $scope.correos = [
                          {email: '', name:''}
                      ];

      if($rootScope.userLogged){
              $scope.correos = [
                                  {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                                ];

      }
   //   alert($rootScope.userLogged);

      $scope.mensaje_email = {};
  //    $scope.div_qr.show = false;
  //    $scope.div_pdf.show = false;
      $scope.correo_modal = !$scope.correo_modal;    
      $scope.div_qr.show = false; 

  };


  $scope.delete = function(index){
      $scope.correos.splice(index,1);
      $scope.bool_correo_exitoso = false;
  };





  $scope.seleccionar_todos = function (){
      $scope.bool_seleccionar_todos = !$scope.bool_seleccionar_todos;

      if($scope.bool_seleccionar_todos == true){
          $scope.bool_acciones = true; 
          $scope.bool_disable_acciones = false;
          $scope.class_check_seleccionar_todos = "btn btn-success";
          for ($scope.i =0 ; $scope.i< $scope.page.data.length; $scope.i++) {
              $scope.page.data[$scope.i].model_checkbox = true;
          };
      }else{
          $scope.class_check_seleccionar_todos = "btn btn-default";
          for ($scope.i =0 ; $scope.i< $scope.page.data.length; $scope.i++) {
              $scope.page.data[$scope.i].model_checkbox = false;
          };
          $scope.bool_disable_acciones = true;
     //     $scope.bool_acciones = false;
      }
  
  };

  $scope.verficar_titulos_seleccionados = function(){ 

    for(var i=0;i< $scope.page.data.length; i++){
          if($scope.page.data[i].model_checkbox == true){
              return true;
          }
    }
    return false;
  };


  $scope.titulo_seleccionado = function (){ 
    if($scope.verficar_titulos_seleccionados()){
        $scope.bool_acciones = true;
        $scope.bool_disable_acciones = false;
    }else{
      $scope.bool_disable_acciones = true;
      //  $scope.bool_acciones = false;
    }
  };



  $scope.verificar_usuario = function(){
      $scope.bool_correo_exitoso = false; 
      $scope.bool_correo_error = false;
      $scope.mensaje_email = {};

      $scope.correos = [{email: '', name: ''}];
      if($rootScope.userLogged){
              $scope.correos = [
                                  {email: AuthService.getUserData().email, name:AuthService.getUserData().username}
                                ];

      }
  };


  $scope.validarEmail = function( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ){
        return true;
    }
    return false;   
};


$scope.validar_correos = function (){
    $scope.valor = false;
    $scope.correos.forEach(function(value, key) {
       $scope.validar = $scope.validarEmail(value.email);
         if($scope.validar){
            $scope.valor = true;
         }
      // alert(value.email);
    });
    return $scope.valor; 
};


  $scope.enviar_recursos_seleccionados = function(){
    //  alert('Enviar recursos seleccionados');
    $scope.v = $scope.validar_correos();

    if($scope.v == true){
          $scope.bool_correo_error = true;
          $scope.bool_correo_exitoso = false;
    }else{      
      $scope.bool_correo_exitoso = false;
      $scope.mensaje_email = {};
      $scope.mensaje_email.correos = $scope.correos;
      $scope.mensaje_email.id_recursos = [];
      $scope.loading_correo = true;
      $scope.bool_disable = true;
      $scope.bool_correo_error = false;
      for(var i=0; i< $scope.page.data.length; i++){
          if($scope.page.data[i].model_checkbox){
              for(var j=0; j< $scope.page.data[i].recursos.length;j++){   
                  $scope.mensaje_email.id_recursos.push($scope.page.data[i].recursos[j].id);  
              }
          }
      }

      $http.post(API.url+'bagtesis/backend/public/recursos/mail', $scope.mensaje_email).
      success(function(data, status, headers, config) {
            $scope.respuesta = data;
            $scope.loading_correo = false;
            $scope.bool_disable = false;
            $scope.bool_correo_exitoso = true;
        //    $scope.carga['visibility'] = 'hidden';
         //   $scope.carga_respuesta['visibility']= 'visible';
        // this callback will be called asynchronously
        // when the response is available
        }).
        error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    

    }

  };



    $scope.generar_ids_recursos_seleccionados = function (){
     $scope.ids_recursos = "";
     $scope.array_ids_recursos = [];
     $scope.rutas_ids_recursos_seleccionados = [];
     $scope.datos_qr = [];
     $scope.iter = 0;
     angular.forEach($scope.page.data, function(titulo, key) {
        if(titulo.model_checkbox==true){
            angular.forEach(titulo.recursos, function(value, key) {
         
                $scope.ids_recursos = $scope.ids_recursos+value.id;
                $scope.rutas_ids_recursos_seleccionados.push(API.url+"bagtesis/backend/public/recursos/"+value.id+"/pdf");
                $scope.ruta_qr = API.url+"bagtesis/backend/public/recursos/"+value.id+"/pdf";
                $scope.cota    = value.ubicacion;
                $scope.titulo  = $scope.page.data[$scope.iter].titulo_salida;
                $scope.ids_recursos = $scope.ids_recursos+" ";
                $scope.datos = {   
                                  ruta_qr: $scope.ruta_qr,
                                  titulo:  $scope.titulo,
                                  cota:    $scope.cota
                               };
                $scope.datos_qr.push($scope.datos);   
            });
        }
        $scope.iter++;    
        }); 
      $scope.ids_recursos = $scope.ids_recursos.substring(0,$scope.ids_recursos.length-1);
      $scope.ruta_ids_recursos = API.url+"bagtesis/backend/public/recursos/"+$scope.ids_recursos+"/pdf";



 /*  for($scope.i=0; $scope.i<$scope.array_ids_recursos.length;$scope.i++){
          $scope.ids_recursos = $scope.ids_recursos+$scope.array_ids_recursos[$scope.i];
          $scope.rutas_ids_recursos_seleccionados.push("http://192.168.10.35/bagtesis/backend/public/recursos/"+$scope.array_ids_recursos[$scope.i]+"/pdf");
          if($scope.i != $scope.array_ids_recursos.length-1){
              $scope.ids_recursos = $scope.ids_recursos+" ";
          }
      }
*/

   //    alert($scope.ids_recursos_pdf);
  };


  $scope.enviar_recurso_seleccionado = function(){
    //  alert('Enviar recursos seleccionado');

    $scope.v = $scope.validar_correos();
    if($scope.v == true){
          $scope.bool_correo_error = true;
          $scope.bool_correo_exitoso = false;
    }else{      
      $scope.bool_correo_error = false;
      $scope.bool_correo_exitoso = false;
      $scope.loading_correo = true;
      $scope.bool_disable = true;
      $scope.mensaje_email = {};
      $scope.recurso_id = $scope.page.data[$scope.index_titulo].recursos[$scope.index_recurso].id;
      $scope.mensaje_email.correos = $scope.correos;
      $scope.mensaje_email.id_recursos = [$scope.recurso_id];

      $http.post(API.url+'bagtesis/backend/public/recursos/mail', $scope.mensaje_email).
          success(function(data, status, headers, config) {
                $scope.respuesta = data;
                $scope.loading_correo = false;
                $scope.bool_correo_exitoso = true;
                $scope.bool_disable = false;
            //    $scope.carga['visibility'] = 'hidden';
             //   $scope.carga_respuesta['visibility']= 'visible';
             //   alert($scope.respuesta);
            // this callback will be called asynchronously
            // when the response is available
            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
      }      
  };




  $scope.clear_qr = function(){
    $scope.datos_qr = [];
  };



});

