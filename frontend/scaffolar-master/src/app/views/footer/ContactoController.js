
angular.module('app')
	.controller('ContactoController', function($rootScope, $scope, $http, API) { 

		$scope.nombres = '';
		$scope.correo  = '';
		$scope.contenido = '';
		$scope.show_loading = false;

		$scope.request = {};
		$scope.aviso = '';


		/*
		*	enviarConsulta
		*   Función para enviar consultas a la BAG en forma de correos.
		*	autor: Cesar Herrera
		*/
		$scope.enviarConsulta = function(){
			console.log('enviarConsulta called()');
			$scope.show_loading = true;
			$scope.request.nombres = $scope.nombres;
			$scope.request.correo = $scope.correo;
			$scope.request.contenido = $scope.contenido;

			$http.post(API.url+'bagtesis/backend/public/contacto',$scope.request).
			  success(function(data, status, headers, config) {
			  	 console.log(data);
			  	 $scope.show_loading = false;
			  	 $scope.aviso = 'Se ha enviado su mensaje.';
			  }).
			  error(function(data, status, headers, config) {
		    	 $scope.show_loading = false;
			  });

		}
	    
	});
