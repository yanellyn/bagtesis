
angular.module('app')
	.controller('HeadersController', function($rootScope, $scope, $location, $http, $cookies,$cookieStore, $location, AuthService) { 

		
            $rootScope.tipo_recurso = $cookieStore.get('tipo_recurso');
        
	    $scope.isActive = function (viewLocation) { 
	    
	    	if(angular.isArray(viewLocation)){
	    		if($.inArray( $location.path(), viewLocation ) != -1){
	    			return true;
	    		}else return false;
	    	}else 
	        	return viewLocation === $location.path();
	    };

    	if(AuthService.isLoggedIn()){
    		if(AuthService.isAdmin())
    			$rootScope.es_administrador = true;
    		else
    			$rootScope.es_administrador = false;

    		$rootScope.userLogged = true;
    		$rootScope.userWelcome = 'Bienvenido '+$cookieStore.get('username');
    	}else{
    		$rootScope.userLogged = false;
    		$rootScope.userWelcome = '';
    		$rootScope.es_administrador = false;
    	}

    	$scope.header_init = function(){
    		console.log('header_init called()');
    		if(AuthService.isLoggedIn()){
	    		if(AuthService.isAdmin())
	    			$rootScope.es_administrador = true;
	    		else
	    			$rootScope.es_administrador = false;

	    		$rootScope.userLogged = true;
	    		$rootScope.userWelcome = 'Bienvenido '+$cookieStore.get('username');
	    	}else{
	    		$rootScope.userLogged = false;
	    		$rootScope.userWelcome = '';
	    		$rootScope.es_administrador = false;
	    	}

    	}
	    
	});
