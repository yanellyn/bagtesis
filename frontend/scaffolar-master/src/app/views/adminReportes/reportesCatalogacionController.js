angular.module('app')
  .controller('reportesCatalogacionController', function($scope, $window, $http, API, $cookieStore) {
    $('#loader').modal('show');
  	$scope.selectedTipoRecurso = "";
  	$scope.tiposRecursos = "";//[{"desc":"Todos","valor":""},{"desc":"Monografías","valor":"M"},{"desc":"Trabajos académicos","valor":"T"},{"desc":"Publicaciones Seriadas","valor":"S"},{"desc":"Otros recursos","valor":"O"}];
  	$scope.selectedTipoOperacion = "";
  	$scope.tiposOperaciones = [{"desc":"Todas","valor":""},{"desc":"Agregar","valor":"Agregar"},{"desc":"Modificar","valor":"Modificar"},{"desc":"Eliminar","valor":"Eliminar"}];
  	$scope.selectedCatalogador = "";
  	$scope.catalogadores = "";
  	$scope.materiaTematica = "";
    $scope.loadingMateria = false;
  	$scope.selectedSalaRecurso = "";
  	$scope.salasRecursos = "";
    $scope.fechaInicio = "";
    $scope.fechaFin = "";
    $scope.tipoReporte = "";
    $scope.msjInformacion = "";

    $http.get(API.url + 'bagtesis/backend/public/obtener_catalogadores').success(function(data, status, headers, config) {           
        $scope.catalogadores = data;
        $http.get(API.url + 'bagtesis/backend/public/obtener_salas').success(function(data, status, headers, config) {           
          $scope.salasRecursos = data;
          $http.get(API.url + 'bagtesis/backend/public/tipo_docs').success(function(data, status, headers, config) {           
            $scope.tiposRecursos = data;
            $('#loader').modal('hide');     
          }).
          error(function(error) {
             $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
          });         
        }).
        error(function(error) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });                
    }).
    error(function(error) {
        $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
    });         

  /**********************CALENDARIO**************************************************/
  $scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];

  $scope.startDateChange = function(orig){
      var dd = orig.getDate();
      var mm = orig.getMonth()+1; //January is 0!
      var yyyy = orig.getFullYear();
      if(dd<10){
          dd='0'+dd
      } 
      if(mm<10){
          mm='0'+mm
      }
      $scope.fechaInicio = dd+'-'+mm+'-'+yyyy;
      console.log($scope.fechaInicio);
  };

  $scope.endDateChange = function(orig){
      var dd = orig.getDate();
      var mm = orig.getMonth()+1; //January is 0!
      var yyyy = orig.getFullYear();
      if(dd<10){
          dd='0'+dd
      } 
      if(mm<10){
          mm='0'+mm
      }
      $scope.fechaFin = dd+'-'+mm+'-'+yyyy;
      console.log($scope.fechaFin);
  };

  $scope.setDate = function(fecha_ed) {
      var dd = fecha_ed.substring(6, 8);
      var mm = fecha_ed.substring(4, 6); //January is 0!
      var yyyy =  fecha_ed.substring(0, 4);
      
      dt_destt = dd+'-'+mm+'-'+yyyy;
      return dt_destt;
  };

  $scope.openEndDate = function(event) {
      event.preventDefault();
      event.stopPropagation();
      $scope.openedEndDate = true;
  };

  $scope.openStartDate = function(event) {
      event.preventDefault();
      event.stopPropagation();
      $scope.openedStartDate = true;
  };

  $scope.dateOptions = {
     formatYear: 'yy',
     startingDay: 1
  };

  $scope.today = function() {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10){
          dd='0'+dd;
      } 
      if(mm<10){
          mm='0'+mm;
      }
      return mm+'-'+dd+'-'+yyyy;
  };

//**********************************FIN CALENDARIO***********************************///
  $scope.loadingStates   = function(val){
      console.log("val "+val);
      $scope.loadingMateria = true;
      return $scope.buscarMateriaTematica(val); 
  };

  $scope.buscarMateriaTematica = function (val){
      $scope.mensaje_buscador_simple = {};
      $scope.mensaje_buscador_simple['data'] = val;

      $scope.promise = $http.post(API.url+'bagtesis/backend/public/autocompletado_materia_tematica', $scope.mensaje_buscador_simple)
                    .success(function(data, status, headers, config) {
                        console.log(JSON.stringify(data));
                        $scope.prom = data;
                        $scope.loadingMateria = false;
                    }).then(function(){
                       
                        $scope.aux_promise = $scope.prom;  
                        return $scope.prom;
                    });
      
      return $scope.promise;
  };

  $scope.generarReporteCatalogacion = function(){
    $('#loader').modal('show'); 
    $scope.filtrosReporte = {};
    $scope.filtrosReporte["selectedTipoRecurso"] = $scope.selectedTipoRecurso;
    $scope.filtrosReporte["selectedTipoOperacion"] = $scope.selectedTipoOperacion;
    $scope.filtrosReporte["selectedCatalogador"] = $scope.selectedCatalogador;
    $scope.filtrosReporte["materiaTematica"] = $scope.materiaTematica;
    $scope.filtrosReporte["selectedSalaRecurso"] = $scope.selectedSalaRecurso;
    $scope.filtrosReporte["fechaInicio"] = $scope.fechaInicio;
    $scope.filtrosReporte["fechaFin"] = $scope.fechaFin;
    $scope.filtrosReporte["tipoReporte"] = $scope.tipoReporte;

    $http.post(API.url + 'bagtesis/backend/public/generar_reporte_catalogacion', $scope.filtrosReporte)
        .success(function(data, status, headers, config) {           
          $scope.nombreArchivo = data["nombre"];
          $('#loader').modal('hide');
          if (data["codigo"] == "0" && $scope.nombreArchivo != "" && $scope.nombreArchivo != null && $scope.nombreArchivo != undefined)
          {
            if($scope.tipoReporte == "excel"){
              window.location.href = API.url+ 'bagtesis/backend/app/storage/public/catalogacion/'+  $scope.nombreArchivo + '.xlsx';             
            }else if($scope.tipoReporte == "pdf"){
              setTimeout(function(){
                var a         = document.createElement('a');
                a.href        = API.url+ 'bagtesis/backend/app/storage/public/catalogacion/'+  $scope.nombreArchivo + '.pdf'; 
                a.target      = '_blank';
                a.download    = $scope.nombreArchivo + '.pdf';
                document.body.appendChild(a);
                a.click();
              }, 0);
            }
          }else if (data["codigo"] == "2") 
          {
            $scope.msjInformacion ="No se encontraron resultados";
            $('#informacion').modal('show'); 
          }else{ 
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
          }
           
        }).
        error(function(error) {
            $('#loader').modal('hide');
            $scope.msjInformacion = 'Ha ocurrido un error procesando su solicitud, por favor intente mas tarde o contacte al administrador técnico';
            $('#informacion').modal('show');
        });   
  }

});

angular.module('app').filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});
