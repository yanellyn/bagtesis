
angular.module('app')
    .controller('ReportesController', function($scope, $http, $cookieStore, AuthService, API, datepickerPopupConfig, $timeout, $window) { 
	
    $scope.usuarios_atentidos = true;
    $scope.recursos_prestados = true;
    $scope.prestamos = true;
    $scope.renovaciones = true;
    $scope.recursos_mas_demandados = true;
    $scope.usuarios_suspendidos = true;
    $scope.promedio_suspension = true;
    $scope.devoluciones = true;

    $scope.today = function() {
	    var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth()+1; //January is 0!

	    var yyyy = today.getFullYear();
	    if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    }
	    $scope.dt_orig = dd+'-'+mm+'-'+yyyy;
	    $scope.dt_dest = dd+'-'+mm+'-'+yyyy;

  	};
  	$scope.today();

    $scope.report_obj = {};
    $scope.reporte_data = {};
    $scope.reporte_activo = false;

    $scope.generar = function(){
    	
    	$scope.report_obj.usuarios_atentidos = $scope.usuarios_atentidos;

    	$scope.report_obj.fecha_origen = $scope.dt_orig;
    	$scope.report_obj.fecha_destino = $scope.dt_dest;
    	console.log($scope.report_obj.fecha_origen);
    	$scope.pdf_down_spin = true;
		$http.defaults.headers.common['X-Auth-Token'] = $cookieStore.get('auth_token')+'_id_'+$cookieStore.get('user_id');
		$http.post(API.url+'bagtesis/backend/public/variables_reportes',$scope.report_obj).
			  success(function(data, status, headers, config) {
			  	$scope.pdf_down_spin = false;
			  	$scope.reporte_data.nro_usuarios_atendidos = data.usuarios_atendidos;
			  	$scope.chartObject_usuarios_atendidos.options.title = "Usuarios atendidos("+$scope.reporte_data.nro_usuarios_atendidos+")";
			  	$scope.reporte_data.nro_usuarios_suspendidos = data.usuarios_suspendidos;
			  	$scope.reporte_data.prestamos = data.prestamos;
			  	$scope.reporte_data.recursos_prestados = data.recursos_prestados;
			  	$scope.chartObject_recursos_prestados.options.title = "Recursos prestados("+$scope.reporte_data.recursos_prestados+")";
			  	$scope.reporte_data.renovaciones = data.renovaciones;
			  	$scope.reporte_data.devoluciones = data.devoluciones;
				$scope.reporte_activo = true;
				console.log($scope.reporte_data);
			  }).
			  error(function(data, status, headers, config) {
			  	 console.log(data);
			  });
    };

    $scope.generar();

    $scope.api_url = API.url;

    $scope.pdf_down_spin = false;

    $scope.start_pdf_down_spin = function(){
    	$scope.pdf_down_spin = true;
    	console.log('start_pdf_down_spin called()');

    	$scope.request_data = {};
    	$scope.request_data.image1 = $scope.img1;
    	$scope.request_data.image2 = $scope.img2;
    	$scope.request_data.image3 = $scope.img3;
    	$scope.request_data.image4 = $scope.img4;

    	$http.post(API.url+'bagtesis/backend/public/reporte_prestamos_chart_generar',$scope.request_data).
			  success(function(data, status, headers, config) {

			  	$window.open(API.url+'bagtesis/backend/public/reporte_prestamos/'+$scope.dt_orig+'/'+$scope.dt_dest, 'iframe1');
    			$timeout(function() { $scope.pdf_down_spin = false;}, 4000);

			  	}).
			  error(function(data, status, headers, config) {
			  	 console.log(data);
			  });


    	


    };

    $scope.descargar_pdf = function(){
    	console.log('descargar_pdf called()');

    	$scope.request_data = {};
    	$scope.request_data.image1 = $scope.img1;
    	$scope.request_data.image2 = $scope.img2;
    	$scope.request_data.image3 = $scope.img3;
    	$scope.request_data.image4 = $scope.img4;

    	$http.post(API.url+'bagtesis/backend/public/reporte_prestamos_chart_generar',$scope.request_data).
			  success(function(data, status, headers, config) {

				
			  	$http.defaults.headers.common['X-Auth-Token'] = $cookieStore.get('auth_token')+'_id_'+$cookieStore.get('user_id');
				$http.post(API.url+'bagtesis/backend/public/reporte_prestamos_pdf',$scope.reporte_data).
				  success(function(data, status, headers, config) {
				  	
					console.log(data);
				  }).
				  error(function(data, status, headers, config) {
				  	 console.log(data);
				  });



			  }).
			  error(function(data, status, headers, config) {
			  	 console.log(data);
			  });

    	

    };

    
  	$scope.setMinDate = function(){
  		var now = new Date();
		var oneYr = new Date();
		oneYr.setYear(now.getYear() - 5);
		$scope.minDate = oneYr;
  	};
  	$scope.setMinDate();
  	

  	$scope.open_orig = function(event) {
	    event.preventDefault();
	    event.stopPropagation();
	    $scope.opened_orig = true;
  	};

  	$scope.open_dest = function(event) {
	    event.preventDefault();
	    event.stopPropagation();
	    $scope.opened_dest = true;
  	};

  	$scope.orig_change = function(orig){
	    var dd = orig.getDate();
	    var mm = orig.getMonth()+1; //January is 0!

	    var yyyy = orig.getFullYear();
	    if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    }
	    $scope.dt_orig = dd+'-'+mm+'-'+yyyy;
  		console.log($scope.dt_orig);
  		$scope.generar();
  	};

  	$scope.dest_change = function(dest){
  		var dd = dest.getDate();
	    var mm = dest.getMonth()+1; //January is 0!

	    var yyyy = dest.getFullYear();
	    if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    }
	    $scope.dt_dest = dd+'-'+mm+'-'+yyyy;
  		console.log($scope.dt_dest);	
  		$scope.generar();
  	};

    $scope.dateOptions = {
       formatYear: 'yy',
       startingDay: 1
    };



  	$scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  	$scope.format = $scope.formats[0];
	
  	$scope.img1 = '';
  	$scope.img2 = '';
  	$scope.img3 = '';
  	$scope.img4 = '';

  	$scope.initChartImg = function (chartWrapper) {
        var chart_div = document.getElementById('ch');
        var chartObject = chartWrapper.getChart();
        //var img_ = chartObject.getImageURI(); 
        //var res = img_.replace("data:image/png", "data:image/jpeg"); 
        //chart_div.innerHTML = '<IMG id="img1" src="' + res + '">';
        chart_div.innerHTML = '<IMG id="img1" src="' + chartObject.getImageURI() + '">';
        $scope.img1 = chartObject.getImageURI();
        console.log(chart_div.innerHTML);

    };

    $scope.initChartImg1 = function (chartWrapper) {
        var chartObject = chartWrapper.getChart();
        $scope.img1 = chartObject.getImageURI();
    };
    $scope.initChartImg2 = function (chartWrapper) {
        var chartObject = chartWrapper.getChart();
        $scope.img2 = chartObject.getImageURI();
    };
    $scope.initChartImg3 = function (chartWrapper) {
        var chartObject = chartWrapper.getChart();
        $scope.img3 = chartObject.getImageURI();
    };
    $scope.initChartImg4 = function (chartWrapper) {
        var chartObject = chartWrapper.getChart();
        $scope.img4 = chartObject.getImageURI();
    };

    $scope.desc_pdf = function(){
    	console.log('desc_pdf con graficos called()');

    	$scope.request_data = {};
    	$scope.request_data.image1 = $scope.img1;
    	$scope.request_data.image2 = $scope.img2;
    	$scope.request_data.image3 = $scope.img3;
    	$scope.request_data.image4 = $scope.img4;

    	$http.post(API.url+'bagtesis/backend/public/reporte_prestamos_chart_generar',$scope.request_data).
			  success(function(data, status, headers, config) {

				console.log(data);
			  }).
			  error(function(data, status, headers, config) {
			  	 console.log(data);
			  });
    }


  	/*Inicio pie chart I*/
	$scope.chartObject_usuarios_atendidos = {
	  "type": "PieChart",
	  "displayed": true,
	  "data": {
	    "cols": [
	      {
	        "id": "month",
	        "label": "Month",
	        "type": "string",
	        "p": {}
	      },
	      {
	        "id": "cost-id",
	        "label": "Shipping",
	        "type": "number"
	      }
	    ],
	    "rows": [
	      {
	        "c": [
	          {
	            "v": "(34)Docentes"
	          },
	          {
	            "v": 34,
	            "f": "34 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(85)Alumnos Biología"
	          },
	          {
	            "v": 85,
	            "f": "85 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(32)Alumnos Computación"
	          },
	          {
	            "v": 32,
	            "f": "32 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(23)Alumnos Física"
	          },
	          {
	            "v": 23,
	            "f": "23 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(71)Alumnos Geoquímica"
	          },
	          {
	            "v": 71,
	            "f": "71 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(12)Alumnos Matemática"
	          },
	          {
	            "v": 12,
	            "f": "12 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(54)Alumnos Química"
	          },
	          {
	            "v": 54,
	            "f": "54 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(42)Otros"
	          },
	          {
	            "v": 42,
	            "f": "42 veces"
	          }
	        ]
	      }
	    ]
	  },
	  "options": {
	    "title": "Usuarios atendidos(353)",
	    "isStacked": "true",
	  	"pieHole": 0.4,
	    "fill": 20,
	    "displayExactValues": true,
	  },
	  "formatters": {}
	};


  	/*Fin pie chart I */


  	$scope.chartObject_recursos_prestados = {
	  "type": "PieChart",
	  "displayed": true,
	  "data": {
	    "cols": [
	      {
	        "id": "month",
	        "label": "Month",
	        "type": "string",
	        "p": {}
	      },
	      {
	        "id": "cost-id",
	        "label": "Shipping",
	        "type": "number"
	      }
	    ],
	    "rows": [
	      {
	        "c": [
	          {
	            "v": "(34)Libros"
	          },
	          {
	            "v": 34,
	            "f": "34 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(85)Publicaciones periódicas"
	          },
	          {
	            "v": 85,
	            "f": "85 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(32)Tesis"
	          },
	          {
	            "v": 32,
	            "f": "32 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(23)Otros"
	          },
	          {
	            "v": 23,
	            "f": "23 veces"
	          }
	        ]
	      }
	    ]
	  },
	  "options": {
	    "title": "Recursos prestados(174)",
	    "isStacked": "true",
	  	"pieHole": 0.4,
	    "fill": 20,
	    "displayExactValues": true,
	  },
	  "formatters": {}
	};



	$scope.chartObject_operaciones_realizadas = {
	  "type": "PieChart",
	  "displayed": true,
	  "data": {
	    "cols": [
	      {
	        "id": "month",
	        "label": "Month",
	        "type": "string",
	        "p": {}
	      },
	      {
	        "id": "cost-id",
	        "label": "Shipping",
	        "type": "number"
	      }
	    ],
	    "rows": [
	      {
	        "c": [
	          {
	            "v": "(64)Préstamos"
	          },
	          {
	            "v": 64,
	            "f": "64 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(25)Renovaciones"
	          },
	          {
	            "v": 25,
	            "f": "25 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(51)Devoluciones"
	          },
	          {
	            "v": 51,
	            "f": "51 veces"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(7)Suspensiones"
	          },
	          {
	            "v": 7,
	            "f": "7 veces"
	          }
	        ]
	      }
	    ]
	  },
	  "options": {
	    "title": "Operaciones realizadas(147)",
	    "isStacked": "true",
	  	"pieHole": 0.4,
	    "fill": 20,
	    "displayExactValues": true,
	  },
	  "formatters": {}
	};


	$scope.chartObject_usuarios_por_estado = {
	  "type": "PieChart",
	  "displayed": true,
	  "data": {
	    "cols": [
	      {
	        "id": "month",
	        "label": "Month",
	        "type": "string",
	        "p": {}
	      },
	      {
	        "id": "cost-id",
	        "label": "Shipping",
	        "type": "number"
	      }
	    ],
	    "rows": [
	      {
	        "c": [
	          {
	            "v": "(1230)Solventes"
	          },
	          {
	            "v": 1230,
	            "f": "1230 usuarios"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(32)Con préstamos"
	          },
	          {
	            "v": 32,
	            "f": "32 usuarios"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(17)Suspendidos"
	          },
	          {
	            "v": 17,
	            "f": "17 usuarios"
	          }
	        ]
	      },
	      {
	        "c": [
	          {
	            "v": "(5)Suspensión indefinida"
	          },
	          {
	            "v": 5,
	            "f": "5 usuarios"
	          }
	        ]
	      }
	    ]
	  },
	  "options": {
	    "title": "Usuarios por estado(1284)",
	    "isStacked": "true",
	  	"pieHole": 0.4,
	    "fill": 20,
	    "displayExactValues": true,
	  },
	  "formatters": {}
	};





});


