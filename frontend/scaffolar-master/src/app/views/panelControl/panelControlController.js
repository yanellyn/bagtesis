
angular.module('app')
	.controller('PanelControlController', function($rootScope, $scope, $http, $location, $cookieStore, paginationConfig, AuthService, API) { 

		$scope.dias_prestamos_docentes = '7';
		$scope.dias_prestamos_no_docentes = '3';
		$scope.dias_suspension_retardo_circulante = '3';
		$scope.dias_suspension_retardo_sala = '10';
		$scope.tiempo_sesion_horas = '4';
		$scope.loading = false;
	 

	 /*
	* Función para desplegar ventana modal
	* preguntando al usuario para guardar los datos de panel de control
	*
	* Autor: Cesar Herrera
	*
	*/
	$scope.preGuardarCambios = function(registro){
			console.log('preguardar called()');
			$scope.modalHeader = '¿Desea guardar los datos?';
			$scope.modalB1 = 'Guardar';
			$scope.modalB2 = 'Cancelar';
			$scope.modalShowB1 = true;
			$scope.modalShowB2 = true;
			$scope.onAction = function(){

				$scope.guardar_cambios($scope.prefOneSelected);
			};
			
	};

	$scope.GuardarCambios = function(registro){
			console.log('guardar called()');
			$scope.modalContent = 'Datos guardados exitosamente!';
			
	};



});

