angular.module('app')
    .factory('AuthService', function ($cookieStore, $http, $rootScope, API) {

        return {
            login: function(credentials, successCallBack, errorCallBack){

                $http.post(API.url+'bagtesis/backend/public/login', { username: credentials.username, password: credentials.password })
                    .success(function(response) {
                        if (response.token) {
                            $rootScope.mensaje_login = false;
                            $cookieStore.put('auth_token',response.token);
                            $cookieStore.put('email',response.email);
                            $cookieStore.put('username',response.username);
                            $cookieStore.put('user_id',response.id);
                            $cookieStore.put('logindate',response.logindate);
                            $cookieStore.put('tipo_usp',response.tipo_usp);
                            $cookieStore.put('id_recurso_libro', "");
                            $cookieStore.put('id_recurso_tesis', "");
                            $cookieStore.put('id_recurso_publicacion', "");
                            $cookieStore.put('id_recurso_otros', "");
                            $cookieStore.put('tipo_recurso', "");
                            $cookieStore.put('tipo_recurso_id', "");
                            $cookieStore.put('id_recurso_publicacion_s', "");
                            $cookieStore.put('cota', "");
                            $rootScope.tipo_usp = $cookieStore.get('tipo_usp');
                     
                            if($rootScope.tipo_usp == 'Administrati'){
                              $rootScope.es_administrador = true;
                            }else{
                              $rootScope.es_administrador = false;
                            }

                            $rootScope.userLogged = true;
                            $http.defaults.headers.common['X-Auth-Token'] = response.token+'_id_'+response.id;
                        }else{
                            $rootScope.mensaje_login = true;
                        }


                        if (angular.isFunction(successCallBack))
                            successCallBack(response);
                    })
                    .error(function(response) {
                        if (angular.isFunction(errorCallBack))
                            errorCallBack(response);
                    });
                    
            },
            logout: function(callBack) {

              $http.get(API.url+'bagtesis/backend/public/logout')
                    .success(function(response) {
                      $rootScope.es_administrador = false;
                      $cookieStore.remove('auth_token');
                      $cookieStore.remove('user_id');
                      $cookieStore.remove('email');
                      $cookieStore.remove('username');
                      $cookieStore.remove('logindate');
                      $cookieStore.remove('tipo_usp');
                      $rootScope.userLogged = false;
                      $http.defaults.headers.common['X-Auth-Token'] = ''; 
                    })
                    .error(function(response) {});

                if (angular.isFunction(callBack))
                    callBack();
            },
            isLoggedIn: function() {
                return $cookieStore.get('auth_token') == undefined ? false : true;
            },
            isAdmin: function() {
                return $cookieStore.get('tipo_usp') == 'Administrati' ? true : false;
            },
            get: function() {
                return $cookieStore.get('user_id');
            },
            getUserData: function() {
                $user = {id:$cookieStore.get('user_id'),
                         username:$cookieStore.get('username'),
                         email:$cookieStore.get('email'),
                         tipo_usp:$cookieStore.get('tipo_usp')};
                return $user;
            },
            set: function(user_id) {
              $cookieStore.put('user_id',user_id);
            },
            getServerDate: function(){
              var serverDate = '';
              $http.get(API.url+'bagtesis/backend/public/server_date')
                    .success(function(response) {
                      serverDate = response;
                    })
                    .error(function(response) {});
              if(serverDate !='')
                return serverDate;
              else
                return new Date();
            },
            sessionExpire: function(){
                
                $http.get(API.url+'bagtesis/backend/public/logout')
                    .success(function(response) {

                        $cookieStore.remove('auth_token');
                        $cookieStore.remove('user_id');
                        $cookieStore.remove('email');
                        $cookieStore.remove('username');
                        $cookieStore.remove('logindate');
                        $cookieStore.remove('tipo_usp');
                        $http.defaults.headers.common['X-Auth-Token'] = '';
                        $rootScope.userWelcome = '';
                        $rootScope.AuthTitle = 'Iniciar Sesión';
                        $rootScope.AuthURL = 'login';
                        $rootScope.userLogged = false;
                        $rootScope.es_administrador = false;
                    })
                    .error(function(response) {});
            },
            getExpirationTime: function(){
              // 4 hours in seconds
              return 14400; 
            }
        };
    });