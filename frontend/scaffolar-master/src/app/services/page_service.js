angular.module('app')
    .factory('PageAuthService', function ($rootScope, $state, $http) {

        return {
            get: function() {
                if ($state.is($state.current.name)) {

                    $rootScope.page = {
                        title: $state.current.label
                    };

                    $http.defaults.headers.common['X-Auth-Token'] = SessionService.get('auth_token');
                    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                }
            },
            isAuthRoute: function(route) {
                if (route) {
                    console.log('ruta: '+route.auth);
                    return route.auth;
                }
                return false;
            },
        };
    });
