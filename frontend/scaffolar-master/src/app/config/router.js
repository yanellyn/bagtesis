angular.module ('app')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        controller: 'HomeController',
        templateUrl: 'app/views/home/home.tpl.html',
        title: 'Inicio'
      })
      .state('ejemplo', {
        url: '/miEjemplo',
        controller: 'TestController',
        templateUrl: 'app/views/Test/ejp.tpl.html',
        title: 'Prueba Ejemplo TEST'
      })
      .state('indice', {
        url: '/bag/colecciones/indices',
        controller: 'IndicesController',
        templateUrl: 'app/views/indices/indices.tpl.html',
        title: 'Indice Titulos',
        auth: false
      })

      .state('buscador_simple', {
        url: '/buscador_simple',
        controller: 'BuscadorSimpleController',
        templateUrl: 'app/views/buscadores/buscador_simple.tpl.html',
        title: 'Buscadores',
        auth: false
      })

      .state('buscador_avanzado', {
        url: '/buscador_avanzado',
        controller: 'BuscadorAvanzadoController',
        templateUrl: 'app/views/buscadores/buscador_avanzado.tpl.html',
        title: 'Buscadores',
        auth: false
      })      

       .state('contacto', {
        url: '/contacto',
        controller: 'ContactoController',
        templateUrl: 'app/views/footer/contacto.tpl.html',
        title: 'Contacto',
        auth: false
      })      

      .state('panel_control', {
        url: '/panel_control',
        controller: 'PanelControlController',
        templateUrl: 'app/views/panelControl/panelControl.tpl.html',
        title: 'Panel de Control',
        auth: true
      })

      .state('prestamos', {
        url: '/prestamos',
        controller: 'PrestamosController',
        templateUrl: 'app/views/prestamos/prestamos.tpl.html',
        title: 'Prestamos',
        auth: true
      })

      .state('registrar_prestamos', {
        url: '/registrar_prestamos',
        controller: 'RegistrarPrestamosController',
        templateUrl: 'app/views/adminPrestamos/registrarPrestamos.tpl.html',
        title: 'Registrar Prestamos',
        auth: false
      })

      .state('mis_prestamos', {
        url: '/mis_prestamos',
        controller: 'MisPrestamosController',
        templateUrl: 'app/views/prestamos/mis_prestamos.tpl.html',
        title: 'Mis Prestamos',
        auth: true
      })

      .state('prestamosConsulta', {
        url: '/prestamos_consulta_ci',
        controller: 'PrestamosConsultaController',
        templateUrl: 'app/views/prestamos/prestamosConsulta.tpl.html',
        title: 'PrestamosConsulta',
        auth: false
      })

      .state('headers', {
        url: '/headers',
        controller: 'HeadersController',
        templateUrl: 'app/views/headers/headers.tpl.html',
        title: 'Headers',
        auth: false
      })

      .state('reportes', {
        url: '/reportes',
        controller: 'ReportesController',
        templateUrl: 'app/views/adminReportes/reportes.tpl.html',
        title: 'Reportes',
        auth: true
      })

      .state('politicas', {
        url: '/politicas',
        controller: 'PoliticasController',
        templateUrl: 'app/views/politicas/politicas.tpl.html',
        title: 'Politicas',
        auth: true
      })

      .state('login', {
        url: '/login',
        controller: 'LoginController',
        templateUrl: 'app/views/login/login.tpl.html',
        title: 'Login',
        auth: false
      })

      .state('perfil', {
        url: '/perfil',
        controller: 'PerfilController',
        templateUrl: 'app/views/perfil/perfil.tpl.html',
        title: 'Perfil',
        auth: true
      })

      .state('logout', {
        url: '/logout',
        controller: 'LogoutController',
        templateUrl: 'app/views/login/login.tpl.html',
        title: 'Login',
        auth: true
      })
      .state('librosPrincipal', {
        url: '/catalogacion/libros',
        controller: 'librosController',
        templateUrl: 'app/views/catalogacion/libros/principal.tpl.html',
        title: 'Libros',
        auth: true
      })
      .state('librosAutores', {
        url: '/catalogacion/autores',
        controller: 'autoresController',
        templateUrl: 'app/views/catalogacion/autores/autores.tpl.html',
        title: 'Autores',
        auth: true
      })
      .state('librosTitulos', {
        url: '/catalogacion/titulos',
        controller: 'titulosController',
        templateUrl: 'app/views/catalogacion/titulos/titulos.tpl.html',
        title: 'Titulos',
        auth: true
      })
      .state('librosEjemplares', {
        url: '/catalogacion/ejemplares',
        controller: 'ejemplaresController',
        templateUrl: 'app/views/catalogacion/ejemplares/ejemplares.tpl.html',
        title: 'Ejemplares',
        auth: true
      })
      .state('librosDescriptores', {
        url: '/catalogacion/descriptores',
        controller: 'descriptoresController',
        templateUrl: 'app/views/catalogacion/descriptores/descriptores.tpl.html',
        title: 'Descriptores',
        auth: true
      })
      .state('soporte', {
        url: '/catalogacion/soporte',
        controller: 'soporteController',
        templateUrl: 'app/views/catalogacion/soporte/soporte.tpl.html',
        title: 'Soporte',
        auth: true
      })
      .state('librosConferencias', {
        url: '/catalogacion/conferencias',
        controller: 'conferenciasController',
        templateUrl: 'app/views/catalogacion/conferencias/conferencias.tpl.html',
        title: 'Conferencias',
        auth: true
      })
      .state('publicacionesSeriadas', {
        url: '/catalogacion/publicacionesSeriadas',
        controller: 'publicacionesSeriadasController',
        templateUrl: 'app/views/catalogacion/publicacionesSeriadas/principal.tpl.html',
        title: 'Publicaciones Seriadas',
        auth: true
      })
      .state('volumenes', {
        url: '/catalogacion/volumenes',
        controller: 'volumenesController',
        templateUrl: 'app/views/catalogacion/volumenes/volumenes.tpl.html',
        title: 'Volumenes',
        auth: true
      })
       .state('numeros', {
        url: '/catalogacion/numeros',
        controller: 'numerosController',
        templateUrl: 'app/views/catalogacion/numeros/numeros.tpl.html',
        title: 'Números',
        auth: true
      })
      .state('trabajosAcademicos', {
        url: '/catalogacion/trabajosAcademicos',
        controller: 'trabajosAcademicosController',
        templateUrl: 'app/views/catalogacion/trabajosAcademicos/principal.tpl.html',
        title: 'Trabajos Académicos',
        auth: true
      }).state('exportar', {
        url: '/catalogacion/exportar',
        controller: 'exportarController',
        templateUrl: 'app/views/catalogacion/exportar/exportar.tpl.html',
        title: 'Exportar',
        auth: true
      }).state('importar', {
        url: '/catalogacion/importar',
        controller: 'importarController',
        templateUrl: 'app/views/catalogacion/importar/importar.tpl.html',
        title: 'Importar',
        auth: true
      }).state('reportesCatalogacion', {
        url: '/reportesCatalogacion',
        controller: 'reportesCatalogacionController',
        templateUrl: 'app/views/adminReportes/reportesCatalogacion.tpl.html',
        title: 'Reportes Catalogación',
        auth: true
      }).state('otrosRecursos', {
        url:  '/catalogacion/otrosRecursos',
        controller: 'otrosRecursosController',
        templateUrl:  'app/views/catalogacion/otrosRecursos/principal.tpl.html',
        title: 'Otros Recursos',
        auth: true
      });
    $urlRouterProvider.otherwise('/');
  });


