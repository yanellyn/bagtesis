
{{ HTML::script('js/jquery-1.11.2.min.js') }}

{{ Form::open(array('action' => 'PrestamosController@test_send', 'method' => 'POST', 'id' => 'id_form')) }}
    
{{ Form::label('text', 'Texto: ') }}
{{ Form::text('text') }}

{{ Form::submit('Enviar') }}

{{ Form::close() }}

<!--en este div mostramos el preloader-->
<div style='margin: 10px 0px 0px 300px' class='before'></div>   
<!--en este los errores del formulario-->
<div class='errors_form'></div>

<div style='display: none' class='success_message alert-box success'></div>

<script>
	$(document).ready(function() {

		var form = $('#id_form');
        form.bind('submit',function () {
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                beforeSend: function(){
                    $('.before').show().append('<img src="img/ajax_loader.gif" />');

                },
                complete: function(data){
                    $('.before').html('');
                },
                success: function (data) {
                    var obj = JSON.parse(data);

                    $('.before').hide();
                    $('.errors_form').html('');
                    $('.success_message').hide().html('');
                    $('.success_message').show().append('Titulo: '+obj.titulo+'<br>');
                    $('.success_message').show().append('Cantidad de Ejemplares: '+obj.cantidad+'<br>');
                    $('.success_message').show().append('Cantidad de Ejemplares Disponibles: '+obj.cantidad_disponibles+'<br>');
                    //$('.success_message').show().html(data);

                    
                },
                error: function(errors){
                    $('.before').hide();
                    $('.errors_form').html('');
                    $('.errors_form').html(errors);
                }
            });
       return false;
    });


	});
	
</script>