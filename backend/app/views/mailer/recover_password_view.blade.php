<div>
	<fieldset id="recover">
		<p><strong>Recuperar Contraseña.</strong></p>

		{{ Form::open(array('url' => 'recover_password_post', 'method'=>'POST')) }}

	        <p class="clearfix"><label for="email">Email</label>

	        {{ Form::text('email', Input::old('email'), array('id'=>'email')); }}

	        {{ Form::submit('Listo', array('id'=>'submit'))}}
	           
	    {{ Form::close() }}

	</fieldset>
</div> 