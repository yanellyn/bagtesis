<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>

    {{HTML::script('js/jquery.js')}}
    {{HTML::script('js/jquery.min.js')}}
    {{HTML::script('js/jquery-1.8.2.min.js')}}
    {{HTML::script('js/modernizr-1.5.min.js')}}
    {{HTML::style('bootstrap/css/bootstrap.css')}}
    {{HTML::script('bootstrap/js/bootstrap.min.js')}}
<style type="text/css">@import url("Calendario/css/calendar-blue.css");</style>
<script type="text/javascript" src="Calendario/js/calendar.js"></script>
<script type="text/javascript" src="Calendario/js/calendar-es.js" ></script>
<script type="text/javascript" src="Calendario/js/calendar-setup.js" ></script>


    {{HTML::style('css/style.css')}}


<!-- Latest compiled and minified CSS -->



    

</head>  
<body>
	
	<div id="header">
    	<a href="#"><img src="images/logo.png" title="Affiliate Promo logo" id="logo" alt="Logo" /></a>
        <ul id="navBar">
        	<li class="current"><a href="index.html">Home</a></li>
            <li><a href="#">Our Services</a></li>
            <li><a href="#">Pricing Plans</a></li>
     <!--      <li><a href="#">Agencias</a></li> -->
                <li>{{link_to('/agencia','Agencias')}}</li>
            <li><a href="#">Contactos</a></li>   
            <li></li>         
        </ul>
    </div>

    <div id="welcomeMessage">
    	<h2>Bienvenido a Distribuidora Dijkstra</h2>
    	<p>La entrega de sus productos en el menor tiempo posible y de la forma mas eficiente!</p>
        <p>Todo al alcance de sus manos desde cualquier parte web!</span>
    </div>

<!--  CONTENIDO --> 
    <div id="main" class="row">
            
            @yield('content')

    </div>
<!-- CONTENIDO -->

<!--  <div id="wrapper">
    	<div id="secWrapper">
        	<div id="container" class="clearfix">
            	<div id="mainCol" class="clearfix">
                	<div id="services">
                    <h3>Our Services</h3>
                    <ul>
                    <li>
                    <h4>Service title</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </li>
                     <li>
                    <h4>Service title</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </li>
                     <li>
                    <h4>Service title</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </li>
                    </ul>
                    </div>
                    <h3 id="why">Why Choose Your Business ?</h3>
                    <ul id="maincon">
                    <li class="clearfix">
                    <img src="images/image1.jpg" alt="image1" />
                    <h2>Services you could depend on</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                    </li>
                    <li class="clearfix">
                    <img src="images/image2.jpg" alt="image1" />
                    <h2>Guranteed added value</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                    </li>
                    <li class="clearfix">
                    <img src="images/image3.jpg" alt="image1" />
                    <h2>Support you can trust</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                    </li>
                    <li class="clearfix last">
                    <img src="images/image4.jpg" alt="image1" />
                    <h2>Professional team at you help</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                    </li>
                    </ul>

                </div>
                <div id="secCol">
                    <h3 id="news">Latest News</h3>
                    <ul>
                    	<li class="clearfix">
                        <h4><a href="#">News title goes here</a></h4>
                        <span>19 November, 2008</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipis elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim niam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <a href="#" class="more">Read more</a>
                        </li>
                        <li class="clearfix">
                        <h4><a href="#">News title goes here</a></h4>
                        <span>19 November, 2008</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipis elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim niam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <a href="#" class="more">Read more</a>
                        </li>
                    </ul>
                    <h3 id="test">Testimonials</h3>
                    <ul>
                    <li class="clearfix">
                    <p>Lorem ipsum dolor sit amet, consectetur adipis elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim niam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <p class="test"><span>Lorem ipsum</span>, <a href="#">dolorsit.com</a></p>
                    </li>
                    </ul>
                    
                </div> 
            </div>
        </div>
    </div> -->







    <div id="footer">
    	<ul>
	        <li><a href="#">Home</a>&nbsp;&nbsp;-&nbsp;&nbsp;</li>
  	        <li><a href="#">Terms and Conditions</a>&nbsp;&nbsp;-&nbsp;&nbsp;</li>
			<li><a href="#">Privacy Policy</a>&nbsp;&nbsp;-&nbsp;&nbsp;</li>
 			<li><a href="#">Sitemap</a>&nbsp;&nbsp;-&nbsp;&nbsp;</li>
  	        <li><a href="#">Support</a>&nbsp;&nbsp;-&nbsp;&nbsp;</li>
			<li><a href="#">Contact Us</a></li>
        </ul>
        <p>Copyrights &copy; 2008 yourbusiness.com, All Rights Reserved</p>
    </div>
</body>
</html>


