<html>
<head>

	    <title></title>
</head>
<body>


	<div style="width: 100%">
		<div style="width:220px; height:110px; display: inline-block; ">
			<img style="width:200px; height: 80px; display: inline-block;" src="<?php echo $message->embed('http://localhost/bagtesis/backend/public/img/bag.png'); ?>"><br>
		</div>
		<div style="width:95px; height:110px; display: inline-block; ">
			<img style="width:70px; height: 70px; display: inline-block;" src="<?php echo $message->embed('http://localhost/bagtesis/backend/public/img/logociens.jpg'); ?>"><br>	
		</div>
		<div style="width:100px; height:110px; display: inline-block; margin-left:-5px; ">		
			<img style="width:80px; height: 80px; display: inline-block;" src="<?php echo $message->embed('http://localhost/bagtesis/backend/public/img/logo-ucv.jpg'); ?>"><br>
		</div>		
	</div>
	<p>Se han consultado los siguientes recursos: </p><br>


<?php foreach ($user->recursos as $recurso) {  ?>


	<table style="border: 1px solid black; width:100%">
		  <tr>
		    <th>Cota</th>

		    <th>Título</th>

			<?php 
    	    	$array_tipo_autores = array();
        	    	foreach ($recurso->autores as $autor) {
        	    		array_push($array_tipo_autores,$autor->tipo_autor);
        	    	}

        	    	$array_tipo_autores = array_unique($array_tipo_autores);
      				foreach ($array_tipo_autores as $tipo_autor) {
      					?> <th>
      							<?php $pos=0; while(1){ if($recurso->autores[$pos]->tipo_autor == $tipo_autor){ echo $recurso->autores[$pos]->nombre;break;}$pos++;} ?>
      						</th>
      				<?php } ?>
					    <?php 
		    	if($recurso->tipo_liter == 'T'){
		    		echo "<th> Grado académico </th>";
		    	}
		    	
		     ?>		

		  </tr>
		  <tr>
		  	<td style="border: 1px solid black;"><?php print_r($recurso->ubicacion); ?></td>
		    <td style="border: 1px solid black;">
		    		<?php 
	                	foreach ($recurso->titulos as $titulo) {
		                  		if($titulo->tipo_tit == 'OP'){
									print_r($titulo->titulo_salida);
								}

							}
					?>
		    </td>
			
		    <?php foreach ($array_tipo_autores as $tipo_autor) {  ?>
		 		<td style="border: 1px solid black;">
					<?php 	foreach ($recurso->autores as $autor) {
								if($autor->tipo_autor == $tipo_autor){
									echo $autor->autor_salida;
								}
	              		    }  
	              	?>	
		 		</td>
		    	<?php } ?>

		   <?php  

	    	if($recurso->tipo_liter == 'T'){
		    		echo '<td style="border: 1px solid black;">';
		    		echo $recurso->grado_academico;
		    		echo '</td>';
		    	}
		   	?>
		  </tr>	

	</table>


	<br>
	<br>

	<table style="border: 1px solid black; width:100%">
		<tr>
			<th> Tipo de recurso</th>
			<th> Nro de páginas</th>
			<th> Año de publicación</th>
			<?php 
    	    	$array_tipo_descriptores = array();
        	    	foreach ($recurso->descriptores as $descriptor) {
        	    		array_push($array_tipo_descriptores,$descriptor->tipo);
        	    	}

        	    	$array_tipo_descriptores = array_unique($array_tipo_descriptores);
      				foreach ($array_tipo_descriptores as $tipo_descriptor) {
      					?> <th>
      						<?php 
      							foreach ($recurso->descriptores as $descriptor) {
		       						if($descriptor->tipo == $tipo_descriptor){
		       							echo $descriptor->descrip_desc;
		       							break;
		       						}
		       					}
      						?>
      						</th>
      				<?php } ?>
		</tr>

		<tr>
			<td style="border: 1px solid black;"> <?php print_r($recurso->tipo_doc);?></td>
			<td style="border: 1px solid black;"> <?php print_r($recurso->paginas); ?></td>
			<td style="border: 1px solid black;"> <?php print_r($recurso->fecha_pub); ?></td>

		    <?php foreach ($array_tipo_descriptores as $tipo_descriptor) {  ?>
		 		<td style="border: 1px solid black;">
					<?php 	foreach ($recurso->descriptores as $descriptor) {
								if($descriptor->tipo == $tipo_descriptor){
									print_r($descriptor->descriptor_salida);
								}
	              		    }  
	              	?>	
		 		</td>
		    	<?php } ?>
		</tr>
	</table>

<?php } ?>

	<br>
	<br>

	<div style="width: 100%; text-align: center">
		
	<p>Av. Los Ilustres, Los Chaguaramos, Facultad de Ciencias, Universidad Central de Venezuela</p>
	<p>Caracas ZP 1040, Apartado Postal 20513</p>
	<p>(58212) 6051671/ 1665 / 2136 (FAX)</p>
	</div>
</body>
</html>