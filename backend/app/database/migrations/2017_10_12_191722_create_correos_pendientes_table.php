<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCorreosPendientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('correos_pendientes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('destinatarios');
			$table->text('contenido');
			$table->integer('prioridad')->nullable();
			$table->string('fecha_registrado')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('correos_pendientes');
	}

}
