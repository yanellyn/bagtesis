<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVolumenesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('volumenes', function(Blueprint $table)
		{
			$table->foreign(array('recurso_id','ext_recurso_id'), 'volumenes_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('volumenes', function(Blueprint $table)
		{
			$table->dropForeign('volumenes_recurso_id_ext_recurso_id_foreign');
		});
	}

}
