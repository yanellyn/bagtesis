<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuspensionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('suspension', function(Blueprint $table)
		{
			$table->string('id');
			$table->string('id_usuario');
			$table->string('id_ejemplar')->nullable();
			$table->string('fecha');
			$table->string('tipo');
			$table->string('dias');
			$table->string('pago')->nullable();
			$table->string('observacion')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('suspension');
	}

}
