<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecursosBibliotecasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recursos_bibliotecas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('recurso_id')->index();
			$table->integer('ext_recurso_id')->index();
			$table->integer('biblioteca_id')->nullable()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recursos_bibliotecas');
	}

}
