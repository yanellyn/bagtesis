<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBibliotecasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bibliotecas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('biblioteca');
			$table->string('biblioteca_salida');
			$table->string('siglas');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bibliotecas');
	}

}
