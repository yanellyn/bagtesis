<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEditorialesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('editoriales', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('editorial')->nullable()->index();
			$table->string('ciudad')->nullable();
			$table->string('pais_id')->nullable();
			$table->string('direccion')->nullable();
			$table->string('direccion_id')->nullable();
			$table->string('datos_dudosos')->nullable();
			$table->string('editorial_salida')->nullable()->index();
			$table->string('fabricante')->nullable();
			$table->string('ciudad_fabricacion')->nullable();
			$table->string('fecha_manufactura')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('editoriales');
	}

}
