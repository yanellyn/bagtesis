<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEjemplaresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ejemplares', function(Blueprint $table)
		{
			$table->foreign(array('recurso_id','ext_recurso_id'), 'ejemplares_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('estado')->references('tipo_estado')->on('tipoestadoejemplar')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('tipo_liter')->references('id')->on('tipo_docs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ejemplares', function(Blueprint $table)
		{
			$table->dropForeign('ejemplares_recurso_id_ext_recurso_id_foreign');
			$table->dropForeign('ejemplares_estado_foreign');
			$table->dropForeign('ejemplares_tipo_liter_foreign');
		});
	}

}
