<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoAutoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipo_autores', function(Blueprint $table)
		{
			$table->string('tipo_doc');
			$table->string('tipo_autor');
			$table->string('nombre')->nullable();
			$table->integer('orden')->nullable();
			$table->primary(['tipo_doc','tipo_autor'], 'tipo_autores_pkey');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipo_autores');
	}

}
