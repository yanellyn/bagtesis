<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrestamoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prestamo', function(Blueprint $table)
		{
			$table->string('id');
			$table->string('id_ejemplar')->nullable();
			$table->string('fecha_sol')->nullable();
			$table->string('fecha_pre')->nullable();
			$table->string('hora')->nullable();
			$table->string('fecha_exp')->nullable();
			$table->string('fecha_dev')->nullable();
			$table->string('id_admin')->nullable();
			$table->string('prestamo')->nullable();
			$table->integer('periodos_renov')->nullable();
			$table->string('codigo_aut')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('prestamo');
	}

}
