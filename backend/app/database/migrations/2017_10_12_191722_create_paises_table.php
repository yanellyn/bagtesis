<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaisesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('paises', function(Blueprint $table)
		{
			$table->string('id')->primary('paises_pkey');
			$table->string('pais')->nullable();
			$table->string('cod_cont')->nullable();
			$table->string('codp_usmarc')->nullable();
			$table->integer('codigo_pais')->nullable();
			$table->integer('codigo_tlf')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('paises');
	}

}
