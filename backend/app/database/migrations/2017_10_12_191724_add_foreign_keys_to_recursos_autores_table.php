<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecursosAutoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recursos_autores', function(Blueprint $table)
		{
			$table->foreign('cod_autor')->references('id')->on('autores')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign(array('recurso_id','ext_recurso_id'), 'recursos_autores_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign(array('tipo_doc','tipo_autor'), 'recursos_autores_tipo_doc_tipo_autor_foreign')->references(array('tipo_doc','tipo_autor'))->on('tipo_autores')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recursos_autores', function(Blueprint $table)
		{
			$table->dropForeign('recursos_autores_cod_autor_foreign');
			$table->dropForeign('recursos_autores_recurso_id_ext_recurso_id_foreign');
			$table->dropForeign('recursos_autores_tipo_doc_tipo_autor_foreign');
		});
	}

}
