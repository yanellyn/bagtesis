<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVariablesReportesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('variables_reportes', function(Blueprint $table)
		{
			$table->integer('reporte_id');
			$table->integer('variable_id');
			$table->string('tipo')->nullable();
			$table->string('nombre')->nullable();
			$table->string('valor')->nullable();
			$table->string('valor_binario')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('variables_reportes');
	}

}
