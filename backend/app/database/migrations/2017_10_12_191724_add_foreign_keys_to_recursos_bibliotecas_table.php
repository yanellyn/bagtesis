<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecursosBibliotecasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recursos_bibliotecas', function(Blueprint $table)
		{
			$table->foreign('biblioteca_id')->references('id')->on('bibliotecas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign(array('recurso_id','ext_recurso_id'), 'recursos_bibliotecas_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recursos_bibliotecas', function(Blueprint $table)
		{
			$table->dropForeign('recursos_bibliotecas_biblioteca_id_foreign');
			$table->dropForeign('recursos_bibliotecas_recurso_id_ext_recurso_id_foreign');
		});
	}

}
