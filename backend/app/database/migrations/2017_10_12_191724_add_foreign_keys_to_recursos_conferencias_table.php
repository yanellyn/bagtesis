<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecursosConferenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recursos_conferencias', function(Blueprint $table)
		{
			$table->foreign(array('recurso_id','ext_recurso_id'), 'recursos_conferencias_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('conferencia_id')->references('id')->on('conferencias')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recursos_conferencias', function(Blueprint $table)
		{
			$table->dropForeign('recursos_conferencias_recurso_id_ext_recurso_id_foreign');
			$table->dropForeign('recursos_conferencias_conferencia_id_foreign');
		});
	}

}
