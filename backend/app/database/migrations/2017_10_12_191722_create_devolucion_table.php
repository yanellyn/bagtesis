<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDevolucionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('devolucion', function(Blueprint $table)
		{
			$table->string('id');
			$table->string('id_ejemplar');
			$table->string('fecha_prestamo')->nullable();
			$table->string('fecha_solicitud')->nullable();
			$table->string('fecha_expiracion')->nullable();
			$table->string('fecha_devolucion');
			$table->string('hora')->nullable();
			$table->string('hora_dev');
			$table->string('fecha_sus')->nullable();
			$table->string('cod_adm_prest')->nullable();
			$table->string('cod_adm_devol')->nullable();
			$table->string('prestamo')->nullable();
			$table->integer('periodos_renov')->nullable();
			$table->string('cod_aut_prest')->nullable();
			$table->string('cod_aut_devol')->nullable();
			$table->primary(['id','id_ejemplar','fecha_devolucion','hora_dev'], 'devolucion_pkey');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('devolucion');
	}

}
