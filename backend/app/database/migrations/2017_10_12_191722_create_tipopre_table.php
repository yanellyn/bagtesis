<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipopreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipopre', function(Blueprint $table)
		{
			$table->string('tipo_pre');
			$table->string('descrip_pre')->nullable();
			$table->string('archivo_bmp')->nullable();
			$table->string('prog_adm')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipopre');
	}

}
