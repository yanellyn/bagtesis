<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCamposadicionalesTipodocTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('camposadicionales_tipodoc', function(Blueprint $table)
		{
			$table->integer('campo_adicional');
			$table->string('tipo_doc');
			$table->primary(['campo_adicional','tipo_doc'], 'camposadicionales_tipodoc_pkey');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('camposadicionales_tipodoc');
	}

}
