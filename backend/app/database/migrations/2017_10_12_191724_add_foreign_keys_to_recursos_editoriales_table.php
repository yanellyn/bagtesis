<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecursosEditorialesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recursos_editoriales', function(Blueprint $table)
		{
			$table->foreign('editorial_id')->references('id')->on('editoriales')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign(array('recurso_id','ext_recurso_id'), 'recursos_editoriales_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recursos_editoriales', function(Blueprint $table)
		{
			$table->dropForeign('recursos_editoriales_editorial_id_foreign');
			$table->dropForeign('recursos_editoriales_recurso_id_ext_recurso_id_foreign');
		});
	}

}
