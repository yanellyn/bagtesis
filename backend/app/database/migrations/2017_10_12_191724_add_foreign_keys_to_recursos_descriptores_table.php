<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecursosDescriptoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recursos_descriptores', function(Blueprint $table)
		{
			$table->foreign('descriptor_id')->references('id')->on('descriptores')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign(array('recurso_id','ext_recurso_id'), 'recursos_descriptores_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recursos_descriptores', function(Blueprint $table)
		{
			$table->dropForeign('recursos_descriptores_descriptor_id_foreign');
			$table->dropForeign('recursos_descriptores_recurso_id_ext_recurso_id_foreign');
		});
	}

}
