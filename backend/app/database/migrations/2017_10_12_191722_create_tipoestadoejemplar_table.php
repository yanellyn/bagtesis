<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoestadoejemplarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipoestadoejemplar', function(Blueprint $table)
		{
			$table->string('tipo_estado')->primary('tipoestadoejemplar_pkey');
			$table->string('tipo_doc')->nullable();
			$table->string('descripcion')->nullable();
			$table->integer('orden')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipoestadoejemplar');
	}

}
