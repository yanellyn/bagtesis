<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTesisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tesis', function(Blueprint $table)
		{
			$table->foreign('grado_academico_id')->references('id')->on('grados_academicos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign(array('recurso_id','ext_recurso_id'), 'tesis_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('cod_inst', 'tesis_cod_inst_institucion_foreign')->references('id')->on('instituciones')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('nivel_id')->references('id')->on('niveles_academicos')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('trabajo_id')->references('id')->on('tipo_trabajo')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tesis', function(Blueprint $table)
		{
			$table->dropForeign('tesis_grado_academico_id_foreign');
			$table->dropForeign('tesis_recurso_id_ext_recurso_id_foreign');
			$table->dropForeign('tesis_cod_inst_institucion_foreign');
			$table->dropForeign('tesis_nivel_id_foreign');
			$table->dropForeign('tesis_trabajo_id_foreign');
		});
	}

}
