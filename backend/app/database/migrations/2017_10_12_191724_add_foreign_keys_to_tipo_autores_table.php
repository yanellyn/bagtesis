<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTipoAutoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tipo_autores', function(Blueprint $table)
		{
			$table->foreign('tipo_doc')->references('id')->on('tipo_docs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tipo_autores', function(Blueprint $table)
		{
			$table->dropForeign('tipo_autores_tipo_doc_foreign');
		});
	}

}
