<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHistorialCatalogacionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('historial_catalogacion', function(Blueprint $table)
		{	

			$table->foreign(array('recurso_id','ext_recurso_id'), 'historial_catalogacion_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_catalogador')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('historial_catalogacion', function(Blueprint $table)
		{
			$table->dropForeign('historial_catalogacion_recurso_id_ext_recurso_id_foreign');
			$table->dropForeign('historial_catalogacion_id_catalogador_foreign');
		});
	}

}
