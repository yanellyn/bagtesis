<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHistorialCatalogacionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('historial_catalogacion', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('id_catalogador')->nullable();
			$table->string('recurso_id')->nullable();
			$table->integer('ext_recurso_id')->nullable();
			$table->string('operacion')->nullable();
			$table->string('modulo')->nullable();
			$table->string('fecha')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('historial_catalogacion');
	}

}
