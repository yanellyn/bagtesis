<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDatosReportesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('datos_reportes', function(Blueprint $table)
		{
			$table->integer('id')->primary('datos_reportes_pkey');
			$table->string('tipo')->nullable();
			$table->string('nombre')->nullable();
			$table->string('archivo')->nullable();
			$table->string('tipo_doc')->nullable();
			$table->string('nivel_reg')->nullable();
			$table->string('por_omision')->nullable();
			$table->string('por_pantalla')->nullable();
			$table->string('usa_select')->nullable();
			$table->string('usa_temporal')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('datos_reportes');
	}

}
