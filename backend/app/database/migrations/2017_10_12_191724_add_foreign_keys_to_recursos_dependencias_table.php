<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecursosDependenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recursos_dependencias', function(Blueprint $table)
		{
			$table->foreign('dependencia_id')->references('id')->on('dependencias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign(array('recurso_id','ext_recurso_id'), 'recursos_dependencias_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recursos_dependencias', function(Blueprint $table)
		{
			$table->dropForeign('recursos_dependencias_dependencia_id_foreign');
			$table->dropForeign('recursos_dependencias_recurso_id_ext_recurso_id_foreign');
		});
	}

}
