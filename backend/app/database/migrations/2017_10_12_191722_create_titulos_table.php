<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTitulosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('titulos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('titulo', 512)->nullable()->index();
			$table->integer('caracter_orden')->nullable();
			$table->string('titulo_salida', 512)->nullable()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('titulos');
	}

}
