<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDescriptoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('descriptores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('descriptor')->nullable()->index();
			$table->string('describe')->nullable();
			$table->string('tipo')->nullable()->index();
			$table->string('codigo_externo')->nullable();
			$table->integer('orden')->nullable();
			$table->integer('cod_desc_nivel_sup')->nullable();
			$table->integer('cod_desc_sinonimo')->nullable();
			$table->string('descriptor_salida')->nullable()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('descriptores');
	}

}
