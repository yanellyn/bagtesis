<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecursosCamposadicionalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recursos_camposadicionales', function(Blueprint $table)
		{
			$table->foreign(array('tipo_doc','campo_adicional'), 'recursos_camposadicionales_tipo_doc_campo_adicional_foreign')->references(array('tipo_doc','campo_adicional'))->on('camposadicionales_tipodoc')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign(array('recurso_id','ext_recurso_id'), 'recursos_camposadicionales_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recursos_camposadicionales', function(Blueprint $table)
		{
			$table->dropForeign('recursos_camposadicionales_tipo_doc_campo_adicional_foreign');
			$table->dropForeign('recursos_camposadicionales_recurso_id_ext_recurso_id_foreign');
		});
	}

}
