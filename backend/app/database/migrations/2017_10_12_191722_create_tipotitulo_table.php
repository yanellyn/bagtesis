<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipotituloTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipotitulo', function(Blueprint $table)
		{
			$table->string('tipo_titulo');
			$table->string('tipo_doc');
			$table->string('descripcion');
			$table->integer('orden');
			$table->primary(['tipo_titulo','tipo_doc'], 'tipotitulo_pkey');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipotitulo');
	}

}
