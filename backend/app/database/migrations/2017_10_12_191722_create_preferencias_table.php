<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePreferenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('preferencias', function(Blueprint $table)
		{
			$table->string('id');
			$table->string('recurso_id');
			$table->string('fecha_sol');
			$table->string('hora_sol');
			$table->string('id_admin')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('preferencias');
	}

}
