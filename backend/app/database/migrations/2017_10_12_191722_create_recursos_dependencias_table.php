<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecursosDependenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recursos_dependencias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('recurso_id')->index();
			$table->integer('ext_recurso_id')->index();
			$table->integer('dependencia_id')->nullable()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recursos_dependencias');
	}

}
