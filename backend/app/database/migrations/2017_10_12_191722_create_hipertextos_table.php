<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHipertextosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hipertextos', function(Blueprint $table)
		{
			$table->integer('id');
			$table->string('recurso_id')->nullable();
			$table->integer('ext_recurso_id')->nullable();
			$table->string('dir_virtual')->nullable();
			$table->string('dir_fisica')->nullable();
			$table->string('archivo')->nullable();
			$table->string('descripcion')->nullable();
			$table->integer('valor')->nullable();
			$table->string('uso')->nullable();
			$table->integer('orden')->nullable();
			$table->string('icono_reg')->nullable();
			$table->integer('nro_pagina')->nullable();
			$table->string('codigo_crea')->nullable();
			$table->integer('tamano')->nullable();
			$table->string('fecha_crea')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hipertextos');
	}

}
