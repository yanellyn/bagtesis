<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecursosAutoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recursos_autores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('recurso_id');
			$table->integer('ext_recurso_id');
			$table->integer('cod_autor')->nullable();
			$table->string('tipo_doc')->nullable();
			$table->string('tipo_autor')->nullable();
			$table->string('portada')->nullable();
			$table->integer('orden')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recursos_autores');
	}

}
