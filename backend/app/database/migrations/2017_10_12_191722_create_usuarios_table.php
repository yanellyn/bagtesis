<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function(Blueprint $table)
		{
			$table->string('id')->primary('usuarios_pkey');
			$table->string('carnet')->nullable();
			$table->string('dir_electr')->nullable();
			$table->string('apellidos')->nullable();
			$table->string('nombres')->nullable();
			$table->string('cedula')->nullable();
			$table->string('password')->nullable();
			$table->string('pais')->nullable();
			$table->string('sexo')->nullable();
			$table->string('direccion')->nullable();
			$table->string('ciudad')->nullable();
			$table->string('categoria')->nullable();
			$table->string('profesion')->nullable();
			$table->string('fecha_nac')->nullable();
			$table->string('fecha_in')->nullable();
			$table->string('fecha_out')->nullable();
			$table->string('status')->nullable();
			$table->string('observa')->nullable();
			$table->string('ult_modif')->nullable();
			$table->string('usr_ult_m')->nullable();
			$table->string('conexion')->nullable();
			$table->string('dependencia')->nullable();
			$table->string('tipo_usp')->nullable();
			$table->integer('cod_objeto')->nullable();
			$table->integer('cod_categoria')->nullable();
			$table->string('hora_ult_modif')->nullable();
			$table->rememberToken();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}
