<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEjemplaresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ejemplares', function(Blueprint $table)
		{
			$table->string('id')->index();
			$table->string('recurso_id')->nullable()->index();
			$table->integer('ext_recurso_id')->nullable();
			$table->string('ejemplar')->nullable();
			$table->string('existencia')->nullable();
			$table->binary('observa')->nullable();
			$table->string('impresion')->nullable();
			$table->string('estado')->nullable();
			$table->string('prestamo')->nullable();
			$table->string('propiedad')->nullable();
			$table->string('tipo_copia')->nullable();
			$table->string('sala')->nullable();
			$table->string('nro_inventario')->nullable();
			$table->string('tipo_liter')->nullable();
			$table->string('tipo_encuader')->nullable();
			$table->string('cod_objeto')->nullable();
			$table->integer('reserva')->nullable();
			$table->string('edo_prestamo')->nullable();
			$table->string('sala_salida')->nullable();
			$table->string('proveedor')->nullable();
			$table->string('forma_adquisicion')->nullable();
			$table->string('doi')->nullable();
			$table->integer('doc_electronico')->nullable();
			$table->string('nombre_doc_electronico')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ejemplares');
	}

}
