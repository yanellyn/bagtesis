<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDescriptoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('descriptores', function(Blueprint $table)
		{
			$table->foreign('tipo')->references('id')->on('tipo_descriptores')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('descriptores', function(Blueprint $table)
		{
			$table->dropForeign('descriptores_tipo_foreign');
		});
	}

}
