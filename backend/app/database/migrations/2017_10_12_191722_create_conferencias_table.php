<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConferenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conferencias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('conferencia')->nullable();
			$table->string('conferencia_salida')->nullable();
			$table->string('ciudad')->nullable();
			$table->string('pais_id')->nullable();
			$table->string('patrocinador')->nullable();
			$table->string('fecha')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conferencias');
	}

}
