<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoDescriptoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipo_descriptores', function(Blueprint $table)
		{
			$table->string('id');
			$table->primary('id');
			$table->string('descrip_desc')->nullable();
			$table->string('tipo_doc')->nullable()->index();
			$table->string('unico')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipo_descriptores');
	}

}
