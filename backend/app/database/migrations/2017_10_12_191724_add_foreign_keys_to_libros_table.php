<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLibrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('libros', function(Blueprint $table)
		{
			$table->foreign(array('recurso_id','ext_recurso_id'), 'libros_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('libros', function(Blueprint $table)
		{
			$table->dropForeign('libros_recurso_id_ext_recurso_id_foreign');
		});
	}

}
