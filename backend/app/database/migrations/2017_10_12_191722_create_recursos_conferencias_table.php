<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecursosConferenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recursos_conferencias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('recurso_id')->nullable();
			$table->integer('ext_recurso_id')->nullable();
			$table->integer('conferencia_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recursos_conferencias');
	}

}
