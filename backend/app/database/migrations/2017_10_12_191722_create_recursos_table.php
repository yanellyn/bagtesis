<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecursosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recursos', function(Blueprint $table)
		{
			$table->string('id')->index();
			$table->integer('ext_id');
			$table->integer('jerarquia')->nullable();
			$table->string('ubicacion')->nullable()->index();
			$table->string('tipo_liter')->nullable()->index();
			$table->string('nivel_reg')->nullable();
			$table->integer('cod_conf')->nullable();
			$table->integer('cod_proy')->nullable();
			$table->integer('cod_editor_inst')->nullable();
			$table->string('ubicacion_anterior')->nullable();
			$table->string('codigo')->nullable();
			$table->string('ubicacion_fia_nivel_sup')->nullable();
			$table->string('servicio_edicion')->nullable();
			$table->string('codigo_edicion')->nullable();
			$table->string('nivel_bibl')->nullable();
			$table->string('tipo_ref_analitica')->nullable();
			$table->string('paginas')->nullable();
			$table->integer('volumen')->nullable();
			$table->integer('volumen_final')->nullable();
			$table->integer('numero')->nullable();
			$table->integer('numero_final')->nullable();
			$table->string('numeracion')->nullable();
			$table->string('enum_nivel_ini_3')->nullable();
			$table->string('enum_nivel_ini_4')->nullable();
			$table->string('enum_nivel_fin_3')->nullable();
			$table->string('enum_nivel_fin_4')->nullable();
			$table->string('edicion')->nullable();
			$table->string('fecha_iso')->nullable();
			$table->string('fecha_iso_final')->nullable();
			$table->string('fecha_pub')->nullable();
			$table->string('isbn')->nullable();
			$table->string('indice_suplemento')->nullable();
			$table->string('disemina')->nullable();
			$table->string('url')->nullable();
			$table->string('n_disk_cin')->nullable();
			$table->string('impresion')->nullable();
			$table->integer('orden')->nullable();
			$table->string('cerrado')->nullable();
			$table->string('hora_iso_inicio')->nullable();
			$table->string('hora_iso_final')->nullable();
			$table->string('dir_electr')->nullable();
			$table->string('herramienta_correo')->nullable();
			$table->string('notas', 2000)->nullable();
			$table->string('resumen', 2000)->nullable();
			$table->string('datos_adicionales', 2000)->nullable();
			$table->string('fecha_catalogacion')->nullable();
			$table->string('id_catalogador')->nullable();
			$table->primary(['id','ext_id'], 'recursos_pkey');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recursos');
	}

}
