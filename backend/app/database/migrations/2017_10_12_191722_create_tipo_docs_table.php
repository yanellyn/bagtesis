<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoDocsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipo_docs', function(Blueprint $table)
		{
			$table->string('id')->primary('tipo_docs_pkey');
			$table->string('descrip_doc_salida')->nullable();
			$table->string('archivo_bmp')->nullable();
			$table->string('prog_adm')->nullable();
			$table->string('icono_peq')->nullable();
			$table->string('campos_opcionales')->nullable();
			$table->integer('orden')->nullable();
			$table->integer('cod_objeto')->nullable();
			$table->string('comentario')->nullable();
			$table->string('icono_gde_ref_a')->nullable();
			$table->string('icono_peq_ref_a')->nullable();
			$table->string('cerrable')->nullable();
			$table->string('seriado')->nullable();
			$table->string('codigo_adm')->nullable();
			$table->string('fecha_ult_mod')->nullable();
			$table->string('usuarios')->nullable();
			$table->integer('grupo_conexion')->nullable();
			$table->string('personal')->nullable();
			$table->string('descrip_doc_singular')->nullable();
			$table->string('hora_ult_mod')->nullable();
			$table->string('descrip_doc')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipo_docs');
	}

}
