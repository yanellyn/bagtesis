<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecursosTitulosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recursos_titulos', function(Blueprint $table)
		{
			$table->foreign('titulo_id')->references('id')->on('titulos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign(array('recurso_id','ext_recurso_id'), 'recursos_titulos_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign(array('tipo_doc','tipo_tit'), 'recursos_titulos_tipo_tit_tipo_doc_foreign')->references(array('tipo_doc','tipo_titulo'))->on('tipotitulo')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recursos_titulos', function(Blueprint $table)
		{
			$table->dropForeign('recursos_titulos_titulo_id_foreign');
			$table->dropForeign('recursos_titulos_recurso_id_ext_recurso_id_foreign');
			$table->dropForeign('recursos_titulos_tipo_tit_tipo_doc_foreign');
		});
	}

}
