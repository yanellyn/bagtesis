<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInstitucionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('instituciones', function(Blueprint $table)
		{
			$table->foreign('pais_id')->references('id')->on('paises')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('instituciones', function(Blueprint $table)
		{
			$table->dropForeign('instituciones_pais_id_foreign');
		});
	}

}
