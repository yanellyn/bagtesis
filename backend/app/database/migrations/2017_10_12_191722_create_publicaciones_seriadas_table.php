<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePublicacionesSeriadasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('publicaciones_seriadas', function(Blueprint $table)
		{
			$table->string('recurso_id')->index();
			$table->integer('ext_recurso_id');
			$table->integer('jerarquia')->nullable();
			$table->string('ubicacion')->nullable()->index();
			$table->string('tipo_liter')->nullable()->index();
			$table->string('nivel_bibl')->nullable();
			$table->string('nivel_reg')->nullable();
			$table->string('tipo_ref_analitica')->nullable();
			$table->string('paginas')->nullable();
			$table->integer('volumen')->nullable();
			$table->integer('volumen_final')->nullable();
			$table->integer('numero')->nullable();
			$table->integer('numero_final')->nullable();
			$table->string('numeracion')->nullable();
			$table->string('enum_nivel_ini_3')->nullable();
			$table->string('enum_nivel_ini_4')->nullable();
			$table->string('enum_nivel_fin_3')->nullable();
			$table->string('enum_nivel_fin_4')->nullable();
			$table->integer('cod_editor')->nullable();
			$table->integer('cod_editor_inst')->nullable();
			$table->string('edicion')->nullable();
			$table->string('fecha_iso')->nullable();
			$table->string('fecha_iso_final')->nullable();
			$table->string('fecha_pub')->nullable();
			$table->string('isbn')->nullable();
			$table->string('indice_suplemento')->nullable();
			$table->integer('cod_conf')->nullable();
			$table->integer('cod_proy')->nullable();
			$table->string('disemina')->nullable();
			$table->string('url')->nullable();
			$table->string('n_disk_cin')->nullable();
			$table->string('impresion')->nullable();
			$table->integer('orden')->nullable();
			$table->string('cerrado')->nullable();
			$table->string('hora_iso_inicio')->nullable();
			$table->string('hora_iso_final')->nullable();
			$table->string('dir_electr')->nullable();
			$table->string('herramienta_correo')->nullable();
			$table->string('periodo')->nullable();
			$table->string('tipo')->nullable();
			$table->string('regularidad')->nullable();
			$table->string('existencia', 5000)->nullable();
			$table->string('issn')->nullable()->index();
			$table->string('ult_susc_desde')->nullable();
			$table->string('ult_susc_hasta')->nullable();
			$table->string('vols_aso')->nullable();
			$table->string('nros_aso')->nullable();
			$table->primary(['recurso_id','ext_recurso_id'], 'publicaciones_seriadas_pkey');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('publicaciones_seriadas');
	}

}
