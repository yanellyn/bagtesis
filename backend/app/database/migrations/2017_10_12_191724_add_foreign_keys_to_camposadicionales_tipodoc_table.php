<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCamposadicionalesTipodocTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('camposadicionales_tipodoc', function(Blueprint $table)
		{
			$table->foreign('campo_adicional')->references('id')->on('campos_adicionales')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('tipo_doc')->references('id')->on('tipo_docs')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('camposadicionales_tipodoc', function(Blueprint $table)
		{
			$table->dropForeign('camposadicionales_tipodoc_campo_adicional_foreign');
			$table->dropForeign('camposadicionales_tipodoc_tipo_doc_foreign');
		});
	}

}
