<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPublicacionesSeriadasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('publicaciones_seriadas', function(Blueprint $table)
		{
			$table->foreign('cod_editor')->references('id')->on('editoriales')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('tipo_liter')->references('id')->on('tipo_docs')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign(array('recurso_id','ext_recurso_id'), 'publicaciones_seriadas_recurso_id_ext_recurso_id_foreign')->references(array('id','ext_id'))->on('recursos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('publicaciones_seriadas', function(Blueprint $table)
		{
			$table->dropForeign('publicaciones_seriadas_cod_editor_foreign');
			$table->dropForeign('publicaciones_seriadas_tipo_liter_foreign');
			$table->dropForeign('publicaciones_seriadas_recurso_id_ext_recurso_id_foreign');
		});
	}

}
