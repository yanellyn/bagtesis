<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipouspTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipousp', function(Blueprint $table)
		{
			$table->string('tipo_usp')->nullable();
			$table->string('descrip_usp')->nullable();
			$table->integer('nro_int')->nullable();
			$table->integer('nro_ext')->nullable();
			$table->integer('periodos_renov')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipousp');
	}

}
