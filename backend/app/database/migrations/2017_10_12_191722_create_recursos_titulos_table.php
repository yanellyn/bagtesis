<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecursosTitulosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recursos_titulos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('recurso_id')->index();
			$table->integer('ext_recurso_id');
			$table->integer('titulo_id')->nullable();
			$table->string('tipo_tit')->nullable()->index();
			$table->string('portada')->nullable();
			$table->integer('orden')->nullable();
			$table->string('tipo_doc')->nullable()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recursos_titulos');
	}

}
