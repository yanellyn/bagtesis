<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConferenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conferencias', function(Blueprint $table)
		{
			$table->foreign('pais_id')->references('id')->on('paises')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conferencias', function(Blueprint $table)
		{
			$table->dropForeign('conferencias_pais_id_foreign');
		});
	}

}
