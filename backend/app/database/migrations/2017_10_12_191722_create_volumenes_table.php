<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVolumenesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('volumenes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('recurso_id')->nullable();
			$table->integer('ext_recurso_id')->nullable();
			$table->string('volumen')->nullable();
			$table->string('subtitulo')->nullable();
			$table->string('isbn')->nullable();
			$table->string('ano')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('volumenes');
	}

}
