<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTipotituloTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tipotitulo', function(Blueprint $table)
		{
			$table->foreign('tipo_doc')->references('id')->on('tipo_docs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tipotitulo', function(Blueprint $table)
		{
			$table->dropForeign('tipotitulo_tipo_doc_foreign');
		});
	}

}
