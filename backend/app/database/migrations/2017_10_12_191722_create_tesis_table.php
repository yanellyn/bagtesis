<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTesisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tesis', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('recurso_id')->index();
			$table->integer('ext_recurso_id');
			$table->integer('cod_inst')->nullable();
			$table->string('grado_acad')->nullable()->index();
			$table->integer('grado_academico_id')->nullable()->index();
			$table->integer('nivel_id')->nullable();
			$table->integer('trabajo_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tesis');
	}

}
