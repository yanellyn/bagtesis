<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAutoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('autores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('tipo_autor')->nullable()->index();
			$table->string('autor')->nullable();
			$table->string('cod_dir')->nullable();
			$table->string('dir_electr')->nullable();
			$table->string('fecha_nac_dec')->nullable();
			$table->string('url')->nullable();
			$table->string('autor_salida')->nullable()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('autores');
	}

}
