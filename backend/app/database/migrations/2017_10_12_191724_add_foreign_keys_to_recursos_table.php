<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecursosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recursos', function(Blueprint $table)
		{
			$table->foreign('id_catalogador')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('tipo_liter')->references('id')->on('tipo_docs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recursos', function(Blueprint $table)
		{
			$table->dropForeign('recursos_id_catalogador_foreign');
			$table->dropForeign('recursos_tipo_liter_foreign');
		});
	}

}
