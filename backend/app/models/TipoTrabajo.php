<?php

class TipoTrabajo extends \Eloquent {
	protected $fillable = [];
	protected $table = 'tipo_trabajo';
	protected $primaryKey = 'id';
	public $timestamps = false;
}