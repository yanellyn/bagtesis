<?php

class TipoAutor extends \Eloquent {
	protected $fillable = [];
	public $timestamps = false;
	protected $table = 'tipo_autores';
	protected $primaryKey = ['tipo_doc', 'tipo_autor'];
	public $incrementing = false;
}