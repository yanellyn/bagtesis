<?php

class PublicacionesSeriada extends \Eloquent {
	protected $fillable = [];
	public $timestamps = false;
	protected $table = 'publicaciones_seriadas';
	protected $primaryKey = ['recurso_id', 'ext_recurso_id'];
	public $incrementing = false;

}