<?php

class RecursosSoportes extends \Eloquent {
	protected $fillable = [];
	protected $table = 'recursos_soportes';
	protected $primaryKey = 'id';
	public $timestamps = false;
}