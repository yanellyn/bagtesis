<?php

class CampoAdicionalTipoDoc extends \Eloquent {
	protected $fillable = [];
	protected $table = 'camposadicionales_tipodoc';
	public $timestamps = false;
	protected $primaryKey = ['campo_adicional', 'tipo_doc'];
	public $incrementing = false;

}