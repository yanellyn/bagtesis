<?php

class RecursosConferencia extends \Eloquent {
	protected $fillable = [];
	protected $table = 'recursos_conferencias';
	protected $primaryKey = 'id';
	public $timestamps = false;
}