<?php

class Recurso extends \Eloquent {
	protected $fillable = [];
	public $timestamps = false;
//	protected $attributes = array('id' => 'ubicacion');

	public function ejemplares()
	{
			return $this->hasMany('Ejemplar','recurso_id');
	}

	public function tesis()
	{
			return $this->hasOne('Tesis','recurso_id');
	}

/*	public function autores()
    {
        return $this->belongsToMany('Autor', 'codautor', 'id','cod_autor');
    }
*/

    public function editorial()
    {
    	return $this->belongsToMany('Editorial','recursos_editoriales','recurso_id','editorial_id');
    }

    public function titulos()
    {
    	return $this->belongsToMany('Titulo','recursos_titulos','recurso_id','titulo_id')->withPivot('tipo_tit');
    }



    public function tipo_documento()
    {
		return $this->belongsTo('TipoDoc','tipo_liter');
//    	return $this->belongsTo('TipoDoc','tipo_liter');

	}


	public function descriptores()
	{
		//return $this->belongsToMany('Autor', 'codautor', 'id','cod_autor');
		$recursos_descriptores = $this->belongsToMany('Descriptor', 'recursos_descriptores', 'recurso_id','descriptor_id')->withPivot('numero','camp_cepal');
	//	return $recursos_descriptores->hasMany('Tipoedesc','tipo');

		return $recursos_descriptores;
	}

	public static function extend_descriptores($id)
	{


	$recursos_descriptores =	DB::table('recursos_descriptores')
						            ->join('recursos', 'recursos.id', '=', 'recursos_descriptores.recurso_id')
						            ->join('descriptores', 'descriptores.id', '=', 'recursos_descriptores.descriptor_id')
						        	->join('tipo_descriptores', 'descriptores.tipo', '=', 'tipo_descriptores.id')
						            ->where('recursos.id','=',$id)
						            ->select('recursos.id','descriptores.tipo','tipo_descriptores.descrip_desc','descriptores.descriptor','descriptores.descriptor_salida')
						            ->get();

		return $recursos_descriptores;			            
	}


	public function autores()
	{
		//return $this->belongsToMany('Autor', 'codautor', 'id','cod_autor');
		$recursos_autores = $this->belongsToMany('Autor', 'recursos_autores', 'recurso_id','cod_autor');
	//	return $recursos_descriptores->hasMany('Tipoedesc','tipo');

		return $recursos_autores;
	}


	public static function extend_autores($id){



//		$recursos_autores =	//DB::table('recursos_autores')
						         //     ->join('recursos', 'recursos.id', '=', 'recursos_autores.recurso_id')
						        //    ->join('autores','autores.id','=','recursos_autores.cod_autor')

						         //   ->join('tipo_autores','tipo_autores.tipo_doc','=','recursos_autores.tipo_doc')
						    	  // 	->join('tipo_autores','tipo_autores.tipo_autor','=','recursos_autores.tipo_autor')
						      /*        ->join('tipo_autores', function($join){
										  $join->on('recursos_autores.tipo_doc', '=', 'tipo_autores.tipo_doc')->orOn(
										  			'recursos_autores.tipo_autor', '=', 'tipo_autores.tipo_autor'

																												);
										  })


							*/
						        //    ->where('recursos.id','=',$id)


									//->select(DB::raw('recursos.id'))
								//->select('recursos.id')
							/*	->whereRaw("recursos.id =  '".$id."'")
				    	 			->whereRaw('recursos_autores.cod_autor = autores.id')
				    	 			->whereRaw('recursos_autores.tipo_doc = tipo_autores.tipo_doc')
				    	 			->whereRaw('recursos_autores.tipo_autor = tipo_autores.tipo_autor')
						           ->select('recursos.id')
						            ->get();
							*/
						   $results = DB::select(DB::raw("SELECT "." r.id, r.ubicacion, ra.cod_autor, ra.tipo_autor, ta.nombre, a.autor  "."FROM recursos r, recursos_autores ra, autores a, tipo_autores ta

						 								   WHERE "."   ra.recurso_id = '".$id."'  AND ra.recurso_id = r.id   AND  ra.cod_autor = a.id  AND ra.tipo_doc = ta.tipo_doc AND ra.tipo_autor = ta.tipo_autor")); 

						 					
						 //   ) );


  /*->join('tipo_autores', function($join){
						  $join->on('recursos_autores.tipo_doc', '=', 'tipo_autores.tipo_doc')->orOn(
						  			'recursos_autores.tipo_autor', '=', 'tipo_autores.tipo_autor'

																								);
						  })
  ->get(); */


//recursos, recursos_autores, autores, tipo_autores, 


//AND   ca.acceso = r.acceso
//AND   ca.cod_autor = a.cod_autor
//AND   ca.tipo_doc = ta.tipo_doc 
//AND   ca.tipo_autor = ta.tipo_autor;
		//return $recursos_autores;		
  		return $results;

	}



	
}