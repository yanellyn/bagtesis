<?php

class Prestamo extends \Eloquent {
	protected $fillable = [];
	protected $table = 'prestamo';
	public $timestamps = false;
	protected $primaryKey = 'key_column';

	public function ejemplar(){
		return $this->belongsTo('Ejemplar','id_ejemplar');
	}

}