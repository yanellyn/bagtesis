<?php

class TipoTitulo extends \Eloquent {
	protected $fillable = [];
	public $timestamps = false;

	/*Nombre de la tabla*/
	protected $table = 'tipotitulo';
	protected $primaryKey = ['tipo_doc', 'tipo_titulo'];
	public $incrementing = false;
}