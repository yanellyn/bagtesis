<?php

class Ejemplar extends \Eloquent {
	protected $fillable = [];
	public $timestamps = false;
	/*Nombre de la tabla*/
	protected $table = 'ejemplares';

	public function recurso(){
		return $this->belongsTo('Recurso','id');
	}

}