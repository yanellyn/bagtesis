<?php

class Tesis extends \Eloquent {
	protected $fillable = [];
	public $timestamps = false;
	protected $table = 'tesis';

	public function recursos()
	{
			return $this->hasOne('Recurso','id');
	}
}