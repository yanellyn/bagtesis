<?php

class RecursosDisciplinas extends \Eloquent {
	protected $fillable = [];
	protected $table = 'recursos_disciplinas';
	protected $primaryKey = 'id';
	public $timestamps = false;
}