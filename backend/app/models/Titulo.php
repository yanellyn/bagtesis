<?php

class Titulo extends \Eloquent {
	protected $fillable = [];
	public $timestamps = false;

	public function recursos(){

		$recursos_titulos = $this->belongsToMany('Recurso','recursos_titulos','titulo_id','recurso_id')->withPivot('titulo_id');


		return $recursos_titulos;

	}


	public function service_titulos_tipo_doc($tipo_doc){
		$recursos_titulos_tipo_doc=	DB::table('recursos_titulos')
					            ->join('titulos', 'titulos.id', '=', 'recursos_titulos.titulo_id')				 
					            ->where('recursos_titulos.tipo_doc','=',$tipo_doc)
					            ->where('recursos_titulos.tipo_tit','=','OP')
					            ->groupBy('titulos.id')		
					            ->orderBy('titulos.id')
					            ->select('titulos.id','titulos.titulo','titulos.titulo_salida',DB::raw('count(titulos.id) as num_recursos'))
					            ->paginate();
			return $recursos_titulos_tipo_doc;
	}
}