<?php
class Autor extends \Eloquent {
	protected $fillable = [];
	protected $table = 'autores';
	public $timestamps = false;

	public function recursos()
    {
        return $this->belongsToMany('Recurso', 'recursos_autores', 'cod_autor', 'recurso_id');
    }
}