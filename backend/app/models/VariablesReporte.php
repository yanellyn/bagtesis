<?php

class VariablesReporte extends \Eloquent {
	protected $fillable = [];
	protected $table = 'variables_reportes';
	protected $primaryKey = 'reporte_id';
	public $timestamps = false;

	public static function incrementar_variable($var_name)
	{
		$fecha_origen = date("d-m-y");
		$var = VariablesReporte::where('tipo', '=', $var_name)
							   ->where('nombre', '=',$fecha_origen)->get()->first();

		if(empty($var)){
			$var_reporte = new VariablesReporte;
			$aux_rep =  VariablesReporte::whereRaw("reporte_id = (select max(reporte_id) from variables_reportes)")->get()->first();
			$var_reporte->reporte_id = $aux_rep['reporte_id'] + 1;
			$var_reporte->variable_id = 1;
			$var_reporte->tipo = $var_name;
			$var_reporte->nombre = $fecha_origen;
			$var_reporte->valor = 1;
			$var_reporte->valor_binario = null;
			$var_reporte->save();
		}else{
			$var->valor = $var->valor + 1;
			$var->save();
		}
		return true;

	}


}