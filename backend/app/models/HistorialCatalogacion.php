<?php

class HistorialCatalogacion extends \Eloquent {
	protected $fillable = [];
	protected $table = 'historial_catalogacion';
	protected $primaryKey = 'id';
	public $timestamps = false;
}