<?php

class RecursoDescriptor extends \Eloquent {
	protected $fillable = [];
	public $timestamps = false;
	protected $table = 'recursos_descriptores';


	public function descriptores()
	{
		return $this->belongsToMany('Recurso', 'recursos_descriptores', 'recurso_id','descriptor_id');
	}

}