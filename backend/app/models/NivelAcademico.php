<?php

class NivelAcademico extends \Eloquent {
	protected $fillable = [];
	protected $table = 'niveles_academicos';
	protected $primaryKey = 'id';
	public $timestamps = false;
}