<?php

class Conferencia extends \Eloquent {
	protected $fillable = [];
	protected $table = 'conferencias';
	protected $primaryKey = 'id';
	public $timestamps = false;
}