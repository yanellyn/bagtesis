<?php

class Soporte extends \Eloquent {
	protected $fillable = [];
	protected $table = 'soportes';
	protected $primaryKey = 'id';
	public $timestamps = false;
}