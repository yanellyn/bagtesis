<?php

class Volumen extends \Eloquent {
	protected $fillable = [];
	protected $table = 'volumenes';
	protected $primaryKey = 'id';
	public $timestamps = false;
}