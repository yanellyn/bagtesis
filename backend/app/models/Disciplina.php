<?php

class Disciplina extends \Eloquent {
	protected $fillable = [];
	protected $table = 'disciplinas';
	protected $primaryKey = 'id';
	public $timestamps = false;
}