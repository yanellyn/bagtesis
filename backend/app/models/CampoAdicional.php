<?php

class CampoAdicional extends \Eloquent {
	protected $fillable = [];
	protected $table = 'campos_adicionales';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
}