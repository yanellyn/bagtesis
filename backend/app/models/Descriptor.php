<?php

class Descriptor extends \Eloquent {
	protected $fillable = [];
	protected $table = 'descriptores';
	protected $primaryKey = 'id';
	public $timestamps = false;
}