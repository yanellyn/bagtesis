<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, X-Auth-Token");

Route::get('/', function()
{
	return View::make('hello');
});

Route::resource('paises','PaisesController');
Route::resource('usuarios','UsuariosController');
Route::resource('recursos','RecursosController');
Route::resource('recursos_titulos','RecursosTitulosController');
Route::resource('ejemplares','EjemplaresController');
Route::resource('tipo_titulos','TipoTitulosController');
Route::resource('tesis','TesisController');
Route::get('teses','TesesController');
Route::resource('libros','LibrosController');
Route::resource('editoriales','EditorialesController');
Route::resource('recursos_editoriales','RecursosEditorialesController');
Route::resource('tipo_editoriales','TipoEditorialesController');
Route::resource('tipo_docs','TipoDocsController');
Route::resource('prestamos','PrestamosController');
Route::resource('tipodescs','TipodescsController');
Route::resource('descriptores','DescriptoresController');
Route::resource('recursos_descriptores','RecursosDescriptoresController');
Route::resource('variables_reportes','VariablesReportesController');
Route::resource('categorias_usuarios','CategInstController');
Route::resource('datos_reportes','DatosReportesController');
Route::resource('publicaciones_seriadas','PublicacionesSeriadasController');
Route::resource('tipo_autores','TipoAutoresController');
Route::resource('recursos_autores','RecursosAutoresController');
Route::resource('buscadores','BuscadoresController');
Route::resource('dependencias','DependenciasController');
Route::resource('escuelas','EscuelasController');
Route::resource('bibliotecas','BibliotecasController');
Route::resource('dependencias','DependenciasController');
Route::resource('recursos_dependencias','RecursosDependenciasController');
Route::resource('recursos_bibliotecas','RecursosBibliotecasController');
Route::resource('grados_academicos','GradosAcademicosController');
/* Pruebas Prestamo */


Route::resource('ejemplaress', 'EjemplaresController');
Route::get('v_prestamo', 'PrestamosController@test_view');
Route::post('v_prestamo', 'PrestamosController@test_send');
Route::post('consulta_ci', 'PrestamosController@consulta_ci');
Route::post('consulta_devolucion', 'PrestamosController@consulta_devolucion');
Route::post('consulta_suspension', 'PrestamosController@consulta_suspension');
Route::post('consulta_prestamos', 'PrestamosController@consulta_prestamos');
Route::post('prestamo_info', array('before' => 'islogged','uses' => 'PrestamosController@prestamo_info'));
Route::post('prestamos',  array('before' => 'isadmin','uses' => 'PrestamosController@store'));
Route::post('prestamos/renovar',  array('before' => 'isadmin','uses' => 'PrestamosController@renovar_prestamos'));
Route::post('prestamos/devolver',  array('before' => 'isadmin','uses' => 'PrestamosController@devolver_prestamos'));
Route::get('prestamos/{id_ejemplar}/{id}/renovar',  array('before' => 'isadmin','uses' => 'PrestamosController@renovar_prestamo'));
Route::get('prestamos/{id_ejemplar}/{id}/{id_admin}/devolver',  array('before' => 'isadmin','uses' => 'PrestamosController@devolver_prestamo'));
Route::post('prestamos/todos',  'PrestamosController@prestamos');


/* API ejemplares*/
Route::get('ejemplar/{id}/cantidad',  'RecursosController@cantidad');
Route::get('ejemplar/{id}/cantidad_disponible',  'RecursosController@cantidad_disponible');

/*CRON JOBS*/
Route::get('cron_job', 'UsuariosController@cron_job_deuda');

/* API variablesReportes*/
Route::post('variables_reportes',  array('before' => 'isadmin','uses' => 'VariablesReportesController@store'));
Route::post('reporte_prestamos_pdf',  array('before' => 'isadmin','uses' => 'VariablesReportesController@reporte_prestamos_pdf'));
Route::get('reporte_prestamos/{fecha_origen_}/{fecha_destino_}',  'VariablesReportesController@reporte_prestamos_pdf_2');
Route::get('reporte_prestamos_chart/{img1}',  'VariablesReportesController@reporte_prestamos_pdf_chart');
Route::post('reporte_prestamos_chart_generar',  'VariablesReportesController@reporte_prestamos_pdf_chart_generar');


/*Api Preferencias*/
Route::post('preferencias',  array('before' => 'islogged','uses' => 'PreferenciasController@store'));
Route::post('consulta_pref', 'PreferenciasController@consulta_pref');
Route::post('preferencias/remove', 'PreferenciasController@remove_prefs');
Route::get('preferencias/{recurso_id}/{id}/delete', 'PreferenciasController@destroy');

/* Api Usuarios */

Route::post('login', 'UsuariosController@doLogin');
Route::get('logout', 'UsuariosController@doLogout');
Route::post('contacto', 'UsuariosController@contacto');
//Route::post('get_ci_by_id', 'UsuariosController@get_ci_by_id');
Route::get('server_date',function(){
	date_default_timezone_set('America/Caracas');
	return json_encode(date('Y/m/d H:i:s'));
});




/* Api Titulos */ 
Route::resource('titulos','TitulosController');
Route::get('obtenertitulos','TitulosController@obtenerTitulos');
Route::get('titulos/tipo_doc/{tipo_doc}','TitulosController@titulos_tipo_doc');
Route::get('titulos/letra/{letra}','TitulosController@titulos_letra'); 
Route::get('titulos/tipo_doc/{tipo_doc}/letra/{letra}','TitulosController@titulos_tipo_doc_letra');
Route::get('recursos/titulos/{id}','TitulosController@recursos_titulo');



Route::post('titulos/eliminar_titulo_recurso','TitulosController@eliminar_titulo_recurso'); // Eliminar titulo a recurso
Route::post('titulos/modificar_titulo_recurso','TitulosController@modificar_titulo_recurso'); // Modificar titulo a recurso
Route::post('titulos/agregar_titulo_recurso','TitulosController@agregar_titulo_recurso'); // Modificar titulo a recurso
			

/* Api Autores */ 

Route::resource('autores','AutoresController');
Route::get('obtenerautores','AutoresController@obtenerAutores');
Route::get('autores/letra/{letra}','AutoresController@autores_letra'); // Autores con el conteo de todos los tipos de recursos 
Route::get('autores/tipo_doc/{tipo_doc}','AutoresController@autores_tipo_doc');
Route::get('autores/tipo_doc/{tipo_doc}/letra/{letra}','AutoresController@autores_tipo_doc_letra');
Route::get('autores/{id}/recursos/tipo_doc/{tipo_doc}','AutoresController@autor_recursos_tipo_doc');

Route::post('autores/obtener_autores','AutoresController@buscar_autores_recurso'); //Obtener autores de un recurso

Route::post('autores/agregar_autor_recurso','AutoresController@agregar_autor_recurso'); // Agregar autor a recurso

Route::post('autores/eliminar_autor_recurso','AutoresController@eliminar_autor_recurso'); // Eliminar autor a recurso

Route::post('autores/modificar_autor_recurso','AutoresController@modificar_autor_recurso'); // Modificar autor a recurso


/* Api recursos */

Route::resource('recursos/{id}/titulos','RecursosController@recurso_titulos');
Route::resource('recursos/{id}/autores',  'RecursosController@recurso_autores');
Route::get('recursos/{id}/descriptores','RecursosController@recurso_descriptores');
Route::get('recursos/{id}/pdf','RecursosController@recurso_pdf');
Route::post('recursos/pdf','RecursosController@recursos_pdf');
Route::post('recursos/recursos_info','RecursosController@recursos_info');
Route::get('recursos/{id}/bibliotecas','RecursosController@recurso_bibliotecas');
Route::get('recursos/{id}/dependencias','RecursosController@recurso_dependencias');
Route::get('recursos/{id}/{ejemplar}/modal_info', 'RecursosController@modal_info');
Route::resource('recursos/{id}/editoriales','RecursosController@recurso_editoriales');
Route::resource('recursos/{id}/grado_academico','RecursosController@recurso_grado_academico');
/* Api mailer */

Route::get('recursos/{id}/mail','RecursosController@mail');
Route::post('recursos/mail','RecursosController@recursos_mail');
Route::post('recursos/mail2','RecursosController@recursos_mail2');



/* Mailer Routes */

Route::post('recover_password_post', 'UsuariosController@recover_password');
Route::get('recover_password', 'UsuariosController@recover_password_view');


/* API buscador  */

Route::post('buscadores/buscador_titulo_autocompletado_typeahead','BuscadoresController@autocompletado_titulo_typeahead');
Route::post('buscadores/buscador_editorial_autocompletado_typeahead','BuscadoresController@autocompletado_editorial_typeahead');
Route::post('buscadores/buscador_autor_autocompletado_typeahead','BuscadoresController@autocompletado_autor_typeahead');



Route::post('buscadores/buscador_typeahead','BuscadoresController@buscador_typeahead');
Route::post('buscadores/buscador_titulo_typeahead','BuscadoresController@buscador_titulo_typeahead');
Route::post('buscadores/buscador_autor_typeahead','BuscadoresController@buscador_autor_typeahead');
Route::post('buscadores/buscador_jurado_typeahead','BuscadoresController@buscador_autor_typeahead');
Route::post('buscadores/buscador_tutor_academico_typeahead','BuscadoresController@buscador_tutor_academico_typeahead');
Route::post('buscadores/buscador_cota_typeahead','BuscadoresController@buscador_cota_typeahead');
Route::post('buscadores/buscador_materia_typeahead','BuscadoresController@buscador_materia_typeahead');
Route::post('buscadores/buscador_palabra_clave_typeahead','BuscadoresController@buscador_materia_typeahead');
Route::post('buscadores/buscador_tipo_de_trabajo_typeahead','BuscadoresController@buscador_tipo_de_trabajo_typeahead');
Route::post('buscadores/buscador_volumen_typeahead','BuscadoresController@buscador_volumen_typeahead');
Route::post('buscadores/buscador_edicion_typeahead','BuscadoresController@buscador_edicion_typeahead');
Route::post('buscadores/buscador_idioma_typeahead','BuscadoresController@buscador_idioma_typeahead');
Route::post('buscadores/buscador_isbn_typeahead','BuscadoresController@buscador_isbn_typeahead');
Route::post('buscadores/buscador_issn_typeahead','BuscadoresController@buscador_issn_typeahead');
Route::post('buscadores/buscador_editorial_typeahead','BuscadoresController@buscador_editorial_typeahead');
Route::post('buscadores/buscador_simple','BuscadoresController@buscador_simple');
Route::post('buscadores/buscador_avanzado','BuscadoresController@buscador_avanzado');
Route::get('buscadores/buscador_google/{id}', 'BuscadoresController@buscador_google');

Route::get('/create_new_resource','RecursosController@create_new_resource');
Route::get('/hash','BuscadoresController@hash'); 
Route::get('/order','BuscadoresController@order');


//////**************CRUD MONOGRAFIAS**************/
Route::post('monografias/agregar_libro','LibrosController@agregar_libro');
Route::post('monografias/guardar_libro','LibrosController@guardar_libro');
Route::post('monografias/eliminar_libro','LibrosController@eliminar_libro');
Route::post('buscadores/buscador_simple2','BuscadoresController@buscador_simple2');


//////**************CRUD Publicaciones**************/
Route::post('publicaciones/agregar_publicacion','PublicacionesSeriadasController@agregar_publicacion');
Route::post('publicaciones/guardar_publicacion','PublicacionesSeriadasController@guardar_publicacion');
Route::post('publicaciones/eliminar_publicacion','PublicacionesSeriadasController@eliminar_publicacion');
Route::post('buscadores/buscador_simple2','BuscadoresController@buscador_simple2');

/************* API EDITORIALES ******************/

Route::post('obtener_editoriales_recurso','EditorialesController@obtener_editoriales_recurso');
Route::get('obtener_editoriales_sistema','EditorialesController@obtener_editoriales_sistema');
Route::post('guardar_editoriales','EditorialesController@guardar_editoriales');
Route::post('agregar_editoriales','EditorialesController@agregar_editoriales');
Route::post('eliminar_editoriales','EditorialesController@eliminar_editoriales');
Route::post('guardar_editoriales_sistema','EditorialesController@guardar_editoriales_sistema');

/************* API DESCRIPTORES ******************/

Route::get('tipo_descriptores','TipodescsController@tipo_descriptores');
Route::get('descriptores_recurso','DescriptoresController@descriptores_recurso');
Route::post('eliminar_descriptores_recurso','DescriptoresController@eliminar_descriptores_recurso');
Route::get('obtener_descriptores_sistema','DescriptoresController@obtener_descriptores_sistema');
Route::get('obtener_descriptores_sistema/{tipo}','DescriptoresController@obtener_descriptores_sistema');
Route::get('obtener_descriptores_sistema/{tipo}/{busqueda}','DescriptoresController@obtener_descriptores_sistema');
Route::post('agregar_descriptor','DescriptoresController@agregar_descriptor');
Route::post('guardar_descriptores_recurso','DescriptoresController@guardar_descriptores_recurso');
Route::post('autocompletado_descriptor_typeahead','DescriptoresController@autocompletado_descriptor_typeahead'); 


/*************************************************/

/*************API CONFERENCIAS ***********************/
Route::resource('conferencias', 'ConferenciasController');
Route::get('eliminar_conferencia/{id}', 'ConferenciasController@destroy');
Route::post('guardar_conferencia/{id}', 'ConferenciasController@update');
Route::get('obtener_conferencias', 'ConferenciasController@conferencias_sistema');
Route::post('buscadores/autocompletado_conferencia_typeahead','BuscadoresController@autocompletado_conferencia_typeahead');



/**********API EJEMPLARES *************************************/
Route::post('ejemplares/obtener_ejemplares',  'EjemplaresController@obtener_ejemplares');
Route::get('ejemplares_tipoestado/obtener_estados_ejemplares',  'EjemplaresController@obtener_estados_ejemplares');
Route::post('ejemplares/agregar_ejemplar',  'EjemplaresController@agregar_ejemplar');
Route::post('ejemplares/modificar_ejemplar',  'EjemplaresController@modificar_ejemplar');
Route::post('ejemplares/eliminar_ejemplar',  'EjemplaresController@eliminar_ejemplar');


/**********API SOPORTE *************************************/
Route::get('obtener_inf_descriptiva_sistema','SoportesController@obtener_inf_descriptiva_sistema');
Route::get('obtener_inf_descriptiva_recurso/{id}','SoportesController@obtener_inf_descriptiva_recurso');
Route::post('agregar_soporte','SoportesController@agregar_soporte');
Route::post('guardar_soporte','SoportesController@guardar_soporte');
Route::post('eliminar_soporte','SoportesController@eliminar_soporte');
Route::post('autocompletado_soporte','SoportesController@buscadorSoporteTypeahead');


/************API INSTITUCIONES******************/
Route::resource('instituciones','InstitucionesController');
Route::post('guardar_institucion/{id}', 'InstitucionesController@update');
Route::get('eliminar_institucion/{id}', 'InstitucionesController@destroy');
Route::get('buscar_institucion', 'InstitucionesController@buscarInstitucion');
Route::post('autocompletado_institucion', 'InstitucionesController@autocompletado_institucion');

/***********API TESIS***************/

Route::get('grados_academicos','TesisController@obtener_grados_academicos');
Route::post('agregar_tesis','TesisController@agregar_tesis');
Route::post('guardar_tesis','TesisController@guardar_tesis');
Route::post('eliminar_tesis','TesisController@eliminar_tesis');
//Route::post('monografias/guardar_libro','LibrosController@guardar_libro');
//Route::post('monografias/eliminar_libro','LibrosController@eliminar_libro');
//Route::post('buscadores/buscador_simple2','BuscadoresController@buscador_simple2');
//Utilizado para permitir peticiones PUT Y DELETE. Pero se tiene comentado dado que se manejo en app/filters.php
//header('Access-Control-Allow-Origin:  *');
//header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE'); 
//header('Access-Control-Allow-Headers:  Origin, X-Requested-With, Content-Type, Accept, X-Auth-Token');


//Utilizado para Importar un archivo xlsx
//Route::post('importar','RecursosController@importar');
Route::post('importar/{id}','RecursosController@importar');
Route::post('cargar_recurso/{idejemplar}/{idsuario}','RecursosController@cargar_recurso');
Route::get('eliminar_carga_recurso/{idejemplar}/{nombrearchivo}','RecursosController@eliminar_carga_recurso');



//Utilizado para Exportar un archivo xlsx
Route::post('exportar','RecursosController@exportarRecursos');

//dependencias

Route::post('autocompletado_dependencias', 'DependenciasController@autocompletado_dependencias');


/***********API REPORTES CATALOGACION***************/

Route::get('obtener_catalogadores','HistorialCatalogacionController@obtenerCatalogadores');
Route::get('obtener_salas','HistorialCatalogacionController@obtenerSalas');
Route::post('autocompletado_materia_tematica','HistorialCatalogacionController@buscadorMateriaTematicaTypeahead');
Route::post('generar_reporte_catalogacion','HistorialCatalogacionController@generarReporteCatalogacion');

/******************* API VOLUMENES *******************/

Route::resource('volumenes','VolumenesController');
Route::get('recurso_titulos_principal/{id}', 'RecursosController@recurso_titulos_principal');
Route::post('guardar_volumen/{id}', 'VolumenesController@update');


/******************* API VOLUMENES SERIES *******************/
Route::post('agregar_volumen_serie', 'VolumenesController@agregar_volumen_serie');
Route::post('agregar_numero_serie', 'VolumenesController@agregar_numero_serie');
Route::post('obtener_volumenes_numeros', 'VolumenesController@obtener_volumenes_numeros');
Route::post('eliminar_numero', 'VolumenesController@eliminar_numero');
Route::post('modificar_numero', 'VolumenesController@modificar_numero');
Route::post('editar_volumen', 'VolumenesController@editar_volumen');

/******************* API DISCIPLINAS *******************/
Route::post('autocompletado_disciplinas', 'DisciplinasController@autocompletado_disciplinas');

/******************* API NIVEL ACADEMICO *******************/
Route::resource('niveles_academicos','NivelesAcademicosController');

/******************* API tipo_trabajo *******************/
Route::resource('tipo_trabajo','TipoTrabajoController');


/******************* API grado_acad Tesis *******************/
Route::post('buscadores/autocompletado_grado_acad_tesis','BuscadoresController@autocompletado_grado_acad_tesis');

/*************API CAMPOS ADICIONALES ***********************/
Route::resource('campos_adicionales', 'CamposAdicionalesController');
Route::post('buscador_campos_typeahead', 'CamposAdicionalesController@buscador_campos_typeahead');
Route::post('obtener_campos_portipo', 'CamposAdicionalesController@obtener_campos_portipo');
Route::get('recurso_camposadicionales/{id}/{tipo_doc}', 'RecursosController@recurso_camposadicionales');



/*************API CAMPOS TIPO DOCS ***********************/
Route::resource('tipo_docs', 'TipoDocsController');
Route::get('tipo_docs_sinprincipales', 'TipoDocsController@tipo_docs_sinprincipales');
Route::post('guardar_tipodoc/{id}', 'TipoDocsController@update');

/***********API OTROS RECURSOS***************/
Route::post('agregar_otro_recurso','OtrosRecursosController@agregar_otro_recurso');
Route::post('guardar_otro_recurso','OtrosRecursosController@guardar_otro_recurso');
Route::post('eliminar_otro_recurso','OtrosRecursosController@eliminar_otro_recurso');


/*********** API CODIGO DE BARRA ****************/

Route::get('buscar_codigo_barra/{codigoBarra}','LibrosController@buscar_codigo_barra');