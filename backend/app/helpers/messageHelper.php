<?php

/* Status enumeration */
abstract class Status
{
    const error = 'error';
    const warning = 'warning';
    const success = 'success';
}

/* Status enumeration */
abstract class Actions
{
    const found = 'found';
    const stored = 'stored';
    const updated = 'updated';
    const deleted = 'deleted';
}

/* Message class helper */
class MessageHelper{

    /* */
    public static function getMessage($resource, $action, $status, $data = null)
    {
        $resource = empty($resource)?"resource":$resource;
        $requestStatus = array('message' => 'The '.$resource, 'action' => $action, 'status' => $status, 'data' => $data );

        switch ($status) {
            case Status::error:
                $requestStatus['message'] .= ' could not be '.$action;
                break;

            case Status::success:
                $requestStatus['message'] .= ' was '.$action;
                break;

            case Status::warning:

                break;

        }

        return $requestStatus;
    }
}
