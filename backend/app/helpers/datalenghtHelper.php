<?php

class DatalenghtHelper{

    /* Acces to Array select for model */
    public static function getArray($model, $data_lenght){
        $default="default";
        if($data_lenght==null){
            eval("\$array = \$model::$\$default;");
            return $array;
        }
        eval("\$array = \$model::$\$data_lenght;");
        return $array;
    }
}

/* Exception */
class DataLenghtException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {
        // some code

        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function customFunction() {
        echo "A custom function for this type of exception\n";
    }
}
