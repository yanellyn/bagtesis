<?php

abstract class Templates
{
    const welcome = "emails.welcome";
    const active_account = "emails.active_account";
    const envio_recurso = "emails.envio_recurso";
    const active_account_recurso = "emails.active_account_recurso";
    const prestamo_recursos = "emails.prestamo_recursos";
}


class EmailHelper{

    /* Subjects linked to templates */
    public static $subjects = array(
                                        'emails.welcome' => 'Welcome to CPI',
                                        'emails.active_account' => 'Welcome to CPI. Please activate your account',
                                        'emails.envio_recurso' => 'Módulo de Consulta OPAC de la Biblioteca Alonso Gamero - Facultad de Ciencias - Universidad Central de Venezuela UCV',
                                        'emails.active_account_recurso' => 'Bienvenido',
                                        'emails.prestamo_recursos' => 'Préstamos registrados exitosamente'  
                                    );


    /* Send mail using custom templates
     * @param  AsociativeArray  $teampleteData
     * @param  Template  $template
     * return  bool  email_sent */
    public static function sendMail($template,$teampleteData){

        $subject = EmailHelper::$subjects[$template];


        $mailStatus = Mail::send($template, $teampleteData, function($message) use ($teampleteData, $template, $subject)
        {
        /*    if($teampleteData == null)
                throw new Exception("teampleteData is null");

            if (!isset($teampleteData["user"]))
                throw new Exception("User data not found");

            if (!isset($teampleteData["user"]->email))
                throw new Exception("Email is not set in user");

            if (!isset($teampleteData["user"]->name))
                $user->name = $teampleteData["user"]->name;

            */    
           // $message->to('alvaropaz20@gmail.com', 'Alvaro Paz')->subject( $subject );
     //   $message->to('alvaropaz20@gmail.com', 'Alvaro Paz')->subject( $subject );

          foreach ($teampleteData['user']->correos as $correo) {
      //       $message->to($teampleteData['user']->correos)->subject( $subject );
            $message->to($correo['email'], $correo['name'])->subject( $subject );
          }
        });



       // return array('to' => $teampleteData["user"]->email, 'template' => $template, 'sent' => 1, 'message' => 'Mail sent');

    }
}
