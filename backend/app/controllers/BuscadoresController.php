<?php

/*include(app_path().'/helpers/emailHelper.php'); */
/*include(app_path().'/helpers/MessageHelper.php');*/
/*include(app_path().'/helpers/datalenghtHelper.php');*/

abstract class  Template{

	static $Template_sub_query = array(
		'autor'  =>  "SELECT ra.recurso_id FROM autores a, recursos_autores ra WHERE (ra.tipo_autor='P' OR ra.tipo_autor = 'S')  AND a.id = ra.cod_autor AND a.autor",
		'tutor_academico' => "SELECT ra.recurso_id FROM autores a, recursos_autores ra WHERE ra.tipo_autor='TA' AND a.id = ra.cod_autor AND a.autor",	
		'jurado' => "SELECT  ra.recurso_id FROM autores a, recursos_autores ra WHERE ra.tipo_autor='J' AND a.id = ra.cod_autor AND a.autor",
		'titulo' => "SELECT  rt.recurso_id FROM titulos t, recursos_titulos rt WHERE rt.tipo_tit='OP' AND t.id = rt.titulo_id AND t.titulo",
		'volumen' => "SELECT r.id FROM recursos r WHERE r.volumen ",
		'materia' => "SELECT rd.recurso_id FROM descriptores d,tipo_descriptores td,  recursos_descriptores rd WHERE (td.id like '%M-650%' OR td.id like '%M%') AND d.id = rd.descriptor_id AND td.id = d.tipo AND  d.descriptor",
		'isbn'    => "SELECT l.recurso_id FROM libros l WHERE l.isbn",
		'issn'    => "SELECT ps.recurso_id FROM publicaciones_seriadas ps WHERE ps.issn",
		'editorial' => "SELECT  re.recurso_id FROM editoriales e, recursos_editoriales re WHERE e.id = re.editorial_id AND e.editorial ",
		'idioma' => "SELECT rd.recurso_id FROM descriptores d,tipo_descriptores td,  recursos_descriptores rd WHERE (td.id like '%I%') AND d.id = rd.descriptor_id AND td.id = d.tipo AND  d.descriptor",
 		'palabra_clave' => "SELECT rd.recurso_id FROM descriptores d,tipo_descriptores td,  recursos_descriptores rd WHERE (td.id like '%M-650%' OR td.id like '%M%') AND d.id = rd.descriptor_id AND td.id = d.tipo AND  d.descriptor",
 		'cota' => "SELECT r.id FROM recursos r WHERE r.ubicacion",
 		'edicion' => "SELECT r.id FROM recursos r WHERE r.edicion",
 		'tipo_de_trabajo' => "SELECT r.id FROM recursos r, tesis t, tipo_trabajo tt WHERE r.id = t.recurso_id AND t.trabajo_id = tt.id AND tt.trabajo ",
 		'recursos_dependencias' => "SELECT rdep.recurso_id FROM recursos_dependencias rdep WHERE rdep.dependencia_id = ",
 		'recursos_bibliotecas'  => "SELECT rb.recurso_id FROM recursos_bibliotecas rb WHERE rb.biblioteca_id =",
 		'anios' =>  "SELECT r.id FROM recursos r WHERE "
 	);
}


abstract class Templateee{
	const titulo = "titulos t, recursos_titulos";
}


class BuscadoresController extends \BaseController {

	public $split_data = [];
	public $A = array();
	/**
	 * Display a listing of the resource.
	 * GET /buscadores
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return 'Exitos';
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /buscadores/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /buscadores
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /buscadores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		return $id; 
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /buscadores/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /buscadores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /buscadores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function convetir_string($data){
		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
  		$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'Ñ', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
  		return str_replace($a, $b, mb_strtoupper(trim($data)));
	}

	public function buscador_typeahead(){
		$data = Input::all();
		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_titulos = [];		  

		/*$busqueda_titulos = Titulo::select('titulo_salida',DB::raw('count(titulo_salida) as num_recursos'))
									->where('titulo','=',$aux_data)
								//	->orderBy('titulo','DESC')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->groupBy('titulo_salida')
								//	->select('titulo_salida', DB::raw('count(titulo_salida) as num_recursos'))
									
									->paginate(15);

		$datos_busqueda = array_merge($datos_busqueda,$busqueda_titulos->lists('titulo_salida'));*/
		if(Input::get('tipo_recurso')){
			$tipo_recurso  = Input::get('tipo_recurso');
			if($tipo_recurso == 'tesis'){
				$tipo_recurso = 'T';
			}else{
				if($tipo_recurso == 'monografia'){
					$tipo_recurso = 'M';
				}else{
					if($tipo_recurso == 'publicacion_seriada'){
						$tipo_recurso = 'S';
					}
				}
			}
		}else{
			$tipo_recurso = "";
		}


		$busqueda_titulos = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_tit','=','OP')
									->where('titulos.titulo','=',$aux_data)
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('titulos.titulo_salida')
									->select('titulos.titulo_salida', DB::raw('count(titulos.titulo_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('titulo_salida')));
		

		$busqueda_titulos = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_tit','=','OP')
									->where('titulos.titulo','like',$aux_data.'%')
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('titulos.titulo_salida')
									->select('titulos.titulo_salida', DB::raw('count(titulos.titulo_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('titulo_salida')));




		$busqueda_titulos = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_tit','=','OP')
									->where('titulos.titulo','like','%'.$aux_data.'%')
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('titulos.titulo_salida')
									->select('titulos.titulo_salida', DB::raw('count(titulos.titulo_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('titulo_salida')));

		$busqueda_titulos = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_tit','=','OP')
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('titulos.titulo', 'like', '%'.$aux.'%');
										}

									})
								//	->where('titulos.titulo','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('titulos.titulo_salida')
									->select('titulos.titulo_salida', DB::raw('count(titulos.titulo_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('titulo_salida')));



		/*$busqueda_autores = Autor::select('autor_salida',DB::raw('count(autor_salida) as num_autores'))
									  ->where('autor','=',$aux_data)  						
									  ->groupBy('autor_salida')
									 // ->select('autor_salida', DB::raw('count(autor_salida) as num_autores')) 
									  ->paginate(15);

		$datos_busqueda = array_merge($datos_busqueda,$busqueda_autores->lists('autor_salida'));*/


		


		$busqueda_cotas = Recurso::select('ubicacion', DB::raw('count(ubicacion) as num_recursos') )
						->where('ubicacion','like','%'.$aux_data.'%')
						->where('tipo_liter','like',$tipo_recurso.'%')
						->groupBy('ubicacion')
						->paginate(15);

		$datos_busqueda = array_merge($datos_busqueda,$busqueda_cotas->lists('ubicacion'));
		$datos_busqueda = array_unique($datos_busqueda);

		if($tipo_recurso != ''){
			$busqueda_autores = DB::table('recursos_autores')
								->join('autores','autores.id','=','recursos_autores.cod_autor')
								->where('recursos_autores.tipo_autor','=','P')
								->where('autores.autor','like',$aux_data)
								->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
								->groupBy('autores.autor_salida')
								->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
								->paginate(15);

			$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_autores->lists('autor_salida')));

			$busqueda_autores = DB::table('recursos_autores')
								->join('autores','autores.id','=','recursos_autores.cod_autor')
								->where('recursos_autores.tipo_autor','=','P')
								->where('autores.autor','like',$aux_data.'%')
								->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
								->groupBy('autores.autor_salida')
								->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
								->paginate(15);

			$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_autores->lists('autor_salida')));

			$busqueda_autores = DB::table('recursos_autores')
								->join('autores','autores.id','=','recursos_autores.cod_autor')
								->where('recursos_autores.tipo_autor','=','P')
								->where('autores.autor','like','%'.$aux_data.'%')
								->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
								->groupBy('autores.autor_salida')
								->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
								->paginate(15);

			$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_autores->lists('autor_salida')));

			$busqueda_autores = DB::table('recursos_autores')
				->join('autores','autores.id','=','recursos_autores.cod_autor')
				->where('recursos_autores.tipo_autor','=','P')
				->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
				->where(function($query){
					foreach ($this->split_data as $aux) {
						$query->where('autores.autor', 'like', '%'.$aux.'%');
					}
				})
			//	->where('titulos.titulo','like','%'.$aux_data.'%')
			//	->orderBy('titulos.titulo','DESC')
				->groupBy('autores.autor_salida')
				->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
				
				//->where('recursos_titulos.titulo_id','=','titulos.id')
				//->orWhere('titulo','like','%'$aux_data.'%')
				->paginate(15);
			$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_autores->lists('autor_salida')));

		}else{
			$busqueda_autores = Autor::select('autor_salida',DB::raw('count(autor_salida) as num_autores'))
										  ->where('autor','like',$aux_data.'%')  						
										  ->groupBy('autor_salida')
										 // ->select('autor_salida', DB::raw('count(autor_salida) as num_autores')) 
										  ->paginate(15);

			$datos_busqueda = array_merge($datos_busqueda,$busqueda_autores->lists('autor_salida'));

			$busqueda_autores = Autor::select('autor_salida',DB::raw('count(autor_salida) as num_autores'))
								
										  ->where('autor','like','%'.$aux_data.'%')  						
										  ->groupBy('autor_salida')
									//	  ->select('autor_salida', DB::raw('count(autor_salida) as num_autores'))
										  ->paginate(15);

			$datos_busqueda = array_merge($datos_busqueda,$busqueda_autores->lists('autor_salida'));
			$datos_busqueda = array_unique($datos_busqueda);


			$busqueda_autores = Autor::select('autor_salida',DB::raw('count(autor_salida) as num_autores'))
								
										  ->where( function($query){
		  										foreach ($this->split_data as $aux) {
												$query->where('autores.autor', 'like', '%'.$aux.'%');
												}
										   })					
										  ->groupBy('autor_salida')
										  ->paginate(15);

			$datos_busqueda = array_merge($datos_busqueda,$busqueda_autores->lists('autor_salida'));
			$datos_busqueda = array_unique($datos_busqueda);
		}

		$busqueda_editoriales = DB::table('recursos_editoriales')
									->join('editoriales','editoriales.id','=','recursos_editoriales.editorial_id')
									->where('editoriales.editorial','like','%'.$aux_data.'%')
									->where('recursos_editoriales.tipo_doc','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('editoriales.editorial_salida')
									->select('editoriales.editorial_salida', DB::raw('count(editoriales.editorial_salida) as num_recursos'))
									->paginate(15);

		$datos_busqueda = array_merge($datos_busqueda,$busqueda_editoriales->lists('editorial_salida'));
		$datos_busqueda = array_unique($datos_busqueda);
	




/*
		$busqueda_editoriales = Editorial::select('editorial_salida',DB::raw('count(editorial_salida) as num_editoriales'))
									  ->where('editorial','=', $aux_data)  						
								 	  ->groupBy('editorial_salida')
									// ->select('editorial_salida', DB::raw('count(editorial_salida) as num_editoriales'))
									 
									  ->paginate(15);

		$datos_busqueda = array_merge($datos_busqueda,$busqueda_editoriales->lists('editorial_salida'));
		

		$busqueda_editoriales = Editorial::select('editorial_salida',DB::raw('count(editorial_salida) as num_editoriales'))
									  ->where('editorial','like','%'.$aux_data.'%')  						
									  ->groupBy('editorial_salida')
								//    ->select('editorial_salida', DB::raw('count(editorial_salida) as num_editoriales'))
									  ->paginate(15);

		$datos_busqueda = array_merge($datos_busqueda,$busqueda_editoriales->lists('editorial_salida'));
		

		$busqueda_cotas = Recurso::select('ubicacion')
		 						  ->where('ubicacion','=',$aux_data)

								  ->orWhere('ubicacion','like','%'.$aux_data.'%')

								  ->paginate(15);
		$datos_busqueda = array_merge($datos_busqueda,$busqueda_cotas->lists('ubicacion'));
		


/*		if(count($datos_busqueda)==0 || count($datos_busqueda)<4){

		$busqueda_titulos = Titulo::select('titulo_salida')
								
									->where(function($query){
									 		foreach ($this->split_data as $value){
									 			$query->where('titulo','like','%'.$value.'%');
									 		}
						            	})
									//->orderBy('titulo','like','%'.$this->split_data[0].'%')
									->paginate(15);			            

			$datos_busqueda = array_merge($datos_busqueda,$busqueda_titulos->lists('titulo_salida'));
		}

		$busqueda_cotas = Recurso::select('ubicacion')
								  ->where(function($query){
									 	foreach ($this->split_data as $value){
									 			$query->orWhere('ubicacion','like','%'.$value.'%');
									 		}
						            	})						
								   ->paginate(15);

		$datos_busqueda = array_merge($datos_busqueda,$busqueda_cotas->lists('ubicacion'));

		$busqueda_autores = Autor::select('autor_salida')
									  ->where('autor','like',$aux_data.'%')  						
									  ->paginate(15);

		$datos_busqueda = array_merge($datos_busqueda,$busqueda_autores->lists('autor_salida'));




		$busqueda_autores = Autor::select('autor_salida')
									  ->where(function($query) {
										 	foreach ($this->split_data as $value){
										 			$query->where('autor','like','%'.$value.'%');
										 		}
							            	})						
									   ->paginate(15);

		$datos_busqueda = array_merge($datos_busqueda,$busqueda_autores->lists('autor_salida'));


		$busqueda_editoriales = Editorial::select('editorial_salida')
									
									  ->orWhere(function($query){
										 	foreach ($this->split_data as $value){
										 			$query->where('editorial','like','%'.$value.'%');
										 		}
							            	})						
									   ->paginate(15);

		$datos_busqueda = array_merge($datos_busqueda,$busqueda_editoriales->lists('editorial_salida'));
*/
		return $datos_busqueda;

	}


public function  myquicksort($izq,$der,$orden){                         // Accion del ordenamiendo quicsort para ordenar alfabeticamente el listado de contactos
	    $aux;
	    $x;
	    $i = $izq;  
	    $j = $der; 

	    $elem = $this->A[($izq + $der)/2]['elem'];
	    while( $i <= $j ){     
	    	if($orden == 'asc'){

		  		while( ($this->A[$i]['elem']< $elem) && ($j <= $der) ){ 

		       		$i++;
		    	}
		    	
			    while(($elem< $this->A[$j]['elem']) && ($j > $izq) ) { 
			        $j--; 
			    }
		    }else{
		    	if($orden == 'desc'){
			  		while( ($this->A[$i]['elem'] > $elem) && ($j <= $der) ){ 

			       		$i++;
			    	}
			    	
				    while(($elem > $this->A[$j]['elem']) && ($j > $izq) ) { 
				        $j--; 
				    }
			    }
		    }
		    
	    if( $i <= $j ) { 
	        $aux= $this->A[$i]; $this->A[$i]= $this->A[$j]; $this->A[$j]= $aux; $i++; $j--;
	    
	    }
	       


	    }   
	    
	    if( $izq < $j )
	        $this->myquicksort($izq, $j,$orden); 
	    if( $i < $der ) 
	        $this->myquicksort($i, $der,$orden);              
	}




	public function gestionar_orden($datos_busqueda,$orden){
		$this->A = array();
		$recursos_controller = new RecursosController();

		$split_orden = explode('_', $orden);
		$campo = $split_orden[0];
		$orden = $split_orden[1];
		$titulo = [];
		if($campo == 'titulo'){
			$aux_titulos = array();
			foreach ($datos_busqueda as $dato) {
				$titulo = "";
				$titulos = $recursos_controller->recurso_titulos($dato);

				foreach ($titulos as $titulo) {
					if($titulo->tipo_tit == 'OP'){
						$titulo = $titulo->titulo;
					}
				}

				array_push($this->A, array('id' => $dato,  'elem' => $titulo));

			}
			$this->myquicksort(0,count($this->A)-1,$orden);
			$datos_busqueda = array_fetch($this->A, 'id');
		}else{
			if($campo == 'autor'){
				$aux_autores= array();
				foreach ($datos_busqueda as $dato) {
					$autor = "";
					$autores = $recursos_controller->recurso_autores($dato);

					foreach ($autores as $autor) {
						if($autor->tipo_autor == 'P'){
							$autor = $autor->autor;
							break;
						}
					}

					array_push($this->A, array('id' => $dato,  'elem' => $autor));
				}
				$this->myquicksort(0,count($this->A)-1,$orden);
				$datos_busqueda = array_fetch($this->A, 'id');				
			}else{
				if($campo == 'anio'){
					$aux_anios= array();
					foreach ($datos_busqueda as $dato) {
						$autor = "";
						$aux_anio = Recurso::find($dato)->fecha_iso;

						if(strlen($aux_anio) >= 4){
								$aux_anio = $aux_anio[0].''.$aux_anio[1].''.$aux_anio[2].''.$aux_anio[3];
						}

						array_push($this->A, array('id' => $dato,  'elem' => $aux_anio));
					}
					$this->myquicksort(0,count($this->A)-1,$orden);
					$datos_busqueda = array_fetch($this->A, 'id');	
				}
			}	
		  }

		return $datos_busqueda;
	}


	public function buscador_simple(){
		$data = Input::all();
		$num_items_pagina = 10;
		$orden = '';

		if(Input::get('num_items_pagina')){
			$num_items_pagina = Input::get('num_items_pagina');
		}

		if(Input::get('orden')){
			$orden = Input::get('orden');
		//	return $orden;
		}



		$aux_data = $this->convetir_string($data['data']);
		$this->split_data = explode(' ',$aux_data);
		$datos_busqueda = [];
		
		//return $aux_data;

		$busqueda_titulos = DB::table('recursos_titulos')
			 			->where('recursos_titulos.tipo_tit','=','OP')
						->where('titulos.titulo','=', $aux_data)
						->join('titulos','titulos.id','=','recursos_titulos.titulo_id') 

					//	->orderBy('titulos.titulo_salida','ASC')
						->select('recursos_titulos.recurso_id','titulos.titulo_salida')
						->get();
				
		$aux_array = array_fetch((array)$busqueda_titulos, 'recurso_id');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);
		


		$busqueda_titulos = DB::table('recursos_titulos')
			 			->where('recursos_titulos.tipo_tit','=','OP')
						->where('titulos.titulo','like', $aux_data.'%')
						->join('titulos','titulos.id','=','recursos_titulos.titulo_id') 

					//	->orderBy('titulos.titulo_salida','ASC')
						->select('recursos_titulos.recurso_id','titulos.titulo_salida')
						->get();
				
		$aux_array = array_fetch((array)$busqueda_titulos, 'recurso_id');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);


		$busqueda_titulos = DB::table('recursos_titulos')

						->join('titulos','titulos.id','=','recursos_titulos.titulo_id')  
					//	->join('tipotitulo','tipotitulo.tipo_doc','=','recursos_titulos.tipo_doc')    
						->where('recursos_titulos.tipo_tit','=','OP')
						->where('titulos.titulo','like','%'.$aux_data.'%')
				//		->orderBy('titulos.titulo_salida','ASC')
						->select('recursos_titulos.recurso_id','titulos.titulo_salida')
						->get();

		$aux_array = array_fetch((array)$busqueda_titulos, 'recurso_id');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);


		$busqueda_titulos = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_tit','=','OP')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('titulos.titulo', 'like', '%'.$aux.'%');
										}

									})
								//	->where('titulos.titulo','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->select('recursos_titulos.recurso_id','titulos.titulo_salida')
									->get();
		$aux_array = array_fetch((array)$busqueda_titulos, 'recurso_id');							

		$datos_busqueda = array_merge($datos_busqueda, $aux_array);		


/*		$busqueda_titulos = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_tit','=','OP')
									->where(function($query){

										foreach ($this->split_data as $aux) {
					
													$query->orWhere('titulos.titulo', 'like', '%'.$aux.'%');
				
										}

									})
								//	->where('titulos.titulo','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->select('recursos_titulos.recurso_id','titulos.titulo_salida')
									->get();
		$aux_array = array_fetch((array)$busqueda_titulos, 'recurso_id');							

		$datos_busqueda = array_merge($datos_busqueda, $aux_array);		
	*/


		$busqueda_autores = DB::table('recursos_autores')
						->where('autores.autor','=',$aux_data)
						->join('autores','autores.id','=','recursos_autores.cod_autor')      				
						->select('recursos_autores.recurso_id','autores.autor_salida')
						->get();
		$aux_array = array_fetch((array)$busqueda_autores, 'recurso_id');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);


		$busqueda_autores = DB::table('recursos_autores')
						->where('autores.autor','like','%'.$aux_data.'%')
						->join('autores','autores.id','=','recursos_autores.cod_autor')      				
						->select('recursos_autores.recurso_id','autores.autor_salida')
						->get();
		$aux_array = array_fetch((array)$busqueda_autores, 'recurso_id');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
			$datos_busqueda = array_merge($datos_busqueda, $aux_array);

	
		$busqueda_editoriales = DB::table('recursos_editoriales')
						->where('editoriales.editorial','=',$aux_data)
						->join('editoriales','editoriales.id','=','recursos_editoriales.editorial_id')      				
						->select('recursos_editoriales.recurso_id','editoriales.editorial_salida')
						->get();
		$aux_array = array_fetch((array)$busqueda_editoriales, 'recurso_id');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
			$datos_busqueda = array_merge($datos_busqueda, $aux_array);


		$busqueda_editoriales = DB::table('recursos_editoriales')
						->where('editoriales.editorial','like','%'.$aux_data.'%')
						->join('editoriales','editoriales.id','=','recursos_editoriales.editorial_id')      				
						->select('recursos_editoriales.recurso_id','editoriales.editorial_salida')
						->get();
		$aux_array = array_fetch((array)$busqueda_editoriales, 'recurso_id');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
			$datos_busqueda = array_merge($datos_busqueda, $aux_array);


		$busqueda_cotas = DB::table('recursos')
		 						   ->where(function($query) use($aux_data){
		 						  		foreach ($this->split_data as $aux) {
		 						  			$query->where('ubicacion','like','%'.$aux.'%');
		 						  		}
		 						   })

								  ->select('recursos.id','recursos.ubicacion')
								  ->get();
		$aux_array = array_fetch((array)$busqueda_cotas, 'id');		
						  
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$aux_array));


	/*	$busqueda_id = DB::table('recursos')
						->where('recursos.id','like','%'.$aux_data.'%') 				
						->select('recursos.id','recursos.ubicacion')
						->get();
		$aux_array = array_fetch((array)$busqueda_id, 'id');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);
	*/

		$busqueda_id = DB::table('ejemplares')
						->where('ejemplares.id','like','%'.$aux_data.'%') 				
						->select('ejemplares.recurso_id')
						->get();
		$aux_array = array_fetch((array)$busqueda_id, 'recurso_id');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);



	/*	$busqueda_cotas = DB::table('recursos')
		 						   ->where(function($query) use($aux_data){
		 						  		for ($i=0; $i<count($this->split_data)-1;$i++) {
		 						  			$query->where('ubicacion','like','%'.$this->split_data[$i].'%');
		 						  		}
		 						   })
		 						   
								  ->select('recursos.id','recursos.ubicacion')
								  ->get();
		$aux_array = array_fetch((array)$busqueda_cotas, 'id');		
						  
		$datos_busqueda =array_unique(array_merge($datos_busqueda,$aux_array));
	*/

		if($orden!=''  && $orden!='normal'){
			$datos_busqueda = $this->gestionar_orden($datos_busqueda,$orden);
		}

		$perPage = $num_items_pagina;
		$currentPage = Input::get('page') - 1;
		$pagedData = array_slice($datos_busqueda, $currentPage * $perPage, $perPage);
		$matches = Paginator::make($pagedData, count($datos_busqueda), $perPage);
		return $matches;
//		return Paginator::make($datos_busqueda,count($datos_busqueda),10);

		////$array_id_recusos = array_merge($array_id_recusos, $id_recursos);


//		return $array_id_recusos;
		//$array_id_recursos = array_fetch((array)$id_recursos, 'recurso_id');
		
	//	return array_unique($array_id_recursos);

		//array_fetch($array, 'developer.name')
	//	$datos_busqueda = array_merge($datos_busqueda, $id_recursos->lists('recurso_id'));


/*		$id_recursos = DB::table('recursos_titulos')
			 			->where('recursos_titulos.tipo_tit','=','OP')
						->where('titulos.titulo','like','%'.$aux_data.'%')
						->join('titulos','titulos.id','=','recursos_titulos.titulo_id')      				
						->select('recursos_titulos.recurso_id','titulos.titulo_salida')
						->paginate();

		$datos_busqueda = array_merge($datos_busqueda, $id_recursos->lists('recurso_id'));
*/
//		$datos_busqueda = Paginator::make(array_unique($datos_busqueda),count(array_unique($datos_busqueda)),10);



		//$datos_busqueda = Paginator::make($datos_busqueda, count($datos_busqueda), 10);
	




	/*	$id_recursos = DB::table('recursos_titulos')
			 			->where('recursos_titulos.tipo_tit','=','OP')
						->where('titulos.titulo','like','%'.$aux_data.'%')
						->join('titulos','titulos.id','=','recursos_titulos.titulo_id')      				
						->select('recursos_titulos.recurso_id','titulos.titulo_salida')
						->paginate();
		*/				


/*		$recursos_por_titulo = DB::table('recursos_titulos')
			->join('recursos','recursos.id','=','recursos_titulos.recurso_id')
            ->join('titulos', 'titulos.id', '=', 'recursos_titulos.titulo_id')       
           	->join('tipotitulo', function ($join){
         			$join->on('tipotitulo.tipo_titulo','=','recursos_titulos.tipo_tit');
         			$join->on('tipotitulo.tipo_doc','=','recursos_titulos.tipo_doc');
         	})
           	->join('tipo_docs','tipo_docs.id','=','recursos.tipo_liter')
         	->where('recursos_titulos.tipo_tit','=','OP')
         //	->where('titulos.titulo','like','='.$aux_data)
         	->where(function($query) use ($aux_data){
           			$query->where('titulos.titulo','=',$aux_data);
       				$query->orWhere('titulos.titulo','like','%'.$aux_data.'%');
        
           	})	   	
*/
       /*    	->where(function($query) use ($aux_data){
           			$query->where('titulos.titulo','like','%'.$aux_data.'%');
           			foreach ($this->split_data as $value) {
           				if(strlen($value)>3){
           					$query->orWhere('titulos.titulo','like','%'.$value.'%');
           				}
           			} 
           	})	
     	*/	
        /*  	->Orwhere(function($query){
           			foreach ($this->split_data as $value) {
           				if(strlen($value)>3){
           					$query->where('titulos.titulo','like','%'.$value.'%');
           				}
           			} 
           	})
          */	
           // ->orderBy('recursos_titulos.tipo_doc','ASC')
           //	->orderBy('titulos.titulo','DESC')
     //       ->select('recursos.ubicacion','recursos.id','titulos.titulo_salida','recursos.tipo_liter','tipo_docs.descrip_doc','recursos.fecha_iso','recursos.fecha_pub')          
       //     ->paginate(10);

		//return $datos_busqueda;

	}


	public function generate_inputs_unique($inputs){

		$unique_inputs = [];
		foreach ($inputs as  $input) {

			$unique_inputs [] = $input['opt1']['select_model']['value'];
		}

		return array_unique($unique_inputs);
	}

	public function generate_from_query($aux_inputs_unique,$inputs){


		$aux_from = "";

	//	return Template::$Template_from_query[$inputs[0]['opt1']['select_model']['value']];
		if(count($inputs)>=2 && Template::$Template_from_query[$inputs[0]['opt1']['select_model']['value']] == ''){
				$aux_from .=" recursos r, ";

		}else{
			if(count($inputs)==1 && Template::$Template_from_query[$inputs[0]['opt1']['select_model']['value']] == ''){
				$aux_from .=" recursos r";
			}else{

				if( count($inputs)==1 && Template::$Template_from_query[$inputs[0]['opt1']['select_model']['value']] != ''){
					  $aux_from .=" recursos r, ";
				}else{
					if( count($inputs)>1 && Template::$Template_from_query[$inputs[0]['opt1']['select_model']['value']] == ''){
						$aux_from .= " recursos r, ";
					}else{
						if( count($inputs)>1 && Template::$Template_from_query[$inputs[0]['opt1']['select_model']['value']] != ''){
							$aux_from .= " recursos r, ";
						}
					}


				}

			}

		}




		$i = 0;


	$aux = $aux_inputs_unique ;
		$aux_inputs = [];
		

		$aux_join = "";


		foreach ($aux_inputs_unique as $input) {
			$aux_inputs [] = $input;
		}

		$aux_inputs_unique = $aux_inputs;

	
		$iter = 0;
		$i= 0;
		foreach ($aux_inputs_unique as $input) {
			if($input == "autor" || $input == "jurado" || $input == "tutor_academico"){
				$aux_inputs_unique[$i] = 'autor'; 		
			}

			if($input == "materia" || $input == "idioma"){
				$aux_inputs_unique[$i] = 'descriptor'; 		
			}

			$i++;
		}


		$aux_inputs_unique = array_unique($aux_inputs_unique);

	//	return $aux_inputs_unique;




//	return Template::$Template_from_query[$aux_inputs_unique[4]];		
		foreach($aux_inputs_unique as $input_unique){

			$aux_from .=" ".Template::$Template_from_query[$input_unique];

			if ( $i != count($aux_inputs_unique)-1 ){
						
					if(Template::$Template_from_query[$input_unique]!= ''){	
						$aux_from .=",";
					}
					
					//return Template::$Template_join_query[$input_unique];
			} 
			$i++;

 		}
 	//	return $aux_from;

 	/*	if($aux_from == " "){
 			return " recursos r ";
 		}
	
 		if($aux_from[2]!= 'r'){
 			$aux_from = 'recuros r, '.$aux_from;
 		}
*/	

 		if($aux_from[strlen($aux_from)-4] == ","){
 			$aux_from[strlen($aux_from)-4] = '';
 		}

 		if($aux_from[strlen($aux_from)-3] == ","){
 			$aux_from[strlen($aux_from)-3] = '';
 		}

 		if($aux_from[strlen($aux_from)-1] == "," || $aux_from[strlen($aux_from)-2]==","){
 			$aux_from[strlen($aux_from)-1] = '';
 		}

 		return $aux_from;	

	}


	public function generate_tipo_autores($aux_inputs_unique){
		$aux_query = "";
		$i = 0;
		foreach ($aux_inputs_unique as $input) {
			if($input == "jurado"){
				$aux_query .= " 'J', ";
			}else{
				if($input == "autor"){
					$aux_query .= "'P', 'S', "; 
				}else{
					if($input == "tutor_academico"){
						$aux_query .= " 'TA', "; 
					}
				}
			}
		}


		if(!empty($aux_query)){
			$aux_query[strlen($aux_query)-2]='';
		}

		return $aux_query;
	}


	public function generate_tipo_descriptores($aux_inputs_unique){
		$aux_query = "";
		$i = 0;
		foreach ($aux_inputs_unique as $input) {
			if($input == "idioma"){
				$aux_query .= " 'I', ";
			}else{
				if($input == "materia"){
					$aux_query .= " 'M-650', 'M', ";  
				}
			}
		}


		if(!empty($aux_query)){
			$aux_query[strlen($aux_query)-2]='';
		}

		return $aux_query;

		
	}



	public function generate_join_query($aux_inputs_unique){
		$aux = $aux_inputs_unique ;
		$aux_inputs = [];
		
		$aux_join = "";


		foreach ($aux_inputs_unique as $input) {
			$aux_inputs [] = $input;
		}

		$aux_inputs_unique = $aux_inputs;

	
		$iter = 0;
		$i= 0;
		foreach ($aux_inputs_unique as $input) {
			if($input == "autor" || $input == "jurado" || $input == "tutor_academico"){
				$aux_inputs_unique[$i] = 'autor'; 		
			}

			if($input == "materia" || $input == "idioma"){
				$aux_inputs_unique[$i] = 'descriptor'; 		
			}

			$i++;
		}



		$aux_inputs_unique = array_unique($aux_inputs_unique);
		//return $aux_inputs_unique;	
	

	//	return $aux_inputs_unique;
		$i = 0;
	//	return $aux_inputs_unique;
		foreach($aux_inputs_unique as $input_unique){

			$aux_join .=" ".Template::$Template_join_query[$input_unique];
			if ( $i != count($aux_inputs_unique)-1   ){
						
					if(Template::$Template_join_query[$input_unique]!= ''){	
						$aux_join .=" AND ";
					}
					
					//return Template::$Template_join_query[$input_unique];
			} 
			$i++;
 		}


 		$aux_query_tipo_autor =     $this->generate_tipo_autores($aux);
 		$aux_query_tipo_descriptor= $this->generate_tipo_descriptores($aux);



 		if(!empty($aux_query_tipo_autor)){
 			$aux_join .= " AND ra.tipo_autor IN (".$aux_query_tipo_autor." )";
 		}

 		if(!empty($aux_query_tipo_descriptor)){
 			$aux_join .= " AND td.id IN (".$aux_query_tipo_descriptor." )";
 		}
 
 //		return $aux_join;

 //		return $aux;



// 		return len([1,2,3,4,5]);


// 		return $aux_join[len($aux_join)];


 		if($aux_join == " "){
 			//	return "(r.ubicacion = r.ubicacion)";
 			return "";
 		}

 		if($aux_join == "  "){
 			return "";
 		}
 	
 		return "(".$aux_join." ";	

 	//	return "( ".$aux_join."hola"." )";

	}


	public function generate_tipo_recurso($aux_tipo_recurso){
		switch ($aux_tipo_recurso) {
			case 'tesis':
				return "r.tipo_liter = 'T'";
				break;
			case 'monografia':
				return "r.tipo_liter = 'M'";
				break;
			case 'publicacion_seriada':
				return "r.tipo_liter = 'S'";
				# code...
				break;			
			default:
				# code...
				break;
		}
	}


	public function generate_filtros_operadores_logicos($inputs){

		$aux_filtro = " ";
		for($i=0; $i<count($inputs); $i++) {
			$input  = $inputs[$i]['opt1']['select_model']['value'];
			$text   = $inputs[$i]['opt1']['text_model'];
			$elem  = "";
			$porcent = "";
			$porcent_init = "";
			$porcent_fin =  "";
			if($input == 'volumen'){
				$elem = "=";	
				$porcent_init = "";
				$porcent_fin =  "";
			}else{
				$elem = "LIKE";
				$porcent_init = "'%";
				$porcent_fin =  "%'";
			}


			$aux_logico = "";
			
			if($i>=1){
				if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
					$elem = "NOT LIKE";
					$aux_logico = " AND ";
				}
			}

			$aux_filtro .=  " ".$aux_logico." ".Template::$Template_inputs_operadores_logicos_query[$input]." ".$elem." ".$porcent_init.$this->convetir_string($text).$porcent_fin." ";


			if($i<count($inputs)-1){
		
				if($inputs[$i]['opt2']['select_model']['value'] != 'NOT LIKE'){
					$aux_filtro.= $inputs[$i]['opt2']['select_model']['value']." ";
				}
	
			}
		}
		return "( ".$aux_filtro." )";
	}



	public function f_remove_odd_characters($string){
	      // these odd characters appears usually 
	      // when someone copy&paste from MSword to an HTML form
	      $string = str_replace("\n","[NEWLINE]",$string);
	      $string=htmlentities($string);
	      $string=preg_replace('/[^(\x20-\x7F)]*/','',$string);
	      $string=html_entity_decode($string);     
	      $string = str_replace("[NEWLINE]","\n",$string);
	       return $string;
	  }


	public function buscador_avanzado(){

		$input  = Input::all();
		$query_tipo_recurso = "";
		$this->A = array();

		$orden = '';

		if(Input::get('orden')){
			$orden = Input::get('orden');
		//	return $orden;
		}

		$tipo_recurso = $input['tipo_recurso'];

		switch ($input['tipo_recurso']) {
			case 'tesis':
				$query_tipo_recurso = " AND tipo_liter = 'T'";
				# code...
				break;
			case 'monografia':
				$query_tipo_recurso = " AND tipo_liter = 'M'";
				# code...
			break;	
			case 'publicacion_seriada':
				$query_tipo_recurso = "AND tipo_liter = 'S'";// AND nivel_reg !='ms'";			
				# code...
			break;	
			case 'todo':
				$query_tipo_recurso = " AND tipo_liter like '%%' AND nivel_reg !='ms'";			
				# code...
			break;				
			default:
				$query_tipo_recurso = " AND tipo_liter <> 'S' AND tipo_liter <> 'M' AND tipo_liter <> 'T' ";	
			break;
		}
		$inputs = $input['inputs'];
		$anios  = $input['anios'];
		$recursos = array();
		$query_dependencia = "";
		$query_biblioteca  = "";
		$query_biblioteca  = "";
		$query_anios = "";
		if($input['dependencia']['value'] != "todas"){
			$query_dependencia = " AND id IN  (".Template::$Template_sub_query['recursos_dependencias'].$input['dependencia']['value'].")";
		}
		if($input['biblioteca']['value'] != "todas"){
			$query_dependencia = " AND id IN  (".Template::$Template_sub_query['recursos_bibliotecas'].$input['biblioteca']['value'].")";
		}
		
		


		if($input['anios']['anio_origen']!= "" AND $input['anios']['anio_destino']!= ""){

			if((int)($input['anios']['anio_origen'])< 1001 ){
				$input['anios']['anio_origen'] = "1001";
			}

			if((int)($input['anios']['anio_origen']) > (int)$input['anios']['anio_destino'] ){
				$input['anios']['anio_destino'] = '';
			}
			$query_anios = " AND id IN (".Template::$Template_sub_query['anios']." fecha_iso >= '".(string)((int)$input['anios']['anio_origen'] )."' AND fecha_iso <= '".(string)((int)$input['anios']['anio_destino'] + 1)."')";
		
		}
		if($input['anios']['anio_origen']!= "" AND $input['anios']['anio_destino']== ""){
			if((int)($input['anios']['anio_origen'])< 1001 ){
				$input['anios']['anio_origen'] = "1001";
			}
			$query_anios = " AND id IN (".Template::$Template_sub_query['anios']." fecha_iso >= '".(string)((int)$input['anios']['anio_origen'] )."')";
		
		}else if($input['anios']['anio_origen']== "" AND $input['anios']['anio_destino']!= ""){
			$query_anios = " AND id IN (".Template::$Template_sub_query['anios']."fecha_iso <= '".(string)((int)$input['anios']['anio_destino'] + 1)."')";
		
		}

		



		$num_items_pagina = 10;
		if(Input::get('num_items_pagina')){
			$num_items_pagina = Input::get('num_items_pagina');
		}

		$query_parte1 = 'SELECT id, jerarquia FROM recursos WHERE ext_id = 0 AND id IN';
		$query_parte2 = '';


		$i = 0;


		foreach ($inputs as $input) {

			$elem = "";
			$operador = "";
			$porcent_init = "";
			$porcent_fin  =  "";
			$inclusion = "";

			switch ($input['opt1']['select_model']['value']) {

				case 'autor':

					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";
					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}
					# code...
					break;				
				case 'tutor_academico':

					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";
					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}							
					# code...
					break;
				case 'jurado':

					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";
					# code...
					break;	
				case 'titulo':
					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";
					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}					
					# code...
					break;		
				case 'materia':
					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";
					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}						
					# code...
					break;	
				case 'volumen':
					$operador = " = ";
					$porcent_init = "";
					$porcent_fin  = "";

					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " != ";
						}
					}	

					# code...
					break;		
				case 'isbn':
					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";

					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}	

					# code...
					break;	
				case 'issn':
					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";

					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}	
				case 'editorial':
					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";

					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}	
					# code...
					break;											
				case 'idioma':
					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";

					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}	
					# code...
					break;	
				case 'palabra_clave':
					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";

					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}	
					# code...
					break;	

				case 'cota':
					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";

					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}	
					# code...
					break;		
				case 'edicion':
					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";

					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}	
					# code...
					break;		
				case 'tipo_de_trabajo':
					$operador = " LIKE ";
					$porcent_init = "'%";
					$porcent_fin  = "%'";

					if($i>=1){
						if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
							$operador = " NOT LIKE ";
						}
					}	
					# code...
					break;																				
				default:
					# code...
					break;
		
			}

			$valor = $input['opt1']['text_model'];
			if($input['opt1']['select_model']['value'] != 'edicion'){
				$valor =  $this->convetir_string($valor);
			}
				
			$query_parte2.= ' ('.Template::$Template_sub_query[$input['opt1']['select_model']['value']].$operador.$porcent_init.$valor.$porcent_fin.')';
	

			if($i!= count($inputs)-1){
				if($i>=1){
					if($inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
						$inclusion = " id IN ";
					}
				}

				if($input['opt2']['select_model']['value'] == 'AND'){
					$inclusion = " AND id IN ";
				}else{
					if($input['opt2']['select_model']['value'] == 'OR'){
						$inclusion = " OR id IN ";
					}
				}			

			}	


			$query_parte2.=$inclusion;
			$i++;

			if($i>=1 && $inputs[$i-1]['opt2']['select_model']['value'] == 'NOT LIKE'){
				$query_parte2.=" AND id IN";
			}

		}


		$query_parte2.= $query_dependencia;
		$query_parte2.= $query_anios;
		$query_parte2.= $query_tipo_recurso;

		$query = DB::select(DB::raw($query_parte1." ".$query_parte2));
		$ids_recursos = array_fetch($query, 'id');

		if($tipo_recurso== 'publicacion_seriada'){
			$jerarquiasConRepetidos = array_fetch($query, 'jerarquia');
			$jerarquias = array_unique($jerarquiasConRepetidos);
			$jerarquias = array_merge($jerarquias, []);
			
			$publicaciones = DB::table('recursos')
			        ->where('recursos.nivel_reg','!=','ms')
					->whereIn('jerarquia', $jerarquias)
				    ->select('recursos.id as id') 
					->get();
			$ids_recursos = array_fetch($publicaciones, 'id');
		}

		if($orden!=''  && $orden!='normal'){
			$ids_recursos = $this->gestionar_orden($ids_recursos,$orden);
		}

		$perPage = $num_items_pagina;
		$currentPage = Input::get('page') - 1;
		$pagedData = array_slice($ids_recursos, $currentPage * $perPage, $perPage);
		$matches = Paginator::make($pagedData, count($ids_recursos), $perPage);
		return $matches;



	}

	public function buscador_google($id){

		/*	$searchResults = GoogleSearch::getResults('a');
			return $searchResults;
		*/
		$results = SphinxSearch::search('algoritmos')->get();
		return Response::json($results);

	}

	public function hash(){

		return Hash::make('hola12341234');

	 /*   $pdf = new \Thujohn\Pdf\Pdf();
	    $content = $pdf->load(View::make('recursos.recurso_pdf'))->download();
	    File::put(public_path('test'.'1'.'pdf'), $content);
	*/
	}


	public function buscador_titulo_typeahead(){
		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_titulos = [];		  
		$busqueda_titulos = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_tit','=','OP')
									->where('titulos.titulo','=',$aux_data)
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('titulos.titulo_salida')
									->select('titulos.titulo_salida', DB::raw('count(titulos.titulo_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('titulo_salida')));
		

		$busqueda_titulos = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_tit','=','OP')
									->where('titulos.titulo','like',$aux_data.'%')
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('titulos.titulo_salida')
									->select('titulos.titulo_salida', DB::raw('count(titulos.titulo_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('titulo_salida')));




		$busqueda_titulos = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_tit','=','OP')
									->where('titulos.titulo','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
									->groupBy('titulos.titulo_salida')
									->select('titulos.titulo_salida', DB::raw('count(titulos.titulo_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('titulo_salida')));

		$busqueda_titulos = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_tit','=','OP')
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('titulos.titulo', 'like', '%'.$aux.'%');
										}

									})
								//	->where('titulos.titulo','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('titulos.titulo_salida')
									->select('titulos.titulo_salida', DB::raw('count(titulos.titulo_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('titulo_salida')));
		$datos_busqueda = array_unique($datos_busqueda);



		return $datos_busqueda;
	}



	public function buscador_autor_typeahead(){
		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_titulos = [];		  
		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','=',$aux_data)
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));
		

		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','like',$aux_data.'%')
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));




		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));

		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('autores.autor', 'like', '%'.$aux.'%');
										}

									})
								//	->where('titulos.titulo','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));
		$datos_busqueda = array_unique($datos_busqueda);



		return $datos_busqueda;

	}

	public function buscador_jurado_typeahead(){
		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}
		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_titulos = [];		  
		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','=',$aux_data)
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->where('recursos_autores.tipo_autor', '=', 'J')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));
		

		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','like',$aux_data.'%')
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->where('recursos_autores.tipo_autor', '=', 'J')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));




		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->where('recursos_autores.tipo_autor', '=', 'J')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));

		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->where('recursos_autores.tipo_autor', '=', 'J')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('autores.autor', 'like', '%'.$aux.'%');
										}

									})
								//	->where('titulos.titulo','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));
		$datos_busqueda = array_unique($datos_busqueda);



		return $datos_busqueda;

	}

	public function buscador_tutor_academico_typeahead(){
		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_titulos = [];		  
		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','=',$aux_data)
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->where('recursos_autores.tipo_autor', '=', 'TA')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));
		

		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','like',$aux_data.'%')
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->where('recursos_autores.tipo_autor', '=', 'TA')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));




		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->where('recursos_autores.tipo_autor', '=', 'TA')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));

		$busqueda_titulos = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->where('recursos_autores.tipo_autor', '=', 'TA')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('autores.autor', 'like', '%'.$aux.'%');
										}

									})
								//	->where('titulos.titulo','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('autores.autor_salida')
									->select('autores.autor_salida', DB::raw('count(autores.autor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('autor_salida')));
		$datos_busqueda = array_unique($datos_busqueda);



		return $datos_busqueda;

	}





	public function buscador_cota_typeahead(){
		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_titulos = [];		  
		$busqueda_titulos = DB::table('recursos')
									->where('recursos.ubicacion','=',$aux_data)
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('recursos.ubicacion')
									->select('recursos.ubicacion', DB::raw('count(recursos.ubicacion) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('ubicacion')));
		

		$busqueda_titulos = DB::table('recursos')
									->where('recursos.ubicacion','like',$aux_data.'%')
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('recursos.ubicacion')
									->select('recursos.ubicacion', DB::raw('count(recursos.ubicacion) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('ubicacion')));




		$busqueda_titulos = DB::table('recursos')
									->where('recursos.ubicacion','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
									->groupBy('recursos.ubicacion')
									->select('recursos.ubicacion', DB::raw('count(recursos.ubicacion) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('ubicacion')));
		$datos_busqueda = array_unique($datos_busqueda);


		return $datos_busqueda;

	}


	public function buscador_materia_typeahead(){

	$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}
		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_materias = [];		  
		$busqueda_materias = DB::table('recursos_descriptores')
									->join('descriptores', function ($join){
										$join->on('descriptores.id','=','recursos_descriptores.descriptor_id');
									})
									->join('tipo_descriptores',function($join){
										$join->on('descriptores.tipo','=','tipo_descriptores.id');
									})
					
									->where(function($query){
					           			$query->where('tipo_descriptores.id','like','%M-650%');
					       				$query->orWhere('tipo_descriptores.id','like','%M%');
					           		})	 
					           	
									->where('descriptores.descriptor','=',$aux_data)
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('descriptores.descriptor_salida')
									->select('descriptores.descriptor_salida', DB::raw('count(descriptores.descriptor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materias->lists('descriptor_salida')));

		$busqueda_materias = DB::table('recursos_descriptores')
									->join('descriptores', function ($join){
										$join->on('descriptores.id','=','recursos_descriptores.descriptor_id');
									})
									->join('tipo_descriptores',function($join){
										$join->on('descriptores.tipo','=','tipo_descriptores.id');
									})
					
									->where(function($query){
					           			$query->where('tipo_descriptores.id','like','%M-650%');
					       				$query->orWhere('tipo_descriptores.id','like','%M%');
					           		})	 
					           	
									->where('descriptores.descriptor','like', $aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('descriptores.descriptor_salida')
									->select('descriptores.descriptor_salida', DB::raw('count(descriptores.descriptor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materias->lists('descriptor_salida')));


		$busqueda_materias = DB::table('recursos_descriptores')
									->join('descriptores', function ($join){
										$join->on('descriptores.id','=','recursos_descriptores.descriptor_id');
									})
									->join('tipo_descriptores',function($join){
										$join->on('descriptores.tipo','=','tipo_descriptores.id');
									})
					
									->where(function($query){
					           			$query->where('tipo_descriptores.id','like','%M-650%');
					       				$query->orWhere('tipo_descriptores.id','like','%M%');
					           		})	 
					           	
									->where('descriptores.descriptor','like', '%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('descriptores.descriptor_salida')
									->select('descriptores.descriptor_salida', DB::raw('count(descriptores.descriptor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materias->lists('descriptor_salida')));


		$busqueda_materias = DB::table('recursos_descriptores')
									->join('descriptores', function ($join){
										$join->on('descriptores.id','=','recursos_descriptores.descriptor_id');
									})
									->join('tipo_descriptores',function($join){
										$join->on('descriptores.tipo','=','tipo_descriptores.id');
									})
					
									->where(function($query){
					           			$query->where('tipo_descriptores.id','like','%M-650%');
					       				$query->orWhere('tipo_descriptores.id','like','%M%');
					           		})	 
					           	
					           		->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('descriptores.descriptor','like', '%'.$aux.'%');
										}

									})
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('descriptores.descriptor_salida')
									->select('descriptores.descriptor_salida', DB::raw('count(descriptores.descriptor_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materias->lists('descriptor_salida')));


		$datos_busqueda = array_unique($datos_busqueda);
		return $datos_busqueda;

	}


	public function buscador_tipo_de_trabajo_typeahead(){

	$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_tipo_de_trabajo = [];		  
		$busqueda_tipo_de_trabajo = DB::table('recursos')
									->join('tesis', function ($join){
										$join->on('recursos.id','=','tesis.recurso_id');
									})
									->join('tipo_trabajo',function($join){
										$join->on('tesis.trabajo_id','=','tipo_trabajo.id');
									})
									->where('tipo_trabajo.trabajo','=',$aux_data)
									->groupBy('tipo_trabajo.trabajo_salida')
									->select('tipo_trabajo.trabajo_salida', DB::raw('count(tipo_trabajo.trabajo_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_tipo_de_trabajo->lists('trabajo_salida')));

		$busqueda_tipo_de_trabajo = DB::table('recursos')
									->join('tesis', function ($join){
										$join->on('recursos.id','=','tesis.recurso_id');
									})
									->join('tipo_trabajo',function($join){
										$join->on('tesis.trabajo_id','=','tipo_trabajo.id');
									})
									->where('tipo_trabajo.trabajo','like',$aux_data.'%')
									->groupBy('tipo_trabajo.trabajo_salida')
									->select('tipo_trabajo.trabajo_salida', DB::raw('count(tipo_trabajo.trabajo_salida) as num_recursos'))
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_tipo_de_trabajo->lists('trabajo_salida')));


		$busqueda_tipo_de_trabajo = DB::table('recursos')
									->join('tesis', function ($join){
										$join->on('recursos.id','=','tesis.recurso_id');
									})
									->join('tipo_trabajo',function($join){
										$join->on('tesis.trabajo_id','=','tipo_trabajo.id');
									})
									->where('tipo_trabajo.trabajo','like','%'.$aux_data.'%')
									->groupBy('tipo_trabajo.trabajo_salida')
									->select('tipo_trabajo.trabajo_salida', DB::raw('count(tipo_trabajo.trabajo_salida) as num_recursos'))
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_tipo_de_trabajo->lists('trabajo_salida')));


		$busqueda_tipo_de_trabajo =  DB::table('recursos')
									->join('tesis', function ($join){
										$join->on('recursos.id','=','tesis.recurso_id');
									})
									->join('tipo_trabajo',function($join){
										$join->on('tesis.trabajo_id','=','tipo_trabajo.id');
									})
					           		->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('tipo_trabajo.trabajo','like', '%'.$aux.'%');
										}

									})
									->groupBy('tipo_trabajo.trabajo_salida')
									->select('tipo_trabajo.trabajo_salida', DB::raw('count(tipo_trabajo.trabajo_salida) as num_recursos'))
									->paginate(15);


		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_tipo_de_trabajo->lists('trabajo_salida')));


		$datos_busqueda = array_unique($datos_busqueda);
		return $datos_busqueda;

	}


	public function buscador_volumen_typeahead(){
				$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
	//	$aux_data = (int)$aux_data;
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_titulos = [];		  
		$busqueda_titulos = DB::table('recursos')
									->where('recursos.volumen','=',$aux_data)
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('recursos.volumen')
									->select('recursos.volumen', DB::raw('count(recursos.volumen) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('volumen')));
		$datos_busqueda = array_unique($datos_busqueda);

		return $datos_busqueda;
	}


	public function buscador_edicion_typeahead(){
		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $data['data'];
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_edicion = [];		  
		$busqueda_edicion = DB::table('recursos')
									->where('recursos.edicion','=',$aux_data)
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('recursos.edicion')
									->select('recursos.edicion', DB::raw('count(recursos.edicion) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_edicion->lists('edicion')));
		

		$busqueda_edicion = DB::table('recursos')
									->where('recursos.edicion','like',$aux_data.'%')
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('recursos.edicion')
									->select('recursos.edicion', DB::raw('count(recursos.edicion) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_edicion->lists('edicion')));




		$busqueda_edicion = DB::table('recursos')
									->where('recursos.edicion','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
									->groupBy('recursos.edicion')
									->select('recursos.edicion', DB::raw('count(recursos.edicion) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_edicion->lists('edicion')));
		$datos_busqueda = array_unique($datos_busqueda);

		return $datos_busqueda;

	}


	public function buscador_idioma_typeahead(){

	$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_idioma = [];		  
		$busqueda_idioma = DB::table('recursos_descriptores')
									->join('descriptores', function ($join){
										$join->on('descriptores.id','=','recursos_descriptores.descriptor_id');
									})
									->join('tipo_descriptores',function($join){
										$join->on('descriptores.tipo','=','tipo_descriptores.id');
									})
					
									->where(function($query){
					           			$query->where('tipo_descriptores.id','like','%I%');
					           		})	 
					           	
									->where('descriptores.descriptor','=',$aux_data)
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('descriptores.descriptor_salida')
									->select('descriptores.descriptor_salida', DB::raw('count(descriptores.descriptor_salida) as num_recursos'))
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_idioma->lists('descriptor_salida')));

		$busqueda_idioma = DB::table('recursos_descriptores')
									->join('descriptores', function ($join){
										$join->on('descriptores.id','=','recursos_descriptores.descriptor_id');
									})
									->join('tipo_descriptores',function($join){
										$join->on('descriptores.tipo','=','tipo_descriptores.id');
									})
					
									->where(function($query){
					           			$query->where('tipo_descriptores.id','like','%I%');
					           		})	 
					           	
									->where('descriptores.descriptor','like', $aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('descriptores.descriptor_salida')
									->select('descriptores.descriptor_salida', DB::raw('count(descriptores.descriptor_salida) as num_recursos'))
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_idioma->lists('descriptor_salida')));


		$busqueda_idioma = DB::table('recursos_descriptores')
									->join('descriptores', function ($join){
										$join->on('descriptores.id','=','recursos_descriptores.descriptor_id');
									})
									->join('tipo_descriptores',function($join){
										$join->on('descriptores.tipo','=','tipo_descriptores.id');
									})
					
									->where(function($query){
					           			$query->where('tipo_descriptores.id','like','%I%');
					           		})	 
					           	
									->where('descriptores.descriptor','like', '%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('descriptores.descriptor_salida')
									->select('descriptores.descriptor_salida', DB::raw('count(descriptores.descriptor_salida) as num_recursos'))
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_idioma->lists('descriptor_salida')));


		$busqueda_idioma = DB::table('recursos_descriptores')
									->join('descriptores', function ($join){
										$join->on('descriptores.id','=','recursos_descriptores.descriptor_id');
									})
									->join('tipo_descriptores',function($join){
										$join->on('descriptores.tipo','=','tipo_descriptores.id');
									})
					
									->where(function($query){
					           			$query->where('tipo_descriptores.id','like','%I%');
					           		})	 
					           	
					           		->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('descriptores.descriptor','like', '%'.$aux.'%');
										}

									})

									->groupBy('descriptores.descriptor_salida')
									->select('descriptores.descriptor_salida', DB::raw('count(descriptores.descriptor_salida) as num_recursos'))
									
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_idioma->lists('descriptor_salida')));


		$datos_busqueda = array_unique($datos_busqueda);
		return $datos_busqueda;

	}

 	public function buscador_isbn_typeahead(){


		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $data['data'];
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_titulos = [];		  
		$busqueda_titulos = DB::table('recursos')
									->join('libros','libros.recurso_id','=','recursos.id')
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
									->where('libros.isbn', '=', $aux_data)
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('libros.isbn')
									->select('libros.isbn', DB::raw('count(libros.isbn) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('isbn')));
		

		$busqueda_titulos = DB::table('recursos')
									->join('libros','libros.recurso_id','=','recursos.id')
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
									->where('libros.isbn', 'like', $aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('libros.isbn')
									->select('libros.isbn', DB::raw('count(libros.isbn) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('isbn')));




		$busqueda_titulos = DB::table('recursos')
									->join('libros','libros.recurso_id','=','recursos.id')
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
									->where('libros.isbn', 'like', '%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('libros.isbn')
									->select('libros.isbn', DB::raw('count(libros.isbn) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('isbn'))); 
		$datos_busqueda = array_unique($datos_busqueda);


		return $datos_busqueda;

 	}

 	public function buscador_issn_typeahead(){

		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $data['data'];
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_titulos = [];		  
		$busqueda_titulos = DB::table('recursos')
									->join('publicaciones_seriadas','publicaciones_seriadas.recurso_id','=','recursos.id')
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
									->where('publicaciones_seriadas.issn', '=', $aux_data)
									->where('recursos.nivel_reg','=', 's')
									->groupBy('publicaciones_seriadas.issn')
									->select('publicaciones_seriadas.issn', DB::raw('count(publicaciones_seriadas.issn) as num_recursos'))
									
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('issn')));
		

		$busqueda_titulos = DB::table('recursos')
										->join('publicaciones_seriadas','publicaciones_seriadas.recurso_id','=','recursos.id')
										->where('recursos.tipo_liter','like',$tipo_recurso.'%')
										->where('publicaciones_seriadas.issn', 'like', $aux_data.'%')
										->where('recursos.nivel_reg','=', 's')
										->groupBy('publicaciones_seriadas.issn')
										->select('publicaciones_seriadas.issn', DB::raw('count(publicaciones_seriadas.issn) as num_recursos'))
										
										->paginate(15);
	

	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('issn')));




		$busqueda_titulos = DB::table('recursos')
									->join('publicaciones_seriadas','publicaciones_seriadas.recurso_id','=','recursos.id')
									->where('recursos.tipo_liter','like',$tipo_recurso.'%')
									->where('publicaciones_seriadas.issn', 'like', '%'.$aux_data.'%')
									->where('recursos.nivel_reg','=', 's')
									->groupBy('publicaciones_seriadas.issn')
									->select('publicaciones_seriadas.issn', DB::raw('count(publicaciones_seriadas.issn) as num_recursos'))

									->paginate(15);
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('issn'))); 
		$datos_busqueda = array_unique($datos_busqueda);

		return $datos_busqueda;
 	}


 	public function buscador_editorial_typeahead(){
		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'todo')
			$tipo_recurso = '';
		else if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}else
					$tipo_recurso = 'OTR';
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_titulos = [];		  
		$busqueda_titulos = DB::table('recursos_editoriales')
									->join('editoriales','editoriales.id','=','recursos_editoriales.editorial_id')
									->where('editoriales.editorial','=',$aux_data)
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('editoriales.editorial_salida')
									->select('editoriales.editorial_salida', DB::raw('count(editoriales.editorial_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('editorial_salida')));
		

		$busqueda_titulos = DB::table('recursos_editoriales')
									->join('editoriales','editoriales.id','=','recursos_editoriales.editorial_id')
									->where('editoriales.editorial','like',$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('editoriales.editorial_salida')
									->select('editoriales.editorial_salida', DB::raw('count(editoriales.editorial_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('editorial_salida')));


		$busqueda_titulos = DB::table('recursos_editoriales')
									->join('editoriales','editoriales.id','=','recursos_editoriales.editorial_id')
									->where('editoriales.editorial','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('editoriales.editorial_salida')
									->select('editoriales.editorial_salida', DB::raw('count(editoriales.editorial_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('editorial_salida')));

		$busqueda_titulos = DB::table('recursos_editoriales')
									->join('editoriales','editoriales.id','=','recursos_editoriales.editorial_id')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('editoriales.editorial', 'like','%'.$aux.'%');
										}

									})
								//	->where('titulos.titulo','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->groupBy('editoriales.editorial_salida')
									->select('editoriales.editorial_salida', DB::raw('count(editoriales.editorial_salida) as num_recursos'))
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);
		
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_titulos->lists('editorial_salida')));
		$datos_busqueda = array_unique($datos_busqueda);

		return $datos_busqueda;
 	}


	

 	public function order(){

 		$this->A = array(
 							array( 'id'   => 'T2020',
 								   'elem' => 'ac' 

 							),
 							array(
 								'id'   => 'T2020',
 								'elem' => 'ab' 
 							),
  							array(
 								'id'   => 'T2020',
 								'elem' => 'az'
 							),
 							array(
 								'id'   => 'T2020',
 								'elem' => 'at'
 							),	
  							array(
 								'id'   => 'T2020',
 								'elem' => 'ao'
 							),
 							array(
 								'id'   => 'T2020',
 								'elem' => 'ap'
 							),	
 							array(
 								'id'   => 'T2020',
 								'elem' => 'al'
 							),
 							array(
 								'id'   => 'T2020',
 								'elem' => 'ae'
 							),														
 				   );

 		$this->myquicksort(0,8-1,'desc');
	 	return $this->A;
 	}






///////******************************************************////////////

public function autocompletado_titulo_typeahead(){
		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}
			}
		}

		$datos_busqueda = [];
		$aux = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$datos_titulos = [];	
		
		$query = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('titulos.titulo', '=', $aux_data)
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
									->orderBy('titulos.titulo','ASC')
									->select('titulos.titulo_salida', 'titulos.id', 'recursos_titulos.tipo_tit')
									->paginate(15);
									
		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "titulo"=> $row->titulo_salida, "tipo_titulo"=> $row->tipo_tit);
			array_push($datos_titulos, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];

		$query = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('titulos.titulo','like',$aux_data.'%')
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
									->orderBy('titulos.titulo','ASC')
									->select('titulos.titulo_salida', 'titulos.id', 'recursos_titulos.tipo_tit')
									->paginate(15);
	
		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "titulo"=> $row->titulo_salida, "tipo_titulo"=> $row->tipo_tit);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];


		$query = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('titulos.titulo','like','%'.$aux_data.'%')
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
									->orderBy('titulos.titulo','ASC')
									->select('titulos.titulo_salida', 'titulos.id', 'recursos_titulos.tipo_tit')
									->paginate(15);
	
		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "titulo"=> $row->titulo_salida, "tipo_titulo"=> $row->tipo_tit);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);


		$queryq = DB::table('recursos_titulos')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
									->where(function($query){
										foreach ($this->split_data as $aux) {
											$query->where('titulos.titulo', 'like', '%'.$aux.'%');
										}

									})
									->orderBy('titulos.titulo','ASC')
									->select('titulos.titulo_salida', 'titulos.id', 'recursos_titulos.tipo_tit')
									->paginate(15);

		foreach ($queryq as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "titulo"=> $row->titulo_salida, "tipo_titulo"=> $row->tipo_tit);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		return $datos_busqueda;
	}






	public function autocompletado_autor_typeahead(){
		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		$tipo_autor = Input::get('tipo_autor');
		if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
				}
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$datos_titulos = [];		  
		$query = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','=',$aux_data)
									->where('autores.tipo_autor','=',$tipo_autor)
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->orderBy('autores.autor','ASC')
									->select('autores.*')
									->paginate(15);

		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "autor_salida"=> $row->autor_salida, "tipo_autor"=> $row->tipo_autor, "dir_electr"=> $row->dir_electr, "fecha_nac_dec"=> $row->fecha_nac_dec, "url"=> $row->url);
			array_push($datos_titulos, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];


		$query = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','like',$aux_data.'%')
									->where('autores.tipo_autor','=',$tipo_autor)
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->orderBy('autores.autor','ASC')
									->select('autores.*')
									->paginate(15);
	
	
		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "autor_salida"=> $row->autor_salida, "tipo_autor"=> $row->tipo_autor, "dir_electr"=> $row->dir_electr, "fecha_nac_dec"=> $row->fecha_nac_dec, "url"=> $row->url);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];

		$query = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.autor','like','%'.$aux_data.'%')
									->where('autores.tipo_autor','=',$tipo_autor)
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->orderBy('autores.autor','ASC')
									->select('autores.*')
									->paginate(15);
	

	
		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "autor_salida"=> $row->autor_salida, "tipo_autor"=> $row->tipo_autor, "dir_electr"=> $row->dir_electr, "fecha_nac_dec"=> $row->fecha_nac_dec, "url"=> $row->url);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];

		$query = DB::table('recursos_autores')
									->join('autores','autores.id','=','recursos_autores.cod_autor')
									->where('autores.tipo_autor','=',$tipo_autor)
									->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('autores.autor', 'like', '%'.$aux.'%');
										}

									})
									->orderBy('autores.autor','ASC')
									->select('autores.*')
									->paginate(15);

		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "autor_salida"=> $row->autor_salida, "tipo_autor"=> $row->tipo_autor, "dir_electr"=> $row->dir_electr, "fecha_nac_dec"=> $row->fecha_nac_dec, "url"=> $row->url);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];
		$datos_busqueda = array_merge($datos_busqueda,$datos_titulos);


		return $datos_busqueda;
	}

 	public function autocompletado_editorial_typeahead(){
		$data = Input::all();
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){

					$tipo_recurso = 'S';
				}
			}
		}

		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$datos_titulos = [];		  
		$query = DB::table('editoriales')
									->where('editoriales.editorial','=',$aux_data)
									->join('paises', 'paises.id','=','editoriales.pais_id')
									->orderBy('editoriales.editorial','ASC')
									//->groupBy('editoriales.editorial_salida')
									 ->select('editoriales.*', 'paises.pais')
									
									->paginate(15);
		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "editorial_salida"=> $row->editorial_salida, "fabricante" => $row->fabricante, "ciudad_fabricacion"=> $row->ciudad_fabricacion, "fecha_manufactura" => $row->fecha_manufactura, "ciudad" => $row->ciudad, "editorial"=> $row->editorial, "pais"=> $row->pais, "pais_id"=> $row->pais_id);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];

		$query = DB::table('editoriales')
									->where('editoriales.editorial','=',$aux_data)
									->join('paises', 'paises.id','=','editoriales.pais_id')
									->orderBy('editoriales.editorial','ASC')
									//->groupBy('editoriales.editorial_salida')
									 ->select('editoriales.*', 'paises.pais')
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		
		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "editorial_salida"=> $row->editorial_salida, "fabricante" => $row->fabricante, "ciudad_fabricacion"=> $row->ciudad_fabricacion, "fecha_manufactura" => $row->fecha_manufactura, "ciudad" => $row->ciudad, "editorial"=> $row->editorial, "pais"=> $row->pais, "pais_id"=> $row->pais_id);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];

		$query = DB::table('editoriales')
									->where('editoriales.editorial','=',$aux_data)
									->join('paises', 'paises.id','=','editoriales.pais_id')
									->orderBy('editoriales.editorial','ASC')
									//->groupBy('editoriales.editorial_salida')
									 ->select('editoriales.*', 'paises.pais')
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		
		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "editorial_salida"=> $row->editorial_salida, "fabricante" => $row->fabricante, "ciudad_fabricacion"=> $row->ciudad_fabricacion, "fecha_manufactura" => $row->fecha_manufactura, "ciudad" => $row->ciudad, "editorial"=> $row->editorial, "pais"=> $row->pais, "pais_id"=> $row->pais_id);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];

		$queryq = DB::table('editoriales')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('editoriales.editorial', 'like','%'.$aux.'%');
										}

									})
									->join('paises', 'paises.id','=','editoriales.pais_id')
									->orderBy('editoriales.editorial','ASC')
									//->groupBy('editoriales.editorial_salida')
									 ->select('editoriales.*', 'paises.pais')
									
									//->where('recursos_titulos.titulo_id','=','titulos.id')
									//->orWhere('titulo','like','%'$aux_data.'%')
									->paginate(15);

		
		foreach ($queryq as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "editorial_salida"=> $row->editorial_salida, "fabricante" => $row->fabricante, "ciudad_fabricacion"=> $row->ciudad_fabricacion, "fecha_manufactura" => $row->fecha_manufactura, "ciudad" => $row->ciudad, "editorial"=> $row->editorial, "pais"=> $row->pais, "pais_id"=> $row->pais_id);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);

		if( Input::get('page') != -1){
		$perPage = 15;
		$currentPage = Input::get('page') - 1;
		$pagedData = array_slice($datos_busqueda, $currentPage * $perPage, $perPage);
		$matches = Paginator::make($pagedData, count($datos_busqueda), $perPage);
		return $matches;
		}else
		return json_encode($datos_busqueda);
 	}


	public function buscador_simple2(){
		$data = Input::all();
		$num_items_pagina = 10;
		$orden = '';


		if(Input::get('num_items_pagina')){
			$num_items_pagina = Input::get('num_items_pagina');
		}
		if(Input::get('orden')){
			$orden = Input::get('orden');
		//	return $orden;
		}
		$nivel_reg = '';
		$tipo_recurso = Input::get('tipo_recurso');
		if($tipo_recurso == 'tesis'){
			$tipo_recurso = 'T';
		}else{
			if($tipo_recurso == 'monografia'){
				$tipo_recurso = 'M';
			}else{
				if($tipo_recurso == 'publicacion_seriada'){
					$tipo_recurso = 'S';
					$nivel_reg = 's';
				}
			}
		}

		

		$aux_data = $this->convetir_string(strtoupper($data['data']));
		$this->split_data = explode(' ',$aux_data);
		$datos_busqueda = [];
		$datos_busqueda2 = []; 
		
		//return $aux_data;

		$busqueda_titulos = DB::table('recursos_titulos')
						->join('recursos', 'recursos.id','=','recursos_titulos.recurso_id')
						->where('recursos.nivel_reg','like',$nivel_reg.'%')
			 			->where('recursos_titulos.tipo_tit','=','OP')
			 			->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
			 			->where('recursos_titulos.ext_recurso_id','=',0)
						->where('titulos.titulo','=', $aux_data)
						->join('titulos','titulos.id','=','recursos_titulos.titulo_id') 
					//	->orderBy('titulos.titulo_salida','ASC')
						->select('recursos_titulos.recurso_id','titulos.titulo_salida')
						->get();
				
		$aux_array = array_fetch((array)$busqueda_titulos, 'recurso_id');
		$aux_array2 = array_fetch((array)$busqueda_titulos, 'titulo_salida');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);
		$datos_busqueda23 = array_merge($datos_busqueda2, $aux_array2);
		


		$busqueda_titulos = DB::table('recursos_titulos')
		                ->join('recursos', 'recursos.id','=','recursos_titulos.recurso_id')
						->where('recursos.nivel_reg','like',$nivel_reg.'%')
			 			->where('recursos_titulos.tipo_tit','=','OP')
			 			->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
			 			->where('recursos_titulos.ext_recurso_id','=',0)
						->where('titulos.titulo','like', $aux_data.'%')
						->join('titulos','titulos.id','=','recursos_titulos.titulo_id') 

					//	->orderBy('titulos.titulo_salida','ASC')
						->select('recursos_titulos.recurso_id','titulos.titulo_salida')
						->get();
				
		$aux_array = array_fetch((array)$busqueda_titulos, 'recurso_id');
		$aux_array2 = array_fetch((array)$busqueda_titulos, 'titulo_salida');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);
		$datos_busqueda2 = array_merge($datos_busqueda2, $aux_array2);


		$busqueda_titulos = DB::table('recursos_titulos')
						->join('recursos', 'recursos.id','=','recursos_titulos.recurso_id')
						->where('recursos.nivel_reg','like',$nivel_reg.'%')
						->join('titulos','titulos.id','=','recursos_titulos.titulo_id')  
					//	->join('tipotitulo','tipotitulo.tipo_doc','=','recursos_titulos.tipo_doc')    
						->where('recursos_titulos.tipo_tit','=','OP')
						->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
						->where('recursos_titulos.ext_recurso_id','=',0)
						->where('titulos.titulo','like','%'.$aux_data.'%')
				//		->orderBy('titulos.titulo_salida','ASC')
						->select('recursos_titulos.recurso_id','titulos.titulo_salida')
						->get();

		$aux_array = array_fetch((array)$busqueda_titulos, 'recurso_id');
		$aux_array2 = array_fetch((array)$busqueda_titulos, 'titulo_salida');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);
		$datos_busqueda2 = array_merge($datos_busqueda2, $aux_array2);


		$busqueda_titulos = DB::table('recursos_titulos')
									->join('recursos', 'recursos.id','=','recursos_titulos.recurso_id')
						            ->where('recursos.nivel_reg','like',$nivel_reg.'%')
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->where('recursos_titulos.ext_recurso_id','=',0)
									->where('recursos_titulos.tipo_tit','=','OP')
									->where('recursos_titulos.tipo_doc','like',$tipo_recurso.'%')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('titulos.titulo', 'like', '%'.$aux.'%');
										}

									})
								//	->where('titulos.titulo','like','%'.$aux_data.'%')
								//	->orderBy('titulos.titulo','DESC')
									->select('recursos_titulos.recurso_id','titulos.titulo_salida')
									->get();
		$aux_array = array_fetch((array)$busqueda_titulos, 'recurso_id');	
		$aux_array2 = array_fetch((array)$busqueda_titulos, 'titulo_salida');						
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);		
		$datos_busqueda2 = array_merge($datos_busqueda2, $aux_array2);

		$busqueda_autores = DB::table('recursos_autores')
						->join('recursos', 'recursos.id','=','recursos_autores.recurso_id')
						->where('recursos.nivel_reg','like',$nivel_reg.'%')
		                ->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
		                ->where('recursos_autores.ext_recurso_id','=',0)
						->where('autores.autor','=',$aux_data)
						->join('autores','autores.id','=','recursos_autores.cod_autor')      				
						->select('recursos_autores.recurso_id','autores.autor_salida')
						->get();
		$aux_array = array_fetch((array)$busqueda_autores, 'recurso_id');
		$aux_array2 = array_fetch((array)$busqueda_autores, 'autor_salida');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);
		$datos_busqueda2 = array_merge($datos_busqueda2, $aux_array2);


		$busqueda_autores = DB::table('recursos_autores')
						->join('recursos', 'recursos.id','=','recursos_autores.recurso_id')
						->where('recursos.nivel_reg','like',$nivel_reg.'%')
						->where('recursos_autores.tipo_doc','like',$tipo_recurso.'%')
						->where('recursos_autores.ext_recurso_id','=',0)
						->where('autores.autor','like','%'.$aux_data.'%')
						->join('autores','autores.id','=','recursos_autores.cod_autor')      				
						->select('recursos_autores.recurso_id','autores.autor_salida')
						->get();
		$aux_array = array_fetch((array)$busqueda_autores, 'recurso_id');
		$aux_array2 = array_fetch((array)$busqueda_titulos, 'autor_salida');

	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);
		$datos_busqueda2 = array_merge($datos_busqueda2, $aux_array2);

		if($tipo_recurso == 'M' || $tipo_recurso == 'S'){
			$busqueda_editoriales = DB::table('recursos_editoriales')
							->join('recursos', 'recursos.id','=','recursos_editoriales.recurso_id')
						    ->where('recursos.nivel_reg','like',$nivel_reg.'%')
			                ->where('recursos_editoriales.tipo_doc','like',$tipo_recurso.'%')
			                ->where('recursos_editoriales.ext_recurso_id','=',0)
							->where('editoriales.editorial','=',$aux_data)
							->join('editoriales','editoriales.id','=','recursos_editoriales.editorial_id')      				
							->select('recursos_editoriales.recurso_id','editoriales.editorial_salida')
							->get();

			$aux_array = array_fetch((array)$busqueda_editoriales, 'recurso_id');
			$aux_array2 = array_fetch((array)$busqueda_titulos, 'editorial_salida');	

		//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
			$datos_busqueda = array_merge($datos_busqueda, $aux_array);
			$datos_busqueda2 = array_merge($datos_busqueda2, $aux_array2);


			$busqueda_editoriales = DB::table('recursos_editoriales')
			                ->join('recursos', 'recursos.id','=','recursos_editoriales.recurso_id')
						    ->where('recursos.nivel_reg','like',$nivel_reg.'%')
			                ->where('recursos_editoriales.tipo_doc','like',$tipo_recurso.'%')
			                ->where('recursos_editoriales.ext_recurso_id','=',0)
							->where('editoriales.editorial','like','%'.$aux_data.'%')
							->join('editoriales','editoriales.id','=','recursos_editoriales.editorial_id')      				
							->select('recursos_editoriales.recurso_id','editoriales.editorial_salida')
							->get();
			$aux_array = array_fetch((array)$busqueda_editoriales, 'recurso_id');
			$aux_array2 = array_fetch((array)$busqueda_titulos, 'editorial_salida');
		//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
			$datos_busqueda = array_merge($datos_busqueda, $aux_array);
			$datos_busqueda2 = array_merge($datos_busqueda2, $aux_array2);

			$busqueda_cotas = DB::table('recursos')
					   ->where(function($query) use($aux_data){
					  		foreach ($this->split_data as $aux) {
					  			$query->where('ubicacion','like','%'.$aux.'%')
					  			->where('ext_id',0)
					  			->where('tipo_liter','M');
					  		}
					   })

				  ->select('recursos.id','recursos.ubicacion')
				  ->get();
			$aux_array = array_fetch((array)$busqueda_cotas, 'id');		
			$aux_array2 = array_fetch((array)$busqueda_cotas, 'ubicacion');		
			
			$datos_busqueda = array_unique(array_merge($datos_busqueda,$aux_array));
			

			$datos_busqueda2 = array_merge($datos_busqueda2, $aux_array2);
	

			
		}else if ($tipo_recurso == 'T'){
			$busqueda_dependencias = DB::table('recursos_dependencias')
			                ->join('dependencias','dependencias.id','=','recursos_dependencias.dependencia_id')  
			                ->join('recursos', 'recursos.id','=','recursos_dependencias.recurso_id')
						    ->where('recursos.nivel_reg','like',$nivel_reg.'%')
			                ->where('recursos_dependencias.ext_recurso_id','=',0)
							->where('dependencias.dependencia','=',$aux_data)
							    				
							->select('recursos_dependencias.recurso_id','dependencias.dependencia_salida')
							->get();
			$aux_array = array_fetch((array)$busqueda_dependencias, 'recurso_id');
			$aux_array2 = array_fetch((array)$busqueda_dependencias, 'dependencia_salida');

				
		//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
			$datos_busqueda = array_merge($datos_busqueda, $aux_array);
			$datos_busqueda2 = array_merge($datos_busqueda2, $aux_array2);

			$busqueda_dependencias = DB::table('recursos_dependencias')
			                ->join('dependencias','dependencias.id','=','recursos_dependencias.dependencia_id')
			                ->join('recursos', 'recursos.id','=','recursos_dependencias.recurso_id')
						    ->where('recursos.nivel_reg','like',$nivel_reg.'%') 
			                ->where('recursos_dependencias.ext_recurso_id','=',0)
							->where('dependencias.dependencia','like', '%'.$aux_data.'%')     	
							->select('recursos_dependencias.recurso_id','dependencias.dependencia_salida')
							->get();
			$aux_array = array_fetch((array)$busqueda_dependencias,'recurso_id');
			$aux_array2 = array_fetch((array)$busqueda_dependencias, 'recurso_id');
			$datos_busqueda = array_unique(array_merge($datos_busqueda,$aux_array));
			$busqueda_cotas = DB::table('recursos')
		 						   ->where(function($query) use($aux_data){
		 						  		foreach ($this->split_data as $aux) {
		 						  			$query->where('ubicacion','like','%'.$aux.'%')
		 						  			->where('ext_id',0)
		 						  			->where('tipo_liter','T');
		 						  		}
		 						   })
								  ->select('recursos.id','recursos.ubicacion')
								  ->get();
			$aux_array = array_fetch((array)$busqueda_cotas, 'id');
			$aux_array2 = array_fetch((array)$busqueda_cotas, 'ubicacion');	

			$datos_busqueda = array_unique(array_merge($datos_busqueda,$aux_array));
			$datos_busqueda2 = array_merge($datos_busqueda2, $aux_array2);



		}


		$busqueda_id = DB::table('ejemplares')
						->where('ejemplares.id','like','%'.$aux_data.'%') 				
						->where('ejemplares.ext_recurso_id','=',0) 
						->select('ejemplares.recurso_id')
						->get();
		$aux_array = array_fetch((array)$busqueda_id, 'recurso_id');
		
	//	$datos_busqueda = array_unique(array_merge($datos_busqueda, $aux_array));
		$datos_busqueda = array_merge($datos_busqueda, $aux_array);
		$datos_busqueda = array_unique($datos_busqueda);


		if($orden!=''  && $orden!='normal'){
			$datos_busqueda = $this->gestionar_orden($datos_busqueda,$orden);
		}
		$perPage = $num_items_pagina;
		$currentPage = Input::get('page') - 1;
		 try {
		 	if($data['autocompletado']){
				$pagedData = array_slice($datos_busqueda2, $currentPage * $perPage, $perPage);
				$matches = Paginator::make($pagedData, count($datos_busqueda2), $perPage);	
			}
		 	
		 } catch (Exception $e) {
		 	$pagedData = array_slice($datos_busqueda, $currentPage * $perPage, $perPage);
			$matches = Paginator::make($pagedData, count($datos_busqueda), $perPage);	
		 	
		 }
		
		
		
		return $matches;


	}


	public function autocompletado_conferencia_typeahead(){
		$data = Input::all();
		

		$datos_busqueda = [];
		$this->split_data = [];
		$aux = [];
		$aux_data = $this->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$datos_conferencias = [];	

		
		$query = DB::table('conferencias')
									->where('conferencias.conferencia','like',$aux_data.'%')
									->orderBy('conferencias.conferencia','ASC')
									->select('conferencias.*')
									->paginate(15);

		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "nombre"=> $row->conferencia_salida, "ciudad" => $row->ciudad, "patrocinador"=> $row->patrocinador, "fecha" => $row->fecha, "pais_id"=> $row->pais_id);
			array_push($datos_conferencias, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_conferencias), SORT_REGULAR);
		$datos_conferencias = [];

		$query = DB::table('conferencias')
									->where('conferencias.conferencia','like','%'.$aux_data.'%')
									->orderBy('conferencias.conferencia','ASC')
									->select('conferencias.*')
									->paginate(15);

		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "nombre"=> $row->conferencia_salida, "ciudad" => $row->ciudad, "patrocinador"=> $row->patrocinador, "fecha" => $row->fecha, "pais_id"=> $row->pais_id);
			array_push($datos_conferencias, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_conferencias), SORT_REGULAR);
		$datos_conferencias = [];

		$query = DB::table('conferencias')
									->where(function($query){
										foreach ($this->split_data as $aux) {
											$query->where('conferencias.conferencia', 'like', '%'.$aux.'%');
										}
									})
									->orderBy('conferencias.conferencia','ASC')
									->select('conferencias.*')
									->paginate(15);

		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "nombre"=> $row->conferencia_salida, "ciudad" => $row->ciudad, "patrocinador"=> $row->patrocinador, "fecha" => $row->fecha, "pais_id"=> $row->pais_id);
			array_push($datos_conferencias, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_conferencias), SORT_REGULAR);
		$datos_conferencias = [];

		return $datos_busqueda;
									

	}

	public function autocompletado_grado_acad_tesis(){
		$data = Input::all();
		$datos_busqueda = [];
		$aux = [];
		$this->split_data = [];
		$aux_data = $data['data'];
		$this->split_data =  explode(' ',$aux_data);
		$datos_grado = [];	
		
		$query = DB::table('tesis')
				->where('grado_acad', '=', $aux_data)
				->orderBy('grado_acad','ASC')
				->select('grado_acad')
				->paginate(15);
									
		foreach ($query as $row) {
			$aux = [];
			$aux = array("grado_academico" => $row->grado_acad);
			array_push($datos_grado, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_grado), SORT_REGULAR);
		$datos_grado = [];

		$query = DB::table('tesis')
				->where('grado_acad','like', $aux_data.'%')
				->orderBy('grado_acad','ASC')
				->select('grado_acad')
				->paginate(15);
	
		foreach ($query as $row) {
			$aux = [];
			$aux = array("grado_academico" => $row->grado_acad);
			array_push($datos_grado, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_grado), SORT_REGULAR);
		$datos_grado = [];


		$query = DB::table('tesis')
				->where('grado_acad','like','%'.$aux_data.'%')
				->orderBy('grado_acad','ASC')
				->select('grado_acad')
				->paginate(15);
	
		foreach ($query as $row) {
			$aux = [];
			$aux = array("grado_academico" => $row->grado_acad);
			array_push($datos_grado, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_grado), SORT_REGULAR);


		$queryq = DB::table('tesis')
					->where(function($query){
						foreach ($this->split_data as $aux) {
							$query->where('grado_acad', 'like', '%'.$aux.'%');
						}

					})
					->orderBy('grado_acad','ASC')
					->select('grado_acad')
					->paginate(15);

		foreach ($query as $row) {
			$aux = [];
			$aux = array("grado_academico" => $row->grado_acad);
			array_push($datos_grado, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_grado), SORT_REGULAR);
		return $datos_busqueda;
	}



}
