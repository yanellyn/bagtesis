<?php

class RecursosDescriptoresController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /recursosdescriptores
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return RecursoDescriptor::take(15)->skip(0)->get();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /recursosdescriptores/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /recursosdescriptores
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /recursosdescriptores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	//	return Recurso::find($id)->descriptores;


//	http://localhost/tesis/public/recursos_descriptores/T043500000011
//	http://localhost/tesis/public/recursos_descriptores/T043500008723


	return Recurso::extend_descriptores($id);

	//return Recurso::find($id)->descriptores;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /recursosdescriptores/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /recursosdescriptores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /recursosdescriptores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}