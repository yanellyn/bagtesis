<?php

class CamposAdicionalesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /camposadicionales
	 *
	 * @return Response
	 */
	public function index()
	{
		$tipo_id = Input::get("tipo_id");
		$query_campos = DB::table('camposadicionales_tipodoc')
			->join('campos_adicionales','campos_adicionales.id','=', 'camposadicionales_tipodoc.campo_adicional')
			->where('camposadicionales_tipodoc.tipo_doc', '=', $tipo_id)
			->select('campos_adicionales.*')
			->get();
		return $query_campos;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /camposadicionales/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /camposadicionales
	 *
	 * @return Response
	 */
	public function store($dataCamposAdicionales = null){
		
		if($dataCamposAdicionales){
			$data = $dataCamposAdicionales;
		}else{
			$data = Input::all();
		}

		
		$query_campo = DB::table('campos_adicionales')
			->where('campos_adicionales.campo', '=',strtoupper( App::make('BuscadoresController')->convetir_string($data['campo'])))
			->select('campos_adicionales.*')
			->first();
		
		if($query_campo){
			$query_campos_tipodoc = DB::table('camposadicionales_tipodoc')
						->where('camposadicionales_tipodoc.tipo_doc', '=', $data['tipo_id'])
						->where('camposadicionales_tipodoc.campo_adicional', '=',$query_campo->id)
						->select('camposadicionales_tipodoc.*')
						->first();

			if(!$query_campos_tipodoc){
				$campos_tipodoc = new CampoAdicionalTipoDoc;
				$campos_tipodoc->tipo_doc = $data['tipo_id'];
				$campos_tipodoc->campo_adicional = $query_campo->id;
				$campos_tipodoc->save();

				$respuesta = array("codigo" => "0", "id_campo" => $query_campo->id);
			}else{
				$respuesta = array("codigo" => "1", "id_campo" => $query_campo->id);
			}
		}else{
			$campo_adicional = new CampoAdicional;
			$campo_adicional->campo = strtoupper(App::make('BuscadoresController')->convetir_string($data['campo']));
			$campo_adicional->campo_salida = $data['campo'];
			$campo_adicional->save();

			$campos_tipodoc = new CampoAdicionalTipoDoc;
			$campos_tipodoc->tipo_doc = $data['tipo_id'];
			$campos_tipodoc->campo_adicional = $campo_adicional->id;
			$campos_tipodoc->save();

			$respuesta = array("codigo" => "0", "id_campo" => $campo_adicional->id);
		}
		if($dataCamposAdicionales)
			return $respuesta;
		else
			return json_encode($respuesta);
		
	}

	/**
	 * Display the specified resource.
	 * GET /camposadicionales/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /camposadicionales/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /camposadicionales/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();
		
		$campo_actualizado = DB::table('campos_adicionales')
			->where('id','=',$id)
			->update(array('campo' => strtoupper(App::make('BuscadoresController')->convetir_string($data['campo'])), 'campo_salida' => $data['campo']));

		$respuesta = array("codigo" => "0");
		return $respuesta;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /camposadicionales/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Input::all();

		$delete_camposadicionales_doc = DB::table('recursos_camposadicionales')
			->where('recursos_camposadicionales.tipo_doc', '=',$data["id_tipo"])
			->where('recursos_camposadicionales.campo_adicional', '=',$id)
			->delete();

		$delete_camposadicionales_doc = DB::table('camposadicionales_tipodoc')
			->where('camposadicionales_tipodoc.tipo_doc', '=',$data["id_tipo"])
			->where('camposadicionales_tipodoc.campo_adicional', '=',$id)
			->delete();

		$delete_camposadicionales_doc = DB::table('camposadicionales_tipodoc')
			->where('camposadicionales_tipodoc.tipo_doc', '=',$data["id_tipo"])
			->where('camposadicionales_tipodoc.campo_adicional', '=',$id)
			->delete();

		$camposadicionales_tipodoc = DB::table('camposadicionales_tipodoc')
			->where('camposadicionales_tipodoc.campo_adicional', '=',$id)
			->select('camposadicionales_tipodoc.*')
			->first();
			
		if(!$camposadicionales_tipodoc){
			$delete_camposadicionales_doc = DB::table('campos_adicionales')
				->where('campos_adicionales.id', '=',$id)
				->delete();
		}

		$respuesta = array("codigo" => "0");
		return $respuesta;
	}



	public function buscador_campos_typeahead(){
		$data = Input::all();
		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = App::make('BuscadoresController')->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$busqueda_campos = [];		  
		$busqueda_campos = DB::table('campos_adicionales')
									->where('campos_adicionales.campo','=',$aux_data)
									->orderBy('campos_adicionales.campo','ASC')
									->select('campos_adicionales.campo_salida')
									->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_campos->lists('campo_salida')));

		$busqueda_campos = DB::table('campos_adicionales')
								->where('campos_adicionales.campo','like', $aux_data.'%')
								->orderBy('campos_adicionales.campo','ASC')
								->select('campos_adicionales.campo_salida')
								->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_campos->lists('campo_salida')));

		$busqueda_campos = DB::table('campos_adicionales')
								->where('campos_adicionales.campo','like','%'.$aux_data.'%')
								->orderBy('campos_adicionales.campo','ASC')
								->select('campos_adicionales.campo_salida')
								->paginate(15);

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_campos->lists('campo_salida')));
		
		$busqueda_campos = DB::table('campos_adicionales')
								->where('campos_adicionales.campo','like','%'.$aux_data.'%')
								->orderBy('campos_adicionales.campo','ASC')
								->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('campos_adicionales.campo', 'like', '%'.$aux.'%');
										}

									})
								->select('campos_adicionales.campo_salida')
								->paginate(15);
	
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_campos->lists('campo_salida')));
		$datos_busqueda = array_unique($datos_busqueda);

		return $datos_busqueda;
	}


}