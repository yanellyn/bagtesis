<?php

class PublicacionesSeriadasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /publicacionesseriadas
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return PublicacionesSeriada::take(15)->skip(0)->get();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /publicacionesseriadas/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /publicacionesseriadas
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /publicacionesseriadas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /publicacionesseriadas/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /publicacionesseriadas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /publicacionesseriadas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function agregar_publicacion($dataPublicacionImportar = null){
		
		if($dataPublicacionImportar){
			$data = $dataPublicacionImportar;
		}else{
			$data = Input::all();
		}
		$fecha_actual = date('Ymd');
		$aux = 0;
	//	echo $fecha_actual;
		$codigoRespuesta= "0"; //recurso agregado 1 recurso existe, 2 error agregando el recurso

		if($data["bibliotecaSelect"] != 1){ // si no es de la bag utiliza T1
			$aux_id ='T1'; 
		}else{
			$aux_id ='T0'; 
		}
	    $last_id_recurso = "";
	    $query_id_recurso = "";
	  
	    $query_id_recurso = DB::table('recursos')
	                      ->where('id','like',$aux_id.'%')
	                      ->select(DB::raw('max(recursos.id) as recursos_id'))->first();

	    $id_recurso_actual= $query_id_recurso->recursos_id;
	    $int_id_recurso = filter_var($id_recurso_actual, FILTER_SANITIZE_NUMBER_INT);
	    $id_recurso_nuevo = $aux_id.(string)($int_id_recurso + 1);


	    //Obtener Jerarquia para nuevo recurso
		$query_jerarquia = DB::table('recursos')
		                  ->select(DB::raw('max(recursos.jerarquia) as max_jerarquia'))->first();

		$jerarquia_actual= $query_jerarquia->max_jerarquia;

		//if ($tipo_recurso == 'M' || $tipo_recurso == 'T'){
		$jerarquia_nuevo = $jerarquia_actual + 1;
		//}else{
		//	$jerarquia_nuevo = $jerarquia_actual;
		//}
		$queryRecurso = DB::table('recursos')
				->join('recursos_titulos','recursos_titulos.recurso_id','=','recursos.id')
				->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
				->where('titulos.titulo','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo'])))
				->where('recursos.tipo_liter', '=', 'S')
				->where('recursos.nivel_reg', '=', 's')
				->select('recursos.*')->first();

		$queryTitulo = DB::table('titulos')
				->where('titulo','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo'])))
				->select('titulos.*')->first();
				
		if($queryRecurso){
			if($queryRecurso->ext_id == 0){
				$codigoRespuesta = "1";
				$respuesta = array("codigo" => "1", "id_recurso" => "");
				if($dataPublicacionImportar)
					return $respuesta;
				else
					return json_encode($respuesta);
			}else{

				$queryRecurso = DB::table('recursos_titulos')
				->where('recursos_titulos.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_editoriales')
				->where('recursos_editoriales.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_descriptores')
				->where('recursos_descriptores.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_bibliotecas')
				->where('recursos_bibliotecas.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecursoR = DB::table('recursos')
				->where('recursos.id','=', $queryRecurso->id)
				->select('recursos.*');

				$queryRecurso = DB::table('recursos')
				->where('jerarquia','=', $queryRecursoR->jerarquia)
				->delete();

				$queryRecurso = DB::table('recursos')
				->where('recursos.id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('publicaciones_seriadas')
				->where('publicaciones_seriadas.recurso_id','=', $queryRecurso->id)
				->delete();
				$aux = 1;
			}
		}
		
		if(!$queryRecurso || $aux == 1){
			$recurso = new Recurso;	
			$recurso->id = $id_recurso_nuevo;
			$recurso->ext_id = 0;//
			$recurso->jerarquia = $jerarquia_nuevo;
	        $recurso->tipo_liter = $data['tipoLiter'];
	        $recurso->nivel_reg = 's';
	        $recurso->impresion = $data['impresion'];
	        $recurso->fecha_catalogacion = $fecha_actual;
	        $recurso->id_catalogador = $data['idCatalogador'];
	        $recurso->datos_adicionales = $data['datosAdicionales'];
	        $recurso->notas = $data['notas'];
	        $recurso->resumen = $data['resumen'];
	        $recurso->url = $data['url'];
	 		$recurso->save();


			$recurso_biblioteca = new RecursosBiblioteca;	
				$recurso_biblioteca->recurso_id = $recurso->id;
				$recurso_biblioteca->ext_recurso_id =  $recurso->ext_id;
				$recurso_biblioteca->biblioteca_id = $data["bibliotecaSelect"];
				$recurso_biblioteca->save();

			if($queryTitulo){
				$idTitulo = $queryTitulo->id;	
			}else{
				$titulo = new Titulo;	
				$titulo->caracter_orden = 0;
				$titulo->titulo_salida = $data['nombreTitulo'];
				$titulo->titulo =  strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']));
				$titulo->save();
				$idTitulo = $titulo->id;		
			}

			$recurso_titulo = new RecursosTitulo;	
				$recurso_titulo->recurso_id = $recurso->id;
				$recurso_titulo->ext_recurso_id =  $recurso->ext_id;
				$recurso_titulo->titulo_id = $idTitulo;
				$recurso_titulo->tipo_tit = 'OP';
				$recurso_titulo->tipo_doc = 'S';
				$recurso_titulo->portada = ''; 
				$recurso_titulo->orden = 0;
				$recurso_titulo->save();

			$publicacion = new PublicacionesSeriada;	
				$publicacion->recurso_id = $recurso->id;
				$publicacion->ext_recurso_id =  $recurso->ext_id;
				$publicacion->tipo =$data['tipo'];
				$publicacion->regularidad = $data['regularidad'];
				$publicacion->periodo = $data['periodicidad'];
				$publicacion->vols_aso = $data['vols'];
				$publicacion->nros_aso = $data['nros'];
				$publicacion->issn = $data['issn'];
				$publicacion->url = $data['url'];
				$publicacion->impresion = $data['impresion'];
				$publicacion->disemina = $data['diseminacion'];
				$publicacion->existencia = $data['existencia'];
				$publicacion->nivel_reg = 's';
				$publicacion->save();

			date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data['idCatalogador'];
			$historial->recurso_id = $recurso->id;
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Agregar'; 
			$historial->modulo = 'Ficha Principal';
			$historial->fecha = $date;
			$historial->save();

			$respuesta = array("codigo" => "0" , "id_recurso"=> $recurso->id, "id_titulo"=>  $idTitulo);
			if($dataPublicacionImportar)
				return $respuesta;
			else
				return json_encode($respuesta);
		}
	}


	public function guardar_publicacion(){
		$data = Input::all();
		$fecha_actual = date('Ymd');

		$verificarTituloExistente = DB::table('recursos')
				->join('recursos_titulos','recursos_titulos.recurso_id','=','recursos.id')
				->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
				->where('titulos.titulo','=', App::make('BuscadoresController')->convetir_string($data['nombreTitulo']))
				->where('recursos.id','!=',$data['idRecurso'])
				->where('recursos.tipo_liter', '=', 'S')
				->where('recursos.nivel_reg', '=', 's')
				->select('recursos.*')->first();
		
		if(!$verificarTituloExistente){
			$queryRecurso = DB::table('recursos')
					->where('recursos.id','=',$data['idRecurso'])
					->update(array( 			
						        'datos_adicionales' => $data['datosAdicionales'],
						        'notas' => $data['notas'],
						        'resumen' => $data['resumen']
					 			)
							);

			$recurso = DB::table('recursos')
					->where('recursos.id','=',$data['idRecurso'])
					->select('recursos.*')->first();

			if($data["idTitulo"]!= ""){
				$queryTitulo = DB::table('titulos')
				->where('id','=',$data['idTitulo'])
				->update(array('titulo_salida' => $data['nombreTitulo'], 'titulo' => strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']))));

				$queryTitulo = DB::table('titulos')
				->where('id','=',$data['idTitulo'])
				->select('titulos.*')->first();
				$idTitulo = $queryTitulo->id;		

				
			}else{
				
				$titulo = new Titulo;	
				$titulo->caracter_orden = 0;
				$titulo->titulo_salida = $data['nombreTitulo'];
				$titulo->titulo =  strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']));
				$titulo->save();
				$idTitulo = $titulo->id;	

				$recurso_titulo = new RecursosTitulo;	
				$recurso_titulo->recurso_id = $recurso->id;
				$recurso_titulo->ext_recurso_id =  $recurso->ext_id;
				$recurso_titulo->titulo_id = $idTitulo;
				$recurso_titulo->tipo_tit = 'OP';
				$recurso_titulo->tipo_doc = 'S';
				$recurso_titulo->portada = ''; 
				$recurso_titulo->orden = 0;
				$recurso_titulo->save();	
			}

			$libro = DB::table('publicaciones_seriadas')
				->where('recurso_id','=', $recurso->id)
				->update(array('issn' => $data['issn'],
						'tipo' => $data['tipo'],
						'regularidad' => $data['regularidad'],
						'periodo' => $data['periodicidad'],
						'vols_aso' => $data['vols'],
						'nros_aso' => $data['nros'],
						'issn' => $data['issn'],
						'url'   => $data['url'], 
						'disemina' => $data['diseminacion'],
						'impresion' => $data['impresion'],
						'existencia' => $data['existencia'],

					)
				);

			$biblioteca = DB::table('recursos_bibliotecas')
			->where('recursos_bibliotecas.recurso_id','=',$data['idRecurso'])
			->update(array('biblioteca_id' => $data["bibliotecaSelect"]));

			date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data['idCatalogador'];
			$historial->recurso_id = $data['idRecurso'];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Modificar'; 
			$historial->modulo = 'Ficha Principal';
			$historial->fecha = $date;
			$historial->save();
			$respuesta = array("codigo" => "0");
			return json_encode($respuesta);
		}else{
			$respuesta = array("codigo" => "1");
				return json_encode($respuesta);
		}
		
	}

	public function eliminar_publicacion(){

		$data = Input::all();

		$recurso = DB::table('recursos')
		->where('id','=',$data['idRecurso'])
		->update(array('ext_id' => 1));

		date_default_timezone_set('America/Caracas');
		$historial = new HistorialCatalogacion;
		$date = date('YmdHis', time());
		$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
		$historial->recurso_id = $data['idRecurso'];
		$historial->ext_recurso_id = '1';
		$historial->operacion = 'Eliminar'; 
		$historial->modulo = 'Ficha Principal';
		$historial->fecha = $date;
		$historial->save();
		
		$respuesta = array("codigo" => "0");
		return json_encode($respuesta);
 
	}
	
}