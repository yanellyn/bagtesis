<?php

class UsuariosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /usuarios
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return Test::all();
	}

	/**
	 * Check Internet Connection.
	 * 
	 * @param string $sCheckHost Default: http://www.google.com
	 * @return boolean
	 */
	public static function check_internet_connection($sCheckHost = 'http://www.google.com') 
	{
	    return (bool) @fopen($sCheckHost, 'r');
	}

	/**
	 * Tareas CRON JOBS
	 * para enviar correos automaticamente a los usuarios que deban entregar recursos mañana.
	 *
	 */
	public function cron_job_deuda(){


		if(UsuariosController::check_internet_connection()){

			$hoy = date('d-m-y');
			$hoy_format = date('Ymd');
			$ultima_revision = VariablesReporte::where('tipo', '=','CRON_JOB_aviso_diario_usuarios_con_prestamo')
											   ->where('nombre', '=', $hoy)->get()->first();

			if(empty($ultima_revision) ){

				$id_usuarios = array();
				$mañana = date('Ymd', strtotime($hoy_format.'+1 weekdays' ));
				$mañana_format = date('d/m/Y', strtotime($hoy_format.'+1 weekdays' ));
				$prestamos = Prestamo::where('fecha_exp','=', $mañana)->get();
				//$prestamos = Prestamo::where('fecha_exp','=', '20140404')->get();
				foreach ($prestamos as $key => $value) {
					$id_usuarios[$key] = $value['id'];
					$usuario = Usuario::where('id', '=', $value['id'])->get()->first();
					if(!empty($usuario['dir_electr'])){

						$ejem = Ejemplar::where('id', '=', $value['id_ejemplar'])->get()->first();
						$id_recurso = $ejem['recurso_id'];

						$rec_contr = new RecursosController;
						$aux_rec = $rec_contr->show($id_recurso);
						$aux_rec['autores'] = $rec_contr->recurso_autores($id_recurso);
						$aux_rec['descriptores'] = $rec_contr->recurso_descriptores($id_recurso);
						$aux_rec['fecha_exp'] = $mañana_format;
						
						Mail::send('emails.aviso_devolucion', array('recurso' => $aux_rec), function($message) use ($usuario)
						{
						    $message->to('angklun@gmail.com', $usuario['nombres'].' '.$usuario['apellidos'])->subject('Aviso de devolución - Biblioteca Alonso Gamero');
						    //$message->to($usuario['dir_electr'], $usuario['nombres'].' '.$usuario['apellidos'])->subject('Aviso de devolución - Biblioteca Alonso Gamero');
						});
						

					}
				}


				VariablesReporte::incrementar_variable('CRON_JOB_aviso_diario_usuarios_con_prestamo');
			}
			return json_encode(1);
		}
		
		return json_encode(0);
	}



	/**
	 * funcion para enviar correo de contacto
	 *
	 */
	public function contacto(){
		
		$data = Input::all();

		//enviar a lista de pendientes
		$pendiente = new Pendiente;
		$pendiente->destinatarios = $data['correo'];
		$pendiente->contenido = $data['contenido'];
		$pendiente->prioridad = 1;
		date_default_timezone_set('America/Caracas');
		$hoy = date('Ymd');
		$pendiente->fecha_registrado = $hoy;
		$pendiente->save();


		//retornar data

		return $data;
	
	}
	

	/**
	 * Show the form for creating a new resource.
	 * GET /usuarios/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /usuarios
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /usuarios/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /usuarios/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /usuarios/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /usuarios/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function doLogin()
	{
		//return Input::get('username');
		//return Input::json()->all();
		//return Input::all();
		//return json_encode((array)Input::all());
		
		// validate the info, create rules for the inputs
		$rules = array(
		    'username'    => 'required|email', // make sure the email is an actual email
		    'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);
		
		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return json_encode($validator->messages());
		
		    //return Redirect::to('login')
		    //    ->withErrors($validator) // send back all errors to the login form
		    //    ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {

		    // create our user data for the authentication
		    $userdata = array(
		        'dir_electr'     => Input::get('username'),
		        'password'  => Input::get('password'),
		    );
		    /*
	    	Test user  "aalfonzo289@gmail.com"  password "1234" CON HASH
				UPDATE usuarios
			   SET password='$2y$10$q27pho4ghk5EismJ8ZWoNOgTRrxgLnj/V5UiRnYWXxWFGlqEQy2um'
			 WHERE id='U04350002390';

			 filtro
			 id='U04350002390'
		    */

		    // attempt to do the login
		    if (Auth::attempt($userdata, true)) {
		    	
		        // validation successful!
		        // redirect them to the secure section or whatever
		        // return Redirect::to('secure');
		        // for now we'll just echo success (even though echoing in a controller is bad)
		        //echo 'SUCCESS!';
		        $names = explode(' ',trim(Auth::user()->nombres));
		        $name = $names[0];
		        $lastnames = explode(' ',trim(Auth::user()->apellidos));
		        $lastname = $lastnames[0];
		    	$username = $name.' '.$lastname;
		    	date_default_timezone_set('America/Caracas');
		    	$date = date('Y/m/d H:i:s'); 

		        //return json_encode('{token:{'.Auth::user()->remember_token.'},email:{'.Auth::user()->dir_electr.'},id='.Auth::user()->id.'}}');
		        return json_encode(array('token'=>Auth::user()->remember_token, 'email'=>Auth::user()->dir_electr, 'username' => $username, 'id' => Auth::user()->id, 'logindate' => $date, 'tipo_usp' => Auth::user()->tipo_usp));
		    } else {        

		        // validation not successful, send back to form 
		        return json_encode("No se Logueo");
		        //return Redirect::to('login');

		    }

		}
	}

	public function doLogout()
	{
		if(Auth::check())
			return json_encode(Auth::logout());
		return json_encode(false);
	}


	public function recover_password() 
	{

		$aux = Input::all();

		$email = $aux['email'];

		$new_password = str_random(5);

		//$user  = User::where('email', '=', $aux['email'])->first();

		//$user->password = Hash::make($new_password);

		//$user->save();

		$data = array( 'password' => $new_password);

		Mail::send('mailer/recover_password_mail',$data, function($message) use ($email)
	    {
	        $message->to($email)->subject('Recuperación de contraseña!');
	    });//$email_dest

		$flash = '<br>Se le ha enviado un correo con la nueva contraseña.';
	    print_r($flash)  ;
	        //return Redirect::to('login')
	        //  ->with('mensaje', $flash);
	}

	public function recover_password_view() 
	{
		return View::make('mailer/recover_password_view');
	}

}