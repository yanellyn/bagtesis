<?php

class DescriptoresController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /descriptores
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return Descriptor::take(15)->skip(0)->get();
		
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /descriptores/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /descriptores
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /descriptores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /descriptores/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /descriptores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /descriptores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function descriptores_recurso(){
		$id= Input::get('id_recurso');

		$i = 0;
		$recurso_descriptores = DB::table('recursos_descriptores')
                            ->join('descriptores', 'descriptores.id', '=', 'recursos_descriptores.descriptor_id')              
                            ->join('tipo_descriptores', 'tipo_descriptores.id', '=', 'descriptores.tipo')                                     
                            ->where('recursos_descriptores.recurso_id','=',$id)
                            ->orderBy('recursos_descriptores.descriptor_id', 'ASC')
                            ->select('descriptores.*', 'tipo_descriptores.descrip_desc')->get();

       foreach ($recurso_descriptores as $descriptor) {
        
    		if($descriptor->tipo == 'M-650-A' || $descriptor->tipo == 'M-650-X' || $descriptor->tipo == 'M-650-Z'  ){
    			unset($recurso_descriptores[$i]);
    		}      

        	if($descriptor->descriptor_salida[0] == '$' && strlen($descriptor->descriptor_salida)>2  ){
        		$recurso_descriptores[$i]->descriptor_salida = substr($descriptor->descriptor_salida,2,strlen($descriptor->descriptor_salida)-1);
        	
        	}

        	$descriptor->descriptor_salida = str_replace("\$x"," - ", $descriptor->descriptor_salida);
        	$descriptor->descriptor_salida = str_replace("\$X"," - ", $descriptor->descriptor_salida);
        	$descriptor->descriptor_salida = str_replace("\$Z"," - ", $descriptor->descriptor_salida);
        	$descriptor->descriptor_salida = str_replace("\$z"," - ", $descriptor->descriptor_salida);
        	$i++;
        }

		$recurso_descriptores = array_merge($recurso_descriptores,[]);
		return $recurso_descriptores;

	}


	public function obtener_descriptores_sistema($tipo = NULL, $busqueda = NULL){
		$i = 0;

		if(!is_null($tipo)){

			if($tipo!= 'NINGUNO'){

				if(!is_null($busqueda)){
					$sistema_descriptores = DB::table('descriptores')
								->join('tipo_descriptores', 'tipo_descriptores.id', '=', 'descriptores.tipo')
								->where('descriptores.tipo', '=', $tipo)
								->where('descriptores.descriptor', 'like', '%'.strtoupper(App::make('BuscadoresController')->convetir_string($busqueda)).'%')                     
	                            ->orderBy('descriptores.tipo', 'ASC')
	                            ->orderBy('descriptores.descriptor', 'ASC')
	                            ->select('descriptores.*', 'tipo_descriptores.descrip_desc')
	                            ->paginate(15);

				}else{
					$sistema_descriptores = DB::table('descriptores')  
								->join('tipo_descriptores', 'tipo_descriptores.id', '=', 'descriptores.tipo')
								->where('descriptores.tipo', '=', $tipo)                     
	                            ->orderBy('descriptores.tipo', 'ASC')
	                            ->orderBy('descriptores.descriptor', 'ASC')
	                            ->select('descriptores.*', 'tipo_descriptores.descrip_desc')
	                            ->paginate(15);
	            }

	        }else{

	        	if(!is_null($busqueda)){
	        		$sistema_descriptores = DB::table('descriptores')
	        					->join('tipo_descriptores', 'tipo_descriptores.id', '=', 'descriptores.tipo')
								->where('descriptores.descriptor', 'like', '%'.strtoupper(App::make('BuscadoresController')->convetir_string($busqueda)).'%')                     
	                            ->orderBy('descriptores.tipo', 'ASC')
	                            ->orderBy('descriptores.descriptor', 'ASC')
	                            ->select('descriptores.*', 'tipo_descriptores.descrip_desc')
	                            ->paginate(15);

				}else{
	        		$sistema_descriptores = DB::table('descriptores')
	        					->join('tipo_descriptores', 'tipo_descriptores.id', '=', 'descriptores.tipo')                       
	                            ->orderBy('descriptores.tipo', 'ASC')
	                            ->orderBy('descriptores.descriptor', 'ASC')
	                            ->select('descriptores.*', 'tipo_descriptores.descrip_desc')
	                            ->paginate(15);
	            }
	        }

		}else{
			$sistema_descriptores = DB::table('descriptores')
								->join('tipo_descriptores', 'tipo_descriptores.id', '=', 'descriptores.tipo')                       
	                            ->orderBy('descriptores.tipo', 'ASC')
	                            ->orderBy('descriptores.descriptor', 'ASC')
	                            ->select('descriptores.*', 'tipo_descriptores.descrip_desc')
	                            ->paginate(15);
		}


       	foreach ($sistema_descriptores as $descriptor) {
        
    		if($descriptor->tipo == 'M-650-A' || $descriptor->tipo == 'M-650-X' || $descriptor->tipo == 'M-650-Z'  ){
    			unset($sistema_descriptores[$i]);
    		}      

        	if($descriptor->descriptor_salida[0] == '$' && strlen($descriptor->descriptor_salida)>2  ){
        		$sistema_descriptores[$i]->descriptor_salida = substr($descriptor->descriptor_salida,2,strlen($descriptor->descriptor_salida)-1);
        	
        	}

        	if($descriptor->descriptor_salida[0] == ' '){
        		$sistema_descriptores[$i]->descriptor_salida = substr($descriptor->descriptor_salida,1,strlen($descriptor->descriptor_salida)-1);
        	
        	}

        	$descriptor->descriptor_salida = str_replace("\$x"," - ", $descriptor->descriptor_salida);
        	$descriptor->descriptor_salida = str_replace("\$X"," - ", $descriptor->descriptor_salida);
        	$descriptor->descriptor_salida = str_replace("\$z"," - ", $descriptor->descriptor_salida);
        	$descriptor->descriptor_salida = str_replace("\$Z"," - ", $descriptor->descriptor_salida);
        	$i++;
        }

        return $sistema_descriptores;

	}

	public function agregar_descriptor($dataDescriptor = null){
		
		if($dataDescriptor){
			$data = $dataDescriptor;
		}else{
			$data = Input::all();
		}

		$id_recurso= $data["id_recurso"];
		$tipo = $data["tipo_descriptor"];
		$enc_tem = $data["temDesc"];
		$sub_gen = $data["genDesc"];
		$sub_geo = $data["geoDesc"];
		

		if($tipo == "M-650"){
			$tematica_completa = "\$A".$enc_tem;

			if(!empty($sub_gen)){
				$tematica_completa = $tematica_completa." \$X".$sub_gen;
			}

			if(!empty($sub_geo)){
				$tematica_completa = $tematica_completa." \$Z".$sub_geo;
			}

			
			$existe_descriptor = DB::table('descriptores')
                            ->where('descriptores.descriptor','=', strtoupper(App::make('BuscadoresController')->convetir_string($tematica_completa)))
                            ->where('descriptores.tipo','=', $tipo)
                            ->select('descriptores.*')
                            ->first();

            
            if($existe_descriptor){
            	$id_descriptor = $existe_descriptor->id;

            	$existe_recurso_descriptores = DB::table('recursos_descriptores')
            								->where('recursos_descriptores.recurso_id','=', $id_recurso)
				                            ->where('recursos_descriptores.descriptor_id','=', $id_descriptor)
				                            ->select('recursos_descriptores.*')
                            				->first();

                if($existe_recurso_descriptores){
					
					//RETORNAMOS CODIGO 1
					$respuesta = array("codigo" => "1");
					return json_encode($respuesta);


				}else{
					$nuevo_recurso_descriptores = new RecursoDescriptor;
					$nuevo_recurso_descriptores->recurso_id = $id_recurso;
					$nuevo_recurso_descriptores->descriptor_id = $id_descriptor;
					$nuevo_recurso_descriptores->ext_recurso_id = "0";
					//$nuevo_recurso_descriptores->numero = "";
					$nuevo_recurso_descriptores->save();


					//if(!empty($enc_tem)){

						$existe_descriptor = DB::table('descriptores')
				                            ->where('descriptores.id','=', $id_descriptor + 1)
				                            ->where('descriptores.tipo','=','M-650-A')
				                            ->first();

                        if($existe_descriptor){
                        	$nuevo_recurso_descriptores = new RecursoDescriptor;
							$nuevo_recurso_descriptores->recurso_id = $id_recurso;
							$nuevo_recurso_descriptores->descriptor_id = $id_descriptor + 1;
							$nuevo_recurso_descriptores->ext_recurso_id = "0";
							//$nuevo_recurso_descriptores->numero = "";
							$nuevo_recurso_descriptores->save();
                        }

					//}

					//if(!empty($sub_gen)){

						$existe_descriptor = DB::table('descriptores')
				                            ->where('descriptores.id','=', $id_descriptor + 2)
				                            ->where('descriptores.tipo','=','M-650-X')
				                            ->first();

                        if($existe_descriptor){
                        	$nuevo_recurso_descriptores = new RecursoDescriptor;
							$nuevo_recurso_descriptores->recurso_id = $id_recurso;
							$nuevo_recurso_descriptores->descriptor_id = $id_descriptor + 2;
							$nuevo_recurso_descriptores->ext_recurso_id = "0";
							//$nuevo_recurso_descriptores->numero = "";
							$nuevo_recurso_descriptores->save();
                        }

					//}

					//if(!empty($sub_geo)){

						$existe_descriptor = DB::table('descriptores')
				                            ->where('descriptores.id','=', $id_descriptor + 3)
				                            ->where('descriptores.tipo','=','M-650-Z')
				                            ->first();

                        if($existe_descriptor){
                        	$nuevo_recurso_descriptores = new RecursoDescriptor;
							$nuevo_recurso_descriptores->recurso_id = $id_recurso;
							$nuevo_recurso_descriptores->descriptor_id = $id_descriptor + 3;
							$nuevo_recurso_descriptores->ext_recurso_id = "0";
							//$nuevo_recurso_descriptores->numero = "";
							$nuevo_recurso_descriptores->save();
                        }

					//}

					//RETORNAMOS CODIGO 0
                    date_default_timezone_set('America/Caracas');
					$date = date('YmdHis', time());
					$historial = new HistorialCatalogacion;
					$historial->id_catalogador =  $data["idCatalogador"];
					$historial->recurso_id = $data["id_recurso"];
					$historial->ext_recurso_id = '0';
					$historial->operacion = 'Agregar'; 
					$historial->modulo = 'Descriptores';
					$historial->fecha = $date;
					$historial->save();
					$respuesta = array("codigo" => "0");
					return json_encode($respuesta);
				}

            }else{
			
				//TODO BIEN
            	//M-650
            	$nuevo_descriptor = new Descriptor;	
				$nuevo_descriptor->descriptor =  strtoupper(App::make('BuscadoresController')->convetir_string($tematica_completa));
				$nuevo_descriptor->tipo = $tipo;
				$nuevo_descriptor->descriptor_salida = $tematica_completa;
				$nuevo_descriptor->save();
				$id_descriptor = $nuevo_descriptor->id;

				$nuevo_recurso_descriptores = new RecursoDescriptor;
				$nuevo_recurso_descriptores->recurso_id = $id_recurso;
				$nuevo_recurso_descriptores->descriptor_id = $id_descriptor;
				$nuevo_recurso_descriptores->ext_recurso_id = "0";
				//$nuevo_recurso_descriptores->numero = "";
				$nuevo_recurso_descriptores->save();




				//M-650-A
            	$nuevo_descriptor = new Descriptor;	
				$nuevo_descriptor->descriptor =  strtoupper(App::make('BuscadoresController')->convetir_string($enc_tem));
				$nuevo_descriptor->tipo = "M-650-A";
				$nuevo_descriptor->descriptor_salida = $enc_tem;
				$nuevo_descriptor->save();
				$id_descriptor = $nuevo_descriptor->id;

				$nuevo_recurso_descriptores = new RecursoDescriptor;
				$nuevo_recurso_descriptores->recurso_id = $id_recurso;
				$nuevo_recurso_descriptores->descriptor_id = $id_descriptor;
				$nuevo_recurso_descriptores->ext_recurso_id = "0";
				//$nuevo_recurso_descriptores->numero = "";
				$nuevo_recurso_descriptores->save();

				//M-650-X
				if(!empty($sub_gen)){
	            	$nuevo_descriptor = new Descriptor;	
					$nuevo_descriptor->descriptor =  strtoupper(App::make('BuscadoresController')->convetir_string($sub_gen));
					$nuevo_descriptor->tipo = "M-650-X";
					$nuevo_descriptor->descriptor_salida = $sub_gen;
					$nuevo_descriptor->save();
					$id_descriptor = $nuevo_descriptor->id;

					$nuevo_recurso_descriptores = new RecursoDescriptor;
					$nuevo_recurso_descriptores->recurso_id = $id_recurso;
					$nuevo_recurso_descriptores->descriptor_id = $id_descriptor;
					$nuevo_recurso_descriptores->ext_recurso_id = "0";
					//$nuevo_recurso_descriptores->numero = "";
					$nuevo_recurso_descriptores->save();
				}

				//M-650-Z
				if(!empty($sub_geo)){
	            	$nuevo_descriptor = new Descriptor;	
					$nuevo_descriptor->descriptor =  strtoupper(App::make('BuscadoresController')->convetir_string($sub_geo));
					$nuevo_descriptor->tipo = "M-650-Z";
					$nuevo_descriptor->descriptor_salida = $sub_geo;
					$nuevo_descriptor->save();
					$id_descriptor = $nuevo_descriptor->id;
					$nuevo_recurso_descriptores = new RecursoDescriptor;
					$nuevo_recurso_descriptores->recurso_id = $id_recurso;
					$nuevo_recurso_descriptores->descriptor_id = $id_descriptor;
					$nuevo_recurso_descriptores->ext_recurso_id = "0";
					//$nuevo_recurso_descriptores->numero = "";
					$nuevo_recurso_descriptores->save();
				}
				date_default_timezone_set('America/Caracas');
					$date = date('YmdHis', time());
					$historial = new HistorialCatalogacion;
					$historial->id_catalogador =  $data["idCatalogador"];
					$historial->recurso_id = $data["id_recurso"];
					$historial->ext_recurso_id = '0';
					$historial->operacion = 'Agregar'; 
					$historial->modulo = 'Descriptores';
					$historial->fecha = $date;
					$historial->save();
				$respuesta = array("codigo" => "0");
				return json_encode($respuesta);

            }

		}else{
		
			$existe_descriptor = DB::table('descriptores')
                            ->where('descriptores.descriptor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['descriptor'])))
                            ->where('descriptores.describe','=', $data["describe"])
                            ->where('descriptores.tipo','=', $tipo)
                            ->where('descriptores.codigo_externo','=', $data["codigoExterno"])
                            ->select('descriptores.*')
                            ->first();

            if($existe_descriptor){
            	$id_descriptor = $existe_descriptor->id;

            	$existe_recurso_descriptores = DB::table('recursos_descriptores')
            								->where('recursos_descriptores.recurso_id','=', $id_recurso)
				                            ->where('recursos_descriptores.descriptor_id','=', $id_descriptor)
				                            ->select('recursos_descriptores.*')
                            				->first();

				if($existe_recurso_descriptores){
					//RETORNAMOS CODIGO 1
					$respuesta = array("codigo" => "1");
				return json_encode($respuesta);

				}else{

					$nuevo_recurso_descriptores = new RecursoDescriptor;
					$nuevo_recurso_descriptores->recurso_id = $id_recurso;
					$nuevo_recurso_descriptores->descriptor_id = $id_descriptor;
					$nuevo_recurso_descriptores->ext_recurso_id = "0";
					//$nuevo_recurso_descriptores->numero = "";
					$nuevo_recurso_descriptores->save();


					//RETORNAMOS CODIGO 0
					date_default_timezone_set('America/Caracas');
					$date = date('YmdHis', time());
					$historial = new HistorialCatalogacion;
					$historial->id_catalogador =  $data["idCatalogador"];
					$historial->recurso_id = $data["id_recurso"];
					$historial->ext_recurso_id = '0';
					$historial->operacion = 'Agregar'; 
					$historial->modulo = 'Descriptores';
					$historial->fecha = $date;
					$historial->save();
					$respuesta = array("codigo" => "0");
				return json_encode($respuesta);
				}

            }else{
            	$nuevo_descriptor = new Descriptor;	
				$nuevo_descriptor->descriptor =  strtoupper(App::make('BuscadoresController')->convetir_string($data['descriptor']));
				$nuevo_descriptor->describe = $data["describe"];
				$nuevo_descriptor->tipo = $tipo;
				$nuevo_descriptor->codigo_externo = $data["codigoExterno"];
				$nuevo_descriptor->descriptor_salida = $data["descriptor"];
				$nuevo_descriptor->save();
				$id_descriptor = $nuevo_descriptor->id;

				$nuevo_recurso_descriptores = new RecursoDescriptor;
				$nuevo_recurso_descriptores->recurso_id = $id_recurso;
				$nuevo_recurso_descriptores->descriptor_id = $id_descriptor;
				$nuevo_recurso_descriptores->ext_recurso_id = "0";
				//$nuevo_recurso_descriptores->numero = "";
				$nuevo_recurso_descriptores->save();

				date_default_timezone_set('America/Caracas');
				$date = date('YmdHis', time());
				$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data["idCatalogador"];
				$historial->recurso_id = $data["id_recurso"];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Agregar'; 
				$historial->modulo = 'Descriptores';
				$historial->fecha = $date;
				$historial->save();
				$respuesta = array("codigo" => "0");
				return json_encode($respuesta);

				//RETORNAMOS CODIGO 0

            }

		}

	}
		

	public function guardar_descriptores_recurso(){
		$data = Input::all();
		$id_recurso= $data["id_recurso"];
		$tipo = $data["tipo_descriptor"];
		$enc_tem = $data["temDesc"];
		$sub_gen = $data["genDesc"];
		$sub_geo = $data["geoDesc"];
		$tematica_completa = "";
		$count = 0;
		/*
		$queryRecursoDescriptores = DB::table('recursos_descriptores')
		->where('recursos_descriptores.recurso_id','=',$id_recurso)
		->delete();

		$descritores = $data["descriptores"];
		foreach ($descritores as $row) {
			$count++;
			$recurso_descriptor = new RecursoDescriptor; 
			$recurso_descriptor->recurso_id = $id_recurso;
			$recurso_descriptor->ext_recurso_id = 0;
			$recurso_descriptor->descriptor_id =  $row["id"];
			//$recurso_descriptor->numero = $count;
			$recurso_descriptor->save(); 
		}*/

		
		if($data["id_descriptor"] != 0){
			if($tipo != "M-650"){
				$queryDescriptor = DB::table('descriptores')
				->where('descriptores.id','=',$data['id_descriptor'])
				->update(array(    
				 'descriptor' =>  strtoupper(App::make('BuscadoresController')->convetir_string($data['descriptor'])),
				 'describe' => $data['describe'],
				 'tipo' => $tipo,
				 'codigo_externo' => $data["codigoExterno"],
				 'descriptor_salida' => $data['descriptor']
				 )); 

				$descriptor = DB::table('descriptores')
				->where('descriptores.id','=',$data['id_descriptor'])
				->first();

				date_default_timezone_set('America/Caracas');
				$date = date('YmdHis', time());
				$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data["idCatalogador"];
				$historial->recurso_id = $data["id_recurso"];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Modificar'; 
				$historial->modulo = 'Descriptores';
				$historial->fecha = $date;
				$historial->save();
				$respuesta = array("codigo" => "0",  "descriptor" => $descriptor);
			}else{

				$tematica_completa = "\$A".$enc_tem;

				if(!empty($sub_gen)){
					$tematica_completa = $tematica_completa." \$X".$sub_gen;
				}

				if(!empty($sub_geo)){
					$tematica_completa = $tematica_completa." \$Z".$sub_geo;
				}

				//M-650
				$queryDescriptor = DB::table('descriptores')
				->where('descriptores.id','=',$data['id_descriptor'])
				->where('descriptores.tipo','=',$tipo)
				->update(array(    
				 'descriptor' =>  strtoupper(App::make('BuscadoresController')->convetir_string($tematica_completa)),
				 'descriptor_salida' => $tematica_completa
				 )); 

				$descriptor = DB::table('descriptores')
				->where('descriptores.id','=',$data['id_descriptor'])
				->first();


				//M-650-A
            	$queryDescriptor = DB::table('descriptores')
				->where('descriptores.id','=',$data['id_descriptor'] + 1)
				->where('descriptores.tipo','=',"M-650-A")
				->update(array(    
				 'descriptor' =>  strtoupper(App::make('BuscadoresController')->convetir_string($enc_tem)),
				 'descriptor_salida' => $enc_tem
				 ));

				//M-650-X
            	$queryDescriptor = DB::table('descriptores')
				->where('descriptores.id','=',$data['id_descriptor'] + 2)
				->where('descriptores.tipo','=',"M-650-X")
				->update(array(    
				 'descriptor' =>  strtoupper(App::make('BuscadoresController')->convetir_string($sub_gen)),
				 'descriptor_salida' => $sub_gen
				 ));

				//M-650-Z
            	$queryDescriptor = DB::table('descriptores')
				->where('descriptores.id','=',$data['id_descriptor'] + 3)
				->where('descriptores.tipo','=',"M-650-Z")
				->update(array(    
				 'descriptor' =>  strtoupper(App::make('BuscadoresController')->convetir_string($sub_geo)),
				 'descriptor_salida' => $sub_geo
				 ));


				if($descriptor->descriptor_salida[0] == '$' && strlen($descriptor->descriptor_salida)>2  ){
        			$descriptor->descriptor_salida = substr($descriptor->descriptor_salida,2,strlen($descriptor->descriptor_salida)-1);
	        	}

	        	$descriptor->descriptor_salida = str_replace("\$x"," - ", $descriptor->descriptor_salida);
	        	$descriptor->descriptor_salida = str_replace("\$X"," - ", $descriptor->descriptor_salida);
	        	$descriptor->descriptor_salida = str_replace("\$z"," - ", $descriptor->descriptor_salida);
	        	$descriptor->descriptor_salida = str_replace("\$Z"," - ", $descriptor->descriptor_salida);

	        	date_default_timezone_set('America/Caracas');
				$date = date('YmdHis', time());
				$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data["idCatalogador"];
				$historial->recurso_id = $data["id_recurso"];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Modificar'; 
				$historial->modulo = 'Descriptores';
				$historial->fecha = $date;
				$historial->save();
				$respuesta = array("codigo" => "0",  "descriptor" => $descriptor);
			}
		
		}else{
			$respuesta = array("codigo" => "1", "descriptor" => "");
		}
		
			
		return json_encode($respuesta);

	}

	public function eliminar_descriptores_recurso(){
		$data = Input::all();
		$id= $data["id_recurso"];
		$id_descriptor = $data["id_descriptor"];
		$tipo_descriptor = $data["tipo_descriptor"];

		$queryResultados= DB::table("recursos_descriptores")
						->where('recursos_descriptores.descriptor_id', '=', $id_descriptor)
						->where('recursos_descriptores.recurso_id', '!=', $id)
						->groupBy('recursos_descriptores.recurso_id')
						->select('recursos_descriptores.recurso_id')
						->get();

		$total = count($queryResultados);
		if($total > 0){
			if($data["bool_eliminar_derecurso"] == 1){
				$recurso_descriptores = DB::table('recursos_descriptores')
                            ->where('recursos_descriptores.descriptor_id', '=', $id_descriptor)                                    
                            ->where('recursos_descriptores.recurso_id','=',$id)
                            ->delete();
			}
			
			$respuesta = array("codigo" => "1");

			return $respuesta;
		}

		if($tipo_descriptor != "M-650"){
			$recurso_descriptores = DB::table('recursos_descriptores')
                            ->where('recursos_descriptores.descriptor_id', '=', $id_descriptor)                                    
                            ->where('recursos_descriptores.recurso_id','=',$id)
                            ->delete();

            $descriptores = DB::table('descriptores')
                            ->where('descriptores.id', '=', $id_descriptor)
                            ->delete();


            date_default_timezone_set('America/Caracas');
				$date = date('YmdHis', time());
				$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data["idCatalogador"];
				$historial->recurso_id = $data["id_recurso"];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Eliminar'; 
				$historial->modulo = 'Descriptores';
				$historial->fecha = $date;
				$historial->save();
			$respuesta = array("codigo" => "0");
			return json_encode($respuesta);

		}else{

			//M-650
			$recurso_descriptores = DB::table('recursos_descriptores')
                            ->where('recursos_descriptores.descriptor_id', '=', $id_descriptor)                                    
                            ->where('recursos_descriptores.recurso_id','=',$id)
                            ->delete();

            $descriptores = DB::table('descriptores')
                            ->where('descriptores.id', '=', $id_descriptor)
                            ->delete();


            //M-650-A
            $recurso_descriptores = DB::table('recursos_descriptores')
            				->join('descriptores', 'descriptores.id', '=', 'recursos_descriptores.descriptor_id')
                            ->where('descriptores.tipo','=','M-650-A')
                            ->where('recursos_descriptores.descriptor_id', '=', $id_descriptor + 1)                                    
                            ->where('recursos_descriptores.recurso_id','=',$id)
                            ->select('recursos_descriptores.id')
                            ->first();

            $borrar_rd = DB::table('recursos_descriptores')
                			->where('recursos_descriptores.id', '=', $recurso_descriptores->id)
                			->delete();

            $descriptores = DB::table('descriptores')
                            ->where('descriptores.id', '=', $id_descriptor + 1)
                            ->where('descriptores.tipo', '=', "M-650-A")
                            ->delete();

            //M-650-X
            $recurso_descriptores = DB::table('recursos_descriptores')
            				->join('descriptores', 'descriptores.id', '=', 'recursos_descriptores.descriptor_id')
                            ->where('descriptores.tipo','=','M-650-X')
                            ->where('recursos_descriptores.descriptor_id', '=', $id_descriptor + 2)                                    
                            ->where('recursos_descriptores.recurso_id','=',$id)
                            ->select('recursos_descriptores.id')
                            ->first();

            $borrar_rd = DB::table('recursos_descriptores')
                			->where('recursos_descriptores.id', '=', $recurso_descriptores->id)
                			->delete();

            $descriptores = DB::table('descriptores')
                            ->where('descriptores.id', '=', $id_descriptor + 2)
                            ->where('descriptores.tipo', '=', "M-650-X")
                            ->delete();


            //M-650-Z
            $recurso_descriptores = DB::table('recursos_descriptores')
            				->join('descriptores', 'descriptores.id', '=', 'recursos_descriptores.descriptor_id')
                            ->where('descriptores.tipo','=','M-650-Z')
                            ->where('recursos_descriptores.descriptor_id', '=', $id_descriptor + 3)                                    
                            ->where('recursos_descriptores.recurso_id','=',$id)                            
                            ->select('recursos_descriptores.id')
                            ->first();

            $borrar_rd = DB::table('recursos_descriptores')
                			->where('recursos_descriptores.id', '=', $recurso_descriptores->id)
                			->delete();

            $descriptores = DB::table('descriptores')
                            ->where('descriptores.id', '=', $id_descriptor + 3)
                            ->where('descriptores.tipo', '=', "M-650-Z")
                            ->delete(); 


            date_default_timezone_set('America/Caracas');
				$date = date('YmdHis', time());
				$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data["idCatalogador"];
				$historial->recurso_id = $data["id_recurso"];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Eliminar'; 
				$historial->modulo = 'Descriptores';
				$historial->fecha = $date;
				$historial->save();
			$respuesta = array("codigo" => "0");
			return json_encode($respuesta);

		}

	}

	public function autocompletado_descriptor_typeahead(){
		$data = Input::all();
		$datos_busqueda = [];
		$this->split_data = [];
		$tipo_descriptor = $data['tipo'];
		$aux_data = App::make('BuscadoresController')->convetir_string($data['data']);
		$this->split_data =  explode(' ',$aux_data);
		$datos_descriptores = [];

		
		if($tipo_descriptor == "")
		{
			$query = DB::table('descriptores')
									->where('descriptores.descriptor','=',$aux_data)
									->where('descriptores.tipo','not like','M-650')
									->where('descriptores.tipo','not like','M-650-X')
									->where('descriptores.tipo','not like','M-650-Z')
									->orderBy('descriptores.descriptor','ASC')
									->select('descriptores.*')
									->distinct('descriptores.descriptor','descriptores.tipo')
									->distinct()
									->paginate(15);
		}else{
			$query = DB::table('descriptores')
									->where('descriptores.descriptor','=',$aux_data)
									->where('descriptores.tipo','like',$tipo_descriptor)
									->where('descriptores.tipo','not like','M-650')
									->where('descriptores.tipo','not like','M-650-X')
									->where('descriptores.tipo','not like','M-650-Z')
									->orderBy('descriptores.descriptor','ASC')
									->select('descriptores.*')
									->distinct('descriptores.descriptor','descriptores.tipo')
									->distinct()
									->paginate(15);
		}	
		

		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "descriptor_salida"=> App::make('BuscadoresController')->convetir_string($row->descriptor_salida),"descriptor"=> $row->descriptor, "tipo"=> $row->tipo, "descripcion"=> $row->describe, "codigo"=> $row->codigo_externo);
			array_push($datos_descriptores, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_descriptores), SORT_REGULAR);
		$datos_descriptores = [];


		if($tipo_descriptor == "")
		{
			$query = DB::table('descriptores')			
									->where('descriptores.descriptor','like',$aux_data.'%')
									->where('descriptores.tipo','not like','M-650')
									->where('descriptores.tipo','not like','M-650-X')
									->where('descriptores.tipo','not like','M-650-Z')
									->orderBy('descriptores.descriptor','ASC')
									->select('descriptores.*')
									->distinct('descriptores.descriptor','descriptores.tipo')
									->paginate(15);
		}else{
			$query = DB::table('descriptores')			
									->where('descriptores.descriptor','like',$aux_data.'%')
									->where('descriptores.tipo','like',$tipo_descriptor)
									->where('descriptores.tipo','not like','M-650')
									->where('descriptores.tipo','not like','M-650-X')
									->where('descriptores.tipo','not like','M-650-Z')
									->orderBy('descriptores.descriptor','ASC')
									->select('descriptores.*')
									->distinct('descriptores.descriptor','descriptores.tipo')
									->paginate(15);
		}
	
	
		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "descriptor_salida"=> App::make('BuscadoresController')->convetir_string($row->descriptor_salida),"descriptor"=> $row->descriptor, "tipo"=> $row->tipo, "descripcion"=> $row->describe, "codigo"=> $row->codigo_externo);
			array_push($datos_descriptores, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_descriptores), SORT_REGULAR);
		$datos_descriptores = [];

		if($tipo_descriptor == "")
		{
			$query = DB::table('descriptores')
									->where('descriptores.descriptor','like','%'.$aux_data.'%')
									->where('descriptores.tipo','not like','M-650')
									->where('descriptores.tipo','not like','M-650-X')
									->where('descriptores.tipo','not like','M-650-Z')
									->orderBy('descriptores.descriptor','ASC')
									->select('descriptores.*')
									->distinct('descriptores.descriptor','descriptores.tipo')
									->paginate(15);
		}else{
			$query = DB::table('descriptores')
									->where('descriptores.descriptor','like','%'.$aux_data.'%')
									->where('descriptores.tipo','like',$tipo_descriptor)
									->where('descriptores.tipo','not like','M-650')
									->where('descriptores.tipo','not like','M-650-X')
									->where('descriptores.tipo','not like','M-650-Z')
									->orderBy('descriptores.descriptor','ASC')
									->select('descriptores.*')
									->distinct('descriptores.descriptor','descriptores.tipo')
									->paginate(15);
		}
	

	
		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "descriptor_salida"=> App::make('BuscadoresController')->convetir_string($row->descriptor_salida),"descriptor"=> $row->descriptor, "tipo"=> $row->tipo, "descripcion"=> $row->describe, "codigo"=> $row->codigo_externo);
			array_push($datos_descriptores, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_descriptores), SORT_REGULAR);
		$datos_descriptores = [];

		if($tipo_descriptor == "")
		{
			$query = DB::table('descriptores')
									->where('descriptores.tipo','not like','M-650')
									->where('descriptores.tipo','not like','M-650-X')
									->where('descriptores.tipo','not like','M-650-Z')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('descriptores.descriptor', 'like', '%'.$aux.'%');
										}

									})
									->orderBy('descriptores.descriptor','ASC')
									->select('descriptores.*')
									->distinct('descriptores.descriptor','descriptores.tipo')
									->paginate(15);
		}else{
			$query = DB::table('descriptores')
									->where('descriptores.tipo','like',$tipo_descriptor)
									->where('descriptores.tipo','not like','M-650')
									->where('descriptores.tipo','not like','M-650-X')
									->where('descriptores.tipo','not like','M-650-Z')
									->where(function($query){

										foreach ($this->split_data as $aux) {
											$query->where('descriptores.descriptor', 'like', '%'.$aux.'%');
										}

									})
									->orderBy('descriptores.descriptor','ASC')
									->select('descriptores.*')
									->distinct('descriptores.descriptor','descriptores.tipo')
									->paginate(15);
		}

		foreach ($query as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "descriptor_salida"=> App::make('BuscadoresController')->convetir_string($row->descriptor_salida),"descriptor"=> $row->descriptor, "tipo"=> $row->tipo, "descripcion"=> $row->describe, "codigo"=> $row->codigo_externo);
			array_push($datos_descriptores, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_descriptores), SORT_REGULAR);
		$datos_descriptores = [];
		$datos_busqueda = array_merge($datos_busqueda,$datos_descriptores);

		$datos_busqueda = json_decode(json_encode($datos_busqueda));
		/** flip it to keep the last one instead of the first one **/
		$datos_busqueda = array_reverse($datos_busqueda);

		/** Answer Code begins here **/

		// Build temporary array for array_unique
		$tmp = array();
		foreach($datos_busqueda as $k => $v)
		    $tmp[$k] = $v->descriptor_salida;

		// Find duplicates in temporary array
		$tmp = array_unique($tmp);

		// Remove the duplicates from original array
		foreach($datos_busqueda as $k => $v) {
		  if (!array_key_exists($k, $tmp))
		    unset($datos_busqueda[$k]);
		}

		/** Answer Code ends here **/
		/** flip it back now to get the original order **/
		$datos_busqueda = array_reverse($datos_busqueda);

		return $datos_busqueda;
	}

}
