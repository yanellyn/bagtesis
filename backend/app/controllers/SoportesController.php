<?php

class SoportesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	//Informacion descriptiva del sistema- Ahora llamado Soporte
	public function obtener_inf_descriptiva_sistema(){
		$soportesSistema = DB::table('soportes')
                            ->orderBy('soportes.soporte', 'ASC')
                            ->select('soportes.id','soportes.soporte','soportes.soporte_salida')
                            ->paginate(15);

        return $soportesSistema;

	}

	//Informacion descriptiva del recurso- Ahora llamado Soporte
	public function obtener_inf_descriptiva_recurso($id){
		$soportesRecurso = DB::table('soportes')
							->join('recursos_soportes','recursos_soportes.soporte_id','=','soportes.id')
							->where('recursos_soportes.recurso_id','=',$id)
                            ->orderBy('soportes.soporte', 'ASC')
                            ->select('soportes.id','soportes.soporte','soportes.soporte_salida')
                            ->get();

        return $soportesRecurso;

	}

	public function agregar_soporte($dataSoporte = null){
		
		if($dataSoporte){
			$data = $dataSoporte;
		}else{
			$data = Input::all();
		}

		$id_recurso= $data["id_recurso"];


		$existe_soporte = DB::table('soportes')
                            ->where('soportes.soporte','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombre_soporte'])))
                            ->select('soportes.*')
                            ->first();

        if($existe_soporte){
        	$id_soporte = $existe_soporte->id;

        	$existe_recurso_soporte = DB::table('recursos_soportes')
        								->where('recursos_soportes.recurso_id','=', $id_recurso)
			                            ->where('recursos_soportes.soporte_id','=', $id_soporte)
			                            ->select('recursos_soportes.*')
                        				->first();

			if($existe_recurso_soporte){
				//RETORNAMOS CODIGO 1
				$respuesta = array("codigo" => "1");
				return json_encode($respuesta);

			}else{

				$nuevo_recurso_soporte = new RecursosSoportes;
				$nuevo_recurso_soporte->recurso_id = $id_recurso;
				$nuevo_recurso_soporte->soporte_id = $id_soporte;
				$nuevo_recurso_soporte->ext_recurso_id = "0";
				$nuevo_recurso_soporte->save();
				//RETORNAMOS CODIGO 0
				date_default_timezone_set('America/Caracas');
				$date = date('YmdHis', time());
				$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data["idCatalogador"];
				$historial->recurso_id = $data["id_recurso"];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Agregar'; 
				$historial->modulo = 'Soporte';
				$historial->fecha = $date;
				$historial->save();
				$respuesta = array("codigo" => "0");
				return json_encode($respuesta);
			}

       	}else{

       		$nuevo_soporte = new Soporte;	
			$nuevo_soporte->soporte =  strtoupper(App::make('BuscadoresController')->convetir_string($data['nombre_soporte']));
			$nuevo_soporte->soporte_salida = $data["nombre_soporte"];
			$nuevo_soporte->save();
			$id_soporte = $nuevo_soporte->id;

       		$nuevo_recurso_soportes = new RecursosSoportes;
			$nuevo_recurso_soportes->recurso_id = $id_recurso;
			$nuevo_recurso_soportes->soporte_id = $id_soporte;
			$nuevo_recurso_soportes->ext_recurso_id = "0";
			$nuevo_recurso_soportes->save();
			//RETORNAMOS CODIGO 0
			date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data["idCatalogador"];
			$historial->recurso_id = $data["id_recurso"];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Agregar'; 
			$historial->modulo = 'Soporte';
			$historial->fecha = $date;
			$historial->save();
			$respuesta = array("codigo" => "0");
			return json_encode($respuesta);

       	}

	}

	public function guardar_soporte(){
		$data = Input::all();
		$id_recurso= $data["id_recurso"];
		$count = 0;

		if($data["id_descriptor"] != 0){

			$queryDescriptor = DB::table('soportes')
				->where('soportes.id','=',$data['id_descriptor'])
				->update(array(    
				 'soporte' =>  strtoupper(App::make('BuscadoresController')->convetir_string($data['nombre_soporte'])),
				 'soporte_salida' => $data["nombre_soporte"]
				 ));
			date_default_timezone_set('America/Caracas');
				$date = date('YmdHis', time());
				$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data["idCatalogador"];
				$historial->recurso_id = $data["id_recurso"];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Modificar'; 
				$historial->modulo = 'Soporte';
				$historial->fecha = $date;
				$historial->save();
			$respuesta = array("codigo" => "0");
			return json_encode($respuesta);
		}else{
			$respuesta = array("codigo" => "1");
			return json_encode($respuesta);
		}
	}


	public function eliminar_soporte(){
		$data = Input::all();
		$id= $data["id_recurso"];
		$id_soporte = $data["id_descriptor"];
		$queryResultados= DB::table("recursos_soportes")
						->where('recursos_soportes.soporte_id', '=', $id_soporte)
						->where('recursos_soportes.recurso_id', '!=', $id)
						->groupBy('recursos_soportes.recurso_id')
						->select('recursos_soportes.recurso_id')
						->get();
		$total = count($queryResultados);
		if($total > 0){
			if($data["bool_eliminar_derecurso"] == 1){
				$recurso_soporte = DB::table('recursos_soportes')
                            ->where('recursos_soportes.soporte_id', '=', $id_soporte)                                    
                            ->where('recursos_soportes.recurso_id','=',$id)
                            ->delete();
			}

			$respuesta = array("codigo" => "1");

			return $respuesta;
		}

		$recurso_soporte = DB::table('recursos_soportes')
                        ->where('recursos_soportes.soporte_id', '=', $id_soporte)                                    
                        ->where('recursos_soportes.recurso_id','=',$id)
                        ->delete();

        $soportes = DB::table('soportes')
                        ->where('soportes.id', '=', $id_soporte)
                        ->delete();


        date_default_timezone_set('America/Caracas');
				$date = date('YmdHis', time());
				$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data["idCatalogador"];
				$historial->recurso_id = $data["id_recurso"];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Eliminar'; 
				$historial->modulo = 'Soporte';
				$historial->fecha = $date;
				$historial->save();
		$respuesta = array("codigo" => "0");
		return json_encode($respuesta);

	}



	public function buscadorSoporteTypeahead()
	{
	    $data = Input::all();

	    $datos_busqueda = [];
	    $this->split_data = [];
	    $aux_data = App::make('BuscadoresController')->convetir_string($data['data']);
	    $this->split_data =  explode(' ',$aux_data);
	    $busqueda_materia = [];     
	    $busqueda_materia = DB::table('soportes')
	                  ->where('soporte','=',$aux_data)
	                  ->groupBy('soporte')
	                  ->select('soporte', DB::raw('count(soporte) as num_recursos'))
	                  ->paginate(15);
	    $datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materia->lists('soporte')));
	    
	    $busqueda_materia = DB::table('soportes')
	                  ->where('soporte','like',$aux_data.'%')
	                  ->groupBy('soporte')
	                  ->select('soporte', DB::raw('count(soporte) as num_recursos'))
	                  ->paginate(15);
	    $datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materia->lists('soporte')));

	    $busqueda_materia = DB::table('soportes')
	                  ->where('soporte','like','%'.$aux_data.'%')
	                  ->groupBy('soporte')
	                  ->select('soporte', DB::raw('count(soporte) as num_recursos'))
	                  ->paginate(15);	  
	    $datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materia->lists('soporte')));

	    $busqueda_materia = DB::table('soportes')
	                  ->where(function($query){
	                    foreach ($this->split_data as $aux) {
	                      $query->where('soporte', 'like', '%'.$aux.'%');
	                    }
	                  })
	                  ->groupBy('soporte')
	                  ->select('soporte', DB::raw('count(soporte) as num_recursos'))
	                  ->paginate(15);
	    $datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materia->lists('soporte')));
	    
	    $datos_busqueda = array_unique($datos_busqueda);
	    return $datos_busqueda;
	}
}
