<?php

class PrestamosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /prestamos
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$prestamos = Prestamo::take(1)->skip(0)->get();
		$prestamo = Prestamo::where('id','=','U04350000061')->firstOrFail();
		return $prestamo->ejemplar;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /prestamos/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Check Internet Connection.
	 * 
	 * @param string $sCheckHost Default: http://www.google.com
	 * @return boolean
	 */
	public static function check_internet_connection($sCheckHost = 'http://www.google.com') 
	{
	    return (bool) @fopen($sCheckHost, 'r');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /prestamos
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();
		
		//Verificar estado del usuario solvente
		$user = Usuario::where('id', '=', $data['usuario_id'])->get()->first();
		if(empty($user)) return json_encode(array('error' => 'usuario inexistente.' ));
		if($user->status != 'S' &&  $user->status != 's') return json_encode(array('error' => 'usuario no esta solvente.' ));
		
		$usuario_dependencia = $user->dependencia;
		$usuario_tipo_usp = $user->tipo_usp;

		//verificar cantidad de libros prestados actuales del usuario y si alguno es ejemplar unico
		$count = 0;
		foreach ($data as $key => $value) {
			if($key!='usuario_id' && $key!='admin_id' && !empty($value)){
				$count = $count + 1;
				$obj_ejemplares = Ejemplar::where('recurso_id', '=', $key);
				$ej_disponibles_count = $obj_ejemplares->where('edo_prestamo', '=', 'D')->count();
				if($ej_disponibles_count < 2)
					return json_encode(array('error' => 'no es posible prestar el ejemplar con cota:'.$value['cota'].' por ser unico.' ));
			}
		}
		$user_prestamos_obj = Prestamo::where('id', '=', $data['usuario_id']);
		$count_prestamos = $user_prestamos_obj->count();
		if($count + $count_prestamos > 3) return json_encode(array('error' => 'limite de recursos superado.' ));
		
		//verificar si el usuario ya tiene alguno de los libros que solicita
		$user_prestamos = $user_prestamos_obj->get();
		foreach ($user_prestamos as $key => $value) {
			$aux = Ejemplar::where('id','=',$value->id_ejemplar)->get()->first();
			if(!empty($data[$aux->recurso_id]))
				return json_encode(array('error' => 'no es posible prestar 2 ejemplares del mismo recurso.' ));
		}

		
		$array_prest_info = array();
	
		//guardar préstamo
		foreach ($data as $key => $value) {
			if($key!='usuario_id' && $key!='admin_id' && !empty($value)){

				$ejemp = Ejemplar::where('recurso_id', '=', $key)
		        				 ->where('ejemplar','!=','0001')->get()->first();

				//guardar prestamo
				$prest = new Prestamo;
		        $prest->id       = $data['usuario_id'];
		        $prest->id_ejemplar    = $ejemp->id;
		        date_default_timezone_set('America/Caracas');
		        $prest->fecha_sol = date('Ymd');
		        $prest->fecha_pre = date('Ymd');
		        $prest->hora = date("h:i:sa");
		        if($user->categoria == 'Profesor'){
		        	$prest->fecha_exp = date('Ymd', strtotime($prest->fecha_pre.'+7 weekdays' ));
		        	$array_prest_info[$key] = date('Y-m-d', strtotime($prest->fecha_pre.'+7 weekdays' ));
		        }else{ 
		        	$prest->fecha_exp = date('Ymd', strtotime($prest->fecha_pre.'+3 weekdays' ));
		        	$array_prest_info[$key] = date('Y-m-d', strtotime($prest->fecha_pre.'+3 weekdays' ));
		        }
		        $prest->id_admin = $data['admin_id'];
		        $prest->prestamo = $ejemp->prestamo;
		        $prest->periodos_renov = 0;
		        $prest->save();

		        //actualizar ejemplar
		        $ejemp->edo_prestamo = 'P';
		        $ejemp->save();
		        VariablesReporte::incrementar_variable('recursos_prestados');
		        VariablesReporte::incrementar_variable('recurso_demanda_'.$key);
			}
		}


		VariablesReporte::incrementar_variable('atendido_dependencia_'.$usuario_dependencia.'_tipo_usp_'.$usuario_tipo_usp);

		VariablesReporte::incrementar_variable('usuarios_atendidos');
		VariablesReporte::incrementar_variable('usuarios_atentidos_'.$user->categoria.'_'.$user->dependencia);
		VariablesReporte::incrementar_variable('prestamos');


		if(PrestamosController::check_internet_connection()){

			//enviar correo
			$rec_contr = new RecursosController;

			$user = array(
						  'correos' => array(
							  array('email'  => 'angklun@gmail.com',
							   'name' => 'Cesar Herrera'
							  )
						  ),
						  'recursos' =>  array()
						);	

			foreach ($data as $key => $value) {
				if($key!='usuario_id' && $key!='admin_id' && !empty($value)){
					$aux_rec = $rec_contr->show($key);
					$aux_rec['autores'] = $rec_contr->recurso_autores($key);
					$aux_rec['descriptores'] = $rec_contr->recurso_descriptores($key);
					$aux_rec['fecha_exp'] = $array_prest_info[$key];
					array_push($user['recursos'], $aux_rec);
				}
					
			}

			
			$emailStatus = EmailHelper::sendMail(Templates::prestamo_recursos,array('user' => (object)$user));
		}	

		return $data;
	}

	/**
	 * renovar un préstamo
	 *
	 */
	public function prestamos(){

		$answer = array();



		date_default_timezone_set('America/Caracas');
		$date = date('Ymd');

		$answer['date'] = $date;


		$prestamos = Prestamo::where('id', '!=', '000')->orderBy('fecha_pre', 'DESC')->paginate(5)->toArray();

		$answer['prestamos'] = $prestamos;	

		foreach ($prestamos['data'] as $dev => $val) {
			$ejemplar_aux = Ejemplar::where('id', '=', $val['id_ejemplar'])->get();
			$recurso_id_aux = $ejemplar_aux[0]['recurso_id'];
			/*arreglado ahora envia título*/
			$obj_titulo = RecursosTitulo::where('recurso_id', '=', $recurso_id_aux);
			$id_titulo = $obj_titulo->whereIn('tipo_tit', array('OP', 'OR'))->get();
			$id_titulo = $id_titulo->toArray();
			$titulo = Titulo::find($id_titulo[0]['titulo_id']);
			$titulo = $titulo->toArray();
			$answer['prestamos']['data'][$dev]['titulo'] = $titulo['titulo_salida'];
			/*fin solución para enviar titulo*/
			$cota_aux = Recurso::where('id', '=', $recurso_id_aux)->get();
			$cota = $cota_aux[0]['ubicacion'];
			$answer['prestamos']['data'][$dev]['cota'] = $cota;
		}




		return json_encode($answer);



	}


	/**
	 * renovar un préstamo
	 *
	 */
	public function renovar_prestamo($id_ejemplar, $id){
		
		$prestamo = Prestamo::where('id', '=',$id)
							->where('id_ejemplar', '=', $id_ejemplar)->get()->first();

		if(empty($prestamo))
			return json_encode(array('error' => 'No se encuentra el préstamo'));

		date_default_timezone_set('America/Caracas');
		$hoy = date('Ymd'); 

		if(intval($prestamo['fecha_exp']) == intval($hoy)){
			
			if(intval($prestamo['periodos_renov'])<1)
				return json_encode(array('error' => 'periodos de renovación agotados.'));

			$fecha_exp_despues = date('Ymd', strtotime($hoy.'+3 weekdays' ));

			$prestamo['fecha_exp'] = $fecha_exp_despues;
			$prestamo['periodos_renov'] = intval($prestamo['periodos_renov'])-1;

			$prestamo->save();
			VariablesReporte::incrementar_variable('renovacion');

			return 'renovación exitosa';
		}

		if(intval($prestamo['fecha_exp']) > intval($hoy))
			return json_encode(array('error' => 'no ha llegado el dia de renovar'));
			//return 'solo es posible renovarlo el día en que se vence el préstamo';

		if(intval($prestamo['fecha_exp']) < intval($hoy))
			return json_encode(array('error' => 'vencido el '.date('d/m/Y', strtotime($prestamo['fecha_exp'])) ));
			//return 'No se puede renovar el préstamo, la fecha de expiración ha pasado, el usuario queda suspendido.';

		return $prestamo;
	
	}

	/**
	 * devolver un préstamo
	 *
	 */
	public function devolver_prestamo($id_ejemplar, $id, $id_admin){
		
		$prestamo = Prestamo::where('id', '=',$id)
							->where('id_ejemplar', '=', $id_ejemplar)->get()->first();

		if(empty($prestamo))
			return json_encode(array('error' => 'No se encuentra el préstamo'));

		date_default_timezone_set('America/Caracas');
		$hoy = date('Ymd'); 

		$ejemplar = Ejemplar::where('id', '=', $id_ejemplar)->get()->first();

		//se crea la devolución
		$devolucion = new Devolucion;
		$devolucion->id = $id;
		$devolucion->id_ejemplar = $id_ejemplar;
		$devolucion->fecha_prestamo = $prestamo['fecha_pre'];
		$devolucion->fecha_solicitud = $prestamo['fecha_sol'];
		$devolucion->fecha_expiracion = $prestamo['fecha_exp'];
		$devolucion->fecha_devolucion = $hoy;
		$devolucion->hora = $prestamo['hora'];
		$devolucion->hora_dev = date("h:i:sa");
		$devolucion->cod_adm_prest = $prestamo['id_admin'];
		$devolucion->cod_adm_devol = $id_admin;
		$devolucion->prestamo = $prestamo['prestamo'];
		$devolucion->periodos_renov = '3';

		//verificar si se suspende o no el usuario
		$fecha="2015-04-12 00:00:00";//$prestamo['fecha_exp']
		$segundos=strtotime($prestamo['fecha_exp']) - strtotime('now');
		//$segundos=strtotime($fecha) - strtotime('now');
		$diferencia_dias=intval($segundos/60/60/24);
		if($diferencia_dias<0){
			//suspender usuario.
			$dias_suspension =((-1)*$diferencia_dias)*3;

			$devolucion->fecha_sus = $hoy;

			$last_suspension_id = DB::table('suspension')->max('id');
			
			$suspension = new Suspension;
			$suspension->id = $last_suspension_id+1;
			$suspension->id_usuario = $id;
			$suspension->id_ejemplar = $id_ejemplar;
			$suspension->fecha = $hoy;
			$suspension->tipo = '1';
			$suspension->dias = $dias_suspension;
			$suspension->pago = 'N';


			/*Info Titulos Principales*/
			
			if(!empty($ejemplar))
				$obj_titulo = RecursosTitulo::where('recurso_id', '=', $ejemplar['recurso_id']);
			if(!empty($obj_titulo)){
				$id_titulo = $obj_titulo->whereIn('tipo_tit', array('OP', 'OR'))->get();
				$id_titulo = $id_titulo->toArray();
			}

			if(!empty($id_titulo)){
				$titulo = Titulo::find($id_titulo[0]['titulo_id']);
				$titulo = $titulo->toArray();
			}
			if(!empty($titulo))
				$suspension->observacion = 'Retardo: '.$titulo['titulo_salida'];
			else
				$suspension->observacion = 'Retardo: Título desconocido';

			//suspensión guardada
			$suspension->save();

			

			//guardar devolución
			$devolucion->save();

			//préstamo eliminado
			$prestamo->delete();

			$fecha_suspension_exp = date('d/m/Y', strtotime($hoy.'+'.$dias_suspension.' weekdays' ));
			return 'el usuario fue suspendido hasta el '.$fecha_suspension_exp;
		}

		$ejemplar['edo_prestamo'] = 'D';
		//estado del ejemplar actualizado.
		$ejemplar->save();

		//guardar devolución
		$devolucion->save();

		//préstamo eliminado
		$prestamo->delete();

		return 'devolución exitosa';	
	}

	/**
	 * renovar varios préstamo
	 *
	 */
	public function renovar_prestamos(){
		
		$data_obj = Input::all();
		$data = $data_obj['pres_selected'];
		//return $data;

		$id = $data['usuario_id'];


		date_default_timezone_set('America/Caracas');
		$hoy = date('Ymd');

		foreach ($data as $key => $value) {
			if($key != 'admin_id' && $key != 'usuario_id' && !empty($value)){
				$id_ejemplar = $key;
				$prestamo = Prestamo::where('id', '=',$id)
							->where('id_ejemplar', '=', $id_ejemplar)->get()->first();
				if(empty($prestamo))
					return json_encode(array('error' => 'No se encuentra el préstamo de titulo: '.$data[$key]['titulo']));

				if(intval($prestamo['fecha_exp']) > intval($hoy))
					return json_encode(array('error' => 'no ha llegado el dia de renovar el préstamo de titulo: '.$data[$key]['titulo']));
					//return 'solo es posible renovarlo el día en que se vence el préstamo';

				if(intval($prestamo['fecha_exp']) < intval($hoy))
					return json_encode(array('error' => 'Préstamo con titulo: '.$data[$key]['titulo'].' vencido el '.date('d/m/Y', strtotime($prestamo['fecha_exp'])) ));

				if(intval($prestamo['periodos_renov'])<1)
					return json_encode(array('error' => 'periodos de renovación agotados para el préstamo con titulo: '.$data[$key]['titulo']));

			}
		}

		foreach ($data as $key => $value) {
			if($key != 'admin_id' && $key != 'usuario_id' && !empty($value)){
				$id_ejemplar = $key;
				$prestamo = Prestamo::where('id', '=',$id)
									->where('id_ejemplar', '=', $id_ejemplar)->get()->first();
				if(intval($prestamo['fecha_exp']) == intval($hoy)){
			
					$fecha_exp_despues = date('Ymd', strtotime($hoy.'+3 weekdays' ));

					$prestamo['fecha_exp'] = $fecha_exp_despues;
					$prestamo['periodos_renov'] = intval($prestamo['periodos_renov'])-1;

					
					$prestamo->save();
					VariablesReporte::incrementar_variable('renovacion');
					
				}

			}
		}

		return 'renovación exitosa';

	
	}



	/**
	 * devolver varios préstamo
	 *
	 */
	public function devolver_prestamos(){
		
		$data_obj = Input::all();
		$data = $data_obj['pres_selected'];
		//return $data;

		$id = $data['usuario_id'];
		$id_admin = $data['admin_id'];
		$cantidad_expiraciones = 0;
		$suma_dias_suspension = 0;
		$tiene_suspension = false;

		date_default_timezone_set('America/Caracas');
		$hoy = date('Ymd');


		foreach ($data as $key => $value) {
			if($key != 'admin_id' && $key != 'usuario_id' && !empty($value)){
				$id_ejemplar = $key;
				$prestamo = Prestamo::where('id', '=',$id)
									->where('id_ejemplar', '=', $id_ejemplar)->get()->first();


				if(empty($prestamo))
					return json_encode(array('error' => 'No se encuentra el préstamo'));

				$ejemplar = Ejemplar::where('id', '=', $id_ejemplar)->get()->first();

				//se crea la devolución
				$devolucion = new Devolucion;
				$devolucion->id = $id;
				$devolucion->id_ejemplar = $id_ejemplar;
				$devolucion->fecha_prestamo = $prestamo['fecha_pre'];
				$devolucion->fecha_solicitud = $prestamo['fecha_sol'];
				$devolucion->fecha_expiracion = $prestamo['fecha_exp'];
				$devolucion->fecha_devolucion = $hoy;
				$devolucion->hora = $prestamo['hora'];
				$devolucion->hora_dev = date("h:i:sa");
				$devolucion->cod_adm_prest = $prestamo['id_admin'];
				$devolucion->cod_adm_devol = $id_admin;
				$devolucion->prestamo = $prestamo['prestamo'];
				$devolucion->periodos_renov = '3';

				//verificar si se suspende o no el usuario
				$fecha="2015-04-12 00:00:00";//$prestamo['fecha_exp']
				$segundos=strtotime($prestamo['fecha_exp']) - strtotime('now');
				//$segundos=strtotime($fecha) - strtotime('now');
				$diferencia_dias=intval($segundos/60/60/24);
				if($diferencia_dias<0){
					$tiene_suspension = true;
					//suspender usuario.
					$dias_suspension =((-1)*$diferencia_dias)*3;

					$devolucion->fecha_sus = $hoy;

					$last_suspension_id = DB::table('suspension')->max('id');
					
					$suspension = new Suspension;
					$suspension->id = $last_suspension_id+1;
					$suspension->id_usuario = $id;
					$suspension->id_ejemplar = $id_ejemplar;
					$suspension->fecha = $hoy;
					$suspension->tipo = '1';
					$suspension->dias = $dias_suspension;
					$suspension->pago = 'N';


					/*Info Titulos Principales*/
					
					if(!empty($ejemplar))
						$obj_titulo = RecursosTitulo::where('recurso_id', '=', $ejemplar['recurso_id']);
					if(!empty($obj_titulo)){
						$id_titulo = $obj_titulo->whereIn('tipo_tit', array('OP', 'OR'))->get();
						$id_titulo = $id_titulo->toArray();
					}

					if(!empty($id_titulo)){
						$titulo = Titulo::find($id_titulo[0]['titulo_id']);
						$titulo = $titulo->toArray();
					}
					if(!empty($titulo))
						$suspension->observacion = 'Retardo: '.$titulo['titulo_salida'];
					else
						$suspension->observacion = 'Retardo: Título desconocido';

					//suspensión guardada
					$suspension->save();

					

					//guardar devolución
					$devolucion->save();

					//préstamo eliminado
					$prestamo->delete();
					$cantidad_expiraciones = $cantidad_expiraciones + 1;
					$suma_dias_suspension = $suma_dias_suspension + $dias_suspension;

				}

				$ejemplar['edo_prestamo'] = 'D';
				//estado del ejemplar actualizado.
				$ejemplar->save();

				//guardar devolución
				$devolucion->save();

				//préstamo eliminado
				$prestamo->delete();


			}
		}

		if($tiene_suspension){
			$fecha_suspension_exp = date('d/m/Y', strtotime($hoy.'+'.$suma_dias_suspension.' weekdays' ));
			return 'el usuario fue suspendido hasta el '.$fecha_suspension_exp;
		}

		return 'renovación exitosa';

	
	}
	


	/**
	 * Display the specified resource.
	 * GET /prestamos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /prestamos/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /prestamos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /prestamos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	/**
	 * test view
	 *
	 */
	public function test_view(){

		return View::make('test_prestamo/test_prestamo');
	
	}



	/**
	 * test send
	 *
	 */
	public function prestamo_info(){ //test_send
		
		//$texto = Input::get('text');
		$data = Input::all();
		$id_recursos = '';
		$answer = array();
		$count = 0;

		foreach ($data as $value) {
			$id_recursos = $value;

			/*Info Ejemplares*/

			$obj_ejemplares = Ejemplar::where('recurso_id', '=', $id_recursos);
			$salida = $obj_ejemplares->get();
			$ej_disponibles_count = $obj_ejemplares->where('edo_prestamo', '=', 'D')->count();
			$ej_count = count($salida);
			$salida['cantidad'] = $ej_count;
			$ej_count = count($salida);
			$salida['cantidad_disponibles'] = $ej_disponibles_count;
			if($salida['cantidad_disponibles'] > 1)
				$salida['disponible'] = true;
			else
				$salida['disponible'] = false;
			$salida['recurso_id'] = $id_recursos;
			$obj_recurso =  Recurso::where('id', '=', $salida['recurso_id'])->get();
			$salida['cota'] = $obj_recurso[0]['ubicacion'];

			/*Info Titulos Principales*/
			$obj_titulo = RecursosTitulo::where('recurso_id', '=', $id_recursos);
			$id_titulo = $obj_titulo->whereIn('tipo_tit', array('OP', 'OR'))->get();
			$id_titulo = $id_titulo->toArray();
			$titulo = Titulo::find($id_titulo[0]['titulo_id']);
			$titulo = $titulo->toArray();
			$salida['titulo'] = $titulo['titulo_salida'];

			//$salida = json_encode($salida);
			$answer[$count] = $salida;
			$count = $count + 1;
		}
		//return $salida;
		return json_encode($answer);
	}


	/**
	 * consulta estado de usuario y sus prestamos
	 *
	 */
	public function consulta_ci(){

		
		$ci = Input::get('ci');	

		$answer = array();

		$usuario = Usuario::where('cedula','=',$ci)->get()->first();

		//validar si existe
		if(empty($usuario)){
			$id = Input::get('usuario_id');
			$usuario = Usuario::where('id','=',$id)->get()->first();
			if(empty($usuario))
				return json_encode('{error:usuario inexistente}');
		}

		$answer['name'] = $usuario->nombres;
		$answer['lastname'] = $usuario->apellidos;
		$answer['type'] = $usuario->categoria;
		$answer['dependencia'] = $usuario->dependencia;

		date_default_timezone_set('America/Caracas');
		$date = date('Ymd');

		$answer['date'] = $date;

		$usuario_id = $usuario->id;
		$answer['usuario_id'] = $usuario_id;

		$devolucion = Devolucion::where('id', '=', $usuario_id)->orderBy('fecha_devolucion', 'DESC')->paginate(5)->toArray();
		
		$answer['devolucion'] = $devolucion;

		foreach ($devolucion['data'] as $dev => $val) {
			$ejemplar_aux = Ejemplar::where('id', '=', $val['id_ejemplar'])->get();
			$recurso_id_aux = $ejemplar_aux[0]['recurso_id'];
			$cota_aux = Recurso::where('id', '=', $recurso_id_aux)->get();
			$cota = $cota_aux[0]['ubicacion'];
			$answer['devolucion']['data'][$dev]['cota'] = $cota;
		}

		$suspension = Suspension::where('id_usuario', '=', $usuario_id)
								  ->orderBy('fecha', 'DESC')->get();
		$suspension_p = Suspension::where('id_usuario', '=', $usuario_id)
								  ->orderBy('fecha', 'DESC')->paginate(5)->toArray();
		$prestamos = Prestamo::where('id', '=', $usuario_id)
							->orderBy('fecha_pre', 'DESC')->paginate(5)->toArray();	

		$answer['prestamos'] = $prestamos;	

		foreach ($prestamos['data'] as $dev => $val) {
			$ejemplar_aux = Ejemplar::where('id', '=', $val['id_ejemplar'])->get();
			$recurso_id_aux = $ejemplar_aux[0]['recurso_id'];
			/*arreglado ahora envia título*/
			$obj_titulo = RecursosTitulo::where('recurso_id', '=', $recurso_id_aux);
			$id_titulo = $obj_titulo->whereIn('tipo_tit', array('OP', 'OR'))->get();
			$id_titulo = $id_titulo->toArray();
			$titulo = Titulo::find($id_titulo[0]['titulo_id']);
			$titulo = $titulo->toArray();
			$answer['prestamos']['data'][$dev]['titulo'] = $titulo['titulo_salida'];
			/*fin solución para enviar titulo*/
			$cota_aux = Recurso::where('id', '=', $recurso_id_aux)->get();
			$cota = $cota_aux[0]['ubicacion'];
			$answer['prestamos']['data'][$dev]['cota'] = $cota;
		}


		if(!empty($suspension[0])){
			$total_dias = 0; 
			//return $suspension;
			$suspension_count = Suspension::where('id_usuario', '=', $usuario_id)->count();
			if($suspension_count >= 1) 
				$fecha_menor = $suspension[$suspension_count-1]['fecha'];
			else
				$fecha_menor = '19001010';
			foreach ($suspension as $key => $value) {
				$total_dias = $total_dias + intval($suspension[$key]['dias']);
			}

			$resta_fechas = $date - $fecha_menor;
			$dias	= (strtotime($date)-strtotime($fecha_menor))/86400;
			$dias 	= abs($dias); $dias = floor($dias);
			if($total_dias > $dias)
				$answer['status'] = 'Suspendido por '.$total_dias.' días, restan '.($total_dias-$dias);
			else{
				if($total_dias <= 0)
					$answer['status'] = 'Suspensión Indefinida';
				else
					$answer['status'] = 'Solvente';
			}
		}else
			$answer['status'] = 'Solvente';

		/*
		if(!empty($suspension[0])){
			$resta_fechas = $date - $suspension[0]->fecha;
			$dias	= (strtotime($date)-strtotime($suspension[0]->fecha))/86400;
			$dias 	= abs($dias); $dias = floor($dias);
			if($suspension[0]->dias >= $dias)
				$answer['status'] = 'Suspendido por '.$suspension[0]->dias.' días, restan '.($suspension[0]->dias-$dias);
			else{
				if($suspension[0]->dias <= 0)
					$answer['status'] = 'Suspensión Indefinida';
				else
					$answer['status'] = 'Solvente';
			}
		}else
			$answer['status'] = 'Solvente';

		*/

		$answer['suspension'] = $suspension_p;

		foreach ($suspension_p['data'] as $dev => $val) {
			if($val['id_ejemplar'] == null){
				$answer['suspension']['data'][$dev]['cota'] = 'No existe'; 
				continue;
			}
			$ejemplar_aux = Ejemplar::where('id', '=', $val['id_ejemplar'])->get();
			$recurso_id_aux = $ejemplar_aux[0]['recurso_id'];
			$cota_aux = Recurso::where('id', '=', $recurso_id_aux)->get();
			$cota = $cota_aux[0]['ubicacion'];
			$answer['suspension']['data'][$dev]['cota'] = $cota;
		}	


		return json_encode($answer);
	}


	/**
	 * description
	 *
	 */
	public function consulta_devolucion(){
		
		$devolucion = Devolucion::where('id', '=', Input::get('usuario_id'))
								->orderBy('fecha_devolucion', 'DESC')
								->paginate(5)->toArray();

		foreach ($devolucion['data'] as $dev => $val) {
			$ejemplar_aux = Ejemplar::where('id', '=', $val['id_ejemplar'])->get();
			$recurso_id_aux = $ejemplar_aux[0]['recurso_id'];
			$cota_aux = Recurso::where('id', '=', $recurso_id_aux)->get();
			$cota = $cota_aux[0]['ubicacion'];
			$devolucion['data'][$dev]['cota'] = $cota;
		}

		return json_encode($devolucion);
	}

	/**
	 * description
	 *
	 */
	public function consulta_suspension(){
		
		$suspension = Suspension::where('id_usuario', '=', Input::get('usuario_id'))
								->orderBy('fecha', 'DESC')
								->paginate(5)->toArray();

	    foreach ($suspension['data'] as $dev => $val) {
			if($val['id_ejemplar'] == null){
				$suspension['data'][$dev]['cota'] = 'No existe'; 
				continue;
			}
			$ejemplar_aux = Ejemplar::where('id', '=', $val['id_ejemplar'])->get();
			$recurso_id_aux = $ejemplar_aux[0]['recurso_id'];
			$cota_aux = Recurso::where('id', '=', $recurso_id_aux)->get();
			$cota = $cota_aux[0]['ubicacion'];
			$suspension['data'][$dev]['cota'] = $cota;
		}	

		return json_encode($suspension);
	}	

	/**
	 * description
	 *
	 */
	public function consulta_prestamos(){
		
		$prestamos = Prestamo::where('id', '=', Input::get('usuario_id'))
								->orderBy('fecha_pre', 'DESC')
								->paginate(5)->toArray();

		$answer['prestamos'] = $prestamos;	

		foreach ($prestamos['data'] as $dev => $val) {
			$ejemplar_aux = Ejemplar::where('id', '=', $val['id_ejemplar'])->get();
			$recurso_id_aux = $ejemplar_aux[0]['recurso_id'];
			/*arreglado ahora envia título*/
			$obj_titulo = RecursosTitulo::where('recurso_id', '=', $recurso_id_aux);
			$id_titulo = $obj_titulo->whereIn('tipo_tit', array('OP', 'OR'))->get();
			$id_titulo = $id_titulo->toArray();
			$titulo = Titulo::find($id_titulo[0]['titulo_id']);
			$titulo = $titulo->toArray();
			$answer['prestamos']['data'][$dev]['titulo'] = $titulo['titulo_salida'];
			/*fin solución para enviar titulo*/
			$cota_aux = Recurso::where('id', '=', $recurso_id_aux)->get();
			$cota = $cota_aux[0]['ubicacion'];
			$answer['prestamos']['data'][$dev]['cota'] = $cota;
		}		


		return json_encode($answer['prestamos']);
	}	
	
	
	

}