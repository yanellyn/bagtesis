<?php

class EjemplaresController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /ejemplares
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$ejemplares = Ejemplar::take(10)->skip(0)->get();
		return json_encode($ejemplares);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /ejemplares/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$ejemplares = Ejemplar::take(10)->skip(0)->get();
		return  $ejemplares;
		//return $ejemplares;
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /ejemplares
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /ejemplares/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		return Recurso::find($id)->ejemplares;
		//return Ejemplar::find($id)->recurso;

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /ejemplares/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /ejemplares/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /ejemplares/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function obtener_ejemplares(){
		$data = Input::get('idrecurso');
		$ejemplares_recurso = DB::table('ejemplares')
								   ->join('tipo_docs', 'tipo_docs.id','=','ejemplares.tipo_liter')
								   ->orderBy('ejemplares.ejemplar','ASC')
								   ->where('ejemplares.recurso_id','=',$data)
								   ->select('ejemplares.*', 'tipo_docs.descrip_doc')
								   ->get();
		return json_encode($ejemplares_recurso);
	}	

	public function agregar_ejemplar(){
		$data = Input::all();
		$fecha_actual = date('Ymd');
		$aux_id = "T0";
		$query_id_ejemplar = DB::table('ejemplares')
                    ->where('ejemplares.id','like',$aux_id.'%')
                    ->select(DB::raw('max(ejemplares.id) as ejemplar_id'))->first();

   		$id_ejemplar_actual = $query_id_ejemplar->ejemplar_id;
    	$int_id_ejemplar = filter_var($id_ejemplar_actual, FILTER_SANITIZE_NUMBER_INT);
    	$id_ejemplar_nuevo = $aux_id.(string)($int_id_ejemplar + 1);

    	$query_num_ejemplar = DB::table('ejemplares')
                    ->where('ejemplares.recurso_id','=', $data['idRecurso'])
                    ->select(DB::raw('max(ejemplares.ejemplar) as ejemplar_num'))->first();
          
   		$num_ejemplar_actual = $query_num_ejemplar->ejemplar_num;
    	$int_num_ejemplar = filter_var($num_ejemplar_actual, FILTER_SANITIZE_NUMBER_INT);
    	$num_ejemplar_nuevo = (string)($int_num_ejemplar + 1);

    	if(strlen($num_ejemplar_nuevo) < 4){
	    	$aux = $num_ejemplar_nuevo;
	    	for($i = strlen($num_ejemplar_nuevo); $i<4; $i++){
	    		$aux = '0'.$aux;			
	    	}

	    	$num_ejemplar_nuevo = $aux;
	    }
		$ejemplar = new Ejemplar;	
			$ejemplar->ext_recurso_id = 0;
			$ejemplar->recurso_id = $data['idRecurso'];
			$ejemplar->id = $id_ejemplar_nuevo;
			$ejemplar->ejemplar = $num_ejemplar_nuevo;
			$ejemplar->existencia = 'E';
			$ejemplar->estado = $data['estado'];
			$ejemplar->prestamo = $data['prestamo'];
			$ejemplar->nro_inventario = $fecha_actual;
			$ejemplar->tipo_liter = $data['tipo_liter'];
			$ejemplar->tipo_encuader = $data['tipoencuader'];
			$ejemplar->reserva = $data['reserva'];
			$ejemplar->forma_adquisicion = $data['adquisicion'];
			$ejemplar->proveedor = $data['proveedor'];
			$ejemplar->propiedad = $data['propietario'];
			$ejemplar->edo_prestamo = 'D';
			$ejemplar->doi = $data['doi'];
			$ejemplar->doc_electronico = $data['esElectronico'];
			$ejemplar->nombre_doc_electronico = $data['nombreDocElectronico'];
			$ejemplar->save();
			$respuesta = array("codigo" => "0" ,"ejemplar" => $ejemplar->ejemplar , "id_ejemplar"=> $ejemplar->id);
			
			date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data['idCatalogador'];
			$historial->recurso_id = $data['idRecurso'];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Agregar'; 
			$historial->modulo = 'Ejemplares';
			$historial->fecha = $date;
			$historial->save();

			return json_encode($respuesta);
	}

	public function modificar_ejemplar(){
		$fecha_actual = date('Ymd');
		$data = Input::all();														
		$query_num_ejemplar = DB::table('ejemplares')
                    ->where('ejemplares.id','=', $data['idEjemplar'])
                    ->update(array( 			
							'existencia' => 'E',
							'estado' => $data['estado'],
							'prestamo' => $data['prestamo'],
							'nro_inventario' => $fecha_actual,
							'tipo_liter' => $data['tipo_liter'],
							'tipo_encuader' => $data['tipoencuader'],
							'reserva' => $data['reserva'],
							'edo_prestamo' => 'D',
							'forma_adquisicion' => $data['adquisicion'],
							'proveedor' =>  $data['proveedor'],
							'propiedad' =>  $data['propietario'],
							'doi' => $data["doi"],
							'doc_electronico' => $data["esElectronico"],
							'nombre_doc_electronico' => $data["nombreDocElectronico"]
							)
                   	);
        $respuesta = array("codigo" => "0");
        date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
		$historial->id_catalogador =  $data['idCatalogador']; 
		$historial->recurso_id = $data['idRecurso'];
		$historial->ext_recurso_id = '0';
		$historial->operacion = 'Modificar'; 
		$historial->modulo = 'Ejemplares';
		$historial->fecha = $date;
		$historial->save();

			return json_encode($respuesta);
	}

	public function eliminar_ejemplar(){
		$data = Input::all();
		$query_num_ejemplar = DB::table('ejemplares')
                    ->where('ejemplares.id','=', $data['idEjemplar'])
                    ->delete();
         $respuesta = array("codigo" => "0");
        
        date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
		$historial->id_catalogador =  $data['idCatalogador'];
		$historial->recurso_id = $data['idRecurso'];
		$historial->ext_recurso_id = '0';
		$historial->operacion = 'Eliminar'; 
		$historial->modulo = 'Ejemplares';
		$historial->fecha = $date;
		$historial->save();
         return $respuesta;
	}

	public function obtener_estados_ejemplares(){
		return TipoEstadoEjemplar::all(); 
	}

}