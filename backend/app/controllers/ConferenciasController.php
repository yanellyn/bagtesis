<?php

class ConferenciasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /conferencias
	 *
	 * @return Response
	 */
	public function index()
	{

		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /conferencias/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /conferencias
	 *
	 * @return Response
	 */
	public function store($dataConferencia = null){
		
		if($dataConferencia){
			$data = $dataConferencia;
		}else{
			$data = Input::all();
		}

		
		$query_conferencia = DB::table('conferencias')
			->where('conferencias.conferencia', '=',strtoupper( App::make('BuscadoresController')->convetir_string($data['nombreConf'])))
			->select('conferencias.*')
			->first();


		if($query_conferencia){
			$conferencia = Conferencia::find($query_conferencia->id);
			$conferencia->conferencia = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreConf']));
			$conferencia->conferencia_salida = $data['nombreConf'];
			$conferencia->ciudad = $data['ciudadConf'];
			$conferencia->pais_id = $data['paisConf'];
			$conferencia->patrocinador = $data['patrocinadorConf'];
			$conferencia->fecha = $data['fechaConf'];
			$conferencia->save();


			$query_conferencia_recurso = DB::table('recursos_conferencias')
						->join('conferencias','conferencias.id','=', 'recursos_conferencias.conferencia_id')
						->where('recursos_conferencias.recurso_id', '=',$data['idRecurso'])
						->select('recursos_conferencias.*')
						->first();

			if(!$query_conferencia_recurso ){
				
				$recurso_conferencia = new RecursosConferencia;
				$recurso_conferencia->recurso_id =  $data['idRecurso'];
				$recurso_conferencia->ext_recurso_id = 0;
				$recurso_conferencia->conferencia_id = $conferencia->id;
				$recurso_conferencia->save();
			}

			date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando

			$historial->recurso_id = $data['idRecurso'];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Agregar'; 
			$historial->modulo = 'Conferencias';
			$historial->fecha = $date;
			$historial->save();
			$respuesta = array("codigo" => "0");

		}else{

			$conferencia = new Conferencia;
			$conferencia->conferencia = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreConf']));
			$conferencia->conferencia_salida = $data['nombreConf'];
			$conferencia->ciudad = $data['ciudadConf'];
			$conferencia->pais_id = $data['paisConf'];
			$conferencia->patrocinador = $data['patrocinadorConf'];
			$conferencia->fecha = $data['fechaConf'];
			$conferencia->save();


			$recurso_conferencia = new RecursosConferencia;
			$recurso_conferencia->recurso_id =  $data['idRecurso'];
			$recurso_conferencia->ext_recurso_id = 0;
			$recurso_conferencia->conferencia_id = $conferencia->id;
			$recurso_conferencia->save();

			date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
			$historial->recurso_id = $data['idRecurso'];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Agregar'; 
			$historial->modulo = 'Conferencias';
			$historial->fecha = $date;
			$historial->save();
			$respuesta = array("codigo" => "0");
		}

		return $respuesta;
		//
	}

	/**
	 * Display the specified resource.
	 * GET /conferencias/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$query_conferencia = DB::table('recursos_conferencias')
						->join('conferencias','conferencias.id','=', 'recursos_conferencias.conferencia_id')
						->where('recursos_conferencias.recurso_id', '=',$id)
						->select('conferencias.*')
						->first();

		if($query_conferencia){
			$respuesta = array("codigo" => "0", "datos" => $query_conferencia);
		}else{
			$respuesta = array("codigo" => "1");
		}



		return $respuesta;
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /conferencias/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /conferencias/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();
		$query_conferencia = DB::table('recursos_conferencias')
						->join('conferencias','conferencias.id','=', 'recursos_conferencias.conferencia_id')
						->where('recursos_conferencias.recurso_id', '=',$id)
						->select('conferencias.*')
						->first();

		$conferencia = Conferencia::find($query_conferencia->conferencia_id);
		$conferencia->conferencia = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreConf']));
		$conferencia->conferencia_salida = $data['nombreConf'];
		$conferencia->ciudad = $data['ciudadConf'];
		$conferencia->pais_id = $data['paisConf'];
		$conferencia->patrocinador = $data['patrocinadorConf'];
		$conferencia->fecha = $data['fechaConf'];
		$conferencia->save();

		date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
		$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
		$historial->recurso_id = $id;
		$historial->ext_recurso_id = '0';
		$historial->operacion = 'Modificar'; 
		$historial->modulo = 'Conferencias';
		$historial->fecha = $date;
		$historial->save();

		$respuesta = array("codigo" => "0");
		return $respuesta;


		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /conferencias/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Input::all();
		$query_conferencia = DB::table('recursos_conferencias')
						->join('conferencias','conferencias.id','=', 'recursos_conferencias.conferencia_id')
						->where('recursos_conferencias.recurso_id', '=',$id)
						->first();

		$conf_id = $query_conferencia->conferencia_id;

		$query_conferencia = DB::table('recursos_conferencias')
						->join('conferencias','conferencias.id','=', 'recursos_conferencias.conferencia_id')
						->where('recursos_conferencias.recurso_id', '=',$id)
						->delete();

		$conferencia = DB::table('conferencias')
					->where('id','=',$conf_id)
					->delete();

		date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
		$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
		$historial->recurso_id = $id;
		$historial->ext_recurso_id = '0';
		$historial->operacion = 'Eliminar'; 
		$historial->modulo = 'Conferencias';
		$historial->fecha = $date;
		$historial->save();
		$respuesta = array("codigo" => "0");
		return $respuesta;
	}

	public function conferencias_sistema()
	{
		$query_conferencia = DB::table('conferencias')
						->select('conferencias.*')
						->get();
		return json_encode($query_conferencia);
		//
	}
}