<?php

include(app_path().'/helpers/emailHelper.php');


class RecursosController extends \BaseController {

	public static $email = 'alvaropaz20@gmail.com';

	/**
	 * Display a listing of the resource.
	 * GET /recursos
	 *
	 * @return Response
	 */

	public function index()
	{
		//Hay que traer los recursos por cada página
		//valor por defecto 15 recursos por página.
		$rpp = 5; // Recursos por página
	//	$foo = Recurso::take($rpp)->skip(0)->get();
		$recursos =  Recurso::take($rpp)->skip(0)->get();	
		return $recursos;
		//print_r($foo);
//		return $foo;

	}



	/**
	 * get modal info
	 *
	 */
	public function modal_info($id, $ejemplar){

		$answer = array();

		if($ejemplar == 1){
			$ejemplar_obj = Ejemplar::where('id', '=', $id)->get()->first();
			$id = $ejemplar_obj['recurso_id'];
		}

		$recurso_obj = Recurso::where('id', '=', $id)->get()->first();

		$dependencia = $this->recurso_dependencias($id);

		if(!empty($dependencia)){
			reset($dependencia);
			$answer['dependencia'] = $dependencia[0]->dependencia_salida;
			if($answer['dependencia'] == '' || empty($answer['dependencia']))
				$answer['dependencia'] = 'no disponible';
		}else
			$answer['dependencia'] = 'no disponible';

		if(!empty($recurso_obj)){

			$tipo_doc = TipoDoc::where('id', '=',$recurso_obj['tipo_liter'] )->get()->first();
			$answer['tipo_recurso'] = $tipo_doc['descrip_doc'];
			$answer['paginas'] = $recurso_obj['paginas'];
			$answer['anio'] = $recurso_obj['fecha_pub'];
			$answer['cota'] = $recurso_obj['ubicacion'];
			if($answer['cota'] == '' || empty($answer['cota']))
				$answer['cota'] = 'no disponible';

			if($answer['paginas'] == '' || empty($answer['paginas']))
				$answer['paginas'] = 'no disponible';

			if($answer['anio'] == '' || empty($answer['anio']))
				$answer['anio'] = 'no disponible';

			$answer['isbn'] = $recurso_obj['isbn'];

			if($answer['isbn'] == '' || empty($answer['isbn']))
				$answer['isbn'] = 'no disponible';
		}

		$answer['titulo'] = $this->recurso_titulos_principal($id);

		$autores = $this->recurso_autores($id);
		$autores_count = 0;
		if(!empty($autores)){
			foreach ($autores as $autor) {
				$answer['autores'][$autores_count]['tipo'] = $autor->nombre;
				$answer['autores'][$autores_count]['nombre'] = $autor->autor_salida;
				$autores_count = $autores_count + 1;
			}
		}else{
			$answer['autores'] = 'no disponible';
		}

		$recurso_editorial = RecursosEditoriales::where('recurso_id', '=', $id)->get()->first();
		$editorial_id = $recurso_editorial['editorial_id'];
		$editorial = Editorial::where('id', '=', $editorial_id)->get()->first();
		if(!empty($editorial))
			$answer['editorial'] = $editorial['editorial_salida'];
		else
			$answer['editorial'] = 'no disponible';

		return $answer;
	
	}
	

	/**
	 * Show the form for creating a new resource.
	 * GET /recursos/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /recursos
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /recursos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

//		return $this->recurso_escuela($id);

		$recurso = Recurso::find($id);				// Optimizar colocando el select

		if($recurso->tipo_liter == 'M'){
			
				$aux_isbn = Libro::select('isbn')
								 ->where('recurso_id','=',$id)
								 ->get();
				if($aux_isbn[0]->isbn != null){
					$recurso['isbn'] = $aux_isbn[0]->isbn;	
				}else{
					$recurso['isbn'] = 'isbn no catalogado';
				}				 
		}else{
			if($recurso->tipo_liter == 'S'){
				$aux_issn = PublicacionesSeriada::select('issn')
									->where('recurso_id','=',$id)
									->get();
				if(empty($aux_issn)){
					if($aux_issn[0]->issn != null){
						$recurso['issn'] = $aux_issn[0]->issn;
					}else{
						$recurso['issn'] = 'issn no catalogado';
					}
				}else{
					$recurso['issn'] = 'issn no catalogado';
				}

				$recurso['publicacion_seriada'] = $this->recurso_publicacion_seriada($id);

			}else if($recurso->tipo_liter == 'T'){
				$recurso['grado_academico'] = $this->recurso_grado_academico($id);
				$recurso['institucion'] = $this->recurso_institucion($id);
				$recurso['nivel'] = $this->recurso_nivel($id);
				$recurso['trabajo'] = $this->recurso_trabajo($id);
 			}else{
 				$recurso['campos_adicionales'] = $this->recurso_camposadicionales($id, $recurso->tipo_liter);
 			}
		} 

		$recurso['titulos']      = $this->recurso_titulos($id);
		$recurso['tipo_doc']     = $this->recurso_doc($id);
		$recurso['fecha_pub']    = $this->recurso_fecha($recurso);
		$recurso['autores']		 = $this->recurso_autores($id);
		$recurso['descriptores'] = $this->recurso_descriptores($id);
		$recurso['bibliotecas']  = $this->recurso_bibliotecas($id);
		$recurso['dependencias'] = $this->recurso_dependencias($id);
		$recurso['disciplinas'] = $this->recurso_disciplinas($id);
		$recurso['cantidad']	 = $this->cantidad($id);
		$recurso['cantidad_disponible'] = $this->cantidad_disponible($id);
		$recurso['editoriales']  = $this->recurso_editoriales($id);
		$recurso['soporte']      = $this->recurso_soporte($id);
		$recurso['conferencias'] = $this->recurso_conferencia($id);
		$recurso['ejemplar'] 	 = $this->recurso_ejemplares($id);	
		$recurso['volumenes'] 	 = $this->recurso_volumenes($id);
		return $recurso;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /recursos/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /recursos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /recursos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public static function recurso_volumenes($id){
		$query_volumen = DB::table('volumenes')
						->join('recursos','recursos.id','=', 'volumenes.recurso_id')
						->where('volumenes.recurso_id', '=',$id)
						->orderBy('volumenes.id', 'ASC')
						->select('volumenes.id','volumenes.recurso_id','volumenes.volumen','volumenes.subtitulo','volumenes.isbn','volumenes.ano')
						->get();
        return $query_volumen;
	}

	public static function recurso_publicacion_seriada($id){

		$publicacionSeriada = DB::table('publicaciones_seriadas')
						->where('publicaciones_seriadas.recurso_id', '=',$id)
						->where('publicaciones_seriadas.nivel_reg', '=','s')
						->select('publicaciones_seriadas.*')
						->get();
						
        return $publicacionSeriada;
	}


	public static function recurso_soporte($id){
		$soportesRecurso = DB::table('soportes')
							->join('recursos_soportes','recursos_soportes.soporte_id','=','soportes.id')
							->where('recursos_soportes.recurso_id','=',$id)
                            ->orderBy('soportes.soporte', 'ASC')
                            ->select('soportes.id','soportes.soporte','soportes.soporte_salida')
                            ->get();
        return $soportesRecurso;
	}

	public static function recurso_conferencia($id){
		$query_conferencia = DB::table('recursos_conferencias')
						->join('conferencias','conferencias.id','=', 'recursos_conferencias.conferencia_id')
						->where('recursos_conferencias.recurso_id', '=',$id)
						->select('conferencias.*')
						->get();
		return $query_conferencia;	
	}



	public static function recurso_camposadicionales($id, $tipo_doc){
		$query_campo_adicional = DB::table('recursos_camposadicionales')
						->join('camposadicionales_tipodoc','camposadicionales_tipodoc.campo_adicional','=', 'recursos_camposadicionales.campo_adicional')
						->join('campos_adicionales','campos_adicionales.id','=', 'camposadicionales_tipodoc.campo_adicional')
						->where('recursos_camposadicionales.recurso_id', '=',$id)
						->where('recursos_camposadicionales.tipo_doc', '=',$tipo_doc)
						->select('campos_adicionales.*','recursos_camposadicionales.descripcion as descripcion')
						->get();
						$query_campo_adicional = array_unique($query_campo_adicional, SORT_REGULAR);
						$aux = [];
						$query_campo_adicional = array_merge($query_campo_adicional,$aux); // para evitar q el json se reotne con posiciones adicionales
		return $query_campo_adicional;	
	}

	public static function recurso_ejemplares($id){
		$query_ejemplares = DB::table('ejemplares')
            ->where('ejemplares.recurso_id','=', $id)
            ->orderby('ejemplares.ejemplar', 'ASC')
            ->select('ejemplares.*')->first();
		return $query_ejemplares;	
	}

	public static function recurso_titulos_principal($id){
			$recursos_titulo = RecursosTitulo::where('recurso_id', '=', $id)
						  				->where('tipo_tit', '=', 'OP')->get()->first();
			$titulo =  Titulo::where('id', '=', $recursos_titulo['titulo_id'])->get()->first();

			if(empty($titulo)){
				return false;
			}
			return $titulo['titulo_salida'];
    }			

    public static function cantidad($id)
    {
    	$obj_ejemplares = Ejemplar::where('recurso_id', '=', $id);
		$salida = $obj_ejemplares->get();
		return count($salida);
    }

    public static function cantidad_disponible($id)
    {
    	$obj_ejemplares = Ejemplar::where('recurso_id', '=', $id);
		return $obj_ejemplares->where('edo_prestamo', '=', 'D')->count();
    }

	public function recurso_titulos($id){
			$recursos_titulos =  DB::table('recursos_titulos')
									->where('recursos_titulos.recurso_id','=',$id)
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->join('tipotitulo',function($join){
										$join->on('tipotitulo.tipo_titulo','=','recursos_titulos.tipo_tit');
         								$join->on('tipotitulo.tipo_doc','=','recursos_titulos.tipo_doc');
									})
									->orderby('recursos_titulos.orden', 'ASC')
									->select('titulos.id', 'recursos_titulos.orden','recursos_titulos.tipo_tit','tipotitulo.descripcion','titulo','titulos.titulo_salida')
									->get();
			return $recursos_titulos;
		
			/*	$recurso_titulos = DB::select(DB::raw("SELECT r.ubicacion, ct.tipo_tit, tt.descripcion, t.titulo_salida
  						FROM recursos r, recursos_titulos ct, titulos t, tipotitulo tt
  						WHERE r.id = '".$id."' "."    
  						AND  r.id = ct.recurso_id 
  						AND  ct.titulo_id = t.id
  						AND  ct.tipo_tit = tt.tipo_titulo
  						AND  ct.tipo_doc =  tt.tipo_doc;
  						"
					 ));
			*/
			return $recurso_titulos;
		

	/*		$recursos_titulos = DB::table('recursos_titulos')
									
									->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
									->join(function($join){
										$join->on('tipotitulo.tipo_titulo','=','recursos_titulos.tipo_tit');
         								$join->on('tipotitulo.tipo_doc','=','recursos_titulos.tipo_doc');
									})
									->where('recursos_titulos.recurso_id','=',$id)
									->select('recursos_titulos.tipo_tit','tipotitulo.descripcion', 'titulos.titulo_salida')
									->get();
			return $recursos_titulos;
	*/
	}		

	public function recurso_nivel($id){
		/*$nivel = DB::table('tesis')
		           ->join('grados_academicos', 'grados_academicos.id', '=', 'tesis.grado_academico_id' )
				   ->where('tesis.recurso_id', '=', $id)
				   ->select('grados_academicos.*' )
				   ->first();
		*/
		$nivel = DB::table('tesis')
		           ->join('niveles_academicos', 'niveles_academicos.id', '=', 'tesis.nivel_id' )
				   ->where('tesis.recurso_id', '=', $id)
				   ->select('niveles_academicos.*' )
				   ->first();
		if($nivel)
			return $nivel;
		else
			return '';
	}

	public function recurso_trabajo($id){

		$trabajo = DB::table('tesis')
		           ->join('tipo_trabajo', 'tipo_trabajo.id', '=', 'tesis.trabajo_id' )
				   ->where('tesis.recurso_id', '=', $id)
				   ->select('tipo_trabajo.*' )
				   ->first();
		if($trabajo)
			return $trabajo;
		else
			return '';
	}

	public function recurso_institucion($id){
		$institucion = DB::table('tesis')
				   ->join('instituciones', 'instituciones.id', '=', 'tesis.cod_inst' )
				   ->where('tesis.recurso_id', '=', $id)
				   ->select('instituciones.*')
				   ->first();
		if($institucion)
			return $institucion;
		else
			return [];
	}

	public function recurso_grado_academico($id){
		$grado_acad = DB::table('tesis')
		->where('tesis.recurso_id', '=', $id)
				   ->select('tesis.*')
				   ->first();		
		if($grado_acad)
			return $grado_acad->grado_acad;
		else
			return '';
	}

	public function recurso_editoriales($id){
		$recurso_editoriales = DB::table('recursos_editoriales')
								   ->join('editoriales', 'editoriales.id','=','recursos_editoriales.editorial_id')
								   ->where('recursos_editoriales.recurso_id','=',$id)
								   ->select('editoriales.*')
								   ->get();
		return $recurso_editoriales;
	}


	public function recurso_bibliotecas($id){
		$recurso_bibliotecas = DB::table('recursos_bibliotecas')
							   ->join('bibliotecas','bibliotecas.id','=','recursos_bibliotecas.biblioteca_id')
							   ->where('recursos_bibliotecas.recurso_id','=',$id)
							   ->select('bibliotecas.*')
							   ->get();
		return $recurso_bibliotecas;
	}


	public function recurso_dependencias($id){
		$recurso_dependencias = DB::table('recursos_dependencias')
							   ->join('dependencias','dependencias.id','=','recursos_dependencias.dependencia_id')
							   ->where('recursos_dependencias.recurso_id','=',$id)
							   ->select('dependencias.*')
							   ->get();
		return $recurso_dependencias;
	}

	public function recurso_disciplinas($id){
		$recurso_disciplinas = DB::table('recursos_disciplinas')
							   ->join('disciplinas','disciplinas.id','=','recursos_disciplinas.disciplina_id')
							   ->where('recursos_disciplinas.recurso_id','=',$id)
							   ->select('disciplinas.*')
							   ->get();
		return $recurso_disciplinas;
	}


	public function recurso_doc($id){
           return  TipoDoc::find(Recurso::find($id)->tipo_liter)->descrip_doc_salida;
    }

    public function recurso_dependencia($id){
    		return  Dependencia::find(Recurso::find($id)->dependencia_id);
    }


    public function recurso_fecha($recurso){
		if($recurso['fecha_iso'] != null && $recurso['fecha_pub'] == null){
                		$aux = $recurso['fecha_iso'];
                		$recurso['fecha_pub']= $aux[0].''.$aux[1].''.$aux[2].''.$aux[3];
            }else{
            	if($recurso['fecha_iso'] == null && $recurso['fecha_pub'] == null){
					$recurso['fecha_pub'] = 'Año no catalogado';
            	}	
            }
        return $recurso['fecha_pub'];
	}

	public function recurso_escuela($id){
		return Escuela::find(Tesis::where('recurso_id','=',$id)->get()[0]->escuela_id)['escuela_salida'];
	}

	public function recurso_autores($id){
	
	   $recurso_autores = DB::select(DB::raw("SELECT "." r.id, r.ubicacion, ra.cod_autor, ra.tipo_autor, ta.nombre, a.autor,a.autor_salida, a.id as id_autor, a.dir_electr as correo, a.tipo_autor as tipo_autor_principal, ra.orden as orden2, ta.orden  "."FROM recursos r, recursos_autores ra, autores a, tipo_autores ta

						 WHERE "."   ra.recurso_id = '".$id."'  AND ra.recurso_id = r.id   AND  ra.cod_autor = a.id  AND ra.tipo_doc = ta.tipo_doc AND ra.tipo_autor = ta.tipo_autor ORDER BY (ta.orden)")); 
	   return $recurso_autores;
	}


	public function recurso_descriptores($id){
		$i = 0;

		/*	SELECT rd.recurso_id, rd.descriptor_id,  d.descriptor, d.tipo, td.descrip_desc
			FROM  recursos_descriptores rd, descriptores d, tipo_descriptores td
			WHERE rd.recurso_id = 'T043500008723'
			AND  rd.descriptor_id = d.id
			AND  d.tipo =  td.id;
		*/


		$recurso_descriptores = DB::table('recursos_descriptores')
                            ->join('descriptores', 'descriptores.id', '=', 'recursos_descriptores.descriptor_id')              
                            ->join('tipo_descriptores', 'tipo_descriptores.id', '=', 'descriptores.tipo')                                     
                            ->where('recursos_descriptores.recurso_id','=',$id)
                            ->select('recursos_descriptores.recurso_id','recursos_descriptores.descriptor_id','descriptores.tipo','tipo_descriptores.descrip_desc','descriptores.descriptor_salida','descriptores.codigo_externo','descriptores.describe')->get();
       foreach ($recurso_descriptores as $descriptor) {
        
    		if($descriptor->tipo == 'M-040'){
        		//	$recurso_descriptores[$i] = ;
    		//	unset($recurso_descriptores[$i]);
    		}      

        	if($descriptor->descriptor_salida[0] == '$' && strlen($descriptor->descriptor_salida)>2  ){
        		$recurso_descriptores[$i]->descriptor_salida = substr($descriptor->descriptor_salida,2,strlen($descriptor->descriptor_salida)-1);  
 
        	     //return substr($descriptor->descriptor_salida,2,strlen($descriptor->descriptor_salida)-1);
        		
        		//return $descriptor->descriptor_salida;
        		//return substr($descriptor->descrip_salida, 2, 5);
        	}
        	$i++;
        } 

        return $recurso_descriptores;
		//http://localhost/bagtesis/backend/public/recursos/T043500019120/descriptores

	}


	public function mail($id){
		/*$user = (object) array( 'email'  => 'angklun@gmail.com', 
					   'name' => 'Cesar Herrera'
				);
		*/

		$user = array( 
				   'correos' => array(
				   					array('email'  => 'albertpaz8@gmail.com', 
				   						  'name' => 'Albert Paz'
				   					),

   									array('email'  => 'alvaropaz20@gmail.com', 
				   						  'name' => 'Alvaro Paz'
				   					
				   					),
				   				),
				   'datos' => 'Datos del Recurso'	
					);


		$emailStatus = EmailHelper::sendMail(Templates::envio_recurso,array('user' => (object) $user));
	}

	public function recursos_mail(){

		$input = Input::all();
		$user = $input; 

		$user['recursos'] = array();
		foreach ($user['id_recursos'] as $recurso_id) {
			$recurso = $this->show($recurso_id);
			$recurso['autores'] = $this->recurso_autores($recurso_id);
			$recurso['descriptores'] = $this->recurso_descriptores($recurso_id);
			array_push($user['recursos'], $recurso);
	}

//		return $user;

//		$user['recursos'] = array( array('id' => '20'), array('id' => '20'));
	//	return $user;
/*		$user = $input; /*array( 
				   'correos' => array(
				   					array('email'  => 'albertpaz8@gmail.com', 
				   						  'name' => 'Albert Paz'
				   					),

   									array('email'  => 'alvaropaz20@gmail.com', 
				   						  'name' => 'Alvaro Paz'
				   					
				   					),
				   				),
				   'recurso' =>  array()				
				   'datos' => 'Datos del Recurso'	

					);
				*/
//		$user_ =  array('user' =>  $user);
	//	return $user_['user']->email;        //  Template Data            // 

	//	return $user;
		$emailStatus = EmailHelper::sendMail(Templates::envio_recurso,array('user' => (object)$user));
		return "Correo enviado Exitosamente";
	}


	public function recursos_mail2(){	
			return Input::all();
	}

	public function recurso_pdf($id){
		$ids_split =  explode(' ',$id);
	//	return $ids_split;
	//	return $id_split;
		$recursos = array();

		foreach ($ids_split as $key => $id) {
			$recurso = $this->show($id);
			$recurso['autores'] = $this->recurso_autores($id);
			$recurso['descriptores'] = $this->recurso_descriptores($id);
			array_push($recursos, $recurso);
		}		

/*		$recurso = $this->show($ids_split[0]);
		$recurso['autores'] = $this->recurso_autores($ids_split[0]);
		$recurso['descriptores'] = $this->recurso_descriptores($ids_split[0]);
		array_push($recursos, $recurso);
*/
	    $pdf = new \Thujohn\Pdf\Pdf();
	    $content = $pdf->load(View::make('recursos.recurso_pdf')->with('recursos',$recursos))->download();
	    File::put(public_path('test'.'1'.'pdf'), $content);

	}

	public function recursos_pdf(){
		$input = Input::all();
		$id_recursos = $input['id_recursos'];
		$recursos = array();


		foreach ($id_recursos as $recurso_id) {
			$recurso = $this->show($recurso_id);
			$recurso['autores'] = $this->recurso_autores($recurso_id);
			$recurso['descriptores'] = $this->recurso_descriptores($recurso_id);
			array_push($recursos, $recurso);			
		}

	    $pdf = new \Thujohn\Pdf\Pdf();
	    $content = $pdf->load(View::make('recursos.recurso_pdf')->with('recursos',$recursos))->download();
	    File::put(public_path('test'.'1'.'pdf'), $content);

	}

	public function recursos_info(){
		$input = Input::all();
		$recursos_ids = $input;
		$recursos = array();
		foreach ($recursos_ids as $recurso_id) {
			array_push($recursos,$this->show($recurso_id));
		}	
		return $recursos;
	}


	public static function recursos_por_cota($cota,$tipo_doc){
		$recurso_id = Recurso::where('ubicacion','like',$cota)
					          ->where('tipo_liter','=',$tipo_doc)
					          ->get()
					          ->first()->id;
		$recursosController = new RecursosController();
		$recurso =  $recursosController->show($recurso_id);
		$recurso['autores'] = $recursosController->recurso_autores($recurso_id);
		$recurso['descriptores'] = $recursosController->recurso_descriptores($recurso_id);
		return $recurso;
	}

	public static function recursos_por_titulo($titulo,$tipo_doc){
		$recursos = array();
		$recursosController = new RecursosController();
		$recursos_id = DB::table('recursos_titulos')
						 ->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
						 ->where('titulos.titulo','like','%'.$titulo.'%')
						 ->where('recursos_titulos.tipo_doc','=',$tipo_doc)
						 ->select('recurso_id')
						 ->lists('recurso_id');
		return $recursos_id;
	}


	public static function recursos_por_materia($descriptor,$tipo_doc){

		$recursos = array();
		$recursosController = new RecursosController();
		$recursos_id = DB::table('recursos_descriptores')
						 ->join('descriptores','descriptores.id','=','recursos_descriptores.descriptor_id')
						 ->join('tipo_descriptores','tipo_descriptores.id','=','descriptores.tipo')
						 ->join('recursos','recursos.id','=','recursos_descriptores.recurso_id')
						 ->where('recursos.tipo_liter','=',$tipo_doc)
						 ->where(function($query){  
						 		$query->where('tipo_descriptores.id','=','M-650');
						 		$query->orWhere('tipo_descriptores.id','=','M');
						 })
						 ->where('descriptores.descriptor','like','%'.$descriptor.'%')
						 ->select('recursos_descriptores.recurso_id')
						 ->lists('recurso_id');

	   return $recursos_id;
	}	



	public static function recurso_tits($id){
		$recursosController = new RecursosController();
		return $recursosController->recurso_titulos($id);
	}

	public static function create_new_resource(){
	
/*		$input = array( 'id' => 'T123456789120',
						'ext_id' => 0,
						'ubicacion' => 'TG-19679',
						'tipo_liter' => 'T',
						'paginas' => '82 h.',
						'fecha_iso' => '20140000',
						'fecha_pub' => '2014',
						'url'  => 'http://www.widget-pc.com/bagtesis/backend/public/texto_completo/TG-19679.pdf'
				 );
*/				 

	
	//TG-19679
		$usuarios = new Usuario();
		$usuarios->id = 'U12345678915';
		$usuarios->carnet = '20027561';
		$usuarios->dir_electr = 'alvaro.paz.ciens.ucv@gmail.com';
		$usuarios->apellidos  = 'Paz Monsalve';
		$usuarios->nombres    =  'Alvaro Joel';
		$usuarios->cedula = '20027561';
		$usuarios->password = '$2y$10$JW7XtxNqHx38gQuxMQ7ire7LjUIY.m5eNJhdk/St4HTQwIj.ZDT6K';
		$usuarios->sexo = 'M';
		$usuarios->categoria = 'Estudiante';
		$usuarios->fecha_in = '20130121';
		$usuarios->fecha_out = '20140530';
		$usuarios->status= 'S';
		$usuarios->ult_modif = '20130121';
		$usuarios->dependencia = 'COMPUTACION';
		$usuarios->tipo_usp = 'PregComput'; 
		$usuarios->save();
		return 'Exitos!!';
	}


	function validar_fecha($date){
		if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
    		return true;
    	}
    		return false;
	}

	function validar_fecha_espanol($fecha){
		$valores = explode('/', $fecha);
		if(count($valores) == 3 && checkdate($valores[1], $valores[0], $valores[2])){
			return true;
	    }
		return false;
	}

	public function importar($id){
     	$data = Input::all();
    	$file = $data['name'];
    	$file = $data['file'];
    	$user_id = $id;
    	$file= $_FILES["file"]["name"];
    	if(!is_dir("catalogacion/")){
    		mkdir("catalogacion/",0777);
    	}

    	if($file && move_uploaded_file($_FILES["file"]["tmp_name"], "catalogacion/".$file)){
    	}
    	$validaciones = array();
    	$autoresRecursos = array();
    	$titulosRecursos = array();
    	$editorialesRecursos = array();
    	$institucionesRecursos = array();
    	$conferenciaRecursos = array();
    	$soportesRecursos = array();
    	$descriptoresRecursos = array();
    	$volumenesRecursos = array();
    	$recursos = array();
    	$volumenesSeries = array();

   		$rows = Excel::load('\public\catalogacion\\' . $file)->get();
   		$i = 0;
   		$validaciones = array();
   		foreach($rows as $row){
   			if($row->tipo_de_recurso == null){
					$validacion = array('linea' => $i+1, 'campo' => 'Tipo de recurso', 'error' => "El tipo de recurso no puede estar vacío");
					array_push($validaciones, $validacion);	
					break;
			}

			$tiposRecurso = TipoDoc::all();
			$coincideTipo = false;
			foreach ($tiposRecurso as $tipo) {
				if($tipo->id == $row->tipo_de_recurso){
					$coincideTipo = true;
					break;
				}
			}

			if(strtoupper(trim($row->tipo_de_recurso)) != 'M' && strtoupper(trim($row->tipo_de_recurso)) != 'T' && strtoupper(trim($row->tipo_de_recurso)) != 'S' && !$coincideTipo) {
				$validacion = array ('linea' => $i+1, 'campo' => 'Tipo de recurso',  'error' => "El tipo de recurso es inválido, por favor verifique los tipos de recursos válidos en la tabla de formatos");	 
				array_push($validaciones, $validacion);
			}
		}

		if(sizeof($validaciones) != 0){
			return $respuesta = array("file" => $file, "codigo" => "1", "validaciones" => $validaciones);	
		}
		$validaciones = array();
		foreach($rows as $row){
			$descriptores = array();
			if(!$row->cota && (strtoupper(trim($row->tipo_de_recurso)) == "T" || strtoupper(trim($row->tipo_de_recurso)) == "M" ) ) {
				$validacion = array('linea' => $i+1, 'campo' => 'cota', 'error' => "La cota no puede estar vacía");	
				array_push($validaciones, $validacion);

			}
			$autores = array();
			$autor = '';
		    $nombre_autor = '';
		    $tipo_autor = '';
			if(!$row->autor_principal && trim(strtoupper($row->tipo_de_recurso != 'S')) ){
				$validacion = array('linea' => $i+1, 'campo' => 'Autor principal', 'error' => "El autor principal no puede estar vacío");	
				array_push($validaciones, $validacion);
			}else{
				$autor_principal = explode('|', trim($row->autor_principal));
				$indiceAutor = 0;
				foreach($autor_principal as $autor){
					if(trim($autor)!=''){

						
						$aux = explode(':', $autor);
					
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor principal', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}else{
							if($indiceAutor == 0 && $row->tipo_de_recurso != 'S' ){
								$nombre_autor = explode(':', $autor)[1];
								$tipo_autor = explode(':', $autor)[0];
							}
						}
						if($indiceAutor != 0){
							$autorPush = array( 
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "P",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
							array_push($autores, $autorPush);
						}
					$indiceAutor++;
					}
				}	
			}
			if($row->titulo_principal == null){
				$validacion = array('linea' => $i+1, 'campo' => 'Titulo principal', 'error' => "El titulo principal no puede estar vacío");	
				array_push($validaciones, $validacion);
			}
			$fechaFinal = "";
			if(!$this->validar_fecha(explode(' ', trim($row->fecha))[0]) && !$this->validar_fecha_espanol(explode(' ', trim($row->fecha))[0]) && trim($row->fecha)!=null ) {
				$validacion = array('linea' => $i+1, 'campo' => 'Fecha', 'error' => "La fecha es incorrecta. Nota: La sintaxis de la fecha debe ser: dd/mm/aaaa");	 
					array_push($validaciones, $validacion);
			}else{
				if($row->fecha!=null && trim($row->fecha) != ''){
				$fecha = explode('/', trim($row->fecha));
				$fechaFinal = $fecha[2] . $fecha[1] . $fecha[0]; 
				}else{
					$fechaFinal = "";
				}
			}

				$autor_entrada_secundario = explode('|', trim($row->autor_entrada_secundario));
				foreach($autor_entrada_secundario as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor entrada secundario', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "M-700",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$autor_secundario = explode('|', trim($row->autor_secundario));
				foreach($autor_secundario as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor secundario', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "S",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$autor_institucional_entrada_principal = explode('|', trim($row->autor_institucional_entrada_principal));
				foreach($autor_institucional_entrada_principal as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor institucional entrada principal', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "M-110",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
		 
				$autor_tesista = explode('|', trim($row->autor_tesista));
				foreach($autor_tesista as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor tesista', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "T",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);

					}
				}
				$autor_congreso_reunion_entrada_principal = explode('|', trim($row->autor_congreso_reunion_entrada_principal));
				foreach($autor_congreso_reunion_entrada_principal as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor congreso reunion entrada principal', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "M-111",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$autor_traductor = explode('|', trim($row->autor_traductor));
				foreach($autor_traductor as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor traductor', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "R",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$autor_institucional_entrada_secundaria = explode('|', trim($row->autor_institucional_entrada_secundaria));
				foreach($autor_institucional_entrada_secundaria as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor institucional entrada secundaria', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "M-710",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$autor_ilustrador = explode('|', trim($row->autor_ilustrador));
				foreach($autor_ilustrador as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor ilustrador', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "I",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);

						array_push($autores, $autorPush);
					}
				}
				$autor_prologuista = explode('|', trim($row->autor_prologuista));
				foreach($autor_prologuista as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor prologuista', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "O",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$autor_compilador = explode('|', trim($row->autor_compilador));
				foreach($autor_compilador as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor compilador', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "C",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$autor_editor = explode('|', trim($row->autor_editor));
				foreach($autor_editor as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor editor', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "E",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$autor_revisor = explode('|', trim($row->autor_revisor));
				foreach($autor_revisor as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor revisor', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "V",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$autor_director = explode('|', trim($row->autor_director));
				foreach($autor_director as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor revisor', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "D",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$autor_coordinador = explode('|', trim($row->autor_coordinador));
				foreach($autor_coordinador as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Autor coordinador', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "F",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				
				$tutor_industrial = explode('|', trim($row->tutor_industrial));
				foreach($tutor_industrial as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Tutor industrial', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "TI",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$asesor = explode('|', trim($row->asesor));
				foreach($asesor as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Asesor', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "A",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
				$jurado = explode('|', trim($row->jurado));
				foreach($jurado as $autor){
					if(trim($autor)!=''){
						$aux = explode(':', $autor);
						if((strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'P' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'C' && strtoupper(trim(App::make('BuscadoresController')->convetir_string($aux[0]))) != 'I') || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Jurado', 'error' => "
							La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
							array_push($validaciones, $validacion);
							break;
						}
						$autorPush = array(
								"id_recurso" => "",
								"tipo_recurso" => trim($row->tipo_de_recurso),
								"nombreAutor" => trim($aux[1]), 
								"tipoAutor1" => trim($aux[0]), 
								"tipoAutor2" => "J",
								"fechaAutor" => "",
								"correoAutor" => "",
								"entidadAutor" => "",
								"urlAutor" => "",
								"idCatalogador" => $user_id);
						array_push($autores, $autorPush);
					}
				}
			
				if(trim($row->descriptor_materia_tematica)){
					$materias = explode('|', trim($row->descriptor_materia_tematica));
					foreach($materias as $materia){
						if(trim($materia)!=''){
							$aux = explode(':', $materia);
							if(!$aux[0]){
								$validacion = array('linea' => $i+1, 'campo' => 'Descriptor materia', 'error' => ' 
								El encabezamiento temático no puede estar vacío');	 
								array_push($validaciones, $validacion);
								break;

							}
							if(sizeof($aux) != 3 ){
							$validacion = array('linea' => $i+1, 'campo' => 'Descriptor materia', 'error' => ' 
								La sintaxis es inválida, Se debe colocar el encabezamiento tematico, la subdivision general y la subdivision geográfica separadas por dos puntos ":" =. Ej: "Encabezamiento tem : Subdv general : Subdv  geografica". En dado caso de que no posea subdivision general o subdivisión geográfica se deben mantener los dos puntos ":" con el espacio vacío. Por ejemplo Si solo posee encabezamiento temático se debe colocar: "Encabezamiento tematico : : " Nota: Si es mas de un descriptor temático se deben separar por el símbolo pipe: |');	 
								array_push($validaciones, $validacion);
								break;
							}else{
								$descriptorPush = array(
										"id_recurso" => "",
										"tipo_descriptor" => "M-650",
										"temDesc" => trim($aux[0]), 
										"genDesc" => trim($aux[1]), 
										"geoDesc" => trim($aux[2]),
										"descriptor" => "",
										"describe" => "",
										"codigoExterno" => "",
										"idCatalogador" => $user_id);
								array_push($descriptores, $descriptorPush);
							}
						}
					}
				}
				if($row->descriptor_centro_catalogador != null){
					$centros = explode('|', trim($row->descriptor_centro_catalogador));
					foreach($centros as $centro){
						$descriptorPush = array(
								"id_recurso" => "",
								"tipo_descriptor" => "M-040",
								"temDesc" => "", 
								"genDesc" => "", 
								"geoDesc" => "",
								"descriptor" => trim($centro),
								"describe" => "",
								"codigoExterno" => "",
								"idCatalogador" => $user_id);
						array_push($descriptores, $descriptorPush);
					}
				}
				if($row->descriptor_idioma != null){
					$idiomas = explode('|', trim($row->descriptor_idioma));
					foreach($idiomas as $idioma){
						$descriptorPush = array(
								"id_recurso" => "",
								"tipo_descriptor" => "I",
								"temDesc" => "", 
								"genDesc" => "", 
								"geoDesc" => "",
								"descriptor" => trim($idioma),
								"describe" => "",
								"codigoExterno" => "",
								"idCatalogador" => $user_id);
						array_push($descriptores, $descriptorPush);
					}
				}
				if($row->descriptor_sala != null){
					$salas = explode('|', trim($row->descriptor_sala));
					foreach($salas as $sala){
						$descriptorPush = array(
								"id_recurso" => "",
								"tipo_descriptor" => "S",
								"temDesc" => "", 
								"genDesc" => "", 
								"geoDesc" => "",
								"descriptor" => trim($sala),
								"describe" => "",
								"codigoExterno" => "",
								"idCatalogador" => $user_id);
						array_push($descriptores, $descriptorPush);
					}
				}
				
				$nivel_recurso = '';
				$trabajo = '';
				$tutor_academico_principal = '';
				$institucion = array();
				if(trim(strtoupper($row->tipo_de_recurso == "T"))){
				
					if($row->tutor_academico == null){
						$validacion = array('linea' => $i+1, 'campo' => 'Tutor académico', 'error' => "El tutor académico no puede estar vacío");
						array_push($validaciones, $validacion);	
					}else{

						$tutor_academico = explode('|', trim($row->tutor_academico));
						$indiceTutor = 0;
						foreach($tutor_academico as $autor){

							if(trim($autor)!=''){
								$aux = explode(':', $autor);
								if((strtoupper(trim($aux[0])) != 'P' && strtoupper(trim($aux[0])) != 'C' && strtoupper(trim($aux[0])) != 'I') || trim($aux[1]) == ""){
								$validacion = array('linea' => $i+1, 'campo' => 'Tutor académico', 'error' => "
									La sintaxis es inválida. La forma correcta es: La inicial del tipo de autor(Personal: P, Conferencia: C, Institucional: I) seguido de dos puntos (:) y el nombre del autor ej: P: Perez, Pedro. Nota: Si es mas de un autor se deben separar por el símbolo pipe: | ");	 
									array_push($validaciones, $validacion);
									break;
								}
								$tutor_academico_principal = trim($aux[1]);
								if ( $indiceTutor != 0 ){
								$autorPush = array(
									"id_recurso" => "",
									"tipo_recurso" => trim($row->tipo_de_recurso),
									"nombreAutor" => trim($aux[1]), 
									"tipoAutor1" => trim($aux[0]), 
									"tipoAutor2" => "TA",
									"fechaAutor" => "",
									"correoAutor" => "",
									"entidadAutor" => "",
									"urlAutor" => "",
									"idCatalogador" => $user_id);
								array_push($autores, $autorPush);
								
								}
								$indiceAutor++;
							}
						}
					}
					if($row->institucion == null){
						$validacion = array('linea' => $i+1, 'campo' => 'Institucion', 'error' => "La institución no puede estar vacía");	
						array_push($validaciones, $validacion);
					}else{
						$institucion = array(
							"nombre" => trim($row->institucion),
							"url" => "",
							"pais" => "",
							"idCatalogador" => $user_id
							);
					}
				


					if($row->nivel != null){
						$nivel = DB::table('niveles_academicos')
				           ->where('niveles_academicos.nivel', '=', strtoupper(trim(App::make('BuscadoresController')->convetir_string($row->nivel))))
				           ->select('niveles_academicos.*' )
				           ->first();
			
						if($nivel){
							$nivel_recurso = $nivel->id;
						}else{
							$nivel = new NivelAcademico;	
							$nivel->nivel = strtoupper(trim(App::make('BuscadoresController')->convetir_string($row->nivel)));
							$nivel->nivel_salida = trim($row->nivel);
							$nivel->save();
							$nivel_recurso = $nivel->id;
						}	
					
					}else{
						$validacion = array('linea' => $i+1, 'campo' => 'Nivel', 'error' => "El nivel no puede estar vacío");
						array_push($validaciones, $validacion);	
					}

					
					if($row->tipo_trabajo != null){
						$tipo_trabajo = DB::table('tipo_trabajo')
						   ->where('tipo_trabajo.trabajo', '=', strtoupper(trim(App::make('BuscadoresController')->convetir_string($row->tipo_trabajo))))
						   ->select('tipo_trabajo.*' )
						   ->first();
						if($tipo_trabajo){
							$trabajo = $tipo_trabajo->id;
						}else{
							$tipo_trabajo = new TipoTrabajo;	
							$tipo_trabajo->trabajo = strtoupper(trim(App::make('BuscadoresController')->convetir_string($row->tipo_trabajo)));
							$tipo_trabajo->trabajo_salida = trim($row->tipo_trabajo);
							$tipo_trabajo->save();
							$trabajo = $tipo_trabajo->id;
						}	
					}else{
						$validacion = array('linea' => $i+1, 'campo' => 'Tipo trabajo', 'error' => "El tipo de trabajo no puede estar vacío");	
						array_push($validaciones, $validacion);
					}


				}
		
			$titulos = array();

			$indiceTitulo = 0;
			$titulo_principal = explode('|', trim($row->titulo_principal));
			foreach ($titulo_principal as $titulo) {
				if($titulo != '' && $indiceTitulo != 0 ){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "OP",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				
				}
				$indiceTitulo++;
			}

			$titulo_original = explode('|', trim($row->titulo_original));
			foreach ($titulo_original as $titulo) {
				if($titulo != ''){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "OR",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				
				}
			}

			$titulo_traducido = explode('|', trim($row->titulo_traducido));
			foreach ($titulo_traducido as $titulo) {
				if($titulo != ''){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "TP",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				}
			}
			$titulo_mencion_de_responsabilidad = explode('|', trim($row->titulo_mencion_de_responsabilidad));
			foreach ($titulo_mencion_de_responsabilidad as $titulo) {
				if($titulo != ''){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "MR",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				
				}
			}
			$titulo_abreviado = explode('|', trim($row->titulo_abreviado));
			foreach ($titulo_abreviado as $titulo) {
				if($titulo != ''){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "OV",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				
				}
			}
			$titulo_abreviado_traducido = explode('|', trim($row->titulo_abreviado_traducido));
			foreach ($titulo_abreviado_traducido as $titulo) {
				if($titulo != ''){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "TV",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				
				}
			}
			$titulo_anterior = explode('|', trim($row->titulo_anterior));
			foreach ($titulo_anterior as $titulo) {
				if($titulo != ''){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "OA",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				
				}
			}
			$titulo_anterior_traducido = explode('|', trim($row->titulo_anterior_traducido));
			foreach ($titulo_anterior_traducido as $titulo) {
				if($titulo != ''){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "TA",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				
				}
			}
			$titulo_paralelo = explode('|', trim($row->titulo_paralelo));
			foreach ($titulo_paralelo as $titulo) {
				if($titulo != ''){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "OL",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				
				}
			}
			$subtitulo_original = explode('|', trim($row->subtitulo_original));
			foreach ($subtitulo_original as $titulo) {
				if($titulo != ''){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "OS",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				
				}
			}
			$subtitulo_traducido = explode('|', trim($row->subtitulo_traducido));
			foreach ($subtitulo_traducido as $titulo) {
				if($titulo != ''){
					$tituloPush = array(
						"id_recurso" => "",
						"tipo_recurso" => trim($row->tipo_de_recurso),
						"nombreTitulo" =>  trim($titulo), 
						"tipoTitulo" => "TS",
						"idCatalogador" => $user_id);
						
					array_push($titulos, $tituloPush);
				
				}
			}
			$editoriales = array();
			if($row->editoriales!=null){
				$editorialesR = explode('|', trim($row->editoriales));
				foreach ($editorialesR as $editorial) {
					if($editorial != ''){
						$editorialPush = array(
							"idPais" => "",
						    "editorial" => trim($editorial),
						    "fabricante" => "",
						    "ciudadFabricante" => "",
						    "fecha" => "",
						    "ciudad" => "",
						    "tipoDoc" => trim($row->tipo_de_recurso),					    
						    "idRecurso" => "",
						    "idCatalogador" => $user_id);
						array_push($editoriales, $editorialPush);
					
					}
				}
				
			}

			$soportes = array();
			if($row->soporte!=null){
				$soporteR = explode('|', trim($row->soporte));
				foreach ($soporteR as $soporte) {
					if($soporte != ''){
						$soportePush = array(
							"id_recurso" => "",
						    "nombre_soporte" => trim($soporte),
						    "idCatalogador" => $user_id);
						array_push($soportes, $soportePush);
					
					}
				}
			}

			$conferencia = array();
			if($row->conferencia != null){
				$conferencia = array(
					"nombreConf" => trim($row->conferencia),
					"ciudadConf" => trim($row->ciudad_conferencia),
					"paisConf" => "",
					"fechaConf" => "",
					"patrocinadorConf" => "",
					"idRecurso" => "",
					"idCatalogador" => $user_id
					);
			}
			$volumenes = array();
			$volumenes_importar = explode('|', trim($row->volumenes));
			foreach($volumenes_importar as $volumen_importar){
				if(trim($volumen_importar)!=''){
					$aux = explode(':', $volumen_importar);

					if(sizeof($aux) != 4   || trim($aux[0]) == "" || trim($aux[1]) == ""){
						$validacion = array('linea' => $i+1, 'campo' => 'Volúmenes', 'error' => '
						La sintaxis es inválida, Se debe colocar el Número del volumen, el subtítulo, el ISBN y el año separados por dos puntos ":" =. Ej: "Núm Vol : Subtitulo : ISBN : Año". Los campos Número de volumen y subtítulo son obligatorios. En caso de que no posea ISBN o Año se deben mantener los dos puntos ":" con el espacio vacío. Por ejemplo si no posee ISBN colocar los dos puntos dejando el espacio vacio. Ej:  "1 : Subtitulo de Recurso : : 1995".  Nota: Si es mas de un volumen se deben separar por el símbolo pipe: |');	 
						array_push($validaciones, $validacion);
						break;
					}

					
		            if(trim($aux[3]) && !is_numeric(trim($aux[3]))){
		                    $validacion = array('linea' => $i+1, 'campo' => 'Volúmenes', 'error' => '
		                    El año debe ser numérico ');   
		                    array_push($validaciones, $validacion);
		                    break;
		            }

					$volumenPush = array(
								"id" => "",
 								"volumen" => trim($aux[0]), 
								"subtitulo" => trim($aux[1]), 
								"isbn" => trim($aux[2]), 
								"ano" => trim($aux[3]),
								"idCatalogador" => $user_id
								);
					array_push($volumenes, $volumenPush);					
				}
			}

			$campos_adicionales = array();
			$campos_adicionales_importar = explode('|', trim($row->campos_adicionales));
			foreach($campos_adicionales_importar as $campo_adicional_importar){
				if(trim($campo_adicional_importar)!=''){
					$aux = explode(':', $campo_adicional_importar);

					if(sizeof($aux) != 2  || trim($aux[0]) == "" || trim($aux[1]) == ""){

					$validacion = array('linea' => $i+1, 'campo' => 'Campos Adicionales', 'error' => "
						La sintaxis es inválida. La forma correcta es: El nombre del campo seguido de dos puntos (:) y la descripción del campo ej: Idioma: Ingles. Nota: Si es mas de un campo adicional se deben separar por el símbolo pipe: | ");	 
						array_push($validaciones, $validacion);
						break;
					}

					$validar_campo = array(
							"campo" => $aux[0],
							"tipo_id" => $row->tipo_de_recurso);
					$respuesta = App::make('CamposAdicionalesController')->store($validar_campo);

					$campo_adicional = array(
							"id" => $respuesta["id_campo"],
							"descripcion" => $aux[1]);
					array_push($campos_adicionales, $campo_adicional);
					
				}
			}
			$volumenesSerie = array();
			if(trim($row->tipo_de_recurso == 'S')){
		        
		        $volumenes_importar_s = explode('|', trim($row->volumenes_series));
		        foreach($volumenes_importar_s as $volumen_importar){
		            if(trim($volumen_importar)!=''){
		                $aux = explode(':', $volumen_importar);
		                if(sizeof($aux) < 3   || trim($aux[0]) == "" || trim($aux[1]) == "" || trim($aux[1]) == ""){

		                $validacion = array('linea' => $i+1, 'campo' => 'Volúmenes series', 'error' => '
		                    La sintaxis es inválida, Se debe colocar el año, el número del volumen, el número inicial y el número final (opcional), separados por dos puntos ":" . Ej: "Núm Vol : año : número inicial : número final ". En caso de que no posea número final, no se coloca. ');     
		                    array_push($validaciones, $validacion);
		                    break;
		                }
		                if(!is_numeric(trim($aux[1]))){
		                    $validacion = array('linea' => $i+1, 'campo' => 'Volúmenes series', 'error' => '
		                    El número del volumen debe ser numérico');   
		                    array_push($validaciones, $validacion);
		                    break;
		                }
		                if(!is_numeric(trim($aux[0])) || strlen(trim($aux[0])) != 4){
		                    $validacion = array('linea' => $i+1, 'campo' => 'Volúmenes series', 'error' => '
		                    El año debe ser numérico y debe contener 4 caracteres  ');     
		                    array_push($validaciones, $validacion);
		                    break;
		                }

		                if(!is_numeric(trim($aux[2])) ){
		                    $validacion = array('linea' => $i+1, 'campo' => 'Volúmenes series', 'error' => '
		                    El número inicial debe ser numérico');   
		                    array_push($validaciones, $validacion);
		                    break;
		                }
		                if($aux[3]){
		                    if(!is_numeric(trim($aux[3])) ){
		                        $validacion = array('linea' => $i+1, 'campo' => 'Volúmenes series', 'error' => '
		                    El número final debe ser numérico');     
		                    array_push($validaciones, $validacion);
		                    break;
		                    }

		                    if((int)$aux[3] < (int)$aux[2]){
		                        $validacion = array('linea' => $i+1, 'campo' => 'Volúmenes series', 'error' => 'El número final debe ser mayor que el inicial');     
		                    array_push($validaciones, $validacion);
		                    break;
		                    }
		                }

		                $volumenPush = array(
		                            "id" => "",
		                            "volumen" => trim($aux[1]),
		                            "fecha_iso" => trim($aux[0]).'0000', 
		                            "inicial" => trim($aux[2]), 
		                            "final" => trim($aux[3]),
		                            );
		                array_push($volumenesSerie, $volumenPush);                  
		            }

		        }

			    if($row->tipo_publicacion == null){
					$validacion = array('linea' => $i+1, 'campo' => 'Tipo publiacación', 'error' => "El tipo de publicación no puede estar vacío");
					array_push($validaciones, $validacion);	
				}else{
					if(strtoupper(trim($row->tipo_publicacion)) != 'S' && strtoupper(trim($row->tipo_publicacion)) != 'P' && strtoupper(trim($row->tipo_publicacion)) != 'D' && strtoupper(trim($row->tipo_publicacion)) != 'N') {
						$validacion = array('linea' => $i+1, 'campo' => 'Tipo publicación', 'error' => "El tipo de publicación es inválido, los tipos válidos son: S, P, D o N. Para mas información revise la tabla de formatos");
						array_push($validaciones, $validacion);	
					}			
				}
		
		       	if(trim($row->periodicidad) != 'Diaria' && trim($row->periodicidad) != 'Semanal' && trim($row->periodicidad) != 'Quincenal' && trim($row->periodicidad) != 'Mensual' && trim($row->periodicidad) != 'Bimestral' && trim($row->periodicidad) != 'Trimestral' && trim($row->periodicidad) != 'Cuatrimestral' && trim($row->periodicidad) != 'Semestral' && trim($row->periodicidad) != 'Anual' && trim($row->periodicidad) != 'Bianual' ){
		       		$validacion = array('linea' => $i+1, 'campo' => 'Periodicidad', 'error' => "La periodicidad es inválida. Por favor, revise la tabla de formatos");
						array_push($validaciones, $validacion);		
		       	}
		       	if(strtoupper(trim($row->regularidad)) != 'R' && strtoupper(trim($row->regularidad)) != 'D' && strtoupper(trim($row->regularidad)) != 'N' && strtoupper(trim($row->regularidad)) != 'N' && strtoupper(trim($row->regularidad) ) != 'C' ){
		       		$validacion = array('linea' => $i+1, 'campo' => 'Regularidad', 'error' => "La regularidad es inválida. Por favor, revise la tabla de formatos");					
		       			array_push($validaciones, $validacion);		
		       	}
		    }

		    if($row->periodicidad != null){
		    	$row->periodicidad = trim($row->periodicidad);
		    }

            if($row->regularidad != null){
		    	$row->regularidad = trim(strtoupper($row->regularidad));
		    }

		    if($row->titulo_principal != null)
				$titulo = explode('|', trim($row->titulo_principal))[0];

		
   
   			$recurso = array(
   				"cota" => trim($row->cota),
			    "fecha_pub" => trim($row->ano),
			    "tipoAutor"=> $tipo_autor,
			    "nombreAutor" => $nombre_autor,
			    "nombreTitulo" => $titulo,
			    "edicion" => trim($row->edicion),
			    "fechaEdicion" => $fechaFinal,
			    "url" => trim($row->url),
			    "colacion" => trim($row->colacion),
			    "isbn" => trim($row->isbn),
			    "numeracion" => trim($row->numeracion),
			    "impresion" => trim($row->impresion),
			    "volumen" => trim($row->volumen),
			    "tipoLiter" => trim($row->tipo_de_recurso),
			    "idCatalogador" => $user_id,
			    "resumen" => trim($row->resumen),
			    "notas" => trim($row->nota),    
			    "datosAdicionales" => trim($row->datos_adicionales),
			    "institucion" => trim($row->institucion),
			    "nivel" => $nivel_recurso,
			    "dependencia" => trim($row->dependencia),
			    "disciplina" => trim($row->disciplina),
			    "idDependencia" => "",
			    "gradoAcademico" => trim($row->grado_academico),
			    "tutorTesis" => $tutor_academico_principal,
			    "correoTutor" => "",
			    "trabajo" => $trabajo,
			    "campos_adicionales" => $campos_adicionales,
			    "bibliotecaSelect" => 1,
			    "doi" => "" ,
			    "esElectronico" => "0",
			    "nombreDocElectronico" => "",
			    "regularidad" =>  $row->regularidad,
			    "periodicidad" => $row->periodicidad,
			    "tipo" => $row->tipo_publicacion,
			    "issn" => $row->issn, 
			    "nros" => $row->numeros_por_ano,
			    "vols" => $row->volumenes_por_ano,
			    "diseminacion" => $row->diseminacion,
			    "diseminacion" => $row->diseminacion,
			    "existencia" => $row->existencia);
				

			array_push($recursos, $recurso);
			array_push($autoresRecursos, $autores);
			array_push($titulosRecursos, $titulos);
			array_push($descriptoresRecursos, $descriptores);
			array_push($soportesRecursos, $soportes);
			array_push($conferenciaRecursos, $conferencia);
			array_push($editorialesRecursos, $editoriales);
			array_push($institucionesRecursos, $institucion);
			array_push($volumenesRecursos, $volumenes);
			array_push($volumenesSeries, $volumenesSerie);
	
			$i++;		
		}

		if(sizeof($validaciones) != 0){
			return $respuesta = array("file" => $file, "codigo" => "1", "validaciones" => $validaciones);	
		}
		$validaciones = array();
		$i = 0;
		foreach ($recursos as $recurso) {
			if( $recurso["tipoLiter"] == 'S'){
				$queryPublicacion = DB::table('recursos')
				->join('recursos_titulos','recursos_titulos.recurso_id','=','recursos.id')
				->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
				->where('titulos.titulo','=', strtoupper(App::make('BuscadoresController')->convetir_string($recurso['nombreTitulo'])))
				->where('recursos.tipo_liter', '=', 'S')
				->where('recursos.nivel_reg', '=', 's')
				->select('recursos.*')->first();

				if($queryPublicacion){
					$validacion = array('linea' => $i+1, 'campo' => 'Título principal', 'error' => 'El título ingresado ya está asociado a una publicación seriada');	 
						array_push($validaciones, $validacion);
				}
			}else if(substr($recurso["tipoLiter"],0,1) == 'O'){
				$verificarTituloExistente = DB::table('recursos')
				->join('recursos_titulos','recursos_titulos.recurso_id','=','recursos.id')
				->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
				->where('titulos.titulo','=', App::make('BuscadoresController')->convetir_string($recurso['nombreTitulo']))
				->where('recursos.tipo_liter', '=', $recurso['tipoLiter'])
				->select('recursos.*')->first();

				if($verificarTituloExistente){
					$validacion = array('linea' => $i+1, 'campo' => 'Título principal', 'error' => 'El título ingresado ya está asociado a otro recurso de este tipo');	 
						array_push($validaciones, $validacion);
				}
			}else{
				$queryRecurso = DB::table('recursos')
					->where('recursos.ubicacion','=',strtoupper(App::make('BuscadoresController')->convetir_string($recurso["cota"])))
					->where('recursos.ext_id', '=', 0)
					->select('recursos.*')->first();
				if($queryRecurso){
						$validacion = array('linea' => $i+1, 'campo' => 'Cota', 'error' => 'La cota ingresada ya existe');	 							
						array_push($validaciones, $validacion);
				}			
			}
			$i++;
		}
		if(sizeof($validaciones) != 0){
			return $respuesta = array("file" => $file, "codigo" => "1", "validaciones" => $validaciones);	
		}
   
		$indice = 0;
		foreach ($recursos as $recurso) {
			if($recurso["tipoLiter"] == "M"){
				$response = App::make('LibrosController')->agregar_libro($recurso);
			}else if($recurso["tipoLiter"] == "T"){
				$response = App::make('TesisController')->agregar_tesis($recurso);
			}else if($recurso["tipoLiter"] == "S"){
				$response = App::make('PublicacionesSeriadasController')->agregar_publicacion($recurso);
			}else{
				$response = App::make('OtrosRecursosController')->agregar_otro_recurso($recurso);
			}



			for ($i=0; $i < sizeof($autoresRecursos[$indice]); $i++) { 
				if($autoresRecursos[$indice][$i]){ 
					$autoresRecursos[$indice][$i]["id_recurso"] = $response["id_recurso"];
					App::make('AutoresController')->agregar_autor_recurso($autoresRecursos[$indice][$i]);
				}
			}
			for ($i=0; $i < sizeof($titulosRecursos[$indice]); $i++) { 
				if($titulosRecursos[$indice][$i]){  
					$titulosRecursos[$indice][$i]["id_recurso"] = $response["id_recurso"];
					App::make('TitulosController')->agregar_titulo_recurso($titulosRecursos[$indice][$i]);
				}
			}
			for ($i=0; $i < sizeof($editorialesRecursos[$indice]); $i++) {  
				if($editorialesRecursos[$indice][$i]){ 
					$editorialesRecursos[$indice][$i]["idRecurso"] = $response["id_recurso"];
					App::make('EditorialesController')->agregar_editoriales($editorialesRecursos[$indice][$i]);
				}
			}
			for ($i=0; $i < sizeof($descriptoresRecursos[$indice]); $i++) { 
				if($descriptoresRecursos[$indice][$i]){  
					$descriptoresRecursos[$indice][$i]["id_recurso"] = $response["id_recurso"];
					App::make('DescriptoresController')->agregar_descriptor($descriptoresRecursos[$indice][$i]);
					
				}
			}
			for ($i=0; $i < sizeof($soportesRecursos[$indice]); $i++) {  
				if($soportesRecursos[$indice][$i]){ 
					$soportesRecursos[$indice][$i]["id_recurso"] = $response["id_recurso"];
					App::make('SoportesController')->agregar_soporte($soportesRecursos[$indice][$i]);
				}
			}
			for ($i=0; $i < sizeof($conferenciaRecursos); $i++) {  
				if($conferenciaRecursos[$i]){ 
					$conferenciaRecursos[$i]["idRecurso"] = $response["id_recurso"];
					App::make('ConferenciasController')->store($conferenciaRecursos[$i]);
					
				}
			}
			for ($i=0; $i < sizeof($institucionesRecursos); $i++) {  
				if($institucionesRecursos[$i]){ 
					$institucionesRecursos[$i]["id_recurso"] = $response["id_recurso"];
					App::make('institucionesController')->store($institucionesRecursos[$i]);
				}
			}
			if($volumenesRecursos[$indice])
				App::make('VolumenesController')->update($response["id_recurso"], $volumenesRecursos[$indice]);
		
			
			
			for ($i=0; $i < sizeof($volumenesSeries[$indice]); $i++) { 
				if($volumenesSeries[$indice][$i]){ 
					$volumenesSeries[$indice][$i]["id_recurso_publicacion"] = $response["id_recurso"];
					App::make('VolumenesController')->agregar_volumen_serie($volumenesSeries[$indice][$i]);
				}
			}
			$indice++;
		}
		return $respuesta = array("file" => $file, "codigo" => "0", "validaciones" => $validaciones);
    }

    public function exportarRecursos(){ 
    	$input = Input::all();
		$recursos_ids = $input["ids_recursos"];
		$recursos = array();
		foreach ($recursos_ids as $recurso_id) {
			array_push($recursos,$this->show($recurso_id));
		}
    	
    	try{
		   // try code
    		date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
    		$nombre = "recursos_exportados_" . $date;
	    	Excel::create($nombre, function($excel) use($recursos) {
	    		$excel->sheet('Sheetname', function($sheet) use($recursos) {
	    			// Agrego el Header
	    			/*$data = array(
					    array('Header 1', 'Header 2')
					);*/		
					$data = array( 
						array(
						'Tipo de recurso','Cota','Autor principal','Título principal','Editoriales','Año','Fecha','Edición','Url','Colación','ISBN','Numeración','Impresión','Volumen','Resumen','Nota','Datos adicionales','Autor entrada secundario','Autor secundario','Autor institucional entrada principal','Autor tesista','Autor congreso reunión entrada principal','Autor traductor','Autor institucional entrada secundaria','Autor ilustrador','Autor prologuista','Autor compilador','Autor editor','Autor revisor','Autor director','Autor coordinador','Título original','Título traducido','Título mención de responsabilidad','Título abreviado','Título abreviado traducido','Título anterior','Título anterior traducido','Título paralelo','Subtítulo original','Subtítulo traducido','Descriptor sala','Descriptor centro catalogador','Descriptor idioma','Descriptor materia temática','Soporte','Conferencia','Ciudad conferencia','Nivel','Institución','Grado académico', 'Disciplina',	'Dependencia', 'Tipo trabajo', 'Tutor académico','Tutor industrial',' Asesor','Jurado', 'Campos adicionales', 'Volúmenes','Volúmenes series', 'Tipo publicación', 'Periodicidad', 'Regularidad', 'Diseminación', 'Existencia', 'ISSN', 'Números por ano', 'Volúmenes por ano'));

					foreach ($recursos as $recurso) {
						$tipo_recurso = $recurso->tipo_liter;
						$cota = $recurso->ubicacion;
						$autor_principal = '';
						$titulo_principal = '';
						$editoriales = '';
						$anio = ""; 
						$fecha = '';//$recurso->fecha_iso;
						$edicion =  $recurso->edicion;
						$url = $recurso->url;
						$colacion = $recurso->paginas;
						$isbn = $recurso->isbn;
						$numeracion = $recurso->numeracion;
						$impresion = $recurso->impresion;
						$volumen = $recurso->volumen;
						$resumen = $recurso->resumen;
						$notas = $recurso->notas;
						$datos_adicionales = $recurso->datos_adicionales;
						$autor_entrada_sec = '';
						$autor_sec = '';
						$autor_institucional_ent_p = '';
						$autor_tesista = '';
						$autor_congreso_reu_ent = '';
						$autor_traductor = '';
						$autor_institucional_ent_s = '';
						$autor_ilustrador = '';
						$autor_prologuista = '';
						$autor_compilador = '';
						$autor_editor = '';
						$autor_revisor = '';
						$autor_director = '';
						$autor_coordinador = '';	
						$titulo_original = '';
						$titulo_traducido = '';
						$titulo_mencion_resp = '';
						$titulo_abreviado = '';
						$titulo_abreviado_trad = '';
						$titulo_anterior = '';
						$titulo_anterior_trad = '';
						$titulo_paralelo = '';
						$titulo_subt_original = '';
						$titulo_subt_traducido = '';
						$descriptor_sala = '';
						$descriptor_centro_catalogador = '';
						$descriptor_idioma = '';
						$descriptor_materia = '';
						$soporte = '';
						$conferencia = '';
						$ciudad_conferencia = '';
						$pais_conferencia = '';
						$nivel = '';
						$institucion = '';
						$grado_academico = '';
						$disciplina = '';
						$dependencia = '';
						$trabajo = '';
						$tutor = '';
						$tutor_academico = '';
						$tutor_industrial = '';
						$asesor = '';
						$jurado = '';
						$campos_adicionales = '';
						$volumenes = '';
						$tipo_publicacion = '';
						$periodicidad = '';
						$regularidad = '';
						$diseminacion = '';
						$existencia = '';
						$volumenes_series = '';
						$issn = '';
						$nros = '';
						$vols = '';
						if(substr($tipo_recurso , 0,3) == 'OTR'){
							foreach ($recurso->campos_adicionales as $campo_adicional) {
								if($campos_adicionales != '')
									$campos_adicionales .= " | " . $campo_adicional->campo_salida . ':' . $campo_adicional->descripcion;
								else 	
									$campos_adicionales =  $campo_adicional->campo_salida . ':'. $campo_adicional->descripcion;
							}
						}

						if($tipo_recurso == "M"){
							$anio = $recurso->fecha_pub;
						}
						if(strlen(trim($recurso->fecha_iso)) == 8){
							$fecha_aux = substr($recurso->fecha_iso, 0,4) . '-' . substr($recurso->fecha_iso, 4,2) . '-' . substr($recurso->fecha_iso, 6,2);
								if($this->validar_fecha($fecha_aux)){
									$fecha =  substr($recurso->fecha_iso, 6,2) . '/' .  substr($recurso->fecha_iso, 4,2)  . '/' . substr($recurso->fecha_iso, 0,4);
								}else{
									$fecha = "";
								}
						}else{
							$fecha = "";

						}
						foreach ($recurso->autores as $autor) {
							if($autor->tipo_autor == 'P'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_principal != '')
										$autor_principal .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_principal = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_principal != '')
										$autor_principal .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_principal = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_principal != '')
										$autor_principal .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_principal = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'M-700'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_entrada_sec != '')
										$autor_entrada_sec .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_entrada_sec = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_entrada_sec != '')
										$autor_entrada_sec .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_entrada_sec = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_entrada_sec != '')
										$autor_entrada_sec .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_entrada_sec = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'S'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_sec != '')
										$autor_sec .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_sec = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_sec != '')
										$autor_sec .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_sec = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_sec != '')
										$autor_sec .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_sec = "C: " . $autor->autor_salida;
								}
							}

							if($autor->tipo_autor == 'M-110'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_institucional_ent_p != '')
										$autor_institucional_ent_p .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_institucional_ent_p = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_institucional_ent_p != '')
										$autor_institucional_ent_p .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_institucional_ent_p = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_institucional_ent_p != '')
										$autor_institucional_ent_p .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_institucional_ent_p = "C: " . $autor->autor_salida;
								}
							}

							if($autor->tipo_autor == 'T'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_tesista != '')
										$autor_tesista .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_tesista = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_tesista != '')
										$autor_tesista .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_tesista = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_tesista != '')
										$autor_tesista .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_tesista = "C: " . $autor->autor_salida;
								}
							}

							if($autor->tipo_autor == 'M-111'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_congreso_reu_ent != '')
										$autor_congreso_reu_ent .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_congreso_reu_ent = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_congreso_reu_ent != '')
										$autor_congreso_reu_ent .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_congreso_reu_ent = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_congreso_reu_ent != '')
										$autor_congreso_reu_ent .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_congreso_reu_ent = "C: " . $autor->autor_salida;
								}
							}

							if($autor->tipo_autor == 'R'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_traductor != '')
										$autor_traductor .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_traductor = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_traductor != '')
										$autor_traductor .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_traductor = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_traductor != '')
										$autor_traductor .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_traductor = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'M-710'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_institucional_ent_s != '')
										$autor_institucional_ent_s .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_institucional_ent_s = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_institucional_ent_s != '')
										$autor_institucional_ent_s .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_institucional_ent_s = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_institucional_ent_s != '')
										$autor_institucional_ent_s .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_institucional_ent_s = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'I'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_ilustrador != '')
										$autor_ilustrador .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_ilustrador = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_ilustrador != '')
										$autor_ilustrador .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_ilustrador = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_ilustrador != '')
										$autor_ilustrador .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_ilustrador = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'O'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_prologuista != '')
										$autor_prologuista .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_prologuista = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_prologuista != '')
										$autor_prologuista .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_prologuista = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_prologuista != '')
										$autor_prologuista .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_prologuista = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'C'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_compilador != '')
										$autor_compilador .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_compilador = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_compilador != '')
										$autor_compilador .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_compilador = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_compilador != '')
										$autor_compilador .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_compilador = "C: " . $autor->autor_salida;
								}
							}

							if($autor->tipo_autor == 'E'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_editor != '')
										$autor_editor .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_editor = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_editor != '')
										$autor_editor .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_editor = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_editor != '')
										$autor_editor .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_editor = "C: " . $autor->autor_salida;
								}
							}

							if($autor->tipo_autor == 'V'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_revisor != '')
										$autor_revisor .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_revisor = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_revisor != '')
										$autor_revisor .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_revisor = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_revisor != '')
										$autor_revisor .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_revisor = "C: " . $autor->autor_salida;
								}
							}

							if($autor->tipo_autor == 'D'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_director != '')
										$autor_director .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_director = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_director != '')
										$autor_director .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_director = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_director != '')
										$autor_director .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_director = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'F'){
								if($autor->tipo_autor_principal == 'P'){
									if($autor_coordinador != '')
										$autor_coordinador .= " | " . "P: " . $autor->autor_salida;
									else 	
										$autor_coordinador = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($autor_coordinador != '')
										$autor_coordinador .= " | " . "I: " . $autor->autor_salida;
									else 	
										$autor_coordinador = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($autor_coordinador != '')
										$autor_coordinador .= " | " . "C: " . $autor->autor_salida;
									else 	
										$autor_coordinador = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'TA'){
								if($autor->tipo_autor_principal == 'P'){
									if($tutor_academico != '')
										$tutor_academico .= " | " . "P: " . $autor->autor_salida;
									else 	
										$tutor_academico = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($tutor_academico != '')
										$tutor_academico .= " | " . "I: " . $autor->autor_salida;
									else 	
										$tutor_academico = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($tutor_academico != '')
										$tutor_academico .= " | " . "C: " . $autor->autor_salida;
									else 	
										$tutor_academico = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'TI'){
								if($autor->tipo_autor_principal == 'P'){
									if($tutor_industrial != '')
										$tutor_industrial .= " | " . "P: " . $autor->autor_salida;
									else 	
										$tutor_industrial = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($tutor_industrial != '')
										$tutor_industrial .= " | " . "I: " . $autor->autor_salida;
									else 	
										$tutor_industrial = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($tutor_industrial != '')
										$tutor_industrial .= " | " . "C: " . $autor->autor_salida;
									else 	
										$tutor_industrial = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'A'){
								if($autor->tipo_autor_principal == 'P'){
									if($asesor != '')
										$asesor .= " | " . "P: " . $autor->autor_salida;
									else 	
										$asesor = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($asesor != '')
										$asesor .= " | " . "I: " . $autor->autor_salida;
									else 	
										$asesor = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($asesor != '')
										$asesor .= " | " . "C: " . $autor->autor_salida;
									else 	
										$asesor = "C: " . $autor->autor_salida;
								}
							}
							if($autor->tipo_autor == 'J'){
								if($autor->tipo_autor_principal == 'P'){
									if($jurado != '')
										$jurado .= " | " . "P: " . $autor->autor_salida;
									else 	
										$jurado = "P: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'I'){
									if($jurado != '')
										$jurado .= " | " . "I: " . $autor->autor_salida;
									else 	
										$jurado = "I: " . $autor->autor_salida;

								}else if($autor->tipo_autor_principal == 'C'){
									if($jurado != '')
										$jurado .= " | " . "C: " . $autor->autor_salida;
									else 	
										$jurado = "C: " . $autor->autor_salida;
								}
							}
						}


						foreach ($recurso->titulos as $titulo) {
							if($titulo->tipo_tit == 'OP') 
								if($titulo_principal != ''){
									$titulo_principal .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_principal = $titulo->titulo_salida;
							else if($titulo->tipo_tit == 'OR'){ 
								if($titulo_original != ''){
									$titulo_original .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_original = $titulo->titulo_salida;
							}else if($titulo->tipo_tit == 'TP'){ 
								if($titulo_traducido != ''){
									$titulo_traducido .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_traducido = $titulo->titulo_salida;
							}else if($titulo->tipo_tit == 'MR'){ 
								if($titulo_mencion_resp != ''){
									$titulo_mencion_resp .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_mencion_resp = $titulo->titulo_salida;
							}else if($titulo->tipo_tit == 'OV'){ 
								if($titulo_abreviado != ''){
									$titulo_abreviado .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_abreviado = $titulo->titulo_salida;
							}else if($titulo->tipo_tit == 'TV'){ 
								if($titulo_abreviado_trad != ''){
									$titulo_abreviado_trad .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_abreviado_trad = $titulo->titulo_salida;
							}else if($titulo->tipo_tit == 'OA'){ 
								if($titulo_anterior != ''){
									$titulo_anterior .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_anterior = $titulo->titulo_salida;
							}else if($titulo->tipo_tit == 'TA'){ 
								if($titulo_anterior_trad != ''){
									$titulo_anterior_trad .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_anterior_trad = $titulo->titulo_salida;
							}else if($titulo->tipo_tit == 'OL'){ 
								if($titulo_paralelo != ''){
									$titulo_paralelo .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_paralelo = $titulo->titulo_salida;
							}else if($titulo->tipo_tit == 'OS'){ 
								if($titulo_subt_original != ''){
									$titulo_subt_original .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_subt_original = $titulo->titulo_salida;
							}else if($titulo->tipo_tit == 'TS'){ 
								if($titulo_subt_traducido != ''){
									$titulo_subt_traducido .= ' | ' . $titulo->titulo_salida;
								}else
									$titulo_subt_traducido = $titulo->titulo_salida;
							}
						}


						foreach ($recurso->descriptores as $descriptor) {
							if($descriptor->tipo == 'S'){
								if($descriptor_sala != ''){
									$descriptor_sala .= ' | ' . $descriptor->descriptor_salida;
								}else
									$descriptor_sala= $descriptor->descriptor_salida ;
							}else if($descriptor->tipo == 'M-040'){
								if($descriptor_centro_catalogador != ''){
									$descriptor_centro_catalogador .= ' | ' . $descriptor->descriptor_salida;
								}else
									$descriptor_centro_catalogador = $descriptor->descriptor_salida;
							}else if($descriptor->tipo == 'I'){
								if($descriptor_idioma != ''){
									$descriptor_idioma .= ' | ' . $descriptor->descriptor_salida;
								}else
									$descriptor_idioma= $descriptor->descriptor_salida ;
							}else if($descriptor->tipo == 'M-650'){
								if($descriptor->descriptor_salida[0] == '$' && strlen($descriptor->descriptor_salida)>2  ){
        							$descriptor->descriptor_salida = ":" . $descriptor->descriptor_salida;
	        					}

	        					$pos = strpos($descriptor->descriptor_salida, "\$X");
	        					if(!$pos){
	        						$pos =  strpos($descriptor->descriptor_salida, "\$x");
	        					}
	        					$pos2 = strpos($descriptor->descriptor_salida, "\$z");
	        					if(!$pos2){
	        						$pos2 = strpos($descriptor->descriptor_salida, "\$Z"); 
	        					}
								$aux = false;
								$aux2 = false;
								if($pos){
									$descriptor->descriptor_salida = str_replace("\$x",": ", $descriptor->descriptor_salida);
					        		$descriptor->descriptor_salida = str_replace("\$X",": ", $descriptor->descriptor_salida);		
								}else{
									$aux = true;
								}
								
								if($pos2){
									$descriptor->descriptor_salida = str_replace("\$z",": ", $descriptor->descriptor_salida);
					        		$descriptor->descriptor_salida = str_replace("\$Z",": ", $descriptor->descriptor_salida);		
								}else{
	        						$aux2 = true;
								}

								if($aux && $aux2){
									$descriptor->descriptor_salida = $descriptor->descriptor_salida . ': : ';

								}else if ($aux && !$aux2){
									$descriptor->descriptor_salida = substr($descriptor->descriptor_salida,0,$pos2) . ': ' . substr($descriptor->descriptor_salida,$pos2,strlen($descriptor->descriptor_salida)-1);    
								}else if(!$aux && $aux2){
									if(substr_count($descriptor->descriptor_salida, ':')<2)
									$descriptor->descriptor_salida = $descriptor->descriptor_salida . ': ';
								}

	        					if($descriptor_materia != ''){
	        						$descriptor_materia = $descriptor_materia . ' | ' . $descriptor->descriptor_salida;	
	        					}else{

	        						$descriptor_materia = $descriptor->descriptor_salida;	
	        					}
	        				}
						}

						$aux = 0;
						foreach ($recurso->editoriales as $editorial) {
							if($aux == 0){
								$editoriales = $editorial->editorial_salida; 	
								$aux = 1;
							}else{
								$editoriales = $editoriales . " | " . $editorial->editorial_salida;
							}
						}

						foreach ($recurso->soporte as $soporteR) {
							if($soporte != ''){
								$soporte = $soporte .' | '.$soporteR->soporte_salida;
							}else{
								$soporte = $soporteR->soporte_salida;
							}
						}

						if($recurso->conferencias){
							$conferencia = $recurso->conferencias[0]->conferencia_salida;
							$ciudad_conferencia = $recurso->conferencias[0]->ciudad;	
						}
					
						if($recurso->tipo_liter == 'T'){
							if($recurso->grado_academico)
								$grado_academico= $recurso->grado_academico;
							if($recurso->institucion)
								$institucion = $recurso->institucion->institucion_salida;
							if($recurso->nivel)
								$nivel = $recurso->nivel->nivel_salida;
							if($recurso->dependencias)
								$dependencia = $recurso->dependencias[0]->dependencia_salida;
							if($recurso->disciplinas)
								$disciplina = $recurso->disciplinas[0]->disciplina_salida;
							if($recurso->trabajo)
								$trabajo = $recurso->trabajo->trabajo_salida;
						}
						
						foreach ($recurso->volumenes as $vol)  {
							if($volumenes != '')
								$volumenes = $volumenes . " | " . $vol->volumen . " : " . $vol->subtitulo . " : " . $vol->isbn . " : " . $vol->ano;
							else 	
								$volumenes = $vol->volumen . " : " . $vol->subtitulo . " : " . $vol->isbn . " : " . $vol->ano;
						}

						if($tipo_recurso == 'S'){	
							$aux = array("id_recurso_publicacion" => $recurso->id);
							$volumenes_s = App::make('VolumenesController')->obtener_volumenes_numeros($aux);
							//echo json_encode($volumenes_s);
						    $auxFechaIso = array();
						    foreach ($volumenes_s as $vol) {
						        $fecha_iso = $vol->fecha_iso;
						        $volumenI = $vol->volumen;
						        try {
						            if ($auxFechaIso[$fecha_iso] ){
						                try {
						                    if ($auxFechaIso[$fecha_iso][$volumenI]){
						                        array_push($auxFechaIso[$fecha_iso][$volumenI], $vol);
						                    }
						                    
						                } catch (Exception $e) {
						                    $auxFechaIso[$fecha_iso][$volumenI] = array($vol);   
						                }
						            }
						        } catch (Exception $e) {
						            try {
						                if (!$auxFechaIso[$fecha_iso]){
						                    $auxFechaIso[$fecha_iso] = array();
						                }
						                
						            } catch (Exception $e) {}

						            $auxFechaIso[$fecha_iso][$volumenI] = array($vol);
						        }       
						    }					 
						    $volumenes_series = "";
						    foreach ($auxFechaIso as $fechas_aux) {
						        foreach ($fechas_aux as $fecha_i) {
						            if($volumenes_series == ""){
						                $volumenes_series = substr($fecha_i[0]->fecha_iso, 0, 4) . ' : ' . $fecha_i[0]->volumen . ' : '. $fecha_i[0]->numero . ' : '. $fecha_i[sizeof($fecha_i)-1]->numero;   
						            }else{
						                $volumenes_series .= ' | ' . substr($fecha_i[0]->fecha_iso, 0, 4) .' : '.$fecha_i[0]->volumen .' : '. $fecha_i[0]->numero . ' : '. $fecha_i[sizeof($fecha_i)-1]->numero;
						            }
						        }       
						    }
			
							$tipo_publicacion = $recurso->publicacion_seriada[0]->tipo;
							$periodicidad = $recurso->publicacion_seriada[0]->periodo;
							$regularidad = $recurso->publicacion_seriada[0]->regularidad;
							$diseminacion = $recurso->publicacion_seriada[0]->disemina;
							$existencia = $recurso->publicacion_seriada[0]->existencia;
							$url = $recurso->publicacion_seriada[0]->url;
							$impresion = $recurso->publicacion_seriada[0]->impresion;
							$issn = $recurso->publicacion_seriada[0]->issn;
							$nros = $recurso->publicacion_seriada[0]->nros_aso;
							$vols = $recurso->publicacion_seriada[0]->vols_aso;
						}

						

						
						
						 $data[]= array($tipo_recurso, $cota, $autor_principal, $titulo_principal, $editoriales, $anio, $fecha, $edicion,$url,$colacion,$isbn,$numeracion,$impresion,$volumen,$resumen,$notas, $datos_adicionales, $autor_entrada_sec, $autor_sec, $autor_institucional_ent_p, $autor_tesista, $autor_congreso_reu_ent, $autor_traductor, $autor_institucional_ent_s, $autor_ilustrador, $autor_prologuista, $autor_compilador, $autor_editor, $autor_revisor, $autor_director, $autor_coordinador, $titulo_original, $titulo_traducido, $titulo_mencion_resp, $titulo_abreviado, $titulo_abreviado_trad, $titulo_anterior, $titulo_anterior_trad, $titulo_paralelo, $titulo_subt_original, $titulo_subt_traducido, $descriptor_sala,$descriptor_centro_catalogador,$descriptor_idioma, $descriptor_materia, $soporte, $conferencia, $ciudad_conferencia, $nivel, $institucion, $grado_academico, $disciplina, $dependencia, $trabajo, $tutor_academico, $tutor_industrial, $asesor, $jurado, $campos_adicionales, $volumenes, $volumenes_series, $tipo_publicacion, $periodicidad, $regularidad, $diseminacion, $existencia, $issn, $nros, $vols);
					}

	    			$sheet->fromArray($data,null, 'A1', false, false);
	    			$sheet->cell('A1:BQ1', function($cell) {

					    $cell->setFontWeight('bold');

					});

			    });

			})->store('xlsx', storage_path('\public\catalogacion'));

			return $respuesta = array("codigo" => "0", "nombre" => $nombre);
		} 
		catch(\Exception $e){
		   // catch code
			return $respuesta = array("codigo" => "1", "nombre" =>"", "error" =>  $e);

		}
    	
		
    }


public function cargar_recurso($idejemplar, $idusuario){
     	$data = Input::all();
    	$file = $data['name'];
    	$file = $data['file'];
    	$user_id = $idusuario;
    	$file= $_FILES["file"]["name"];
    	if(!is_dir("catalogacion/")){
    		mkdir("catalogacion/",0777);
    	}

    	if($file && move_uploaded_file($_FILES["file"]["tmp_name"], storage_path('/public/catalogacion/').$idejemplar.'_'.$file)){
    		
         	$ejemplar_update = DB::table('ejemplares')
	            ->where('ejemplares.id','=', $idejemplar)
	              ->update(array('doc_electronico' => 1,
				'nombre_doc_electronico' => $idejemplar.'_'.$file
				));

	    		return $respuesta = array("codigo" => "0", "nombre" =>$idejemplar.'_'.$file);
    		//echo $file;
    	}
}


public function eliminar_carga_recurso($idejemplar, $nombrearchivo){
    if(unlink(storage_path('/public/catalogacion/'.$nombrearchivo ) )) {
         $ejemplar_update = DB::table('ejemplares')
            ->where('ejemplares.id','=', $idejemplar)
            ->update(array('nombre_doc_electronico' => ""));

    	return $respuesta = array("codigo" => "0");
    }
}
/*http://codehero.co/laravel-4-desde-cero-eloquent-orm/*/


}


  