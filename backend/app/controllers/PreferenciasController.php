<?php

class PreferenciasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /preferencias
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /preferencias/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /preferencias
	 *
	 * @return Response
	 */
    public function store()
    {
    	$data = Input::all();


    	$preferencia = Preferencia::where('id','=',$data['usuario_id'])
    						      ->where('recurso_id','=',$data['recurso_id'])->get()->first();

    	if(!empty($preferencia)){
    		return json_encode("Ya existe preferencia");
    	}

    	$usuario = Usuario::where('id','=',$data['usuario_id'])->get()->first();

    	$recurso = Recurso::where('id','=',$data['recurso_id'])->get()->first();

    	if(empty($usuario)){
    		return json_encode("Usuario inexistente");
    	}
    	if(empty($recurso)){
    		return json_encode("Recurso inexistente");
    	}

        // store
        $pref = new Preferencia;
        $pref->id       = $data['usuario_id'];
        $pref->recurso_id      = $data['recurso_id'];
        date_default_timezone_set('America/Caracas');
        $pref->fecha_sol = date('Ymd');
        $pref->hora_sol = date("h:i:sa");
        $pref->save();

        return json_encode("Preferencia guardada");

    }

	/**
	 * Display the specified resource.
	 * GET /preferencias/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /preferencias/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /preferencias/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /preferencias/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($recurso_id, $id)
	{

		$pref = Preferencia::where('id', '=',$id)
						   ->where('recurso_id', '=', $recurso_id);
        if($pref->delete())
            return json_encode('Preferencia eliminada');
        else
        	return json_encode('preferencia no existe');
	}


	/**
	 * Función que elimina varias preferencias seleccionadas
	 *
	 * Autor: Cesar Herrera
	 */
	public function remove_prefs(){
		
		$data = Input::all();

		$prefs = $data['pref_selected'];
		$usuario_id = $data['pref_selected']['usuario_id'];
		$success_delete = true;
		$count = 0;
		foreach ($prefs as $key => $value) {
			if($key != 'usuario_id' && $key != 'admin_id'){
				if($value == null) continue;
				$pref = Preferencia::where('id', '=', $usuario_id)
							   ->where('recurso_id', '=', $key);
				
		    	if(!$pref->delete()){
		    		$success_delete = false;
		    	}
		    }
		    $count = $count + 1 ;
		}

		if(!$success_delete)
			return json_encode(array('error'=>'Error borrando preferencias'));
		else
			return json_encode('Preferencias borradas exitosamente');

	}
	


	public function consulta_pref()
	{
		$data = Input::all();

		$usuario_id = $data['usuario_id'];

		$answer = Preferencia::where('id', '=', $usuario_id)->get();

		$answer2 = Preferencia::where('id', '=', $usuario_id)->paginate(5)->toArray();

		/*$answer3 = Preferencia::where('id', '=', $usuario_id)->count();
		return json_encode($answer2);
		return $answer2['data'];*/
		$final_answer2 = array();

		$final_answer2['total'] = $answer2['total'];
		$final_answer2['per_page'] = $answer2['per_page'];
		$final_answer2['current_page'] = $answer2['current_page'];
		$final_answer2['last_page'] = $answer2['last_page'];
		$final_answer2['from'] = $answer2['from'];
		$final_answer2['to'] = $answer2['to'];

		$count2 = 0;
		foreach ($answer2['data'] as $key => $value) {
			$recurso1 = Recurso::where('id', '=', $value['recurso_id'])->get()->first();
			$final_answer2['data'][$count2]['recurso_id'] =  $value['recurso_id'];
			$final_answer2['data'][$count2]['cota'] = $recurso1['ubicacion'];
			$final_answer2['data'][$count2]['titulo'] = RecursosController::recurso_titulos_principal($recurso1['id']);
			$final_answer2['data'][$count2]['cantidad'] = RecursosController::cantidad($recurso1['id']);
			$final_answer2['data'][$count2]['cantidad_disponibles'] = RecursosController::cantidad_disponible($recurso1['id']);
			$count2 = $count2 + 1;
		}

		return $final_answer2;











		if(empty($answer)){
			return json_encode("no hay preferencias");
		}

		//return json_encode(RecursosController::recurso_titulos_principal($answer[0]['recurso_id']));

		
		$final_answer = array();
		$count = 0;
		foreach ($answer as $key => $value) {
			
			$recurso = Recurso::where('id', '=', $value['recurso_id'])->get()->first();
			$final_answer[$count]['recurso_id'] =  $value['recurso_id'];
			$final_answer[$count]['cota'] = $recurso['ubicacion'];
			$final_answer[$count]['titulo'] = RecursosController::recurso_titulos_principal($recurso['id']);
			$final_answer[$count]['cantidad'] = RecursosController::cantidad($recurso['id']);
			$final_answer[$count]['cantidad_disponibles'] = RecursosController::cantidad_disponible($recurso['id']);

			$count = $count + 1;
		}
		return $final_answer;
	}

}