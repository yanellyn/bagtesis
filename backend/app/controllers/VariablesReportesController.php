<?php

class VariablesReportesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /variablesreportes
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /variablesreportes/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /variablesreportes
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();

		if(!empty($data['fecha_origen'])){
			$tempArr=explode('-', $data['fecha_origen']);
			$fecha_origen = date("d-m-y", mktime(0, 0, 0, $tempArr[1], $tempArr[0], $tempArr[2]));
		}
		if(!empty($data['fecha_destino'])){
			$tempArr=explode('-', $data['fecha_destino']);
			$fecha_destino = date("d-m-y", mktime(0, 0, 0, $tempArr[1], $tempArr[0], $tempArr[2]));
		}
		$orden = true;
		if($fecha_origen > $fecha_destino){
			$aux = $fecha_origen;
			$fecha_origen = $fecha_destino;
			$fecha_destino = $aux;
			$orden = false;
		}

		
		$num_usuarios_atendidos_total = 0;
		$num_recursos_prestados_total = 0;
		$num_prestamos_total = 0;
		$num_renovaciones_total = 0;
		$num_devoluciones_total = 0;
		$num_usuarios_suspendidos_total = 0;

		if($fecha_origen < $fecha_destino){
			$ciclo_fechas = true;
			$count_fechas = 0;
			while($ciclo_fechas){
				if(!$orden){
					$origen = date('d-m-y',strtotime($data['fecha_destino']. ' + '.$count_fechas.' day'));
				}else{
					$origen = date('d-m-y',strtotime($data['fecha_origen']. ' + '.$count_fechas.' day'));
				}

				/*Nro Usuarios atendidos*/
				$usuarios_atendidos_fecha_n = VariablesReporte::where('tipo', '=', 'usuarios_atendidos')
										->where('nombre', '=', $origen)->get()->first();
				$num_usuarios_atendidos_total = $num_usuarios_atendidos_total + $usuarios_atendidos_fecha_n['valor'];

				/*Nro Recursos préstados*/
				$recursos_prestados_fecha_n = VariablesReporte::where('tipo', '=', 'recursos_prestados')
										->where('nombre', '=', $origen)->get()->first();
				$num_recursos_prestados_total = $num_recursos_prestados_total + $recursos_prestados_fecha_n['valor'];
				
				/*Nro Préstamos*/
				$prestamos_fecha_n = VariablesReporte::where('tipo', '=', 'prestamos')
										->where('nombre', '=', $origen)->get()->first();
				$num_prestamos_total = $num_prestamos_total + $prestamos_fecha_n['valor'];

				/*Nro renovaciones*/
				$renovaciones_fecha_n = VariablesReporte::where('tipo', '=', 'renovaciones')
										->where('nombre', '=', $origen)->get()->first();
				$num_renovaciones_total = $num_renovaciones_total + $renovaciones_fecha_n['valor'];
				
				/*Nro usuarios suspendidos*/
				$usuarios_suspendidos_fecha_n = VariablesReporte::where('tipo', '=', 'usuarios_suspendidos')
										->where('nombre', '=', $origen)->get()->first();
				$num_usuarios_suspendidos_total = $num_usuarios_suspendidos_total + $usuarios_suspendidos_fecha_n['valor'];
				
				/*Nro devoluciones*/
				$devoluciones_fecha_n = VariablesReporte::where('tipo', '=', 'devoluciones')
										->where('nombre', '=', $origen)->get()->first();
				$num_devoluciones_total = $num_devoluciones_total + $usuarios_suspendidos_fecha_n['valor'];
				



				if($origen == $fecha_destino) $ciclo_fechas = false;
				$count_fechas++;
			}

			$answer = array('usuarios_atendidos' => $num_usuarios_atendidos_total,
						'recursos_prestados' => $num_recursos_prestados_total,
						'prestamos' => $num_prestamos_total,
						'renovaciones' => $num_renovaciones_total,
						'devoluciones' => $num_devoluciones_total,
						'usuarios_suspendidos' => $num_usuarios_suspendidos_total	);
			return $answer;
		}


		if($fecha_origen == $fecha_destino){

			/*Nro usuarios atendidos*/
			$obj_usuarios_atendidos = VariablesReporte::where('tipo', '=', 'usuarios_atentidos')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_usuarios_atendidos))
				$num_usuarios_atendidos_total = $obj_usuarios_atendidos['valor'];		

			/*Nro usuarios atendidos*/
			$obj_recursos_prestados = VariablesReporte::where('tipo', '=', 'recursos_prestados')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_recursos_prestados))
				$num_recursos_prestados_total = $obj_recursos_prestados['valor'];

			/*Nro préstamos*/
			$obj_prestamos = VariablesReporte::where('tipo', '=', 'prestamos')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_recursos_prestados))
				$num_prestamos_total = $obj_prestamos['valor'];	

			/*Nro renovaciones*/
			$obj_renovaciones = VariablesReporte::where('tipo', '=', 'renovaciones')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_renovaciones))
				$num_renovaciones_total = $obj_renovaciones['valor'];	

			/*Nro usuarios_suspendidos*/
			$obj_usuarios_suspendidos = VariablesReporte::where('tipo', '=', 'usuarios_suspendidos')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_usuarios_suspendidos))
				$num_usuarios_suspendidos_total = $obj_usuarios_suspendidos['valor'];	

			/*Nro devoluciones*/
			$obj_devoluciones = VariablesReporte::where('tipo', '=', 'devoluciones')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_devoluciones))
				$num_devoluciones_total = $obj_devoluciones['valor'];	

		}

		$answer = array('usuarios_atendidos' => $num_usuarios_atendidos_total,
						'recursos_prestados' => $num_recursos_prestados_total,
						'prestamos' => $num_prestamos_total,
						'renovaciones' => $num_renovaciones_total,
						'devoluciones' => $num_devoluciones_total,
						'usuarios_suspendidos' => $num_usuarios_suspendidos_total	);

		return json_encode($answer);
	}



	/**
	 * reporte_prestamos_pdf_chart
	 *
	 */
	public function reporte_prestamos_pdf_chart($img1){
		
		return 'recibida';
	
	}

	/**
	 * reporte_prestamos_pdf_chart_generar
	 *
	 */
	public function reporte_prestamos_pdf_chart_generar(){
		
		$data = Input::all();

		$dataimg = $data['image1'];

		list($type, $dataimg) = explode(';', $dataimg);
		list(, $dataimg)      = explode(',', $dataimg);
		$dataimg = base64_decode($dataimg);

		file_put_contents('img/chart1.png', $dataimg);


		$dataimg = $data['image2'];

		list($type, $dataimg) = explode(';', $dataimg);
		list(, $dataimg)      = explode(',', $dataimg);
		$dataimg = base64_decode($dataimg);

		file_put_contents('img/chart2.png', $dataimg);



		$dataimg = $data['image3'];

		list($type, $dataimg) = explode(';', $dataimg);
		list(, $dataimg)      = explode(',', $dataimg);
		$dataimg = base64_decode($dataimg);

		file_put_contents('img/chart3.png', $dataimg);



		$dataimg = $data['image4'];

		list($type, $dataimg) = explode(';', $dataimg);
		list(, $dataimg)      = explode(',', $dataimg);
		$dataimg = base64_decode($dataimg);

		file_put_contents('img/chart4.png', $dataimg);

		return 'listo';

		/*if(empty($data)) return 'asdfffff';
		file_put_contents('img/chart1.png', base64_decode($data['image']));
		return $data;*/
	
	}
	


	/**
	 * Store a newly created resource in storage.
	 * POST /variablesreportes
	 *
	 * @return Response
	 */
	public function reporte_prestamos_pdf_2($fecha_origen_, $fecha_destino_)
	{
		$data['fecha_origen'] = $fecha_origen_;
		$data['fecha_destino'] = $fecha_destino_;

		if(!empty($data['fecha_origen'])){
			$tempArr=explode('-', $data['fecha_origen']);
			$fecha_origen = date("d-m-y", mktime(0, 0, 0, $tempArr[1], $tempArr[0], $tempArr[2]));
		}
		if(!empty($data['fecha_destino'])){
			$tempArr=explode('-', $data['fecha_destino']);
			$fecha_destino = date("d-m-y", mktime(0, 0, 0, $tempArr[1], $tempArr[0], $tempArr[2]));
		}
		$orden = true;
		if($fecha_origen > $fecha_destino){
			$aux = $fecha_origen;
			$fecha_origen = $fecha_destino;
			$fecha_destino = $aux;
			$orden = false;
		}

		
		$num_usuarios_atendidos_total = 0;
		$num_recursos_prestados_total = 0;
		$num_prestamos_total = 0;
		$num_renovaciones_total = 0;
		$num_devoluciones_total = 0;
		$num_usuarios_suspendidos_total = 0;

		if($fecha_origen < $fecha_destino){
			$ciclo_fechas = true;
			$count_fechas = 0;
			while($ciclo_fechas){
				if(!$orden){
					$origen = date('d-m-y',strtotime($data['fecha_destino']. ' + '.$count_fechas.' day'));
				}else{
					$origen = date('d-m-y',strtotime($data['fecha_origen']. ' + '.$count_fechas.' day'));
				}

				/*Nro Usuarios atendidos*/
				$usuarios_atendidos_fecha_n = VariablesReporte::where('tipo', '=', 'usuarios_atendidos')
										->where('nombre', '=', $origen)->get()->first();
				$num_usuarios_atendidos_total = $num_usuarios_atendidos_total + $usuarios_atendidos_fecha_n['valor'];

				/*Nro Recursos préstados*/
				$recursos_prestados_fecha_n = VariablesReporte::where('tipo', '=', 'recursos_prestados')
										->where('nombre', '=', $origen)->get()->first();
				$num_recursos_prestados_total = $num_recursos_prestados_total + $recursos_prestados_fecha_n['valor'];
				
				/*Nro Préstamos*/
				$prestamos_fecha_n = VariablesReporte::where('tipo', '=', 'prestamos')
										->where('nombre', '=', $origen)->get()->first();
				$num_prestamos_total = $num_prestamos_total + $prestamos_fecha_n['valor'];

				/*Nro renovaciones*/
				$renovaciones_fecha_n = VariablesReporte::where('tipo', '=', 'renovaciones')
										->where('nombre', '=', $origen)->get()->first();
				$num_renovaciones_total = $num_renovaciones_total + $renovaciones_fecha_n['valor'];
				
				/*Nro usuarios suspendidos*/
				$usuarios_suspendidos_fecha_n = VariablesReporte::where('tipo', '=', 'usuarios_suspendidos')
										->where('nombre', '=', $origen)->get()->first();
				$num_usuarios_suspendidos_total = $num_usuarios_suspendidos_total + $usuarios_suspendidos_fecha_n['valor'];
				
				/*Nro devoluciones*/
				$devoluciones_fecha_n = VariablesReporte::where('tipo', '=', 'devoluciones')
										->where('nombre', '=', $origen)->get()->first();
				$num_devoluciones_total = $num_devoluciones_total + $usuarios_suspendidos_fecha_n['valor'];
				



				if($origen == $fecha_destino) $ciclo_fechas = false;
				$count_fechas++;
			}

			$answer = array('fecha_origen' => $origen,
							'fecha_destino' => $fecha_destino,
						'usuarios_atendidos' => $num_usuarios_atendidos_total,
						'recursos_prestados' => $num_recursos_prestados_total,
						'prestamos' => $num_prestamos_total,
						'renovaciones' => $num_renovaciones_total,
						'devoluciones' => $num_devoluciones_total,
						'usuarios_suspendidos' => $num_usuarios_suspendidos_total	);

			$pdf = new \Thujohn\Pdf\Pdf();
	    	$content = $pdf->load(View::make('reportes.reporte_prestamos_pdf')->with('data',$answer))->download();

			//return $answer;
		}else


		if($fecha_origen == $fecha_destino){

			/*Nro usuarios atendidos*/
			$obj_usuarios_atendidos = VariablesReporte::where('tipo', '=', 'usuarios_atentidos')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_usuarios_atendidos))
				$num_usuarios_atendidos_total = $obj_usuarios_atendidos['valor'];		

			/*Nro usuarios atendidos*/
			$obj_recursos_prestados = VariablesReporte::where('tipo', '=', 'recursos_prestados')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_recursos_prestados))
				$num_recursos_prestados_total = $obj_recursos_prestados['valor'];

			/*Nro préstamos*/
			$obj_prestamos = VariablesReporte::where('tipo', '=', 'prestamos')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_recursos_prestados))
				$num_prestamos_total = $obj_prestamos['valor'];	

			/*Nro renovaciones*/
			$obj_renovaciones = VariablesReporte::where('tipo', '=', 'renovaciones')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_renovaciones))
				$num_renovaciones_total = $obj_renovaciones['valor'];	

			/*Nro usuarios_suspendidos*/
			$obj_usuarios_suspendidos = VariablesReporte::where('tipo', '=', 'usuarios_suspendidos')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_usuarios_suspendidos))
				$num_usuarios_suspendidos_total = $obj_usuarios_suspendidos['valor'];	

			/*Nro devoluciones*/
			$obj_devoluciones = VariablesReporte::where('tipo', '=', 'devoluciones')
								   ->where('nombre', '=', $fecha_origen)->get()->first();
			if(!empty($obj_devoluciones))
				$num_devoluciones_total = $obj_devoluciones['valor'];	

		}

		$answer = array('fecha_origen' => $fecha_origen,
						'fecha_destino' => $fecha_destino,
						'usuarios_atendidos' => $num_usuarios_atendidos_total,
						'recursos_prestados' => $num_recursos_prestados_total,
						'prestamos' => $num_prestamos_total,
						'renovaciones' => $num_renovaciones_total,
						'devoluciones' => $num_devoluciones_total,
						'usuarios_suspendidos' => $num_usuarios_suspendidos_total	);

		$pdf = new \Thujohn\Pdf\Pdf();
	    $content = $pdf->load(View::make('reportes.reporte_prestamos_pdf')->with('data',$answer))->download();

		//return json_encode($answer);
	}



	/**
	 * reporte_prestamos_pdf
	 *
	 */
	public function reporte_prestamos_pdf(){
			
		$data = Input::all();

		$pdf = new \Thujohn\Pdf\Pdf();
	    $content = $pdf->load(View::make('reportes.reporte_prestamos_pdf')->with('data',$data))->download();
	    //File::put(public_path('test'.'1'.'pdf'), $content);
	
	}
	

	/**
	 * Display the specified resource.
	 * GET /variablesreportes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /variablesreportes/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /variablesreportes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /variablesreportes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}