<?php

class RecursosEditorialesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /recursoseditoriales
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return RecursosEditoriales::take(10)->skip(0)->get();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /recursoseditoriales/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /recursoseditoriales
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /recursoseditoriales/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		return Recurso::find($id)->editorial;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /recursoseditoriales/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /recursoseditoriales/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /recursoseditoriales/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}