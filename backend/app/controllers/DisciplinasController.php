<?php

class DisciplinasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function autocompletado_disciplinas(){
		$data = Input::all();
		
		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = strtoupper(App::make('BuscadoresController')->convetir_string($data['data']));
		$this->split_data =  explode(' ',$aux_data);
		$datos_disciplinas = [];		  
		$query_institucion = DB::table('disciplinas')
						->where('disciplinas.disciplina', '=', $aux_data)
						->select('disciplinas.*')
						->paginate(15);

		foreach ($query_institucion as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "disciplina_salida"=> $row->disciplina_salida, "disciplina"=> $row->disciplina);
			array_push($datos_disciplinas, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_disciplinas), SORT_REGULAR);
		$datos_disciplinas = [];

		$query_institucion = DB::table('disciplinas')
						->where('disciplinas.disciplina', 'like', $aux_data.'%' )
						->select('disciplinas.*')
						->paginate(15);

		foreach ($query_institucion as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "disciplina_salida"=> $row->disciplina_salida, "disciplina"=> $row->disciplina);
			array_push($datos_disciplinas, $aux);
		}


		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_disciplinas), SORT_REGULAR);
		$datos_disciplinas = [];

		$query_institucion = DB::table('disciplinas')
						->where('disciplinas.disciplina', 'like', '%'.$aux_data.'%' )
						->select('disciplinas.*')
						->paginate(15);

		foreach ($query_institucion as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "disciplina_salida"=> $row->disciplina_salida, "disciplina"=> $row->disciplina);
			array_push($datos_disciplinas, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_disciplinas), SORT_REGULAR);
		$datos_disciplinas = [];

		$queryq = DB::table('disciplinas')
					->where(function($query){
						foreach ($this->split_data as $aux) {
							$query->where('disciplinas.disciplina', 'like', '%'.$aux.'%');
						}

					})
					->select('disciplinas.*')
						->paginate(15);

		foreach ($query_institucion as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "disciplina_salida"=> $row->disciplina_salida, "disciplina"=> $row->disciplina);
			array_push($datos_disciplinas, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_disciplinas), SORT_REGULAR);
		return $datos_busqueda;
	}


}
