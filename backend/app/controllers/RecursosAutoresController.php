<?php

class RecursosAutoresController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /recursosautores
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		 return RecursoAutor::take('15')->skip(0)->get();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /recursosautores/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /recursosautores
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /recursosautores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		//return Recurso::find($id)->autores;
		return Recurso::extend_autores($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /recursosautores/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /recursosautores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /recursosautores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}