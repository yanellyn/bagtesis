<?php

class VolumenesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$query_volumen = DB::table('volumenes')
						->join('recursos','recursos.id','=', 'volumenes.recurso_id')
						->where('volumenes.recurso_id', '=',$id)
						->orderBy('volumenes.id', 'ASC')
						->select('volumenes.id','volumenes.recurso_id','volumenes.volumen','volumenes.subtitulo','volumenes.isbn','volumenes.ano')
						->get();

		if($query_volumen){
			$respuesta = array("codigo" => "0", "volumenes" => $query_volumen);
		}else{
			$respuesta = array("codigo" => "1");
		}

		return $respuesta;
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, $dataVolImportar = null)
	{
		if($dataVolImportar){
		
			$volumenes = $dataVolImportar;
		}else{

			$data = Input::all();
			$volumenes = $data['volumenes'];
		}
		
		$updateVolumenes = [];
		$deleteVolumenes = [];

		foreach ($volumenes as $volumen) {

			//if(isset($volumen['id'])){
				if($volumen['id'] != ""){
					$volumenUpdate = Volumen::find($volumen['id']);
					$volumenUpdate->volumen = $volumen['volumen'];
					$volumenUpdate->subtitulo = $volumen['subtitulo'];
					$volumenUpdate->isbn = $volumen['isbn'];
					$volumenUpdate->ano = $volumen['ano'];
					$volumenUpdate->save();
					$updateVolumenes[] = $volumenUpdate->id;
				}else{
					$volumenCreate= new Volumen;
					$volumenCreate->recurso_id = $id;
					$volumenCreate->ext_recurso_id = '0';
					$volumenCreate->volumen = $volumen['volumen'];
					$volumenCreate->subtitulo = $volumen['subtitulo'];
					$volumenCreate->isbn = $volumen['isbn'];
					$volumenCreate->ano = $volumen['ano'];
					$volumenCreate->save();
					$updateVolumenes[] = $volumenCreate->id;
				}
		//	}

		}

		$query_volumen = DB::table('volumenes')
						->join('recursos','recursos.id','=', 'volumenes.recurso_id')
						->where('volumenes.recurso_id', '=',$id)
						->select('volumenes.id')
						->get();

		foreach ($query_volumen as $aux){
			if(!in_array($aux->id,$updateVolumenes)){
				$deleteVolumen = DB::table('volumenes')
					->where('id','=',$aux->id)
					->delete();
				$deleteVolumenes[] = $aux->id;
			}
		}

		$respuesta = array("codigo" => "0");
		return $respuesta;

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function agregar_volumen_serie($dataVolImportar = null){
		if($dataVolImportar){
			$data = $dataVolImportar;
		}else{
			$data = Input::all();	
		}
		$fecha_actual = date('Ymd');

		$biblioteca = DB::table('recursos_bibliotecas')
							   ->join('bibliotecas','bibliotecas.id','=','recursos_bibliotecas.biblioteca_id')
							   ->where('recursos_bibliotecas.recurso_id','=',$data["id_recurso_publicacion"])
							   ->select('bibliotecas.*')
							   ->first();
		
		$recursoPublicacion = DB::table('recursos')
	                      ->where('recursos.id','=',$data["id_recurso_publicacion"])
	                      ->select('recursos.*')
	                      ->first();

	    $validarVolumen = DB::table('recursos')
	    				  ->where('recursos.tipo_liter','=', 'S')
	                      ->where('recursos.jerarquia','=', $recursoPublicacion->jerarquia)
	                      ->where('recursos.volumen', '=', $data["volumen"])
	                      ->where('recursos.fecha_iso', '=', $data["fecha_iso"])
	                      ->select('recursos.*')
	                      ->get();

	    if($validarVolumen){
	    	$respuesta = array("codigo" => "1");
	 
	    }else{
	    	for($i = (int)$data["inicial"]; $i<= (int)$data["final"]; $i++){

				if($biblioteca->id != 1){ // si no es de la bag utiliza T1
					$aux_id ='T1'; 
				}else{
					$aux_id ='T0'; 
				}

			    $query_id_recurso = "";			  
			    $query_id_recurso = DB::table('recursos')
			                      ->where('id','like',$aux_id.'%')
			                      ->select(DB::raw('max(recursos.id) as recursos_id'))->first();

			    $id_recurso_actual= $query_id_recurso->recursos_id;
			    $int_id_recurso = filter_var($id_recurso_actual, FILTER_SANITIZE_NUMBER_INT);
			    $id_recurso_nuevo = $aux_id.(string)($int_id_recurso + 1);

	    		$recurso = new Recurso;	
				$recurso->id = $id_recurso_nuevo;
				$recurso->ext_id = 0;
				$recurso->jerarquia = $recursoPublicacion->jerarquia;
		        $recurso->tipo_liter = 'S';
		        $recurso->nivel_reg = 'ms';
		        $recurso->fecha_catalogacion = $fecha_actual;
		        $recurso->id_catalogador = $recursoPublicacion->id_catalogador;
		        $recurso->fecha_iso = $data["fecha_iso"];
		        $recurso->numero = $i;
		        $recurso->volumen = $data["volumen"];
		 		$recurso->save();	

		 		$query_id_ejemplar = DB::table('ejemplares')
		                      ->where('id','like',$aux_id.'%')
		                      ->select(DB::raw('max(ejemplares.id) as ejemplar_id'))->first();
		   		$id_ejemplar_actual = $query_id_ejemplar->ejemplar_id;
		    	$int_id_ejemplar = filter_var($id_ejemplar_actual, FILTER_SANITIZE_NUMBER_INT);
		    	$id_ejemplar_nuevo = $aux_id.(string)($int_id_ejemplar + 1);

				$ejemplar = new Ejemplar;	
					$ejemplar->ext_recurso_id = $recurso->ext_id;
					$ejemplar->recurso_id = $recurso->id;
					$ejemplar->tipo_liter = 'S';
					$ejemplar->id = $id_ejemplar_nuevo;
					$ejemplar->ejemplar = "0001";
					$ejemplar->existencia = 'E';
					$ejemplar->id = $id_ejemplar_nuevo;
					$ejemplar->doi ="";
					$ejemplar->doc_electronico = '0';
					$ejemplar->nombre_doc_electronico = "";
					$ejemplar->save();	

				$queryTitulo = DB::table('titulos')
					->join('recursos_titulos','recursos_titulos.titulo_id','=','titulos.id')
					->join('recursos','recursos.id','=','recursos_titulos.recurso_id')
					->where('recursos.id','=', $recursoPublicacion->id)
					->select('titulos.*')->first();

				if($queryTitulo){
					$recurso_titulo = new RecursosTitulo;	
						$recurso_titulo->recurso_id = $recurso->id;
						$recurso_titulo->ext_recurso_id =  $recurso->ext_id;
						$recurso_titulo->titulo_id = $queryTitulo->id;
						$recurso_titulo->tipo_tit = 'OP';
						$recurso_titulo->tipo_doc = 'S';
						$recurso_titulo->portada = ''; 
						$recurso_titulo->orden = 0;
						$recurso_titulo->save();
				}
				$respuesta = array("codigo" => "0");
	    	}

	    	date_default_timezone_set('America/Caracas');
			$historial = new HistorialCatalogacion;
			$date = date('YmdHis', time());
			$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
			$historial->recurso_id = $data["id_recurso_publicacion"];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Agregar'; 
			$historial->modulo = 'Números';
			$historial->fecha = $date;
			$historial->save();
	    
	    }
	    return $respuesta;
	                
	}


	public function agregar_numero_serie($dataVolImportar = null){
		if($dataVolImportar){
			$data = $dataVolImportar;
		}else{
			$data = Input::all();
			
		}
		$fecha_actual = date('Ymd');

		$biblioteca = DB::table('recursos_bibliotecas')
							   ->join('bibliotecas','bibliotecas.id','=','recursos_bibliotecas.biblioteca_id')
							   ->where('recursos_bibliotecas.recurso_id','=',$data["id_recurso_publicacion"])
							   ->select('bibliotecas.*')
							   ->first();
		
		$recursoPublicacion = DB::table('recursos')
	                      ->where('recursos.id','=',$data["id_recurso_publicacion"])
	                      ->select('recursos.*')
	                      ->first();

	    $volumenes = DB::table('recursos')
	    				  ->where('recursos.tipo_liter','=', 'S')
	                      ->where('recursos.jerarquia','=', $recursoPublicacion->jerarquia)
	                      ->where('recursos.volumen', '=', $data["volumen"])
	                      ->where('recursos.fecha_iso', '=', $data["fecha_iso"])
	                      ->select('recursos.*')
	                      ->get();

	    $existe_numero = false;
	    foreach ($volumenes as $volumen) {
			if($volumen->numero != null){
				if($data["inicial"] != (int)$data["final"]){
					if($volumen->numero >= (int)$data["inicial"] && $volumen->numero <= (int)$data["final"])
					$existe_numero = true;
				}else if($volumen->numero == (int)$data["inicial"])
					$existe_numero = true;
			}
		}

	    if($existe_numero){
	    	$respuesta = array("codigo" => "1");
	    	//   Para el volumen seleccionado, ya existen números para el rango de numeros introducidos
	 
	    }else{
	    	for($i = (int)$data["inicial"]; $i<= (int)$data["final"]; $i++){

				if($biblioteca->id != 1){ // si no es de la bag utiliza T1
					$aux_id ='T1'; 
				}else{
					$aux_id ='T0'; 
				}

			    $query_id_recurso = "";			  
			    $query_id_recurso = DB::table('recursos')
			                      ->where('id','like',$aux_id.'%')
			                      ->select(DB::raw('max(recursos.id) as recursos_id'))->first();

			    $id_recurso_actual= $query_id_recurso->recursos_id;
			    $int_id_recurso = filter_var($id_recurso_actual, FILTER_SANITIZE_NUMBER_INT);
			    $id_recurso_nuevo = $aux_id.(string)($int_id_recurso + 1);

	    		$recurso = new Recurso;	
				$recurso->id = $id_recurso_nuevo;
				$recurso->ext_id = 0;
				$recurso->jerarquia = $recursoPublicacion->jerarquia;
		        $recurso->tipo_liter = 'S';
		        $recurso->nivel_reg = 'ms';
		        $recurso->fecha_catalogacion = $fecha_actual;
		        $recurso->id_catalogador = $recursoPublicacion->id_catalogador;
		        $recurso->fecha_iso = $data["fecha_iso"];
		        $recurso->numero = $i;
		        $recurso->volumen = $data["volumen"];
		 		$recurso->save();

				$query_id_ejemplar = DB::table('ejemplares')
		                      ->where('id','like',$aux_id.'%')
		                      ->select(DB::raw('max(ejemplares.id) as ejemplar_id'))->first();
		   		$id_ejemplar_actual = $query_id_ejemplar->ejemplar_id;
		    	$int_id_ejemplar = filter_var($id_ejemplar_actual, FILTER_SANITIZE_NUMBER_INT);
		    	$id_ejemplar_nuevo = $aux_id.(string)($int_id_ejemplar + 1);

				$ejemplar = new Ejemplar;	
					$ejemplar->ext_recurso_id = $recurso->ext_id;
					$ejemplar->recurso_id = $recurso->id;
					$ejemplar->tipo_liter = 'S';
					$ejemplar->id = $id_ejemplar_nuevo;
					$ejemplar->ejemplar = "0001";
					$ejemplar->existencia = 'E';
					$ejemplar->id = $id_ejemplar_nuevo;
					$ejemplar->doi ="";
					$ejemplar->doc_electronico = '0';
					$ejemplar->nombre_doc_electronico = "";
					$ejemplar->save();	

				$queryTitulo = DB::table('titulos')
					->join('recursos_titulos','recursos_titulos.titulo_id','=','titulos.id')
					->join('recursos','recursos.id','=','recursos_titulos.recurso_id')
					->where('recursos.id','=', $recursoPublicacion->id)
					->select('titulos.*')->first();

				if($queryTitulo){
					$recurso_titulo = new RecursosTitulo;	
						$recurso_titulo->recurso_id = $recurso->id;
						$recurso_titulo->ext_recurso_id =  $recurso->ext_id;
						$recurso_titulo->titulo_id = $queryTitulo->id;
						$recurso_titulo->tipo_tit = 'OP';
						$recurso_titulo->tipo_doc = 'S';
						$recurso_titulo->portada = ''; 
						$recurso_titulo->orden = 0;
						$recurso_titulo->save();
				}
	    		$respuesta = array("codigo" => "0");
	    	}
	    	
	    	date_default_timezone_set('America/Caracas');
			$historial = new HistorialCatalogacion;
			$date = date('YmdHis', time());
			$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
			$historial->recurso_id = $data["id_recurso_publicacion"];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Agregar'; 
			$historial->modulo = 'Números';
			$historial->fecha = $date;
			$historial->save();
	    	
	    }
	    return $respuesta;
	                
	}


	public function obtener_volumenes_numeros($dataImportar = null){
		if($dataImportar){
			$data = $dataImportar;	
		}else{
			$data = Input::all();	
		}
		
		$recursoPublicacion = DB::table('recursos')
	                      ->where('recursos.id','=',$data["id_recurso_publicacion"])
	                      ->select('recursos.*')
	                      ->first();

	    $volumenes = DB::table('recursos')
						  ->join('ejemplares', 'ejemplares.recurso_id', '=', 'recursos.id')
						  ->where('recursos.tipo_liter','=', 'S')
			              ->where('recursos.jerarquia','=', $recursoPublicacion->jerarquia)
			              ->whereNotNull('recursos.numero')
			              ->whereNotNull('volumen')
			              ->orderBy('fecha_iso', 'ASC')
			              ->orderBy('volumen', 'ASC')
			              ->orderBy('numero', 'ASC')
			              ->select('recursos.*', 'ejemplares.id as ejemplar_id', 'ejemplares.existencia as existencia')
			              ->get();
	    return $volumenes;

    }

	public function eliminar_numero(){
		$data = Input::all();
		$recurso = DB::table('recursos')
		->where('id','=',$data['idRecurso'])
		->select('recursos.*')
		->first();	

		$volumenes = DB::table('recursos')
						  ->where('recursos.tipo_liter','=', 'S')
						  ->where('recursos.id','!=', $recurso->id)
	                      ->where('recursos.jerarquia','=', $recurso->jerarquia)
	                      ->where('recursos.volumen', '=', $recurso->volumen)
	                      ->whereNotNull('recursos.numero')
	                      ->select('recursos.*')
	                      ->get();

	    if(!$volumenes){
	    	 $recurso = DB::table('recursos')
	    	->where('recursos.tipo_liter','=', 'S')
	    	->where('recursos.jerarquia','=', $recurso->jerarquia)
			->where('volumen','=',$recurso->volumen)
			->delete();
	    }

	    $recurso = DB::table('recursos')
		->where('id','=',$data['idRecurso'])
		->delete();


		$respuesta = array("codigo" => "0");

		date_default_timezone_set('America/Caracas');
			$historial = new HistorialCatalogacion;
			$date = date('YmdHis', time());
			$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
			$historial->recurso_id = $data["id_recurso_publicacion"];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Eliminar'; 
			$historial->modulo = 'Números';
			$historial->fecha = $date;
			$historial->save();   
	    return $respuesta; 
	}

	public function modificar_numero(){
		$data = Input::all();
		//echo $data['existencia'].' '. $data['idRecurso']; 
		$queryRecurso = DB::table('ejemplares')
		->where('ejemplares.recurso_id','=',$data['idRecurso'])
		->update(array( 			  
	        'existencia' => $data['existencia']
 			)
		);    

		$queryRecurso = DB::table('recursos')
			->where('recursos.id','=',$data['idRecurso'])
			->update(array( 			  
				        'paginas' => $data['colacion'],
				        'numeracion' => $data['numeracion'],
				        'datos_adicionales' => $data['datos_adicionales'],
			 			)
					);     
			$respuesta = array("codigo" => "0");  
		date_default_timezone_set('America/Caracas');
			$historial = new HistorialCatalogacion;
			$date = date('YmdHis', time());
			$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
			$historial->recurso_id = $data["id_recurso_publicacion"];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Modificar'; 
			$historial->modulo = 'Números';
			$historial->fecha = $date;
			$historial->save();
	    	
		return $respuesta;       
	}


	public function editar_volumen(){
		$data = Input::all();
		$recursoPublicacion = DB::table('recursos')
	                      ->where('recursos.id','=',$data["id_recurso_publicacion"])
	                      ->select('recursos.*')
	                      ->first();

	    $validarVolumen = DB::table('recursos')
	    				  ->where('recursos.tipo_liter','=', 'S')
	                      ->where('recursos.jerarquia','=', $recursoPublicacion->jerarquia)
	                      ->where('recursos.volumen', '=', $data["volumen_actual"])
	                      ->where('recursos.fecha_iso', '=', $data["fecha_iso"])
	                      ->select('recursos.*')
	                      ->get();

	    if($validarVolumen){
	    	$respuesta = array("codigo" => "1");
	 
	    }else{    
	    	$queryRecurso = DB::table('recursos')
	    	->where('recursos.tipo_liter','=', 'S')
			->where('recursos.jerarquia','=', $recursoPublicacion->jerarquia)
			->where('recursos.volumen', '=', $data["volumen_anterior"])
			->where('recursos.fecha_iso', '=', $data["fecha_iso_anterior"])
			->where('recursos.nivel_reg', '=', 'ms')
			->update(array( 			  
				        'volumen' => $data["volumen_actual"],
				        'fecha_iso' => $data["fecha_iso"],
			 			)
					);   
			$respuesta = array("codigo" => "0");  
	    }
	    date_default_timezone_set('America/Caracas');
			$historial = new HistorialCatalogacion;
			$date = date('YmdHis', time());
			$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
			$historial->recurso_id = $data["id_recurso_publicacion"];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Modificar'; 
			$historial->modulo = 'Números';
			$historial->fecha = $date;
			$historial->save();
	
	    return $respuesta;
	}


}


