<?php

class TipoTitulosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /tipotitulos
	 *
	 * @return Response
	 */
	public function index()
	{
		//-- 01 de enero de 2015 -- cesar
		// cambiar por ::take()  ya que si la tabla se llena demasiado
		// no se obtendran datos.
		//--
		
		$data = Input::all();
		try {

			return TipoTitulo::where('tipo_doc',$data['tipo_recurso'])->orderBy('orden')->get();

		} catch (Exception $e) {

			return TipoTitulo::where('tipo_doc','M')->orderBy('orden')->get();
		}

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tipotitulos/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tipotitulos
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /tipotitulos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tipotitulos/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tipotitulos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tipotitulos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function obtener_titulos()
	{
		//
	}

}