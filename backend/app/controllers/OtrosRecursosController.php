<?php

class OtrosRecursosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /otrosrecursos.php
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /otrosrecursos.php/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /otrosrecursos.php
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /otrosrecursos.php/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /otrosrecursos.php/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /otrosrecursos.php/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /otrosrecursos.php/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function agregar_otro_recurso($dataOtroRecursoImportar = null){
		
		if($dataOtroRecursoImportar){
			$data = $dataOtroRecursoImportar;
		}else{
			$data = Input::all();
		}
		$fecha_actual = date('Ymd');
		$aux = 0;
	//	echo $fecha_actual;
		$codigoRespuesta= "0"; //recurso agregado 1 recurso existe, 2 error agregando el recurso
		if($data["bibliotecaSelect"] != 1){ // si no es de la bag utiliza T1
			$aux_id ='T1'; 
		}else{
			$aux_id ='T0'; 
		}
	    $last_id_recurso = "";
	    $query_id_recurso = "";
	  

	    $query_id_recurso = DB::table('recursos')
	                      ->where('id','like',$aux_id.'%')
	                      ->select(DB::raw('max(recursos.id) as recursos_id'))->first();

	    $id_recurso_actual= $query_id_recurso->recursos_id;
	    $int_id_recurso = filter_var($id_recurso_actual, FILTER_SANITIZE_NUMBER_INT);
	    $id_recurso_nuevo = $aux_id.(string)($int_id_recurso + 1);


	    //Obtener Jerarquia para nuevo recurso
		$query_jerarquia = DB::table('recursos')
		                  ->select(DB::raw('max(recursos.jerarquia) as max_jerarquia'))->first();

		$jerarquia_actual= $query_jerarquia->max_jerarquia;

		//if ($tipo_recurso == 'M' || $tipo_recurso == 'T'){
		$jerarquia_nuevo = $jerarquia_actual + 1;
		//}else{
		//	$jerarquia_nuevo = $jerarquia_actual;
		//}

		


		$queryAutor = DB::table('autores')
				->where('autor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor'])))
				->where('tipo_autor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['tipoAutor'])))
				->select('autores.*')->first();
				
		$queryTitulo = DB::table('titulos')
				->where('titulo','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo'])))
				->select('titulos.*')->first();

		$queryRecurso = DB::table('recursos')
				->join('recursos_titulos','recursos_titulos.recurso_id','=','recursos.id')
				->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
				->where('titulos.titulo','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo'])))
				->where('recursos.tipo_liter', '=', $data['tipoLiter'])
				->select('recursos.*')->first();

		if($queryRecurso){
			if($queryRecurso->ext_id == 0){
				$codigoRespuesta = "1";
				$respuesta = array("codigo" => "1", "id_recurso" => "");
				if($dataOtroRecursoImportar)
					return $respuesta;
				else
					return json_encode($respuesta);
			}else{
				$queryDelete = DB::table('recursos_autores')
				->where('recursos_autores.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryDelete = DB::table('recursos_titulos')
				->where('recursos_titulos.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryDelete = DB::table('recursos_camposadicionales')
				->where('recursos_camposadicionales.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryDelete = DB::table('recursos_descriptores')
				->where('recursos_descriptores.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryDelete = DB::table('recursos_bibliotecas')
				->where('recursos_bibliotecas.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryDelete = DB::table('recursos')
				->where('recursos.id','=', $queryRecurso->id)
				->delete();
				$aux = 1;
			}
		}	

		if(!$queryRecurso || $aux == 1){
			$recurso = new Recurso;	
			$recurso->id = $id_recurso_nuevo;
			$recurso->ext_id = 0;//
			$recurso->jerarquia = $jerarquia_nuevo;
	        $recurso->tipo_liter = $data['tipoLiter'];
	        $recurso->url = $data['url'];
	        $recurso->fecha_iso = $data["fechaEdicion"]; 
	        $recurso->fecha_catalogacion = $fecha_actual;
	        $recurso->id_catalogador = $data['idCatalogador'];
	        $recurso->nivel_reg = '';
	        $recurso->datos_adicionales = $data['datosAdicionales'];
	        $recurso->notas = $data['notas'];
	        $recurso->resumen = $data['resumen'];
	 		$recurso->save();

	 		if($queryAutor){
				$idAutor = $queryAutor->id;	
			}else{
				$autor = new Autor;	
				$autor->autor = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor']));
				$autor->autor_salida = $data['nombreAutor'];
				$autor->tipo_autor = $data['tipoAutor'];
				$autor->cod_dir = '';
				$autor->dir_electr = ''; 
				$autor->fecha_nac_dec = ''; 
				$autor->save();
				$idAutor = $autor->id;	
			}
			
			$recurso_autor = new RecursoAutor;	
				$recurso_autor->recurso_id = $recurso->id;
				$recurso_autor->ext_recurso_id =  $recurso->ext_id;
				$recurso_autor->cod_autor = $idAutor;
				$recurso_autor->tipo_doc = $data["tipoLiter"];
				$recurso_autor->tipo_autor = 'P';
				$recurso_autor->portada = "1"; 
				$recurso_autor->orden = 0;
				$recurso_autor->save();

			$recurso_biblioteca = new RecursosBiblioteca;	
				$recurso_biblioteca->recurso_id = $recurso->id;
				$recurso_biblioteca->ext_recurso_id =  $recurso->ext_id;
				$recurso_biblioteca->biblioteca_id = $data["bibliotecaSelect"];
				$recurso_biblioteca->save();

			if($queryTitulo){
				$idTitulo = $queryTitulo->id;	
			}else{
				$titulo = new Titulo;	
				$titulo->caracter_orden = 0;
				$titulo->titulo_salida = $data['nombreTitulo'];
				$titulo->titulo =  strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']));
				$titulo->save();
				$idTitulo = $titulo->id;		
			}

			$recurso_titulo = new RecursosTitulo;	
				$recurso_titulo->recurso_id = $recurso->id;
				$recurso_titulo->ext_recurso_id =  $recurso->ext_id;
				$recurso_titulo->titulo_id = $idTitulo;
				$recurso_titulo->tipo_tit = 'OP';
				$recurso_titulo->tipo_doc = $data["tipoLiter"];
				$recurso_titulo->portada = ''; 
				$recurso_titulo->orden = 0;
				$recurso_titulo->save();

			$aux_id = "T0";
			$query_id_ejemplar = DB::table('ejemplares')
	                      ->where('id','like',$aux_id.'%')
	                      ->select(DB::raw('max(ejemplares.id) as ejemplar_id'))->first();
	   		$id_ejemplar_actual = $query_id_ejemplar->ejemplar_id;
	    	$int_id_ejemplar = filter_var($id_ejemplar_actual, FILTER_SANITIZE_NUMBER_INT);
	    	$id_ejemplar_nuevo = $aux_id.(string)($int_id_ejemplar + 1);

			$ejemplar = new Ejemplar;	
				$ejemplar->ext_recurso_id = $recurso->ext_id;
				$ejemplar->recurso_id = $recurso->id;
				$ejemplar->tipo_liter = $data['tipoLiter'];
				$ejemplar->id = $id_ejemplar_nuevo;
				$ejemplar->ejemplar = "0001";
				$ejemplar->id = $id_ejemplar_nuevo;
				$ejemplar->doi = $data['doi'];
				$ejemplar->doc_electronico = $data['esElectronico'];
				$ejemplar->nombre_doc_electronico = $data['nombreDocElectronico'];
				$ejemplar->save();


			$queryRecurso = DB::table('recursos_camposadicionales')
				->where('recursos_camposadicionales.recurso_id','=', $recurso->id)
				->delete();
			$campos_adicionales = $data["campos_adicionales"];
			foreach ($campos_adicionales as $row){

				try {
					if($row['descripcion']){
						$recurso_campoadicional = new RecursosCampoAdicional;
						$recurso_campoadicional->recurso_id = $recurso->id;
						$recurso_campoadicional->ext_recurso_id =  $recurso->ext_id;	
						$recurso_campoadicional->tipo_doc =  $data['tipoLiter'];
						$recurso_campoadicional->campo_adicional = $row["id"];
						$recurso_campoadicional->descripcion = $row['descripcion'];
						$recurso_campoadicional->save();
					}		
				} catch (Exception $e) {
					
				}

			}

			date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data['idCatalogador'];
			$historial->recurso_id = $recurso->id;
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Agregar'; 
			$historial->modulo = 'Ficha Principal';
			$historial->fecha = $date;
			$historial->save();

			$respuesta = array("codigo" => "0" , "id_recurso"=> $recurso->id, "id_autor"=> $idAutor, "id_titulo"=>  $idTitulo);
			if($dataOtroRecursoImportar)
				return $respuesta;
			else
				return json_encode($respuesta);
		}
	}


	public function guardar_otro_recurso(){
		$data = Input::all();
		$fecha_actual = date('Ymd');
			$verificarTituloExistente = DB::table('recursos')
				->join('recursos_titulos','recursos_titulos.recurso_id','=','recursos.id')
				->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
				->where('titulos.titulo','=', App::make('BuscadoresController')->convetir_string($data['nombreTitulo']))
				->where('recursos.id','!=',$data['idRecurso'])
				->where('recursos.tipo_liter', '=', $data['tipoLiter'])
				->select('recursos.*')->first();

		if(!$verificarTituloExistente){
			$queryRecurso = DB::table('recursos')
				->where('recursos.id','=',$data['idRecurso'])
				->update(array( 'fecha_iso' => $data['fechaEdicion'],  	        
					        'url' => $data['url'],
					        'id_catalogador' => $data['idCatalogador'],
					        'datos_adicionales' => $data['datosAdicionales'],
					        'notas' => $data['notas'],
					        'resumen' => $data['resumen']
				 			)
						);
			$recurso = DB::table('recursos')
					->where('recursos.id','=',$data['idRecurso'])
					->select('recursos.*')->first();



			if($data['idAutor']!= ""){
				$queryAutor = DB::table('autores')
				->where('id','=',$data['idAutor'])
				->update(array('tipo_autor' => $data['tipoAutor'] , 'autor_salida' => $data['nombreAutor'], 'autor' => strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor']))));
				
				$queryAutor = DB::table('autores')
				->where('id','=',$data['idAutor'])->first();
				$idAutor = $queryAutor->id;		
			}
			else{
				$autor = new Autor;	
				$autor->autor = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor']));
				$autor->autor_salida = $data['nombreAutor'];
				$autor->tipo_autor = $data['tipoAutor'];
				$autor->cod_dir = '';//$data['tipoAutor']; ///??????????
				$autor->dir_electr = ''; //$data['tipoAutor'];  ///??????????
				$autor->fecha_nac_dec = ''; //$data['tipoAutor']; ///??????????
				$autor->save();
				$idAutor = $autor->id;	

				$recurso_autor = new RecursoAutor;	
				$recurso_autor->recurso_id = $recurso->id;
				$recurso_autor->ext_recurso_id =  $recurso->ext_id;
				$recurso_autor->cod_autor = $idAutor;
				$recurso_autor->tipo_doc = $data["tipoLiter"];
				$recurso_autor->tipo_autor = 'P';
				$recurso_autor->portada = ""; 
				$recurso_autor->orden = 0;
				$recurso_autor->save();
			}
		

			if($data["idTitulo"]!= ""){
				$queryTitulo = DB::table('titulos')
				->where('id','=',$data['idTitulo'])
				->update(array('titulo_salida' => $data['nombreTitulo'], 'titulo' => strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']))));

				$queryTitulo = DB::table('titulos')
				->where('id','=',$data['idTitulo'])->first();
				$idTitulo = $queryTitulo->id;		

			}else{
				$titulo = new Titulo;	
				$titulo->caracter_orden = 0;
				$titulo->titulo_salida = $data['nombreTitulo'];
				$titulo->titulo =  strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']));
				$titulo->save();
				$idTitulo = $titulo->id;	

				$recurso_titulo = new RecursosTitulo;	
				$recurso_titulo->recurso_id = $recurso->id;
				$recurso_titulo->ext_recurso_id =  $recurso->ext_id;
				$recurso_titulo->titulo_id = $idTitulo;
				$recurso_titulo->tipo_tit = 'OP';
				$recurso_titulo->tipo_doc = $data["tipoLiter"];
				$recurso_titulo->portada = ''; 
				$recurso_titulo->orden = 0;
				$recurso_titulo->save();	
			}
			$queryRecurso = DB::table('recursos_camposadicionales')
				->where('recursos_camposadicionales.recurso_id','=', $recurso->id)
				->delete();
				
			$campos_adicionales = $data["campos_adicionales"];
			foreach ($campos_adicionales as $row){
				try {
					if($row['descripcion']){
						$recurso_campoadicional = new RecursosCampoAdicional;
						$recurso_campoadicional->recurso_id = $recurso->id;
						$recurso_campoadicional->ext_recurso_id =  $recurso->ext_id;	
						$recurso_campoadicional->tipo_doc =  $data['tipoLiter'];
						$recurso_campoadicional->campo_adicional = $row["id"];
						$recurso_campoadicional->descripcion = $row['descripcion'];
						$recurso_campoadicional->save();
					}		
				} catch (Exception $e) {
					
				}
			}					

			$biblioteca = DB::table('recursos_bibliotecas')
			->where('recursos_bibliotecas.recurso_id','=',$data['idRecurso'])
			->update(array('biblioteca_id' => $data["bibliotecaSelect"]));

			$ejemplar = DB::table('ejemplares')
            ->where('ejemplares.recurso_id','=', $data['idRecurso'])
            ->orderby('ejemplares.ejemplar', 'ASC')
            ->select('ejemplares.*')->first();

         	$ejemplar_update = DB::table('ejemplares')
            ->where('ejemplares.id','=', $ejemplar->id)
            ->update(array('doi' => $data["doi"],
				'doc_electronico' => $data["esElectronico"],
				'nombre_doc_electronico' => $data["nombreDocElectronico"]));

			date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data['idCatalogador'];
				$historial->recurso_id = $data['idRecurso'];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Modificar'; 
				$historial->modulo = 'Ficha Principal';
				$historial->fecha = $date;
				$historial->save();
		
				$respuesta = array("codigo" => "0");
				return json_encode($respuesta);
		}else{
			$respuesta = array("codigo" => "1");
				return json_encode($respuesta);
		}

	}


	public function eliminar_otro_recurso(){
		$data = Input::all();
		$recurso = DB::table('recursos')
		->where('id','=',$data['idRecurso'])
		->update(array('ext_id' => 1));
		$respuesta = array("codigo" => "0");
		date_default_timezone_set('America/Caracas');
		$date = date('YmdHis', time());
		$historial = new HistorialCatalogacion;
		$historial->id_catalogador =  $data['idCatalogador']; 
		$historial->recurso_id = $data['idRecurso'];
		$historial->ext_recurso_id = '1';
		$historial->operacion = 'Eliminar'; 
		$historial->modulo = 'Ficha Principal';
		$historial->fecha = $date;
		$historial->save();
		return json_encode($respuesta);
 	}

}