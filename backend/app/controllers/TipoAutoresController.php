<?php

class TipoAutoresController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /tipoautores
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Input::all();
		try {

			return TipoAutor::where('tipo_doc',$data['tipo_recurso'])->orderBy('orden')->get();

		} catch (Exception $e) {

			return TipoAutor::where('tipo_doc','M')->orderBy('orden')->get();
		}
		
	}


	/**
	 * Show the form for creating a new resource.
	 * GET /tipoautores/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tipoautores
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /tipoautores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tipoautores/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tipoautores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tipoautores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
}