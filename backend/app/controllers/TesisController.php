<?php

class TesisController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /tesis
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$tesiss = Tesis::take(10)->skip(0)->get();
		return $tesiss;

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tesis/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tesis
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /tesis/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		echo '<pre>';
		//
			}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tesis/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tesis/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tesis/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function obtener_grados_academicos(){
		return GradosAcademico::all();
	}



	public function agregar_tesis($dataTesisImportar = null){
		
		if($dataTesisImportar){
			$data = $dataTesisImportar;
		}else{
			$data = Input::all();
		}

		$fecha_actual = date('Ymd');
		$aux = 0;
	//	echo $fecha_actual;
		$codigoRespuesta= "0"; //recurso agregado 1 recurso existe, 2 error agregando el recurso

		if($data["bibliotecaSelect"] != 1){ // si no es de la bag utiliza T1
			$aux_id ='T1'; 
		}else{
			$aux_id ='T0'; 
		}
	    $last_id_recurso = "";
	    $query_id_recurso = "";

	    $query_id_recurso = DB::table('recursos')
	                      ->where('id','like',$aux_id.'%')
	                      ->select(DB::raw('max(recursos.id) as recursos_id'))->first();

	    $id_recurso_actual= $query_id_recurso->recursos_id;
	    $int_id_recurso = filter_var($id_recurso_actual, FILTER_SANITIZE_NUMBER_INT);
	    $id_recurso_nuevo = $aux_id.(string)($int_id_recurso + 1);

	    //Obtener Jerarquia para nuevo recurso
		$query_jerarquia = DB::table('recursos')
		                  ->select(DB::raw('max(recursos.jerarquia) as max_jerarquia'))->first();

		$jerarquia_actual= $query_jerarquia->max_jerarquia;

		//if ($tipo_recurso == 'M' || $tipo_recurso == 'T'){
			$jerarquia_nuevo = $jerarquia_actual + 1;
		//}else{
		//	$jerarquia_nuevo = $jerarquia_actual;
		//}


		$dependencia = $data['dependencia'];
		$queryDependencia = '';
		if($dependencia != ''){ 
			$queryDependencia = DB::table('dependencias')
				->where('dependencia','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['dependencia'])))
				->select('dependencias.*')->first();
		}

		$disciplina = $data['disciplina'];
		$queryDisciplina = '';
		if($disciplina != ''){ 
			$queryDisciplina = DB::table('disciplinas')
				->where('disciplina','=',  strtoupper(App::make('BuscadoresController')->convetir_string($data['disciplina'])))
				->select('disciplinas.*')->first();
		}
		
		$queryRecurso = DB::table('recursos')
				->where('recursos.ubicacion','=',strtoupper(App::make('BuscadoresController')->convetir_string($data['cota'])))
				->select('recursos.*')->first();
 

		$queryAutor = DB::table('autores')
				->where('autor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor'])))
				->where('tipo_autor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['tipoAutor'])))
				->select('autores.*')->first();
				
		$queryTitulo = DB::table('titulos')
				->where('titulo','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo'])))
				->select('titulos.*')->first();
				
		if($queryRecurso){
			if($queryRecurso->ext_id == 0){
				$codigoRespuesta= "1";
				$respuesta = array("codigo" => "1", "id_recurso"=> "");
				if($dataTesisImportar)
					return $respuesta;
				else
					return json_encode($respuesta);
			}else{

				$queryRecurso = DB::table('recursos_autores')
				->where('recursos_autores.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_titulos')
				->where('recursos_titulos.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_editoriales')
				->where('recursos_editoriales.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_descriptores')
				->where('recursos_descriptores.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_bibliotecas')
				->where('recursos_bibliotecas.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos')
				->where('recursos.id','=', $queryRecurso->id)
				->delete();
				$aux = 1;
			}
		}
		
		if(!$queryRecurso || $aux == 1){
			$recurso = new Recurso;	
			$recurso->id = $id_recurso_nuevo;
			$recurso->ext_id = 0;//
			$recurso->ubicacion = strtoupper(App::make('BuscadoresController')->convetir_string($data['cota']));
			$recurso->jerarquia = $jerarquia_nuevo;
	        $recurso->tipo_liter = $data['tipoLiter'];
	        $recurso->nivel_reg = 'm';
	        $recurso->cod_conf = NULL;
	        $recurso->cod_proy = NULL;
	        $recurso->cod_editor_inst = NULL;
	        $recurso->ubicacion_anterior = NULL; //TIENE DATOS
	        $recurso->codigo = NULL;
	        $recurso->ubicacion_fia_nivel_sup = NULL;
	        $recurso->servicio_edicion = NULL;
	        $recurso->codigo_edicion = NULL;
	        $recurso->nivel_bibl = NULL;
	        $recurso->tipo_ref_analitica = NULL; // null, a
	        $recurso->paginas = $data['colacion'];
	        $recurso->volumen = (int)$data['volumen']; //publicaciones seriadas???????
	        $recurso->volumen_final = NULL;
	        $recurso->numero = NULL; // publicaciones seriasas?
	        $recurso->numero_final = NULL;
	        $recurso->numeracion = $data['numeracion'];
	        $recurso->enum_nivel_ini_3 = NULL;
	        $recurso->enum_nivel_ini_4 = NULL;
	        $recurso->enum_nivel_fin_3 = NULL;
	        $recurso->enum_nivel_fin_4 = NULL;
	      //  $recurso->edicion = $data['edicion'];
	        $recurso->fecha_iso = $data["fechaEdicion"]; 
	        $recurso->fecha_iso_final = NULL;
	        $recurso->fecha_pub =  $data['fecha_pub']; //yyyy
	        $recurso->isbn = $data['isbn'];
	        $recurso->indice_suplemento = NULL;
	        $recurso->disemina = NULL; 
	        $recurso->url = $data['url'];
	        $recurso->n_disk_cin = NULL;
	        $recurso->impresion = $data['impresion'];
	        $recurso->orden = NULL; 
	        $recurso->cerrado = NULL;
	        $recurso->hora_iso_inicio = NULL;
	        $recurso->hora_iso_final = NULL;
	        $recurso->dir_electr = NULL;
	        $recurso->herramienta_correo = NULL;
	        $recurso->fecha_catalogacion =  $fecha_actual;
	        $recurso->id_catalogador = $data['idCatalogador'];
	        $recurso->datos_adicionales = $data['datosAdicionales'];
	        $recurso->notas = $data['notas'];
	        $recurso->resumen = $data['resumen'];
	 		$recurso->save();

	 		if($queryAutor){
				$idAutor = $queryAutor->id;	
			}else{
				$autor = new Autor;	
				$autor->autor = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor']));
				$autor->autor_salida = $data['nombreAutor'];
				$autor->tipo_autor = $data['tipoAutor'];
				$autor->cod_dir = '';//$data['tipoAutor']; ///??????????
				$autor->dir_electr = ''; //$data['tipoAutor'];  ///??????????
				$autor->fecha_nac_dec = ''; //$data['tipoAutor']; ///??????????
				$autor->save();
				$idAutor = $autor->id;	
			}
			
			$recurso_autor = new RecursoAutor;	
				$recurso_autor->recurso_id = $recurso->id;
				$recurso_autor->ext_recurso_id =  $recurso->ext_id;
				$recurso_autor->cod_autor = $idAutor;
				$recurso_autor->tipo_doc = 'T';
				$recurso_autor->tipo_autor = 'P';
				$recurso_autor->portada = "1"; 
				$recurso_autor->orden = 0;
				$recurso_autor->save();

			$recurso_biblioteca = new RecursosBiblioteca;	
				$recurso_biblioteca->recurso_id = $recurso->id;
				$recurso_biblioteca->ext_recurso_id =  $recurso->ext_id;
				$recurso_biblioteca->biblioteca_id = $data["bibliotecaSelect"];
				$recurso_biblioteca->save();

			if($queryTitulo){
				$idTitulo = $queryTitulo->id;	
			}else{
				$titulo = new Titulo;	
				$titulo->caracter_orden = 0;
				$titulo->titulo_salida = $data['nombreTitulo'];
				$titulo->titulo =  strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']));
				$titulo->save();
				$idTitulo = $titulo->id;		
			}

			$recurso_titulo = new RecursosTitulo;	
				$recurso_titulo->recurso_id = $recurso->id;
				$recurso_titulo->ext_recurso_id =  $recurso->ext_id;
				$recurso_titulo->titulo_id = $idTitulo;
				$recurso_titulo->tipo_tit = 'OP';
				$recurso_titulo->tipo_doc = 'T';
				$recurso_titulo->portada = ''; 
				$recurso_titulo->orden = 0;
				$recurso_titulo->save();

			if($queryDependencia){
					$idDependencia = $queryDependencia->id;	
			}else{
				if($dependencia != ''){
					$dependencia = new Dependencia;	
					$dependencia->dependencia = strtoupper(App::make('BuscadoresController')->convetir_string($data['dependencia']));
					$dependencia->dependencia_salida = $data['dependencia'];
					$dependencia->siglas = '';
					$dependencia->save();
					$idDependencia = $dependencia->id;	
				}
			}
			
			if($dependencia != ''){
				$recurso_dependencia = new RecursosDependencia;	
					$recurso_dependencia->recurso_id = $recurso->id;
					$recurso_dependencia->ext_recurso_id =  $recurso->ext_id;
					$recurso_dependencia->dependencia_id = $idDependencia;
					$recurso_dependencia->save();
			}

			if($queryDisciplina){
					$idDisciplina = $queryDisciplina->id;	
			}else{
				if($disciplina != ''){
					$disciplina = new Disciplina;	
					$disciplina->disciplina = strtoupper(App::make('BuscadoresController')->convetir_string($data['disciplina']));
					$disciplina->disciplina_salida = $data['disciplina'];
					$disciplina->save();
					$idDisciplina = $disciplina->id;	
				}
			}
			
			if($disciplina != ''){
				$recurso_disciplina= new RecursosDisciplinas;	
					$recurso_disciplina->recurso_id = $recurso->id;
					$recurso_disciplina->ext_recurso_id =  $recurso->ext_id;
					$recurso_disciplina->disciplina_id = $idDisciplina;
					$recurso_disciplina->save();
			}

			if($dataTesisImportar){
				$institucion = DB::table('instituciones')
						->where('instituciones.institucion', '=',strtoupper(App::make('BuscadoresController')->convetir_string($data['institucion'])))
						->first();

				if(!$institucion){
					
					$institucion = new institucion;
					$institucion->institucion_salida = $data['institucion'];
					$institucion->url = '';
					$institucion->pais_id = '';
					$institucion->institucion = strtoupper(App::make('BuscadoresController')->convetir_string($data['institucion']));
					$institucion->save();
					$respuesta = array("codigo" => "0", "id" => $institucion->id);
				}
				$idInstitucion = $institucion->id;
			}else{
				$idInstitucion = (int)$data['institucion'];
			}
			$tesis = new Tesis;	
				$tesis->ext_recurso_id = $recurso->ext_id;
				$tesis->recurso_id = $recurso->id;
				$tesis->cod_inst = $idInstitucion;
				$tesis->nivel_id = (int)$data['nivel'];
				$tesis->trabajo_id = (int)$data['trabajo'];
				$tesis->grado_acad = $data['gradoAcademico'];
				$tesis->save();

			$aux_id = "T0";
			$query_id_ejemplar = DB::table('ejemplares')
	                      ->where('id','like',$aux_id.'%')
	                      ->select(DB::raw('max(ejemplares.id) as ejemplar_id'))->first();

	   		$id_ejemplar_actual = $query_id_ejemplar->ejemplar_id;
	    	$int_id_ejemplar = filter_var($id_ejemplar_actual, FILTER_SANITIZE_NUMBER_INT);
	    	$id_ejemplar_nuevo = $aux_id.(string)($int_id_ejemplar + 1);
	    	
			$ejemplar = new Ejemplar;	
				$ejemplar->ext_recurso_id = $recurso->ext_id;
				$ejemplar->recurso_id = $recurso->id;
				$ejemplar->tipo_liter = $data['tipoLiter'];
				$ejemplar->ejemplar = "0001";
				$ejemplar->id = $id_ejemplar_nuevo;
				$ejemplar->doi = $data['doi'];
				$ejemplar->doc_electronico = $data['esElectronico'];
				$ejemplar->nombre_doc_electronico = $data['nombreDocElectronico'];
				$ejemplar->save();

			if($data['tutorTesis'] != ''){
				$queryAutorT = DB::table('autores')
					->where('autor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['tutorTesis'])))
					->where('tipo_autor','=', 'P')
					->select('autores.*')->first();

				if(!$queryAutorT){
					$autor = new Autor;	
					$autor->autor = strtoupper(App::make('BuscadoresController')->convetir_string($data['tutorTesis']));
					$autor->autor_salida = $data['tutorTesis'];
					$autor->tipo_autor = 'P';
					$autor->cod_dir = '';//$data['tipoAutor']; ///??????????
					$autor->dir_electr = $data['correoTutor']; //$data['tipoAutor'];  ///??????????
					$autor->fecha_nac_dec = ''; //$data['tipoAutor']; ///??????????
					$autor->save();
					$idAutor = $autor->id;	

					$recurso_autor = new RecursoAutor;	
					$recurso_autor->recurso_id = $recurso->id;
					$recurso_autor->ext_recurso_id =  $recurso->ext_id;
					$recurso_autor->cod_autor = $idAutor;
					$recurso_autor->tipo_doc = 'T';
					$recurso_autor->tipo_autor = 'TA';
					$recurso_autor->portada = "0"; 
					$recurso_autor->orden = 0;
					$recurso_autor->save();
				}else{
					$recurso_autor = new RecursoAutor;	
					$recurso_autor->recurso_id = $recurso->id;
					$recurso_autor->ext_recurso_id =  $recurso->ext_id;
					$recurso_autor->cod_autor = $queryAutorT->id;
					$recurso_autor->tipo_doc = 'T';
					$recurso_autor->tipo_autor = 'TA';
					$recurso_autor->portada = "0"; 
					$recurso_autor->orden = 0;
					$recurso_autor->save();	
				}
			}				
			$respuesta = array("codigo" => "0" , "id_recurso"=> $recurso->id, "id_autor"=> $idAutor, "id_titulo"=>  $idTitulo);
			date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			
			$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data['idCatalogador'];
			$historial->recurso_id = $recurso->id;
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Agregar'; 
			$historial->modulo = 'Ficha Principal';
			$historial->fecha = $date;
			$historial->save();

			if($dataTesisImportar)
				return $respuesta;
			else
				return json_encode($respuesta);

		}
	}


public function guardar_tesis(){
		$data = Input::all();
		$fecha_actual = date('Ymd');
		$pruebaCotaExistente = "";
		$pruebaCotaExistente = DB::table('recursos')
				->where('recursos.id','!=',$data['idRecurso'])
				->where('ubicacion','=',$data['cota'])
				->select('recursos.*')->first();

		if(!$pruebaCotaExistente){
			$queryRecurso = DB::table('recursos')
				->where('recursos.id','=',$data['idRecurso'])
				->update(array( 			
							'ubicacion' => strtoupper(App::make('BuscadoresController')->convetir_string($data['cota'])),
					   //   $recurso->tipo_liter = $data['tipoLiter'];    
					        'paginas' => $data['colacion'],
					        'volumen' => (int)$data['volumen'], //publicaciones seriadas???????
					        'numeracion' => $data['numeracion'],
					        'fecha_iso' => $data['fechaEdicion'],
					        'fecha_pub' => $data['fecha_pub'], //yyyymmdd    
					        'isbn' => $data['isbn'],	        
					        'url' => $data['url'],
					        'impresion' => $data['impresion'],
					        'id_catalogador' => $data['idCatalogador'],
					        'datos_adicionales' => $data['datosAdicionales'],
					        'notas' => $data['notas'],
					        'resumen' => $data['resumen']
				 			)
						);
			$recurso = DB::table('recursos')
					->where('recursos.id','=',$data['idRecurso'])
					->select('recursos.*')->first();



			if($data['idAutor']!= ""){
				$queryAutor = DB::table('autores')
				->where('id','=',$data['idAutor'])
				->update(array('tipo_autor' => $data['tipoAutor'] , 'autor_salida' => $data['nombreAutor'], 'autor' => strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor']))));
				
				$queryAutor = DB::table('autores')
				->where('id','=',$data['idAutor'])->first();
				$idAutor = $queryAutor->id;		
			}
			else{
				$autor = new Autor;	
				$autor->autor = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor']));
				$autor->autor_salida = $data['nombreAutor'];
				$autor->tipo_autor = $data['tipoAutor'];
				$autor->cod_dir = '';//$data['tipoAutor']; ///??????????
				$autor->dir_electr = ''; //$data['tipoAutor'];  ///??????????
				$autor->fecha_nac_dec = ''; //$data['tipoAutor']; ///??????????
				$autor->save();
				$idAutor = $autor->id;	

				$recurso_autor = new RecursoAutor;	
				$recurso_autor->recurso_id = $recurso->id;
				$recurso_autor->ext_recurso_id =  $recurso->ext_id;
				$recurso_autor->cod_autor = $idAutor;
				$recurso_autor->tipo_doc = 'T';
				$recurso_autor->tipo_autor = 'P';
				$recurso_autor->portada = ""; 
				$recurso_autor->orden = 0;
				$recurso_autor->save();
			}
		

			if($data["idTitulo"]!= ""){
				$queryTitulo = DB::table('titulos')
				->where('id','=',$data['idTitulo'])
				->update(array('titulo_salida' => $data['nombreTitulo'], 'titulo' => strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']))));

				$queryTitulo = DB::table('titulos')
				->where('id','=',$data['idTitulo'])->first();
				$idTitulo = $queryTitulo->id;		

			}else{
				$titulo = new Titulo;	
				$titulo->caracter_orden = 0;
				$titulo->titulo_salida = $data['nombreTitulo'];
				$titulo->titulo =  strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']));
				$titulo->save();
				$idTitulo = $titulo->id;	

				$recurso_titulo = new RecursosTitulo;	
				$recurso_titulo->recurso_id = $recurso->id;
				$recurso_titulo->ext_recurso_id =  $recurso->ext_id;
				$recurso_titulo->titulo_id = $idTitulo;
				$recurso_titulo->tipo_tit = 'OP';
				$recurso_titulo->tipo_doc = 'T';
				$recurso_titulo->portada = ''; 
				$recurso_titulo->orden = 0;
				$recurso_titulo->save();	
			}

			$dependencia = $data['dependencia'];
			$queryDependencia = '';
			$queryRecursosDependencia = DB::table('recursos_dependencias') 
					->join('recursos','recursos.id','=', 'recursos_dependencias.recurso_id')
					->where('recursos_dependencias.recurso_id','=',$recurso->id)
					->select('recursos_dependencias.id')->first();

			if($dependencia != ''){ 
				if($data["idDependencia"] != ""){
					if($queryRecursosDependencia){
						$queryDependencia = DB::table('recursos_dependencias') 
						->join('recursos','recursos.id','=', 'recursos_dependencias.recurso_id')
						->where('recursos_dependencias.recurso_id','=',$recurso->id)
						->update(array('dependencia_id' =>$data["idDependencia"]));
					}else{
						$recurso_dependencia = new RecursosDependencia;	
						$recurso_dependencia->recurso_id = $recurso->id;
						$recurso_dependencia->ext_recurso_id =  $recurso->ext_id;
						$recurso_dependencia->dependencia_id = $data["idDependencia"];
						$recurso_dependencia->save();
					}
				}else{
					$dependencia = new Dependencia;	
					$dependencia->dependencia = strtoupper(App::make('BuscadoresController')->convetir_string($data['dependencia']));
					$dependencia->dependencia_salida = $data['dependencia'];
					$dependencia->siglas = '';
					$dependencia->save();
					$idDependencia = $dependencia->id;	

					if($queryRecursosDependencia){
						$queryDependencia = DB::table('recursos_dependencias') 
						->join('recursos','recursos.id','=', 'recursos_dependencias.recurso_id')
						->where('recursos_dependencias.recurso_id','=',$recurso->id)
						->update(array('dependencia_id' =>$idDependencia));
					}else{
						$recurso_dependencia = new RecursosDependencia;	
						$recurso_dependencia->recurso_id = $recurso->id;
						$recurso_dependencia->ext_recurso_id =  $recurso->ext_id;
						$recurso_dependencia->dependencia_id = $idDependencia;
						$recurso_dependencia->save();
					}

				}
			}else{
				$borrarRecursosDependencia = DB::table('recursos_dependencias') 
					->join('recursos','recursos.id','=', 'recursos_dependencias.recurso_id')
					->where('recursos_dependencias.recurso_id','=',$recurso->id)
					->delete();	
			}

			$disciplina = $data['disciplina'];
			$queryDisciplina = '';
			$queryRecursosDisciplina = '';
			if($disciplina != ''){ 
				$queryDisciplina = DB::table('disciplinas')
					->where('disciplina','=',  strtoupper(App::make('BuscadoresController')->convetir_string($data['disciplina'])))
					->select('disciplinas.*')->first();
				if($queryDisciplina){
					$idDisciplina = $queryDisciplina->id;	
				}else{
					$disciplina = new Disciplina;	
					$disciplina->disciplina = strtoupper(App::make('BuscadoresController')->convetir_string($data['disciplina']));
					$disciplina->disciplina_salida = $data['disciplina'];
					$disciplina->save();
					$idDisciplina = $disciplina->id;	
				}	

				$queryRecursosDisciplina = DB::table('recursos_disciplinas') 
					->join('recursos','recursos.id','=', 'recursos_disciplinas.recurso_id')
					->where('recursos_disciplinas.recurso_id','=',$recurso->id)
					->select('recursos_disciplinas.id')->first();

				if($queryRecursosDisciplina){
					$updateRecurso = DB::table('recursos_disciplinas')
					->where('recursos_disciplinas.id','=',$queryRecursosDisciplina->id)
					->update(array( 			
							'disciplina_id' => $idDisciplina)
					);	
				}else{
					$recurso_disciplina= new RecursosDisciplinas;	
					$recurso_disciplina->recurso_id = $recurso->id;
					$recurso_disciplina->ext_recurso_id =  $recurso->ext_id;
					$recurso_disciplina->disciplina_id = $idDisciplina;
					$recurso_disciplina->save();	
				}	
			}else{
				$queryRecursosDisciplina = DB::table('recursos_disciplinas') 
					->join('recursos','recursos.id','=', 'recursos_disciplinas.recurso_id')
					->where('recursos_disciplinas.recurso_id','=',$recurso->id)
					->select('recursos_disciplinas.id')->first();

				if($queryRecursosDisciplina){
					$updateRecurso = DB::table('recursos_disciplinas')
					->where('recursos_disciplinas.id','=',$queryRecursosDisciplina->id)
					->delete();	
				}
			}

			$tesis = DB::table('tesis')
				->where('recurso_id','=', $recurso->id)
				->update(array(
						'cod_inst' => (int)$data['institucion'],
						'nivel_id' => (int)$data['nivel'],
						'trabajo_id' => (int)$data['trabajo'],
						'grado_acad' => $data['gradoAcademico']
						));

				if($data['idTutorTesis'] != ''){
					$queryAutorT = DB::table('autores')
						->where('id','=', $data['idTutorTesis'])
						->update(array(
							'autor' => strtoupper(App::make('BuscadoresController')->convetir_string($data['tutorTesis'])),
							'autor_salida' => $data['tutorTesis'],
							'dir_electr' => $data['correoTutor']
							));

				}else if($data['tutorTesis'] != ''){
						$queryAutorT = DB::table('autores')
							->where('autor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['tutorTesis'])))
							->where('tipo_autor','=', 'P')
							->select('autores.*')->first();

					if(!$queryAutorT){
						$autor = new Autor;	
						$autor->autor = strtoupper(App::make('BuscadoresController')->convetir_string($data['tutorTesis']));
						$autor->autor_salida = $data['tutorTesis'];
						$autor->tipo_autor = 'P';
						$autor->cod_dir = '';//$data['tipoAutor']; ///??????????
						$autor->dir_electr = $data['correoTutor']; //$data['tipoAutor'];  ///??????????
						$autor->fecha_nac_dec = ''; //$data['tipoAutor']; ///??????????
						$autor->save();
						$idAutor = $autor->id;	

						$recurso_autor->recurso_id = $recurso->id;
						$recurso_autor->ext_recurso_id =  $recurso->ext_id;
						$recurso_autor->cod_autor = $idAutor;
						$recurso_autor->tipo_doc = 'T';
						$recurso_autor->tipo_autor = 'TA';
						$recurso_autor->portada = "0"; 
						$recurso_autor->orden = 0;
						$recurso_autor->save();
					}else{
						$recurso_autor = new RecursoAutor;	
						$recurso_autor->recurso_id = $recurso->id;
						$recurso_autor->ext_recurso_id =  $recurso->ext_id;
						$recurso_autor->cod_autor = $queryAutorT->id;
						$recurso_autor->tipo_doc = 'T';
						$recurso_autor->tipo_autor = 'TA';
						$recurso_autor->portada = "0"; 
						$recurso_autor->orden = 0;
						$recurso_autor->save();
					}
				}

			$biblioteca = DB::table('recursos_bibliotecas')
			->where('recursos_bibliotecas.recurso_id','=',$data['idRecurso'])
			->update(array('biblioteca_id' => $data["bibliotecaSelect"]));

			$ejemplar = DB::table('ejemplares')
            ->where('ejemplares.recurso_id','=', $data['idRecurso'])
            ->orderby('ejemplares.ejemplar', 'ASC')
            ->select('ejemplares.*')->first();

         	$ejemplar_update = DB::table('ejemplares')
            ->where('ejemplares.id','=', $ejemplar->id)
            ->update(array('doi' => $data["doi"],
				'doc_electronico' => $data["esElectronico"],
				'nombre_doc_electronico' => $data["nombreDocElectronico"]));

				date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data['idCatalogador'];
				$historial->recurso_id = $data['idRecurso'];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Modificar'; 
				$historial->modulo = 'Ficha Principal';
				$historial->fecha = $date;
				$historial->save();
		
				$respuesta = array("codigo" => "0");
				return json_encode($respuesta);
		}else{
			$respuesta = array("codigo" => "1");
				return json_encode($respuesta);
		}

	}

	public function eliminar_tesis(){
		$data = Input::all();
		$recurso = DB::table('recursos')
		->where('id','=',$data['idRecurso'])
		->update(array('ext_id' => 1));
		$respuesta = array("codigo" => "0");
		date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
		$historial->id_catalogador =  $data['idCatalogador']; 
		$historial->recurso_id = $data['idRecurso'];
		$historial->ext_recurso_id = '1';
		$historial->operacion = 'Eliminar'; 
		$historial->modulo = 'Ficha Principal';
		$historial->fecha = $date;
		$historial->save();
		return json_encode($respuesta);
 	}
}

