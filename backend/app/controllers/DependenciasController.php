<?php

class DependenciasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /dependencias
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return Dependencia::all();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /dependencias/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /dependencias
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /dependencias/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /dependencias/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /dependencias/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /dependencias/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function autocompletado_dependencias(){
		$data = Input::all();
		
		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = strtoupper(App::make('BuscadoresController')->convetir_string($data['data']));
		$this->split_data =  explode(' ',$aux_data);
		$datos_dependencias = [];		  
		$query_institucion = DB::table('dependencias')
						->where('dependencias.dependencia', '=', $aux_data)
						->select('dependencias.*')
						->paginate(15);

		foreach ($query_institucion as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "dependencia_salida"=> $row->dependencia_salida, "dependencia"=> $row->dependencia);
			array_push($datos_dependencias, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_dependencias), SORT_REGULAR);
		$datos_dependencias = [];

		$query_institucion = DB::table('dependencias')
						->where('dependencias.dependencia', 'like', $aux_data.'%' )
						->select('dependencias.*')
						->paginate(15);

		foreach ($query_institucion as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "dependencia_salida"=> $row->dependencia_salida, "dependencia"=> $row->dependencia);
			array_push($datos_dependencias, $aux);
		}


		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_dependencias), SORT_REGULAR);
		$datos_dependencias = [];

		$query_institucion = DB::table('dependencias')
						->where('dependencias.dependencia', 'like', '%'.$aux_data.'%' )
						->select('dependencias.*')
						->paginate(15);

		foreach ($query_institucion as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "dependencia_salida"=> $row->dependencia_salida, "dependencia"=> $row->dependencia);
			array_push($datos_dependencias, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_dependencias), SORT_REGULAR);
		$datos_dependencias = [];
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_dependencias), SORT_REGULAR);
		return $datos_busqueda;
	}

}