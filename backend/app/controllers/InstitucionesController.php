<?php

class institucionesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /intituciones
	 *
	 * @return Response
	 */
	public function index()
	{
		$query_institucion = DB::table('instituciones')
						->select('instituciones.*')
						->paginate(15);
		return $query_institucion;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /intituciones/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /intituciones
	 *
	 * @return Response
	 */
	public function store($dataInstitucion = null){
		
		if($dataInstitucion){
			$data = $dataInstitucion;
		}else{
			$data = Input::all();
		}

		$query_institucion = DB::table('instituciones')
						->where('instituciones.institucion', '=',strtoupper(App::make('BuscadoresController')->convetir_string($data['nombre'])))
						->first();

		if($query_institucion){
			$institucion = institucion::find($query_institucion->id);
			$institucion->institucion_salida = $data['nombre'];
			$institucion->url = $data['url'];
			$institucion->pais_id = $data['pais'];
			$institucion->institucion = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombre']));
			$institucion->save();
			$respuesta = array("codigo" => "0");

		}else{
			$institucion = new institucion;
			$institucion->institucion_salida = $data['nombre'];
			$institucion->url = $data['url'];
			$institucion->pais_id = $data['pais'];
			$institucion->institucion = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombre']));
			$institucion->save();
			$respuesta = array("codigo" => "0", "id" => $institucion->id);
		}
		return $respuesta;
	}

	/**
	 * Display the specified resource.
	 * GET /intituciones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$query_institucion = DB::table('instituciones')
						->join('paises','paises.id','=', 'instituciones.pais_id')
						->where('instituciones.id', '=',$id)
						->select('instituciones.*','paises.pais')
						->first();
		if($query_institucion){
			$respuesta = array("codigo" => "0", "datos" => $query_institucion);
		}else{
			$respuesta = array("codigo" => "1");
		}
		return $respuesta;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /intituciones/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /intituciones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$query_institucion = DB::table('instituciones')
						->where('instituciones.id', '=',$id)
						->first();

		$institucion = Institucion::find($query_institucion->id);
		$institucion->institucion_salida = $data['nombres'];
		$institucion->url = $data['url'];
		$institucion->pais_id = $data['pais'];
		$institucion->institucion = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombre']));
		$institucion->save();

		$respuesta = array("codigo" => "0");
		return $respuesta;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /intituciones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$query_institucion = DB::table('instituciones')
					->where('instituciones.id', '=',$id)
					->delete();
			$respuesta = array("codigo" => "0");
			return $respuesta;
	}

	public function buscarInstitucion()
	{
		if(Input::get('coincidencia')){
			$coincidencia = Input::get('coincidencia');
			$coincidencia = strtoupper(App::make('BuscadoresController')->convetir_string($coincidencia));
		}
		$query_institucion = DB::table('instituciones')
						->where('instituciones.institucion', 'like', '%'.$coincidencia.'%' )
						->select('instituciones.*')
						->paginate(15);
		return $query_institucion;
	}


	public function autocompletado_institucion(){
		$data = Input::all();
		// $data;
		$datos_busqueda = [];
		$this->split_data = [];
		$aux_data = strtoupper(App::make('BuscadoresController')->convetir_string($data['data']));
		$this->split_data =  explode(' ',$aux_data);
		$datos_titulos = [];		  
		$query_institucion = DB::table('instituciones')
						->where('instituciones.institucion', '=', $aux_data)
						->select('instituciones.*')
						->paginate(15);

		foreach ($query_institucion as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "institucion_salida"=> $row->institucion_salida, "institucion"=> $row->institucion, "url"=> $row->url, "pais_id"=> $row->pais_id);
			array_push($datos_titulos, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];

		$query_institucion = DB::table('instituciones')
						->where('instituciones.institucion', 'like', $aux_data.'%' )
						->select('instituciones.*')
						->paginate(15);

		foreach ($query_institucion as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "institucion_salida"=> $row->institucion_salida, "institucion"=> $row->institucion, "url"=> $row->url, "pais_id"=> $row->pais_id);
			array_push($datos_titulos, $aux);
		}

		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];

		$query_institucion = DB::table('instituciones')
						->where('instituciones.institucion', 'like', '%'.$aux_data.'%' )
						->select('instituciones.*')
						->paginate(15);
		foreach ($query_institucion as $row) {
			$aux = [];
			$aux = array("id" => $row->id, "institucion_salida"=> $row->institucion_salida, "institucion"=> $row->institucion, "url"=> $row->url, "pais_id"=> $row->pais_id);
			array_push($datos_titulos, $aux);
		}
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		$datos_titulos = [];
		$datos_busqueda = array_unique(array_merge($datos_busqueda,$datos_titulos), SORT_REGULAR);
		return $datos_busqueda;
	}	
}