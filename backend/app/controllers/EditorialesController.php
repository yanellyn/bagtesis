<?php

class EditorialesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /editoriales
	 *
	 * @return Response
	 */
	public function index()
	{
	//	return  Editorial::paginate(100);
	}

	


	/**
	 * Show the form for creating a new resource.
	 * GET /editoriales/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /editoriales
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /editoriales/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		return Recurso::find($id)->editorial;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /editoriales/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /editoriales/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /editoriales/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function obtener_editoriales_recurso(){
		$data = Input::all();
		$recurso_editoriales = DB::table('recursos_editoriales')
								   ->join('editoriales', 'editoriales.id','=','recursos_editoriales.editorial_id')
								   ->join('paises', 'paises.id','=','editoriales.pais_id')
								   ->orderBy('recursos_editoriales.orden','ASC')
								   ->where('recursos_editoriales.recurso_id','=',$data['idRecurso'])
								   ->select('editoriales.*',  'paises.pais' , 'recursos_editoriales.orden')
								   ->get();
		return $recurso_editoriales;
	}

	public function obtener_editoriales_sistema(){
		$recurso_editoriales = DB::table('editoriales')
								   ->join('paises', 'paises.id','=','editoriales.pais_id')
								   ->orderBy('editoriales.editorial','ASC')
								   ->select('editoriales.*', 'paises.pais')
								   ->paginate(15);
		return $recurso_editoriales;
	}

	public function agregar_editoriales($dataEditorial = null){
		
		if($dataEditorial){
			$data = $dataEditorial;
		}else{
			$data = Input::all();
		}

		$queryEditorial = DB::table('editoriales')
				->where('editoriales.editorial','=',strtoupper(App::make('BuscadoresController')->convetir_string($data['editorial'])))
				->where('editoriales.ciudad','=',$data['ciudad'])
				->where('editoriales.fabricante','=',$data['fabricante'])
				->select('editoriales.*')->first();


		if($queryEditorial){
			$idEditorial = $queryEditorial->id;	
			$queryRecursoEditorial = DB::table('recursos_editoriales')
			->where('recursos_editoriales.recurso_id','=',$data["idRecurso"])
			->where('recursos_editoriales.editorial_id','=',$idEditorial)
			->select('recursos_editoriales.*')->first();

			if(!$queryRecursoEditorial){
				$queryOrden = DB::table('recursos_editoriales')
				->where('recursos_editoriales.recurso_id','=',$data["idRecurso"])
				->select(DB::raw('max(recursos_editoriales.orden) as recursos_editoriales_orden'))->first();

				$orden_actual= $queryOrden->recursos_editoriales_orden + 1;
				$recurso_editorial = new RecursosEditoriales;	
					$recurso_editorial->recurso_id = $data['idRecurso'];
					$recurso_editorial->ext_recurso_id = 0;
					$recurso_editorial->editorial_id = $idEditorial;
					$recurso_editorial->tipo_doc =  $data['tipoDoc'];
					$recurso_editorial->orden = $orden_actual;
					$recurso_editorial->save();	
					$respuesta = array("codigo" => "0");
			}else{
				$respuesta = array("codigo" => "1");
			}
			return json_encode($respuesta);
		}else{
			
			$editorial = new Editorial;	
			$editorial->editorial =  strtoupper(App::make('BuscadoresController')->convetir_string($data['editorial'])); 
			$editorial->ciudad = $data['ciudad'];
			$editorial->pais_id = $data['idPais'];
			$editorial->editorial_salida = $data["editorial"];
			$editorial->fabricante = $data['fabricante'];
			$editorial->ciudad_fabricacion = $data['ciudadFabricante'];
			$editorial->fecha_manufactura = $data['fecha'];
			$editorial->save();
			$idEditorial = $editorial->id;	
			
			$queryRecursoEditorial = DB::table('recursos_editoriales')
			->where('recursos_editoriales.recurso_id','=',$data["idRecurso"])
			->select(DB::raw('max(recursos_editoriales.orden) as recursos_editoriales_orden'))->first();
			$orden_actual= $queryRecursoEditorial->recursos_editoriales_orden + 1;
			$recurso_editorial = new RecursosEditoriales;	
			$recurso_editorial->recurso_id = $data['idRecurso'];
			$recurso_editorial->ext_recurso_id = 0;
			$recurso_editorial->editorial_id = $idEditorial;
			$recurso_editorial->tipo_doc =  $data['tipoDoc'];
			$recurso_editorial->orden = $orden_actual;
			$recurso_editorial->save();	
			$respuesta = array("codigo" => "0");
		}
		return json_encode($respuesta);
	}

	public function guardar_editoriales(){
		$data = Input::all();
		$count = 0;
		$queryRecursoEditorial = DB::table('recursos_editoriales')
			->where('recursos_editoriales.recurso_id','=',$data["idRecurso"])
			->delete();

		$count = 0;
		$editoriales = $data["editoriales"];
		foreach ($editoriales as $row) {
			$count++;
			$recurso_editorial = new RecursosEditoriales;	
			$recurso_editorial->recurso_id = $data["idRecurso"];
			$recurso_editorial->ext_recurso_id = 0;
			$recurso_editorial->editorial_id =  $row["id"];
			$recurso_editorial->tipo_doc = $data['tipoDoc'];
			$recurso_editorial->orden = $count;
			$recurso_editorial->save();	
		}

		if($data["idEditorial"] != 0){
			$queryEditorial = DB::table('editoriales')
				->where('editoriales.id','=',$data['idEditorial'])
				->update(array( 			
					'editorial' =>  strtoupper(App::make('BuscadoresController')->convetir_string($data['editorial'])),
					'ciudad' => $data['ciudad'],
					'pais_id' => $data['idPais'],
					'editorial_salida' => $data["editorial"],
					'fabricante' => $data['fabricante'],
					'ciudad_fabricacion' => $data['ciudadFabricante'],
					'fecha_manufactura' => $data['fecha']
					));	
			$queryEditorial = DB::table('editoriales')
				->where('editoriales.id','=',$data['idEditorial'])
				->join('paises', 'paises.id','=','editoriales.pais_id')
				->select('editoriales.*', 'paises.pais')
				->first();

			$respuesta = array("codigo" => "0", "editorial" => $queryEditorial);
		}else{
			$respuesta = array("codigo" => "0", "editorial" => "");
		}
		return json_encode($respuesta);

	}
/*
	public function guardar_editoriales_sistema(){
		$data = Input::all();
		$queryEditorial = DB::table('editoriales')
				->where('editoriales.id','=',$data['idEditorial'])
				->update(array( 			
					'editorial' =>  strtoupper($data['editorial']),
					'ciudad' => $data['ciudad'],
					'pais_id' => $data['idPais'],
					'editorial_salida' => $data["editorial"],
					'fabricante' => $data['fabricante'],
					'ciudad_fabricacion' => $data['ciudadFabricante'],
					'fecha_manufactura' => $data['fecha']
					));


		$queryEditorial = DB::table('editoriales')
				->where('editoriales.id','=',$data['idEditorial'])
				->join('paises', 'paises.id','=','editoriales.pais_id')
				->select('editoriales.*', 'paises.pais')
				->first();

		$respuesta = array("codigo" => "0", "editorial" =>$queryEditorial);
		return json_encode($respuesta);
	}
*/
	public function eliminar_editoriales(){
		$data = Input::all();
		$queryRecursoEditorial = DB::table('recursos_editoriales')
			->where('recursos_editoriales.recurso_id','=',$data["idRecurso"])
			->where('recursos_editoriales.editorial_id','=',$data["idEditorial"])
			->delete();

		$queryRecursoEditorial = DB::table('recursos_editoriales')
			->where('recursos_editoriales.editorial_id','=',$data["idEditorial"])
			->get();

		if($queryRecursoEditorial ){
			$respuesta = array("codigo" => "1");		
		}else{
			$queryEditorial = DB::table('editoriales')
				->where('editoriales.id','=',$data["idEditorial"])
				->delete();
			$respuesta = array("codigo" => "0");
		}
		return json_encode($respuesta);
		
	}

/*
	public function guardar_editoriales(){
		$data = Input::all();
		$count = 0;
		$queryEditorial = DB::table('editoriales')
				->where('editoriales.id','=',$data['idEditorial'])
				->update(array('ciudad' => $data['ciudad'], 'pais_id' => $data['pais_id'], 'editorial_salida' => $data['editorial'], 'fabricante' => $data['fabricante'], 'ciudad_fabricacion' => $data['ciudadFabricante'], 'fecha_manufactura' => $data['fecha']));

		$ordenes = $data["orden_editoriales"];
		foreach ($ordenes as $row) {
			$count++;
			$recurso_editorial = DB::table('recurso_editorial')
			->where('recurso_id','=',$data['idRecurso'])
			->where('editorial_id','=',$row["id"])
			->update(array('orden' => $count));
		}

		$respuesta = array("codigo" => "0");
		return json_encode($respuesta);
		}

	
	}

	public function eliminar_editoriales(){
		$data = Input::all();
		$queryRecursoEditorial = DB::table('recursos_editoriales')
			->where('recursos_editoriales.recurso_id','=',$data["idRecurso"])
			->where('recursos_editoriales.editorial_id','=',$data["idEditorial"])
			->delete();
		$respuesta = array("codigo" => "0");
		return json_encode($respuesta);
		}	
	}
*/
}