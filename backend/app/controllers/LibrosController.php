<?php

class LibrosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /libros
	 *
	 * @return Response
	 */
	public function index()
	{
		//

	//	$libros = Libro::take(10)->skip(0)->get();
		$libros = Libro::all()->paginate();
		return $libros;

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /libros/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /libros
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /libros/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /libros/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /libros/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /libros/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function agregar_libro($dataLibroImportar = null){
		
		if($dataLibroImportar){
			$data = $dataLibroImportar;
		}else{
			$data = Input::all();
		}
		$fecha_actual = date('Ymd');
		$aux = 0;
	//	echo $fecha_actual;
		$codigoRespuesta= "0"; //recurso agregado 1 recurso existe, 2 error agregando el recurso

		if($data["bibliotecaSelect"] != 1){ // si no es de la bag utiliza T1
			$aux_id ='T1'; 
		}else{
			$aux_id ='T0'; 
		}
	    $last_id_recurso = "";
	    $query_id_recurso = "";
	  

	    $query_id_recurso = DB::table('recursos')
	                      ->where('id','like',$aux_id.'%')
	                      ->select(DB::raw('max(recursos.id) as recursos_id'))->first();

	    $id_recurso_actual= $query_id_recurso->recursos_id;
	    $int_id_recurso = filter_var($id_recurso_actual, FILTER_SANITIZE_NUMBER_INT);
	    $id_recurso_nuevo = $aux_id.(string)($int_id_recurso + 1);


	    //Obtener Jerarquia para nuevo recurso
		$query_jerarquia = DB::table('recursos')
		                  ->select(DB::raw('max(recursos.jerarquia) as max_jerarquia'))->first();

		$jerarquia_actual= $query_jerarquia->max_jerarquia;

		//if ($tipo_recurso == 'M' || $tipo_recurso == 'T'){
			$jerarquia_nuevo = $jerarquia_actual + 1;
		//}else{
		//	$jerarquia_nuevo = $jerarquia_actual;
		//}


		
		$queryRecurso = DB::table('recursos')
				->where('recursos.ubicacion','=',strtoupper(App::make('BuscadoresController')->convetir_string($data['cota'])))
				->select('recursos.*')->first();
 

		$queryAutor = DB::table('autores')
				->where('autor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor'])))
				->where('tipo_autor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['tipoAutor'])))
				->select('autores.*')->first();
				
		$queryTitulo = DB::table('titulos')
				->where('titulo','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo'])))
				->select('titulos.*')->first();
				
		if($queryRecurso){
			if($queryRecurso->ext_id == 0){
				$codigoRespuesta= "1";
				$respuesta = array("codigo" => "1", "id_recurso"=> "");
				if($dataLibroImportar)
					return $respuesta;
				else
					return json_encode($respuesta);
			}else{

				$queryRecurso = DB::table('recursos_autores')
				->where('recursos_autores.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_titulos')
				->where('recursos_titulos.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_editoriales')
				->where('recursos_editoriales.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_descriptores')
				->where('recursos_descriptores.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos_bibliotecas')
				->where('recursos_bibliotecas.recurso_id','=', $queryRecurso->id)
				->delete();

				$queryRecurso = DB::table('recursos')
				->where('recursos.id','=', $queryRecurso->id)
				->delete();
				$aux = 1;
			}
		}
		
		if(!$queryRecurso || $aux == 1){
			$recurso = new Recurso;	
			$recurso->id = $id_recurso_nuevo;
			$recurso->ext_id = 0;//
			$recurso->ubicacion = strtoupper(App::make('BuscadoresController')->convetir_string($data['cota']));
			$recurso->jerarquia = $jerarquia_nuevo;
	        $recurso->tipo_liter = $data['tipoLiter'];
	        $recurso->nivel_reg = 'm';
	        $recurso->paginas = $data['colacion'];
	        $recurso->volumen = (int)$data['volumen']; //publicaciones seriadas???????
	        $recurso->numeracion = $data['numeracion'];
	        $recurso->edicion = $data['edicion'];
	        $recurso->fecha_iso = $data["fechaEdicion"]; 
	        $recurso->fecha_pub =  $data['fecha_pub']; //yyyy
	        $recurso->isbn = $data['isbn'];
	        $recurso->url = $data['url'];
	        $recurso->impresion = $data['impresion'];
	        $recurso->fecha_catalogacion =  $fecha_actual;
	        $recurso->id_catalogador = $data['idCatalogador'];
	        $recurso->datos_adicionales = $data['datosAdicionales'];
	        $recurso->notas = $data['notas'];
	        $recurso->resumen = $data['resumen'];
	 		$recurso->save();

	 		if($queryAutor){
				$idAutor = $queryAutor->id;	
			}else{
				$autor = new Autor;	
				$autor->autor = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor']));
				$autor->autor_salida = $data['nombreAutor'];
				$autor->tipo_autor = $data['tipoAutor'];
				$autor->cod_dir = '';//$data['tipoAutor']; ///??????????
				$autor->dir_electr = ''; //$data['tipoAutor'];  ///??????????
				$autor->fecha_nac_dec = ''; //$data['tipoAutor']; ///??????????
				$autor->save();
				$idAutor = $autor->id;	
			}
			
			$recurso_autor = new RecursoAutor;	
				$recurso_autor->recurso_id = $recurso->id;
				$recurso_autor->ext_recurso_id =  $recurso->ext_id;
				$recurso_autor->cod_autor = $idAutor;
				$recurso_autor->tipo_doc = 'M';
				$recurso_autor->tipo_autor = 'P';
				$recurso_autor->portada = "1"; 
				$recurso_autor->orden = 0;
				$recurso_autor->save();

			$recurso_biblioteca = new RecursosBiblioteca;	
				$recurso_biblioteca->recurso_id = $recurso->id;
				$recurso_biblioteca->ext_recurso_id =  $recurso->ext_id;
				$recurso_biblioteca->biblioteca_id = $data["bibliotecaSelect"];
				$recurso_biblioteca->save();

			if($queryTitulo){
				$idTitulo = $queryTitulo->id;	
			}else{
				$titulo = new Titulo;	
				$titulo->caracter_orden = 0;
				$titulo->titulo_salida = $data['nombreTitulo'];
				$titulo->titulo =  strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']));
				$titulo->save();
				$idTitulo = $titulo->id;		
			}

			$recurso_titulo = new RecursosTitulo;	
				$recurso_titulo->recurso_id = $recurso->id;
				$recurso_titulo->ext_recurso_id =  $recurso->ext_id;
				$recurso_titulo->titulo_id = $idTitulo;
				$recurso_titulo->tipo_tit = 'OP';
				$recurso_titulo->tipo_doc = 'M';
				$recurso_titulo->portada = ''; 
				$recurso_titulo->orden = 0;
				$recurso_titulo->save();

			$libro = new Libro;	
				$libro->ext_recurso_id = $recurso->ext_id;
				$libro->recurso_id = $recurso->id;
				$libro->isbn = $data['isbn'];
				$libro->save();

			$aux_id = "T0";
			$query_id_ejemplar = DB::table('ejemplares')
	                      ->where('id','like',$aux_id.'%')
	                      ->select(DB::raw('max(ejemplares.id) as ejemplar_id'))->first();
	   		$id_ejemplar_actual = $query_id_ejemplar->ejemplar_id;
	    	$int_id_ejemplar = filter_var($id_ejemplar_actual, FILTER_SANITIZE_NUMBER_INT);
	    	$id_ejemplar_nuevo = $aux_id.(string)($int_id_ejemplar + 1);

			$ejemplar = new Ejemplar;	
				$ejemplar->ext_recurso_id = $recurso->ext_id;
				$ejemplar->recurso_id = $recurso->id;
				$ejemplar->tipo_liter = $data['tipoLiter'];
				$ejemplar->id = $id_ejemplar_nuevo;
				$ejemplar->ejemplar = "0001";
				$ejemplar->id = $id_ejemplar_nuevo;
				$ejemplar->doi = $data['doi'];
				$ejemplar->doc_electronico = $data['esElectronico'];
				$ejemplar->nombre_doc_electronico = $data['nombreDocElectronico'];
				$ejemplar->save();

			date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data['idCatalogador'];
			$historial->recurso_id = $recurso->id;
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Agregar'; 
			$historial->modulo = 'Ficha Principal';
			$historial->fecha = $date;
			$historial->save();

			$respuesta = array("codigo" => "0" , "id_recurso"=> $recurso->id, "id_autor"=> $idAutor, "id_titulo"=>  $idTitulo);
			if($dataLibroImportar)
				return $respuesta;
			else
				return json_encode($respuesta);
		}
	}


	public function guardar_libro(){
		$data = Input::all();
		$fecha_actual = date('Ymd');
		$pruebaCotaExistente = "";
		$pruebaCotaExistente = DB::table('recursos')
								->where('recursos.id','!=',$data['idRecurso'])
								->where('ubicacion','=',$data['cota'])
								->select('recursos.*')->first();

		if(!$pruebaCotaExistente){
			$queryRecurso = DB::table('recursos')
					->where('recursos.id','=',$data['idRecurso'])
					->update(array( 			
								'ubicacion' => strtoupper(App::make('BuscadoresController')->convetir_string($data['cota'])),
						   //   $recurso->tipo_liter = $data['tipoLiter'];    
						        'paginas' => $data['colacion'],
						        'volumen' => (int)$data['volumen'], //publicaciones seriadas???????
						        'numeracion' => $data['numeracion'],
						        'edicion' => $data['edicion'],
						        'fecha_iso' => $data['fechaEdicion'],
						        'fecha_pub' => $data['fecha_pub'], //yyyymmdd    
						        'isbn' => $data['isbn'],	        
						        'url' => $data['url'],
						        'impresion' => $data['impresion'],
						       // $recurso->fecha_catalogacion =  $fecha_actual;
						       // 'id_catalogador' => $data['idCatalogador'],
						        'datos_adicionales' => $data['datosAdicionales'],
						        'notas' => $data['notas'],
						        'resumen' => $data['resumen']
					 			)
							);
			$recurso = DB::table('recursos')
					->where('recursos.id','=',$data['idRecurso'])
					->select('recursos.*')->first();

			if($data['idAutor']!= ""){
				$queryAutor = DB::table('autores')
				->where('id','=',$data['idAutor'])
				->update(array('tipo_autor' => $data['tipoAutor'] , 'autor_salida' => $data['nombreAutor'], 'autor' => strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor']))));
				
				$queryAutor = DB::table('autores')
				->where('id','=',$data['idAutor'])->first();
				$idAutor = $queryAutor->id;		
			}
			else{
				$autor = new Autor;	
				$autor->autor = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor']));
				$autor->autor_salida = $data['nombreAutor'];
				$autor->tipo_autor = $data['tipoAutor'];
				$autor->cod_dir = '';//$data['tipoAutor']; ///??????????
				$autor->dir_electr = ''; //$data['tipoAutor'];  ///??????????
				$autor->fecha_nac_dec = ''; //$data['tipoAutor']; ///??????????
				$autor->save();
				$idAutor = $autor->id;	

				$recurso_autor = new RecursoAutor;	
				$recurso_autor->recurso_id = $recurso->id;
				$recurso_autor->ext_recurso_id =  $recurso->ext_id;
				$recurso_autor->cod_autor = $idAutor;
				$recurso_autor->tipo_doc = 'M';
				$recurso_autor->tipo_autor = 'P';
				$recurso_autor->portada = ""; 
				$recurso_autor->orden = 0;
				$recurso_autor->save();

			}
		

			if($data["idTitulo"]!= ""){
				$queryTitulo = DB::table('titulos')
				->where('id','=',$data['idTitulo'])
				->update(array('titulo_salida' => $data['nombreTitulo'], 'titulo' => strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']))));

				$queryTitulo = DB::table('titulos')
				->where('id','=',$data['idTitulo'])->first();
				$idTitulo = $queryTitulo->id;		

				
			}else{
				
				$titulo = new Titulo;	
				$titulo->caracter_orden = 0;
				$titulo->titulo_salida = $data['nombreTitulo'];
				$titulo->titulo =  strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']));
				$titulo->save();
				$idTitulo = $titulo->id;	

				$recurso_titulo = new RecursosTitulo;	
				$recurso_titulo->recurso_id = $recurso->id;
				$recurso_titulo->ext_recurso_id =  $recurso->ext_id;
				$recurso_titulo->titulo_id = $idTitulo;
				$recurso_titulo->tipo_tit = 'OP';
				$recurso_titulo->tipo_doc = 'M';
				$recurso_titulo->portada = ''; 
				$recurso_titulo->orden = 0;
				$recurso_titulo->save();	
			}

			$libro = DB::table('libros')
				->where('recurso_id','=', $recurso->id)
				->update(array('isbn' => $data['isbn']));

			$biblioteca = DB::table('recursos_bibliotecas')
			->where('recursos_bibliotecas.recurso_id','=',$data['idRecurso'])
			->update(array('biblioteca_id' => $data["bibliotecaSelect"]));

			$ejemplar = DB::table('ejemplares')
	            ->where('ejemplares.recurso_id','=', $data['idRecurso'])
	            ->orderby('ejemplares.ejemplar', 'ASC')
	            ->select('ejemplares.*')->first();

	         $ejemplar_update = DB::table('ejemplares')
	            ->where('ejemplares.id','=', $ejemplar->id)
	            ->update(array('doi' => $data["doi"],
					'doc_electronico' => $data["esElectronico"],
					'nombre_doc_electronico' => $data["nombreDocElectronico"]));

			date_default_timezone_set('America/Caracas');
				$date = date('YmdHis', time());
				$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data['idCatalogador'];
			$historial->recurso_id = $data['idRecurso'];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Modificar'; 
			$historial->modulo = 'Ficha Principal';
			$historial->fecha = $date;
			$historial->save();
			$respuesta = array("codigo" => "0");
			return json_encode($respuesta);
		}else{
			$respuesta = array("codigo" => "1");
			return json_encode($respuesta);
		}
	}

	public function eliminar_libro(){

		$data = Input::all();

		$recurso = DB::table('recursos')
		->where('id','=',$data['idRecurso'])
		->update(array('ext_id' => 1));

		date_default_timezone_set('America/Caracas');
		$historial = new HistorialCatalogacion;
		$date = date('YmdHis', time());
		$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
		$historial->recurso_id = $data['idRecurso'];
		$historial->ext_recurso_id = '1';
		$historial->operacion = 'Eliminar'; 
		$historial->modulo = 'Ficha Principal';
		$historial->fecha = $date;
		$historial->save();
		
		$respuesta = array("codigo" => "0");
		return json_encode($respuesta);
 
/*
		$data = Input::all();

		$recurso = DB::table('recursos')
				->where('id','=',$data['idRecurso'])
				->update(array('ext_id' => 1));

		/*$libro = DB::table('libros')
				->where('recurso_id','=',$data['idRecurso'])
				->update(array('ext_recurso_id' => 1));

		

		$recursosAutores = DB::table('recursos_autores')
				->where('recurso_id','=', $data['idRecurso'])
				->update(array('ext_recurso_id' => 1));

		$recursosTitulos = DB::table('recursos_titulos')
				->where('recurso_id','=', $data['idRecurso'])
				->update(array('ext_recurso_id' => 1));


		/*$libro = DB::table('libros')
				->where('recurso_id','=', $data['idRecurso'])->delete();
				return;
		$recursosAutores = DB::table('recursos_autores')
				->where('recurso_id','=', $data['idRecurso'])->delete();

		$recursosTitulos = DB::table('recursos_titulos')
				->where('recurso_id','=', $data['idRecurso'])->delete();
		
		$recurso = DB::table('recursos')
				->where('recursos.id','=',$data['idRecurso'])->delete();
		
		$respuesta = array("codigo" => "0");
		return json_encode($respuesta);
		*/
	}
	 
	public function buscar_codigo_barra($codigoBarra){

		$recurso = DB::table('ejemplares')
			->join('recursos','recursos.id','=','ejemplares.recurso_id')
			->where('ejemplares.id','=',$codigoBarra)
			->where('ext_recurso_id','=',0)
			->select('ejemplares.recurso_id', 'ejemplares.tipo_liter','recursos.nivel_reg','recursos.jerarquia')
			->first();

		if($recurso){
			if($recurso->tipo_liter == 'S' && $recurso->nivel_reg != 's'){
			$recurso_s = DB::table('recursos')
					->where('jerarquia','=',$recurso->jerarquia)
					->where('nivel_reg','=','s')
					->select('id as recurso_id', 'tipo_liter','nivel_reg','jerarquia')
					->first();

			$recurso = $recurso_s;
			}
			$respuesta = array("codigo" => "0", "recurso"=> $recurso);
			return $respuesta;
		}else{
			$respuesta = array("codigo" => "1", "recurso"=> "");
			return $respuesta;
		}

		

	}
}