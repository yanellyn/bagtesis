<?php

class TipodescsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /tipodescs
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return Tipodesc::all();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tipodescs/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tipodescs
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /tipodescs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tipodescs/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tipodescs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tipodescs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function tipo_descriptores()
	{
		//
		return Tipodesc::where('id', 'not like', 'M-650-%')->orderBy('descrip_desc')->get();

	}

}