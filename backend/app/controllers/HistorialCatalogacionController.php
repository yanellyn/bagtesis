<?php

class HistorialCatalogacionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function obtenerCatalogadores()
	{
		$catalogadores =  DB::table('usuarios')
								   ->where('tipo_usp','=', 'Administrati')
								   ->orderBy('nombres', 'ASC')
								   ->select('id','nombres','apellidos')
								   ->get();

		return $catalogadores;
	}

	public function obtenerSalas()
	{
		$salas =  DB::table('descriptores')
								   ->where('tipo','=', 'S')
								   ->select('descriptor')
								   ->get();
								   
		return $salas;
	}



	public function buscadorMateriaTematicaTypeahead()
	{
	    $data = Input::all();

	    $datos_busqueda = [];
	    $this->split_data = [];
	    $aux_data = strtoupper(App::make('BuscadoresController')->convetir_string($data['data']));
	    $this->split_data =  explode(' ',$aux_data);
	    $busqueda_materia = [];     
	    $busqueda_materia = DB::table('descriptores')
	    			  ->where('tipo','like','%M-650-A%')
	                  ->where('descriptor','=',$aux_data)
	                  ->groupBy('descriptor')
	                  ->select('descriptor', DB::raw('count(descriptor) as num_recursos'))
	                  ->paginate(15);
	    $datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materia->lists('descriptor')));
	    
	    $busqueda_materia = DB::table('descriptores')
	    			  ->where('tipo','like','%M-650-A%')
	                  ->where('descriptor','like',$aux_data.'%')
	                  ->groupBy('descriptor')
	                  ->select('descriptor', DB::raw('count(descriptor) as num_recursos'))
	                  ->paginate(15);
	    $datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materia->lists('descriptor')));

	    $busqueda_materia = DB::table('descriptores')
	    			  ->where('tipo','like','%M-650-A%')
	                  ->where('descriptor','like','%'.$aux_data.'%')
	                  ->groupBy('descriptor')
	                  ->select('descriptor', DB::raw('count(descriptor) as num_recursos'))
	                  ->paginate(15);	  
	    $datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materia->lists('descriptor')));

	    $busqueda_materia = DB::table('descriptores')
	    			  ->where('tipo','like','%M-650-A%')
	                  ->where(function($query){
	                    foreach ($this->split_data as $aux) {
	                      $query->where('descriptor', 'like', '%'.$aux.'%');
	                    }
	                  })
	                  ->groupBy('descriptor')
	                  ->select('descriptor', DB::raw('count(descriptor) as num_recursos'))
	                  ->paginate(15);
	    $datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_materia->lists('descriptor')));
	    
	    $datos_busqueda = array_unique($datos_busqueda);
	    return $datos_busqueda;
	}

	public function generarReporteCatalogacion()
	{
		$data = Input::all();
		$datos_busqueda = [];
        $myArray = [];

	    $selectedTipoRecurso = $data["selectedTipoRecurso"];
	    $selectedTipoOperacion = $data["selectedTipoOperacion"];
	    $selectedCatalogador = $data["selectedCatalogador"];
	    $materiaTematica = $data["materiaTematica"];
	    $selectedSalaRecurso = $data["selectedSalaRecurso"];
	    $fechaInicio = $data["fechaInicio"];
	    $fechaFin = $data["fechaFin"];
	    $tipoReporte = $data["tipoReporte"];


	    if ($fechaInicio != "")
	    {
	    	$fechaInicio = date('YmdHis', strtotime($fechaInicio));
		}else{

			$fechaInicio = '01-01-2000';
			$fechaInicio = date('YmdHis:', strtotime($fechaInicio));

		}

	    if ($fechaFin != "")
	    {
	    	$fechaFin = date('YmdHis', strtotime($fechaFin));
	    }else{
	    	date_default_timezone_set('America/Caracas');
			$fechaFin = date_default_timezone_get();
			$fechaFin = date('YmdHis', strtotime($fechaFin));

		}

    	$busqueda_salas= DB::raw('(SELECT "r"."id" "recurso_id", string_agg(DISTINCT("d"."descriptor"),\', \') "sala"
		FROM historial_catalogacion sub_hc, descriptores d, recursos_descriptores rc, recursos r 
		WHERE "r"."id" = "sub_hc"."recurso_id"
			AND "rc"."recurso_id" = "r"."id"
			AND "rc"."descriptor_id" = "d"."id"
			AND "d"."tipo" = \'S\'
			AND "d"."descriptor" like \'%'.strtoupper($selectedSalaRecurso).'%\'
		GROUP BY "r"."id") "s"');  

           $busqueda_materia= DB::raw('(SELECT "r"."id" "recurso_id", string_agg(DISTINCT("d"."descriptor"),\', \') "materia"
	FROM historial_catalogacion sub_hc, descriptores d, recursos_descriptores rc, recursos r 
	WHERE "r"."id" = "sub_hc"."recurso_id"
		AND "rc"."recurso_id" = "r"."id"
		AND "rc"."descriptor_id" = "d"."id"
		AND "d"."tipo" = \'M-650-A\'
		AND "d"."descriptor" like \'%'.strtoupper($materiaTematica).'%\'
	GROUP BY "r"."id") "m"');  



    	$busqueda_reporte = DB::table('historial_catalogacion')
                        ->join('usuarios', 'usuarios.id', '=', 'historial_catalogacion.id_catalogador')
                        ->join('recursos', 'recursos.id', '=', 'historial_catalogacion.recurso_id')
                        ->join('recursos_titulos', 'recursos_titulos.recurso_id', '=', 'recursos.id')
                        ->join('titulos', 'titulos.id', '=', 'recursos_titulos.titulo_id')
                        ->join('tipo_docs', 'tipo_docs.id', '=', 'recursos.tipo_liter')
                        ->leftJoin($busqueda_materia, 'm.recurso_id', '=', 'historial_catalogacion.recurso_id')
                        ->leftJoin($busqueda_salas, 's.recurso_id', '=', 'historial_catalogacion.recurso_id')
                        ->where(function($querym) use ($materiaTematica)
						{
						    if (!empty($materiaTematica)) {
						        $querym->where('m.materia','like','%'.strtoupper($materiaTematica).'%');
						    }
						})
						->where(function($querys) use ($selectedSalaRecurso)
						{
						    if (!empty($selectedSalaRecurso)) {
						        $querys->where('s.sala','like','%'.strtoupper($selectedSalaRecurso).'%');
						    }
						})
                        ->where('recursos_titulos.tipo_tit','=','OP')
                        ->where('historial_catalogacion.fecha','>=',$fechaInicio)
                        ->where('historial_catalogacion.fecha','<=',$fechaFin)
                        ->where('historial_catalogacion.operacion','like','%'.$selectedTipoOperacion.'%')
                        ->where('historial_catalogacion.id_catalogador','like','%'.$selectedCatalogador.'%')
                        ->where(function($query) use ($selectedTipoRecurso)
						{
						    if (empty($selectedTipoRecurso)) {
						        $query->where('recursos.tipo_liter', 'like', '%'.$selectedTipoRecurso.'%');
						    }else{
						    	$query->where('recursos.tipo_liter', '=', $selectedTipoRecurso);
						    }
						})
                        ->select('recursos.id as id', 'tipo_docs.descrip_doc_salida as tiporecurso','historial_catalogacion.operacion as TipoOperacion','recursos.ubicacion as Cota', 'titulos.titulo_salida as Titulo',DB::raw(' "usuarios"."nombres" ||\' \'|| "usuarios"."apellidos" as catalogador' ) ,'historial_catalogacion.fecha as Fecha', 'historial_catalogacion.modulo as Modulo','m.materia as MateriaTematica', 's.sala as Sala')
                        ->get();

    	$datos_busqueda = array_unique(array_merge($datos_busqueda,$busqueda_reporte), SORT_REGULAR);
    	
		foreach ($datos_busqueda as $index =>$elemento) {
			$nuevo = new stdClass();
			$nuevo->Id =$elemento->id;
     		$nuevo->TipoRecurso = $elemento->tiporecurso;
     		$nuevo->TipoOperacion = $elemento->TipoOperacion;
     		$nuevo->Cota = $elemento->Cota;
     		$nuevo->Titulo = $elemento->Titulo;
     		$nuevo->Catalogador = $elemento->catalogador;
			$nuevo->Fecha = date('d-m-Y H:i:s', strtotime($elemento->Fecha));
			$nuevo->Modulo = $elemento->Modulo;
			if ($elemento->Sala == "" || $elemento->Sala == null){
				$nuevo->Sala = " ";
			}else{
				$nuevo->Sala = $elemento->Sala; 
			}

			if ($elemento->MateriaTematica == "" || $elemento->MateriaTematica == null){
				$nuevo->MateriaTematica = " ";
			}else{
				$nuevo->MateriaTematica = $elemento->MateriaTematica;
			}

			$myArray[]=$nuevo;
		}
		//return $myArray;
        if ($tipoReporte == 'excel'){
        	return $this->createReporteExcel($myArray);
        }else if ($tipoReporte == 'pdf'){
        	return $this->createReportePDF($myArray);
        }
	    
	}

	public function createReporteExcel($results)
    { 
    	if(sizeof($results) == 0){
    		return $respuesta = array("codigo" => "2", "nombre" =>"");
    	}else{
    		try{
		   // try code
    		date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
    		$nombre = 'reporte_catalogacion_'.$date;
    		//return $nombre;
	    	Excel::create($nombre, function($excel) use($results) {
	    		$excel->sheet('Sheetname', function($sheet) use($results) {
	    			// Agrego el Header
	    			/*$data = array(
					    array('Header 1', 'Header 2')
					);*/
					
					$data = array(
						array('Tipo de recurso','Operación','Módulo','Cota','Título','Catalogador','Materia temática','Sala','Fecha')
						);

					foreach ($results as $result) {
						$tipoRecurso = $result->TipoRecurso;
						$operacion = $result->TipoOperacion;
						$modulo = $result->Modulo; 
						$cota = $result->Cota;
						$titulo = $result->Titulo;
						$catalogador = $result->Catalogador;
						$materiaTematica = $result->MateriaTematica;
						$sala =  $result->Sala;
						$fecha = $result->Fecha;

						$data[]= array($tipoRecurso, $operacion, $modulo, $cota, $titulo, $catalogador, $materiaTematica, $sala,$fecha);


					}
					//Se debe realizar un ciclo para agregar todas las filas correspondientes, la siguiente instruccion agrega una fila
				//	$data[]= array('otra1','otra2');

	    			$sheet->fromArray($data,null, 'A1', false, false);
	    			$sheet->cell('A1:I1', function($cell) {

					    $cell->setFontWeight('bold');

					});

			    });

			})->store('xlsx', storage_path('\public\catalogacion'));

				return $respuesta = array("codigo" => "0", "nombre" => $nombre);
			} 	
			catch(\Exception $e){
			   // catch code
				return $respuesta = array("codigo" => "1", "nombre" =>"", "error" =>  $e);
			}
    	}
		
    }

    public function createReportePDF($data)
    {
    	if(sizeof($data) == 0){
    		return $respuesta = array("codigo" => "2", "nombre" =>"");
    	}else{
	    	date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$nombre = 'reporte_catalogacion_'.$date;
	    	$pdf = new \Thujohn\Pdf\Pdf();
		    $content = $pdf->load(View::make('reportes.reporte_catalogacion_pdf')->with('data',$data))->output();
		    File::put(storage_path('\public\catalogacion\/'.$nombre.'.pdf'), $content);
		    $respuesta = array("codigo" => "0", "nombre" => $nombre);
		    return $respuesta; 
		}
    }



}
