<?php

class TipoDocsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /tipodocs
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return TipoDoc::all();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tipodocs/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tipodocs
	 *
	 * @return Response
	 */
	public function store($dataCamposAdicionales = null){
		
		if($dataCamposAdicionales){
			$data = $dataCamposAdicionales;
		}else{
			$data = Input::all();
		}

		$query_tipodoc = DB::table('tipo_docs')
	                      ->where('tipo_docs.descrip_doc','=',App::make('BuscadoresController')->convetir_string($data['nombre_tipo']))
	                      ->select('tipo_docs.*')
	                      ->get();

	    if(!$query_tipodoc){

			$aux_id ='OTRO_'; 
			$aux_id2 ='OTR'; 
		    $last_id_recurso = "";
		    $query_id_tipodoc = "";	  

		    $query_id_tipodoc = DB::table('tipo_docs')
		                      ->where('id','like',$aux_id.'%')
		                      ->select(DB::raw('max(tipo_docs.id) as tipodocs_id'))->first();

		    if(!$query_id_tipodoc){
		    	$query_id_tipodoc = DB::table('tipo_docs')
		                      ->where('id','like',$aux_id2.'%')
		                      ->select(DB::raw('max(tipo_docs.id) as tipodocs_id'))->first();
		    }

		    $id_tipodoc_actual= $query_id_tipodoc->tipodocs_id;
		    $int_id_tipodoc = filter_var($id_tipodoc_actual, FILTER_SANITIZE_NUMBER_INT);
		    if($int_id_tipodoc){
		    	if((int)$int_id_tipodoc < 9)
		    		$id_tipo_nuevo = $aux_id.'0'.(string)((int)$int_id_tipodoc + 1);	
		    	else
		    		$id_tipo_nuevo = $aux_id.(string)((int)$int_id_tipodoc + 1);	
		    }else{
		    	$id_tipo_nuevo = $aux_id.'01';	
		    }

		    $tipo_doc = new TipoDoc;
			$tipo_doc->id = $id_tipo_nuevo;
			$tipo_doc->descrip_doc = App::make('BuscadoresController')->convetir_string($data['nombre_tipo']);
			$tipo_doc->descrip_doc_salida = $data['nombre_tipo'];
			$tipo_doc->save();

			$tipo_autorv = new TipoAutor;
			$tipo_autorv->tipo_doc = $id_tipo_nuevo;
			$tipo_autorv->tipo_autor = 'P';
			$tipo_autorv->nombre = 'Autor Principal';
			$tipo_autorv->orden = 1;
			
			$tipo_autorv->save();

			$tipo_autor2 = new TipoAutor;
			$tipo_autor2->tipo_doc = $id_tipo_nuevo;
			$tipo_autor2->tipo_autor = 'S';
			$tipo_autor2->nombre = 'Autor Secundario';
			$tipo_autor2->orden = 2;
			$tipo_autor2->save();

			$tipo_titulo = new TipoTitulo;
			$tipo_titulo->tipo_doc =  $id_tipo_nuevo;
			$tipo_titulo->tipo_titulo = 'OP';
			$tipo_titulo->descripcion = 'Titulo Original';
			$tipo_titulo->orden = 1;
			$tipo_titulo->save();

			$tipo_titulo2 = new TipoTitulo;
			$tipo_titulo2->tipo_doc = $id_tipo_nuevo;
			$tipo_titulo2->tipo_titulo = 'OS';
			$tipo_titulo2->descripcion = 'Subtitulo Original';
			$tipo_titulo2->orden = 2;
			$tipo_titulo2->save();

			$respuesta = array("codigo" => "0", "id" => $tipo_doc->id);
	    }else{
	    	$respuesta = array("codigo" => "1");
	    }
			
		
		return $respuesta;
	}
	/**
	 * Display the specified resource.
	 * GET /tipodocs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//

		return TipoDoc::find(Recurso::find($id)->tipo_liter);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tipodocs/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tipodocs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();
		
		$verificar_nombre = DB::table('tipo_docs')
			->where('id','!=',$id)
			->where('descrip_doc','like', App::make('BuscadoresController')->convetir_string($data['nombre_tipo']))
			->select('tipo_docs.*')
			->get();
		if(!$verificar_nombre){
			$campo_actualizado = DB::table('tipo_docs')
				->where('id','=',$id)
				->update(array('descrip_doc' => strtoupper(App::make('BuscadoresController')->convetir_string($data['nombre_tipo'])), 'descrip_doc_salida' => $data['nombre_tipo']));
			$respuesta = array("codigo" => "0");
		}else{
			$respuesta = array("codigo" => "1");	
		}
		return $respuesta;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tipodocs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Input::all();
		$recurso_tipodoc = DB::table('recursos')
			->where('recursos.tipo_liter', '=',$id)
			->select('recursos.*')
			->get();

		$ejemplares_tipodoc = DB::table('ejemplares')
			->where('ejemplares.tipo_liter', '=',$id)
			->select('ejemplares.*')
			->get();

		if(!$recurso_tipodoc && !$ejemplares_tipodoc){
			$delete_entipo_autores = DB::table('tipo_autores')
				->where('tipo_autores.tipo_doc', '=',$id)
				->delete();

			$delete_entipo_tituos = DB::table('tipotitulo')
				->where('tipotitulo.tipo_doc', '=',$id)
				->delete();

			$delete_entipo_docs = DB::table('tipo_docs')
				->where('tipo_docs.id', '=',$id)
				->delete();

			$respuesta = array("codigo" => "0");
		}else{
			$respuesta = array("codigo" => "1");

		}
		
		return $respuesta;
	}

	public function tipo_docs_sinprincipales(){
		$tipo_docs = DB::table('tipo_docs')
			->where('id','like','%OTR%')
			->orderBy('tipo_docs.descrip_doc', 'ASC')
			->select('tipo_docs.*')
			->get();
		return $tipo_docs;
	}
}