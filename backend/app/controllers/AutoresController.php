<?php

class AutoresController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /autores
	 *
	 * @return Response
	 */

	public $setting;


	public function index()
	{

		$autores = Autor::select('id','autor','autor','autor_salida')->orderBy('autor')->paginate(15);
		return $autores;

		//return Autor::take(15)->skip(0)->get();

		//ejemplo con Eleonora Acosta
//		$autores = Autor::find(18288)->recursos;
//		echo '<pre>';
//		print_r($autores);
//		echo '</pre>';
		//return Autor::take(1)->get();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /autores/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /autores
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /autores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /autores/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /autores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /autores/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}



	public function autores_letra($letra){
		//$autores_letra = Autor::where('autor','like',$letra.'%')->orderby('autor')->paginate();

		$num_items_pagina;
		//	if( isset(Input::get('num_items_pagina')) !=null ){
		$num_items_pagina = Input::get('num_items_pagina');

		if($num_items_pagina  == null){
				$num_items_pagina ==10;
		}

        $recursos_titulos_letra =   DB::table('recursos_autores')
                ->join('autores', 'autores.id', '=', 'recursos_autores.cod_autor')
                ->where('autores.autor','like',$letra.'%')
                ->groupBy('autores.id')     
                ->orderBy('autores.autor')
                ->select('autores.id','autores.autor','autores.autor_salida',DB::raw('count(autores.id) as num_recursos'))
                ->paginate($num_items_pagina);
        return $recursos_titulos_letra;


		return $autores_letra;
	}

	public function autores_tipo_doc($tipo_doc){
			$num_items_pagina;
		//	if( isset(Input::get('num_items_pagina')) !=null ){
			$num_items_pagina = Input::get('num_items_pagina');
			if($num_items_pagina  == null){
				$num_items_pagina ==10;
			}
            $recursos_titulos_tipo_doc= DB::table('recursos_autores')
                            ->join('autores', 'autores.id', '=', 'recursos_autores.cod_autor')               
                            ->where('recursos_autores.tipo_doc','=',$tipo_doc)
                            ->groupBy('autores.id')     
                            ->orderBy('autores.id')
                            ->select('autores.id','autores.autor','autores.autor_salida',DB::raw('count(autores.id) as num_recursos'))
                            ->paginate($num_items_pagina);
            return $recursos_titulos_tipo_doc;



/*$recursos_autotes_tipo_doc =	DB::table('recursos_autores')
		            ->join('recursos', 'recursos.id', '=', 'recursos_autores.recurso_id')
		            ->join('autores', 'autores.id', '=', 'recursos_autores.cod_autor')	
		            ->join('tipo_autores', 'tipo_autores.tipo_doc', '=', 'recursos_autores.tipo_doc')				 
					->join('tipo_autores', 'tipo_autores.tipo_autor', '=', 'recursos_autores.tipo_autor')	         
		            ->where('recursos_autores.tipo_doc','=',$tipo_doc)
		            ->groupBy('autores.id')		
		            ->orderBy('autores.id')
		            ->select('autores.id','autores.autor','autores.autor_salida',DB::raw('count(autores.id) as num_recursos'))
		            ->paginate();
		return $recursos_autotes_tipo_doc;
*/
		/*
			SELECT count(a.id) "num_recursos",a.id,  a.autor, a.autor_salida
			FROM recursos_autores ca, autores a, tipo_autores ta, recursos r
			WHERE ca.recurso_id = r.id
			AND   ca.cod_autor = a.id
			AND   ca.tipo_doc = ta.tipo_doc 
			AND   ca.tipo_autor = ta.tipo_autor
			AND   ca.tipo_doc = 'M'
			GROUP BY (a.id)
			ORDER BY (a.id);
		*/

 	/*$results = DB::select(DB::raw("SELECT "."count(a.id) as num_recursos,a.id,  a.autor, a.autor_salida ".
  							"FROM  recursos_autores ra, autores a, tipo_autores ta, recursos r
						 	 WHERE "."ra.tipo_doc = '".$tipo_doc."' 
						 	 AND ra.recurso_id = r.id   
						 	 AND ra.cod_autor = a.id
						 	 AND ra.tipo_doc = ta.tipo_doc 
						 	 AND ra.tipo_autor = ta.tipo_autor
			 	 			 GROUP BY (a.id)
							 ORDER BY (a.autor)
						 	 ")); 

  	 $autores_tipo_doc = Paginator::make($results, count($results ), 50);
  	 return $autores_tipo_doc;  */
	}

	public function autores_tipo_doc_letra($tipo_doc,$letra){
			$num_items_pagina;
		//	if( isset(Input::get('num_items_pagina')) !=null ){
			$num_items_pagina = Input::get('num_items_pagina');
			
			if($num_items_pagina  == null){
				$num_items_pagina ==10;
			}
		    
		    $recursos_titulos_tipo_doc_letra =  DB::table('recursos_autores')
                            ->join('autores', 'autores.id', '=', 'recursos_autores.cod_autor')
                            ->where('autores.autor','like',$letra.'%')
                            ->where('recursos_autores.tipo_doc','=',$tipo_doc)
                            ->groupBy('autores.id')     
                            ->orderBy('autores.autor')
                            ->select('autores.id','autores.autor','autores.autor_salida',DB::raw('count(autores.id) as num_recursos'))
                            ->paginate($num_items_pagina);
            return $recursos_titulos_tipo_doc_letra;
	}

	public function autor_recursos_tipo_doc($id, $tipo_doc){
		
	//	return $id;

			$recursos_titulo = [];

			if($tipo_doc == 'todos' ){
					$recursos_titulo =  DB::table('recursos_autores')
						   ->join('recursos','recursos.id','=','recursos_autores.recurso_id')
						   ->where('recursos_autores.cod_autor','=', $id)
						   ->select('recursos.*')
						   ->get();
			}else{

			$recursos_titulo =  DB::table('recursos_autores')
								   ->join('recursos','recursos.id','=','recursos_autores.recurso_id')
								   ->where('recursos_autores.cod_autor','=', $id)
								   ->where('recursos_autores.tipo_doc','=', $tipo_doc)
								   ->select('recursos.*')
								   ->get();
			}					

		//	$recursos_titulo = json_encode($recursos_titulo;  	
		//	$recursos_titulo = (array)json_decode($recursos_titulo, true);
		//	$recursos_titulo = (array)$recursos_titulo;					  

		for ($i=0; $i < count($recursos_titulo) ; $i++) { 
                $recursos_titulo[$i]->tipo_doc = $this->getTipoDoc($recursos_titulo[$i]->id); 
                $recursos_titulo[$i]->fecha_pub= $this->getFecha($recursos_titulo[$i]);
				
				if($recursos_titulo[$i]->tipo_liter=='M'){
					$recursos_titulo[$i]->isbn = "";
					$aux_isbn =   Libro::select('isbn')->where('recurso_id', '=',$recursos_titulo[$i]->id)->get(); 
					if($aux_isbn[0]->isbn!=null){	
						$recursos_titulo[$i]->isbn = $aux_isbn[0]->isbn;			
					}else{
						$recursos_titulo[$i]->isbn = "isbn no catalogado";	
					}
				}else{
					if($recursos_titulo[$i]->tipo_liter=='S'){
						if($aux_issn[0]->issn!=null){}
							$recursos_titulo[$i]->issn = "";
							$aux_issn =   PublicacionesSeriada::select('issn')->where('recurso_id', '=',$recursos_titulo[$i]->id)->get(); 
							$recursos_titulo[$i]->issn = $aux_issn[0]->issn;	
						}else{
							$recursos_titulo[$i]->issn = "issn no catalogado";
						}
				}

				if($recursos_titulo[$i]->tipo_liter == 'T'){
			//		$recursos_titulo[$i]->escuela = $this->recurso_escuela($recursos_titulo[$i]->id);
				}
			//	$recursos_titulo[$i]->dependencia = $this->recurso_dependencia($recursos_titulo[$i]->id);
        }


			return $recursos_titulo;
	
	//	return 1;
		//return $id;
		//$recursos_autores = Autor::select('id')->find($id)->recursos;
	/*	if(isset($GET_['tipo_doc'])){
	
			$recursos_autores = DB::table('recursos_autores')
								->join('recursos','recursos.id','=','recursos_autores.recurso_id')
								->where('recursos_autores.cod_autor','=',$id)
								->where('recursos_autores.tipo_doc','=',$GET_['tipo_doc'])
								->select('recursos.*')
								->get();
			return $recursos_autores;
		}
		*/
	//	$recursos_autores = Autor::select('id')->find($id)->recursos;
	 
	//		$recursos_autores['tipo_doc'] = 'Monografías';
	//	return json_encode($recursos_autores);

	//	return $recursos_autores;

	}


	public function recurso_escuela($id){
		return Escuela::find(Tesis::where('recurso_id','=',$id)->get()[0]->escuela_id)->escuela_salida;
	}

	public function recurso_dependencia($id){
    		return  Dependencia::find(Recurso::find($id)->dependencia_id)->siglas;
    }





	public function getFecha($recurso){
		if($recurso->fecha_iso != null && $recurso->fecha_pub == null){
                		$aux = $recurso->fecha_iso;
                		$recurso->fecha_pub = $aux[0].''.$aux[1].''.$aux[2].''.$aux[3];
            }else{
            	if($recurso->fecha_iso == null && $recurso->fecha_pub == null){
					$recurso->fecha_pub = 'Año no catalogado';
            	}	
            }
        return $recurso->fecha_pub;
	}

  
    public function getTipoDoc($id){
           return  TipoDoc::find(Recurso::find($id)->tipo_liter)->descrip_doc;
    }


	public function getByPage($page = 1, $limit = 10)
	{
	  $results = StdClass;
	  $results->page = $page;
	  $results->limit = $limit;
	  $results->totalItems = 0;
	  $results->items = array();
	 
	  $users = $this->model->skip($limit * ($page - 1))->take($limit)->get();
	 
	  $results->totalItems = $this->model->count();
	  $results->items = $users->all();
	 
	  return $results;
	}

	public function obtenerAutores(){
		return  Autor::paginate(100);	
	}

	public function buscar_autores_recurso(){
		$data = Input::all();
		$id_recurso = Input::get('id_recurso');
		$tipo_recurso = Input::get('tipo_recurso');	 	  
		$query = DB::table('recursos_autores')
								->where('recursos_autores.recurso_id','=',$id_recurso)
								->join('autores','autores.id','=','recursos_autores.cod_autor')
								->join('tipo_autores','tipo_autores.tipo_autor','=','recursos_autores.tipo_autor')
								->where('tipo_autores.tipo_doc','=',$tipo_recurso)
								->orderby('recursos_autores.orden', 'ASC')
								->select('autores.*','tipo_autores.nombre as tipo_autor_2','tipo_autores.tipo_autor as id_tipo_autor_2','recursos_autores.orden as orden')
								->get();

		return $query;

	}

	public function agregar_autor_recurso($dataAutor = null){
		
		if($dataAutor){
			$data = $dataAutor;
		}else{
			$data = Input::all();	
		}
		
		if($data["fechaAutor"] != ""){
			$data["fechaAutor"] = date('Ymd', strtotime($data["fechaAutor"]));
		}
		$queryAutor = DB::table('autores')
		
				->where('autor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor'])))
				->where('tipo_autor','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['tipoAutor1'])))
				->select('autores.*')->first();

		if($queryAutor){
				$idAutor = $queryAutor->id;	
				$queryRecursoAutores = DB::table('recursos_autores')
				->where('recursos_autores.recurso_id','=',$data["id_recurso"])
				->where('recursos_autores.cod_autor','=',$idAutor)
				->select('recursos_autores.*')->first();

				if(!$queryRecursoAutores){
					$queryRecursoAutores2 = DB::table('recursos_autores')
				    ->where('recursos_autores.recurso_id','=',$data["id_recurso"])
				    ->orderBy('recursos_autores.orden', 'DESC')
					->select('recursos_autores.*')->first();
					if($queryRecursoAutores2){
						$orden_actual= $queryRecursoAutores2->orden;	
					}else{
						$orden_actual = 1;	
					}
					$orden_nuevo = $orden_actual + 1;
			        $recurso_autor = new RecursoAutor;	
					$recurso_autor->recurso_id = $data["id_recurso"];
					$recurso_autor->ext_recurso_id =  '0';
					$recurso_autor->cod_autor = $idAutor;
					$recurso_autor->tipo_doc = $data["tipo_recurso"];
					$recurso_autor->tipo_autor = $data["tipoAutor2"];
					$recurso_autor->portada = "0"; 
					$recurso_autor->orden = $orden_nuevo;
					$recurso_autor->save();
					$respuesta = array("codigo" => "0");
				} else $respuesta = array("codigo" => "1");


		}else{

	        $autor = new Autor;	
	        $autor->autor = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor']));
	        $autor->autor_salida = $data['nombreAutor'];
	        $autor->tipo_autor = $data["tipoAutor1"];
	        $autor->dir_electr = $data["correoAutor"];
	        $autor->fecha_nac_dec = $data["fechaAutor"];
	        $autor->url = $data["urlAutor"];
	        $autor->save();
			$idAutor = $autor->id;

			$queryRecursoAutores = DB::table('recursos_autores')
				->where('recursos_autores.recurso_id','=',$data["id_recurso"])
				->select(DB::raw('max(recursos_autores.orden) as recursos_autores_orden'))->first();


			$orden_actual= $queryRecursoAutores->recursos_autores_orden;
			$orden_nuevo = $orden_actual + 1;

	        $recurso_autor = new RecursoAutor;	
			$recurso_autor->recurso_id = $data["id_recurso"];
			$recurso_autor->ext_recurso_id =  '0';
			$recurso_autor->cod_autor = $idAutor;
			$recurso_autor->tipo_doc = $data["tipo_recurso"];
			$recurso_autor->tipo_autor = $data["tipoAutor2"];
			$recurso_autor->portada = "0"; 
			$recurso_autor->orden = $orden_nuevo;
			$recurso_autor->save();
			$respuesta = array("codigo" => "0");
		}
		date_default_timezone_set('America/Caracas');
		$date = date('YmdHis', time());
		$historial = new HistorialCatalogacion;
		$historial->id_catalogador =  $data["idCatalogador"];
		$historial->recurso_id = $data["id_recurso"];
		$historial->ext_recurso_id = '0';
		$historial->operacion = 'Agregar'; 
		$historial->modulo = 'Autores';
		$historial->fecha = $date;
		$historial->save();
		return json_encode($respuesta);

	}

	public function eliminar_autor_recurso(){
		$data = Input::all();

		$recurso_autores = DB::table('recursos_autores')
		->where('recurso_id','=',$data['id_recurso'])
		->where('cod_autor','=',$data['id_autor'])
		->delete();
		date_default_timezone_set('America/Caracas');
		$date = date('YmdHis', time());
		$historial = new HistorialCatalogacion;
		$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
		$historial->recurso_id = $data['id_recurso'];
		$historial->ext_recurso_id = '0';
		$historial->operacion = 'Eliminar'; 
		$historial->modulo = 'Autores';
		$historial->fecha = $date;
		$historial->save();

		$respuesta = array("codigo" => "0");
		return json_encode($respuesta);

	}

	public function modificar_autor_recurso(){
		$data = Input::all();
		$count = 0;
		if($data["fechaAutor"]!= '') 
			$data["fechaAutor"] = date('Ymd', strtotime($data["fechaAutor"]));

		$autor_actualizado = DB::table('autores')
		->where('id','=',$data["id_autor"])
		->update(array('autor' => strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreAutor'])), 'autor_salida' => $data['nombreAutor'], 'tipo_autor' => $data["tipoAutor1"], 'dir_electr' => $data["correoAutor"], 'fecha_nac_dec' => $data["fechaAutor"], 'url' => $data["urlAutor"]));
		
		$ra_tipo_autor = DB::table('recursos_autores')
			->where('recurso_id','=',$data['id_recurso'])
			->where('cod_autor','=',$data["id_autor"])
			->update(array('tipo_autor' => $data["tipoAutor2"]));

		$ordenes = $data["orden_autores"];

		foreach ($ordenes as $row) {
			$count++;
			$recurso_autores = DB::table('recursos_autores')
			->where('recurso_id','=',$data['id_recurso'])
			->where('cod_autor','=',$row["id"])
			->update(array('orden' => $count));
			
		}
		date_default_timezone_set('America/Caracas');
		$date = date('YmdHis', time());
		$historial = new HistorialCatalogacion;
		$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
		$historial->recurso_id = $data['id_recurso'];
		$historial->ext_recurso_id = '0';
		$historial->operacion = 'Modificar'; 
		$historial->modulo = 'Autores';
		$historial->fecha = $date;
		$historial->save();

		$respuesta = array("codigo" => "0");
		return json_encode($respuesta);

	}


}