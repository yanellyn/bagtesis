<?php

/*include(app_path()."\API\ExtendedController.php");
include(app_path()."\API\ExtendedController2.php");
*/

/*use API\ExtendedController;*/

class TitulosController extends \BaseController{


	/*public function __construct(Titulo  $titulo)
	{
		$this->model = $titulo;
        parent::__construct();
	}
	*/
	/**
	 * Display a listing of the resource.
	 * GET /titulos
	 *
	 * @return Response
	 */
	public function index()
	{

	//	$titulos = $this->service_titulos();
		$titulos = Titulo::select('id','titulo','titulo_salida')->orderBy('titulo')->paginate(10);
		return $titulos;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /titulos/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}
	/**
	 * Store a newly created resource in storage.
	 * POST /titulos
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /titulos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	//	$titulo = $this->service_titulo($id);
        $titulo =	Titulo::find($id);
		return $titulo;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /titulos/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /titulos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /titulos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function titulos_tipo_doc($tipo_doc){
	
	//	$recursos_titulos_tipo_doc = $this->service_titulos_tipo_doc($tipo_doc);


	    $recursos_titulos_tipo_doc= DB::table('recursos_titulos')
                        ->join('titulos', 'titulos.id', '=', 'recursos_titulos.titulo_id')               
                        ->where('recursos_titulos.tipo_doc','=',$tipo_doc)
                        ->where('recursos_titulos.tipo_tit','=','OP')
                        ->groupBy('titulos.id')     
                        ->orderBy('titulos.id')
                        ->select('titulos.id','titulos.titulo','titulos.titulo_salida',DB::raw('count(titulos.id) as num_recursos'))
                        ->paginate(10);
        return $recursos_titulos_tipo_doc;
	}

	public function titulos_letra($letra){


		$num_items_pagina = Input::get('num_items_pagina');
	/*	$orden = Input::get('orden'); */
	/*	$anio_origen = Input::get('anio_origen');
		$anio_destino = Input::get('anio_destino');
	*/	
		if($num_items_pagina  == null){
				$num_items_pagina = 10;
		}

	/*	if($orden  == null){
				$orden ='ASC';
		}
	*/

	/*	if($anio_origen  == null){
				$anio_origen ='1990';
		}

		if($anio_destino  == null){
				$anio_origen = '2000';
		}
		*/

		//	$recursos_titulos_letra = $this->service_titulos_letra($letra);
		$recursos_titulos_letra =   DB::table('recursos_titulos')
                            ->join('titulos', 'titulos.id', '=', 'recursos_titulos.titulo_id')     
                            ->join('recursos', 'recursos.id', '=', 'recursos_titulos.recurso_id')  
                        /*    ->where(function($query) use($anio_origen,$anio_destino){
                            	$query->where('recursos.fecha_pub','>',$anio_origen);
                            	$query->where('recursos.fecha_pub','<',$anio_destino);
                            	
                            })     
                         */ 
                            
                            ->where('recursos_titulos.tipo_tit','=','OP')
                            ->where('titulos.titulo','like',$letra.'%')
                            ->groupBy('titulos.id')     
                      /*      ->orderBy('titulos.titulo',$orden) */
                            ->select('titulos.id','titulos.titulo','titulos.titulo_salida',DB::raw('count(titulos.id) as num_recursos'))
                            ->paginate($num_items_pagina);
        return $recursos_titulos_letra;
		//return $recursos_titulos_letra;
	}

	

	public function  titulos_tipo_doc_letra($tipo_doc,$letra){
	//		$recursos_titulos_tipo_doc_letra = $this->service_titulos_tipo_doc_letra($tipo_doc,$letra);
			//$
			$num_items_pagina;
		//	if( isset(Input::get('num_items_pagina')) !=null ){
				$num_items_pagina = Input::get('num_items_pagina');
		//	}else{
			//	$num_items_pagina = 10;
		//	}
		/*	$orden = Input::get('orden'); */

		/*	if($num_items_pagina  == null){
				$num_items_pagina ==10;
			}
		*/
		/*	if($orden  == null){
					$orden =='ASC';
			}
		*/

	/*	$anio_origen = Input::get('anio_origen');
		$anio_destino = Input::get('anio_destino');
	*/
	/*	if($anio_origen  == null){
				$anio_origen ='1990';
		}

		if($anio_destino  == null){
				$anio_origen = '2000';
		}
	*/

     /*       $recursos_titulos_tipo_doc_letra =  DB::table('recursos_titulos')
                ->join('titulos', 'titulos.id', '=', 'recursos_titulos.titulo_id')              
            /*    ->join('recursos', 'recursos.id', '=', 'recursos_titulos.recurso_id')  
                            ->where(function($query) use($anio_origen,$anio_destino){
                            	$query->where('recursos.fecha_pub','>',$anio_origen);
                            	$query->where('recursos.fecha_pub','<',$anio_destino);
                            })  
            */                
          /*      ->where('recursos_titulos.tipo_tit','=','OP')  
                ->where('titulos.titulo','like',$letra.'%')
                ->where('recursos_titulos.tipo_doc','=',$tipo_doc)
                ->groupBy('titulos.id ')     
             /*   ->orderBy('titulos.titulo',$orden)  */
            /*    ->select('titulos.id',DB::raw('count(titulos.id) as num_recursos'))
                ->paginate($num_items_pagina);
			*/
              $query = DB::select(DB::raw("SELECT t.id , t.titulo, t.titulo_salida, c.num_recursos
											FROM  (
												 SELECT titulo_id,COUNT(titulo_id)  as num_recursos
												 FROM recursos_titulos
												 WHERE tipo_tit = 'OP'
												 AND  tipo_doc = '".$tipo_doc."'
												 GROUP BY (titulo_id)
											) c JOIN titulos t ON t.id = c.titulo_id
											WHERE t.titulo like '".$letra."%'"
										)
              					  );
		        $perPage = $num_items_pagina;
				$currentPage = Input::get('page') - 1;
				$pagedData = array_slice($query, $currentPage * $perPage, $perPage);
				$matches = Paginator::make($pagedData, count($query), $perPage);
				return $matches;  

	    
		//	return $recursos_titulos_tipo_doc_letra;
		//	return $recursos_titulos_tipo_doc_letra;
	}

	public function recursos_titulo($id){
		$recursos_titulo = Titulo::select('id')->find($id)->recursos;
		for ($i=0; $i < count($recursos_titulo) ; $i++) { 
                $recursos_titulo[$i]['tipo_doc'] = $this->getTipoDoc($recursos_titulo[$i]['id']); 
                $recursos_titulo[$i]['fecha_pub'] = $this->getFecha($recursos_titulo[$i]);
				
				if($recursos_titulo[$i]['tipo_liter']=='M'){
					$recursos_titulo[$i]['isbn'] = "";
					$aux_isbn =   Libro::select('isbn')->where('recurso_id', '=',$recursos_titulo[$i]['id'])->get(); 
					if($aux_isbn[0]['isbn']!=null){	
						$recursos_titulo[$i]['isbn'] = $aux_isbn[0]['isbn'];			
					}else{
						$recursos_titulo[$i]['isbn'] = "isbn no catalogado";	
					}
				}else{
					if($recursos_titulo[$i]['tipo_liter']=='S'){
						$aux_issn =   PublicacionesSeriada::select('issn')->where('recurso_id', '=',$recursos_titulo[$i]['id'])->get();
					if(empty($aux_issn)){
					if($aux_issn[0]->issn != null){
						$recurso['issn'] = $aux_issn[0]->issn;
					}else{
						$recurso['issn'] = 'issn no catalogado';
					}
				}else{
					$recurso['issn'] = 'issn no catalogado';
				}	
						/*
						if($aux_issn[0]['issn']!=null){}
							$recursos_titulo[$i]['issn'] = "";
							$aux_issn =   PublicacionesSeriada::select('issn')->where('recurso_id', '=',$recursos_titulo[$i]['id'])->get(); 
							$recursos_titulo[$i]['issn'] = $aux_issn[0]['issn'];	
						}else{
							$recursos_titulo[$i]['issn'] = "issn no catalogado";
						}
						*/
					}
				}


				if($recursos_titulo[$i]['tipo_liter'] == 'T'){
				//	$recursos_titulo[$i]['escuela'] = $this->recurso_escuela($recursos_titulo[$i]['id']);
				}
		//	$recursos_titulo[$i]['dependencia'] = '';
		//	$recursos_titulo[$i]['dependencia'] = $this->recurso_dependencia($recursos_titulo[$i]['id']);

        }

		return $recursos_titulo;
	}

	public function recurso_dependencia($id){
    		return  Dependencia::find(Recurso::find($id)->dependencia_id);
    }


	public function getFecha($recurso){
		if($recurso['fecha_iso'] != null && $recurso['fecha_pub'] == null){
                		$aux = $recurso['fecha_iso'];
                		$recurso['fecha_pub']= $aux[0].''.$aux[1].''.$aux[2].''.$aux[3];
            }else{
            	if($recurso['fecha_iso'] == null && $recurso['fecha_pub'] == null){
					$recurso['fecha_pub'] = 'Año no catalogado';
            	}	
            }
        return $recurso['fecha_pub'];
	}

  
    public function getTipoDoc($id){
           return  TipoDoc::find(Recurso::find($id)->tipo_liter)->descrip_doc;
    }


	public function recurso_escuela($id){
		return Escuela::find(Tesis::where('recurso_id','=',$id)->get()[0]->escuela_id)->escuela_salida;
	}

    public function obtenerTitulos(){
		return DB::table('recursos_titulos')
			->where('recursos_titulos.tipo_doc','M')
            ->join('titulos', 'recursos_titulos.titulo_id', '=', 'titulos.id')
            ->join('tipotitulo',function($join){
				$join->on('tipotitulo.tipo_titulo','=','recursos_titulos.tipo_tit');
				$join->on('tipotitulo.tipo_doc','=','recursos_titulos.tipo_doc');
			})
            ->select('tipotitulo.*', 'titulos.*')
            ->paginate(10);
	}


	public function eliminar_titulo_recurso(){
		$data = Input::all();

		$recurso_titulo = DB::table('recursos_titulos')
		->where('recurso_id','=',$data['id_recurso'])
		->where('titulo_id','=',$data['id_titulo'])
		->delete();

		date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
		$historial->id_catalogador =  $data['idCatalogador']; 
		$historial->recurso_id = $data['id_recurso'];
		$historial->ext_recurso_id = '0';
		$historial->operacion = 'Eliminar'; 
		$historial->modulo = 'Títulos';
		$historial->fecha = $date;
		$historial->save();

		$respuesta = array("codigo" => "0");
		return json_encode($respuesta);

	}


	public function modificar_titulo_recurso(){
		$data = Input::all();
		$count = 0;
		//primero se valida que el titulo no coresponda con una serie
		$queryRecurso = $queryTitulo = DB::table('recursos')
			->where('recursos.id','=',$data["id_recurso"])
			->select('recursos.*')->first();

		$queryValidarPublicacion ="";

		if($queryRecurso->tipo_liter == 'S'){
			$queryValidarPublicacion = DB::table('recursos')
			->join('recursos_titulos','recursos_titulos.recurso_id','=','recursos.id')
			->join('titulos','titulos.id','=','recursos_titulos.titulo_id')
			->where('recursos.tipo_liter','=', 'S')
			->where('recursos.nivel_reg','=', 's')
        	->where('recursos.jerarquia','=', $queryRecurso->jerarquia)
			->select('titulos.*')->first();
        }

        if($queryValidarPublicacion && $queryValidarPublicacion->id == $data["id_titulo"]){
        	$titulo = new Titulo;	
	        $titulo->titulo = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']));
	        $titulo->titulo_salida = $data['nombreTitulo'];
	        $titulo->save();
			$idTitulo = $titulo->id;

			$recurso_titulo = new RecursosTitulo;	
			$recurso_titulo->recurso_id = $data["id_recurso"];
			$recurso_titulo->ext_recurso_id =  '0';
			$recurso_titulo->titulo_id = $idTitulo;
			$recurso_titulo->tipo_tit =  $data["tipoTitulo"];
			$recurso_titulo->tipo_doc = 'S';
			$recurso_titulo->portada = ''; 
			$recurso_titulo->orden = 0;
			$recurso_titulo->save();
			$respuesta = array("codigo" => "0");
        }else{


			$titulo_actualizado = DB::table('titulos')
			->where('id','=',$data["id_titulo"])
			->update(array('titulo' => strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo'])), 'titulo_salida' => $data['nombreTitulo']));


			$rt_tipo_titulo = DB::table('recursos_titulos')
				->where('recurso_id','=',$data['id_recurso'])
				->where('titulo_id','=',$data["id_titulo"])
				->update(array('tipo_tit' => $data["tipoTitulo"]));

			$ordenes = $data["orden_titulos"];

			foreach ($ordenes as $row) {
				$count++;
				$recurso_titulos = DB::table('recursos_titulos')
				->where('recurso_id','=',$data['id_recurso'])
				->where('titulo_id','=',$row["id"])
				->update(array('orden' => $count));
				
			}

			date_default_timezone_set('America/Caracas');
				$date = date('YmdHis', time());
				$historial = new HistorialCatalogacion;
			$historial->id_catalogador =  $data['idCatalogador']; // Se debe enviar el Id del catalogador para este caso porque no se esta enviando
			$historial->recurso_id = $data['id_recurso'];
			$historial->ext_recurso_id = '0';
			$historial->operacion = 'Modificar'; 
			$historial->modulo = 'Títulos';
			$historial->fecha = $date;
			$historial->save();
			$respuesta = array("codigo" => "0");
		}
		return json_encode($respuesta);

	}


	public function agregar_titulo_recurso($dataTitulo = null){
		if($dataTitulo){
			$data = $dataTitulo;
		}else{
			$data = Input::all();
		}
		
		$queryTitulo = DB::table('titulos')
				->join('recursos_titulos', 'recursos_titulos.titulo_id', '=', 'titulos.id')
				->where('titulos.titulo','=', strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo'])))
				->where('recursos_titulos.tipo_tit','=', $data['tipoTitulo'])
				->select('titulos.*')->first();

		if($queryTitulo){
				$idTitulo = $queryTitulo->id;

				$queryRecursoTitulos = DB::table('recursos_titulos')
				->where('recursos_titulos.recurso_id','=',$data["id_recurso"])
				->where('recursos_titulos.titulo_id','=',$idTitulo)
				->select('recursos_titulos.*')->first();

				if(!$queryRecursoTitulos){
					$queryRecursoTitulos2 = DB::table('recursos_titulos')
				    ->where('recursos_titulos.recurso_id','=',$data["id_recurso"])
				    ->orderBy('recursos_titulos.orden', 'DESC')
					->select('recursos_titulos.*')->first();
					if($queryRecursoTitulos2){
						$orden_actual = $queryRecursoTitulos2->orden;	
					}else{
						$orden_actual = 1;	
					}
					$orden_nuevo = $orden_actual + 1;
			 		$recurso_titulo = new RecursosTitulo;	
					$recurso_titulo->recurso_id = $data["id_recurso"];
					$recurso_titulo->ext_recurso_id =  '0';
					$recurso_titulo->titulo_id = $idTitulo;
					$recurso_titulo->tipo_tit = $data["tipoTitulo"];
					$recurso_titulo->tipo_doc = $data["tipo_recurso"];
					$recurso_titulo->portada = "0"; 
					$recurso_titulo->orden = $orden_nuevo;
					$recurso_titulo->save();

					date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
					$historial->id_catalogador =  $data['idCatalogador'];
					$historial->recurso_id = $data["id_recurso"];
					$historial->ext_recurso_id = '0';
					$historial->operacion = 'Agregar'; 
					$historial->modulo = 'Títulos';
					$historial->fecha = $date;
					$historial->save();

					$respuesta = array("codigo" => "0");
				} else $respuesta = array("codigo" => "1");

		}else{
	        $titulo = new Titulo;	
	        $titulo->titulo = strtoupper(App::make('BuscadoresController')->convetir_string($data['nombreTitulo']));
	        $titulo->titulo_salida = $data['nombreTitulo'];
	        $titulo->save();
			$idTitulo = $titulo->id;

			$queryRecursoTitulos = DB::table('recursos_titulos')
				->where('recursos_titulos.recurso_id','=',$data["id_recurso"])
				->select(DB::raw('max(recursos_titulos.orden) as recursos_titulos_orden'))->first();


				$orden_actual= $queryRecursoTitulos->recursos_titulos_orden;
				$orden_nuevo = $orden_actual + 1;

		 		$recurso_titulo = new RecursosTitulo;	
				$recurso_titulo->recurso_id = $data["id_recurso"];
				$recurso_titulo->ext_recurso_id =  '0';
				$recurso_titulo->titulo_id = $idTitulo;
				$recurso_titulo->tipo_tit = $data["tipoTitulo"];
				$recurso_titulo->tipo_doc = $data["tipo_recurso"];
				$recurso_titulo->portada = "0"; 
				$recurso_titulo->orden = $orden_nuevo;
				$recurso_titulo->save();

				date_default_timezone_set('America/Caracas');
			$date = date('YmdHis', time());
			$historial = new HistorialCatalogacion;
				$historial->id_catalogador =  $data['idCatalogador'];
				$historial->recurso_id = $data["id_recurso"];
				$historial->ext_recurso_id = '0';
				$historial->operacion = 'Agregar'; 
				$historial->modulo = 'Títulos';
				$historial->fecha = $date;
				$historial->save();

				$respuesta = array("codigo" => "0");
		}

		

		
		return json_encode($respuesta);

	}

}

//SELECT tipotitulo.*, titulos.* FROM public.tipotitulo, public.titulos, public.recursos_titulos where titulos.id = recursos_titulos.titulo_id and recursos_titulos.tipo_tit = tipotitulo.tipo_titulo and recursos_titulos.tipo_doc = 'M'