<?php

namespace API;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;


/*
 * Controller Extended
 * v0.1.1
 */
abstract class ExtendedController extends Controller{

        public $model;

        public function __construct(){

        }

        public function service_titulos(){
            $titulos = $this->model->select('id','titulo','titulo_salida')->orderBy('titulo')->paginate(10);
            return $titulos;
        }

        public function service_titulo($id){
            $titulo = $this->model->find($id);
            return $titulo;
        }

        public function service_titulos_tipo_doc($tipo_doc){
            $recursos_titulos_tipo_doc= DB::table('recursos_titulos')
                            ->join('titulos', 'titulos.id', '=', 'recursos_titulos.titulo_id')               
                            ->where('recursos_titulos.tipo_doc','=',$tipo_doc)
                            ->where('recursos_titulos.tipo_tit','=','OP')
                            ->groupBy('titulos.id')     
                            ->orderBy('titulos.id')
                            ->select('titulos.id','titulos.titulo','titulos.titulo_salida',DB::raw('count(titulos.id) as num_recursos'))
                            ->paginate(10);
            return $recursos_titulos_tipo_doc;
        }

        public function service_titulos_letra($letra){
            $recursos_titulos_letra =   DB::table('recursos_titulos')
                            ->join('titulos', 'titulos.id', '=', 'recursos_titulos.titulo_id')              
                            ->where('recursos_titulos.tipo_tit','=','OP')
                            ->where('titulos.titulo','like',$letra.'%')
                            ->groupBy('titulos.id')     
                            ->orderBy('titulos.titulo')
                            ->select('titulos.id','titulos.titulo','titulos.titulo_salida',DB::raw('count(titulos.id) as num_recursos'))
                            ->paginate(10);
        return $recursos_titulos_letra;
        }

        public function service_titulos_tipo_doc_letra($tipo_doc,$letra){
            
            $recursos_titulos_tipo_doc_letra =  DB::table('recursos_titulos')
                            ->join('titulos', 'titulos.id', '=', 'recursos_titulos.titulo_id')              
                            ->where('recursos_titulos.tipo_tit','=','OP')
                            ->where('titulos.titulo','like',$letra.'%')
                            ->where('recursos_titulos.tipo_doc','=',$tipo_doc)
                            ->groupBy('titulos.id')     
                            ->orderBy('titulos.titulo')
                            ->select('titulos.id','titulos.titulo','titulos.titulo_salida',DB::raw('count(titulos.id) as num_recursos'))
                            ->paginate(10);
            return $recursos_titulos_tipo_doc_letra;
        }


        public function service_recursos_titulo($id){
        
            $recursos_titulos = $this->model->select('id')->find($id)->recursos;

            return $recursos_titulos;

            /*  $recursos_titulos_tipo_doc_letra =  DB::table('recursos_titulos')
                                    ->join('recursos','recursos.id', '=', 'recursos_titulos.recurso_id')                
                                    ->where('recursos_titulos.titulo_id','=',$id)
                                    ->select('recursos_titulos.recurso_id')
                                    ->get();
                return $recursos_titulos_tipo_doc_letra;
            */  
        }

}

