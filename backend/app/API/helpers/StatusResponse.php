<?php
namespace API\helpers;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;

/*
 * Status response helper
 * v0.1.1
 */
class StatusResponse{

    /**
    * @var _Max depth recurtion in extended call
    **/
    private $extended_max_depht = 3;

    /**
    * @var HTTP code statu
    **/
    protected $statusCode = 200;

    /**
    * @var current resource
    **/
    protected $resource = "no set";

    /**
    * @var Data to response
    **/
    protected $status_message = "";

    /*
     * Setter for status code and resource
     * @param  int HTTP status code
     * @param  string current resource
     */
    public function setStatus($statusCode, $resource = "")
    {
        $this->statusCode = $statusCode;
        $this->resource = $resource;
        return $this;
    }

    /*
     * Return data with succes status
     * return int statusCode
     */
    public function success($data, $extended = true)
    {
        $this->status_message = "success";

        if($extended) $this->extendedData($data);
        $data = $this->colectionToArray( $data );

        return Response::json(array(
                                'data' => $data
                            ));
    }

    /*
     * Return data with error status
     * return int statusCode
     */
    public function error($data, $extended = true)
    {
        $this->status_message = "error";

        if($extended) $this->extendedData($data);
        $data = $this->colectionToArray( $data );

        return Response::json(array(
                                'data' => $data
                            ));
    }

    /*
     * Call exteded method in eloquent object
     * @param Object data to extend
     * @param integer $level recursive current level
     * return Object
     */
    public function extendedData($data, $level = 0)
    {
        /* Extended param exist */
        if(Input::get('extended') != "true" && Input::get('extended') != "1" )
            return $data;

        /* Try extend main data */
        if(method_exists($data,'extended')) $data->extended();

        /* If not array, return data*/
        if( is_array($data) || method_exists($data,'toArray') )
        {
            /* Try extend others objects */
            foreach ($data as $key => $value) {
                if(method_exists($value,'extended')) $value->extended();

                if($this->extended_max_depht > $level)
                    $this->extendedData($value, $level + 1);
            }
        }

        /* return final data */
       return $data;
    }

    /*
     * Transforme collection response to array
     * @param Object $data response data
     * return array
     */
    public function colectionToArray($data)
    {
        if( is_array($data) || method_exists($data,'toArray') )
        {
            foreach ($data as $key => $value) {
                $data[$key] = $this->colectionToArray($value);
            }

            if(method_exists($data,'toArray') )
                $data = $data->toArray();
        }

        return $data;
    }

    /*
     * Getter status code
     * return int statusCode
     */
    public function getCode(){
        return $this->statusCode;
    }

    /*
     * Getter status code
     * return string status_message
     */
    public function getStatus(){
        return $this->status_message;
    }

    /*
     * Getter resource code
     * return string resource
     */
    public function getResource(){
        return $this->resource;
    }
}
