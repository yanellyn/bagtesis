<?php
namespace API;

abstract class APIConfig{

    /*
     * Pagination element return by page
     */
    public static $size_by_page = 2;
}
