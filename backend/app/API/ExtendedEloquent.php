<?php
namespace API;
use Illuminate\Support\Facades\DB;

/*
 * Model Extended Eloquent
 * v0.1.0
 */
abstract class ExtendedEloquent extends \Eloquent
{
    /*
     * He database table used by the model.
     */
    protected $table = '';

    /*
     * fields to select in query
     */
    public $fields = array('*');

    /*
     * include min attributes in select query
     */
    public $min = array("*");

    /*
     * Include medium attributes in select query
     */
    public $medium = array("*");

    /*
     * Include all attributes in select query
     */
    public $all = array("*");

    /*
     * The attributes excluded from fianl response
     */
    protected $hidden = array('updated_at', 'created_at');


    public static function getPagination($data)
    {
        return array(
                        'offsetExists' => $data->offsetExists($data->getCurrentPage()),
                        'total' => $data->getTotal(),
                        'per_page' => $data->getPerPage(),
                        'current_page' => $data->getCurrentPage(),
                        'last_page' => $data->getLastPage(),
                        'from' => $data->getFrom(),
                        'to' => $data->getTo(),
                    );
    }

    /*
     * Describe the table object
     * @param  boolean $show_hidden exclude hidden fields
     * return table fields array
     */
    public function describe($show_hidden = false )
    {
        $fields = DB::select('DESCRIBE '.$this->table );
        if(!$show_hidden)
        {
            $final_fields = [];
            for ($index=0; $index < count($fields); $index++)
            {
                if( array_search( $fields[$index]->Field, $this->hidden ) === FALSE )
                    $final_fields[ count($final_fields) ] = $fields[$index];

            }
            $fields = $final_fields;
        }

        return array('data' => array( 'table' => $this->table, 'rows' => $fields ));
    }

    /*
     * Describe the table object
     * @param  boolean $show_hidden exclude hidden fields
     * return table fields array
     */
    public function setSelectableFields($data_lenght = 'all')
    {
        if($data_lenght == null)
            return $this->fields;

        eval("\$haveArray = isset(\$this->\$data_lenght);");
        $data_lenght = ($haveArray)?$data_lenght:"all";

        eval("\$this->fields = \$this->\$data_lenght;");
        return $this->fields;
    }
}
